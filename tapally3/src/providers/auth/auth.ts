import { Injectable } from '@angular/core'
import { HTTP } from '@ionic-native/http';

@Injectable()
export class AuthProvider {

  apiUrl:any="https://api.authy.com/protected/json/phones/verification/start";

  constructor(public nativeHttp: HTTP) {
  }

  sendVerificationCode(data, type): any {      
    return new Promise((resolve, reject) => {
      this.nativeHttp.post(this.apiUrl, data, {})
      .then((res) => {
        resolve(res);
      },err=>{
        reject(err);
      });
    });
  }

  otpVerify(type): any {       
    return new Promise((resolve, reject) => {
    this.nativeHttp.get("https://api.authy.com/protected/json/" + type,{}, {})
    .then((res) => {
      resolve(res);
    },err=>{
      reject(err);
	  console.log(err);
    });

    });
  }

}
