import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PickContactFromPage } from './pick-contact-from';

@NgModule({
  declarations: [
    PickContactFromPage,
  ],
  imports: [
    IonicPageModule.forChild(PickContactFromPage),
  ],
})
export class PickContactFromPageModule {}
