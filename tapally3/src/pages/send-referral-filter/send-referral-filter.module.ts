import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SendReferralFilterPage } from './send-referral-filter';

@NgModule({
  declarations: [
    SendReferralFilterPage,
  ],
  imports: [
    IonicPageModule.forChild(SendReferralFilterPage),
  ],
})
export class SendReferralFilterPageModule {}
