import { Component } from '@angular/core';
import { IonicPage, MenuController, NavController, Platform } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html'
})
export class TutorialPage {
  slides;

  constructor(public navCtrl: NavController, public menu: MenuController, public platform: Platform) {
    this.slides = [
      {
        title: "Exchange Referral With Ease",
        description: "TapAlly is a convenient way to send and receive referrals among your friends",
        image: "assets/imgs_vectors/man-mobile.png",
      },
      {
        title: "Business Owner ?",
        description: "Make it easier for your customers to exchange your business referral quickly. Grow your brand and sales with ease",
        image: "assets/imgs_vectors/lady-trophy.png",
      }
    ];
  }


  gotoStartPage (){
      this.navCtrl.setRoot('PhonePage');
  }

  onSlideChangeStart(slider) {
    //this.showSkip = !slider.isEnd();
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }

}
