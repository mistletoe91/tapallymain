import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BusinessinfootherPage } from './businessinfoother';

@NgModule({
  declarations: [
    BusinessinfootherPage,
  ],
  imports: [
    IonicPageModule.forChild(BusinessinfootherPage),
  ],
})
export class BusinessinfootherPageModule {}
