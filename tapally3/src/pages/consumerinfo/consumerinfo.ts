import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,App } from 'ionic-angular';

/**
 * Generated class for the ConsumerinfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-consumerinfo',
  templateUrl: 'consumerinfo.html',
})
export class ConsumerinfoPage {

  constructor(public navCtrl: NavController,public app: App, public navParams: NavParams) {
  }

  addbuddy() {
      this.app.getRootNav().push('ContactPage', {source:"ImportpPage"});
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad ConsumerinfoPage');
  }

  backButtonClick (){
    this.navCtrl.pop({animate: true, direction: "forward"});
  }


  sendToBusinessReg (){
      this.app.getRootNav().push("RegisterbusinessPage", {treatNewInstall : 0});
  }


}
