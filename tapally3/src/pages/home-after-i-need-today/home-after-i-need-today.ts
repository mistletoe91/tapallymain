import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events,App } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { ChatProvider } from '../../providers/chat/chat';
import { UserProvider } from '../../providers/user/user';
import { FcmProvider } from '../../providers/fcm/fcm';
import { RequestsProvider } from '../../providers/requests/requests';
import { LoadingProvider } from '../../providers/loading/loading';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { SQLite,SQLiteObject } from '@ionic-native/sqlite';
import { CONST} from '../../app/const';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';
@IonicPage()
@Component({
  selector: 'page-home-after-i-need-today',
  templateUrl: 'home-after-i-need-today.html',
})
export class HomeAfterINeedTodayPage {
  buddy: any;
  newmessage = '';
  cntCounter;
  myfriends;
  isenabled:boolean=true;
  myfriendsList;
  tempmyfriendsList;
  showheader:boolean = true;
  isData: boolean = false;
  authForm: FormGroup;
  message: AbstractControl;
  selectCatId;
  constructor(public app: App,public fcm: FcmProvider,private sqlite: SQLite,public loading: LoadingProvider,public requestservice: RequestsProvider,public fb: FormBuilder,public events: Events,public chatservice: ChatProvider,public userservice: UserProvider,public navCtrl: NavController, public tpStorageService: TpstorageProvider, public toastCtrl: ToastController, public navParams: NavParams) {
    this.selectCatId = navParams.get('cat_id');
    this.authForm = this.fb.group({
        'message': [null, Validators.compose([Validators.required])]
    });
    this.message = this.authForm.controls['message'];
  }


  ionViewWillEnter() {
      this.requestservice.getmyfriends();
      this.myfriends = [];
      this.events.subscribe('friends', () => {
        this.events.unsubscribe('friends');
        this.myfriends = [];
        this.myfriends = this.requestservice.myfriends;
      });
  }
  ionViewWillLeave() {
     this.showheader = false;
  }
  broadcast() {
      this.cntCounter = 0;
      //console.log ("[this.myfriends.length]:"+this.myfriends.length);
      //console.log (this.myfriends);
      this.isenabled = false;

      if(this.myfriends.length>0){
        for (var key=0;key<this.myfriends.length;key++) {
          this.addmessage(this.myfriends[key]);
        }//end for
      } else {
        //You have no friends
        this.loading.presentToast('It appears like your contact list is empty. Invite your friends and let`s get started');
        this.navCtrl.setRoot('TabsPage');
      }
  }

  sendToNextPage (){
      this.cntCounter++;
      if(this.cntCounter>=this.myfriends.length){
        this.cntCounter = 0;
        //this.navCtrl.setRoot("RequestSubmittedPage");//jaswinder
        this.app.getRootNav().push ("RequestSubmittedPage");
      }
  }
  addmessage(buddy) {
    this.newmessage = this.newmessage.trim();

    //Send Push notification : mytodo check if the buddy is online or not. if online then no need to send push notification
    let newMessage = 'You have received a message';
    this.fcm.sendNotification(buddy, newMessage, 'chatpage');

    //Send chat message
    let msgA = '';
    if(this.newmessage.length){
        msgA = this.newmessage;
    }
    this.chatservice.addnewmessageBroadcast(msgA, 'message', buddy, this.selectCatId , false).then(() => {
        this.newmessage = '';
        this.updateFlagInDb ();
    });
  }

  updateFlagInDb() {
       this.sqlite.create({
         name: 'gr.db',
         location: 'default'
       }).then((db: SQLiteObject) => {
         db.executeSql(CONST.create_table_statement_key_val, {})
         .then(res => {
             db.executeSql("SELECT val FROM key_val where key like 'ever_referral_sent'", {})
             .then(res => {
                 this.tpStorageService.setItem('ever_referral_sent', "1");
                 if(res.rows.length>0){
                         //Record exists
                         //console.log ("ever_referral_sent record exists");
                         this.sendToNextPage ();
                 } else {
                         //Record does not exist
                         //console.log ("ever_referral_sent record DOES NOT exists");
                         db.executeSql("INSERT INTO key_val(key,val) VALUES ('ever_referral_sent','1')", {}).then(res => {
                            this.sendToNextPage ();
                         }).catch(e => {
                            //it's ok . cutomer experience is important
                            this.sendToNextPage ();
                         });
                 }//endif
             })
             .catch(e => {
                 this.sendToNextPage ();
             });
         })
         .catch(e => {
             this.sendToNextPage ();
         });//create table
       });//create database
  }//end function


  ionViewDidLoad() {
  }

}
