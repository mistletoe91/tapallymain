import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';
@IonicPage()
@Component({
  selector: 'page-send-referral-backward',
  templateUrl: 'send-referral-backward.html',
})
export class SendReferralBackwardPage {

    buddy_displayName;
    buddy_uid;
    sourcePage;
    showheader:boolean = true;
    thisIsMe:boolean = false;
    constructor(public navCtrl: NavController, public navParams: NavParams,public tpStorageService: TpstorageProvider) {

      this.tpStorageService.get('userUID').then((myUID: any) => {
        this.buddy_displayName = navParams.get('buddy_displayName');
        this.buddy_uid = navParams.get('buddy_uid');
        this.sourcePage = navParams.get('source');

        if(myUID == this.buddy_uid){
          this.thisIsMe = true;
        }//endif
      }).catch(e => {
      });


    }
    ionViewWillLeave() {
       this.showheader = false;
    }
    pickContact (){
      this.navCtrl.push("PickContactFromPage", {source : this.sourcePage,friend_asking_referral_uid : this.buddy_uid,friend_asking_referral_displayName : this.buddy_displayName, friend_asking_referral_catId:0, friend_asking_referral_catName : 0});
    }

    backButtonClick() {
        this.navCtrl.pop({animate: true, direction: "forward"});
    }
    ionViewDidLoad() {
    }
}
