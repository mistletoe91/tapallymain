import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-request-status',
  templateUrl: 'request-status.html',
})
export class RequestStatusPage {
  showheader:boolean = true;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  gotoPage() {
    //testing
      this.navCtrl.setRoot("ChooseClubPage");
  }
  ionViewWillLeave() {
     this.showheader = false;
  }
  ionViewDidLoad() {
    //console.log('ionViewDidLoad RequestStatusPage');
  }

}
