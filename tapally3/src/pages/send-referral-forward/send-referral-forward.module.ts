import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SendReferralForwardPage } from './send-referral-forward';

@NgModule({
  declarations: [
    SendReferralForwardPage,
  ],
  imports: [
    IonicPageModule.forChild(SendReferralForwardPage),
  ],
})
export class SendReferralForwardPageModule {}
