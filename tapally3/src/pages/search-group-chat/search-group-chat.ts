import { Component,NgZone,ViewChild, } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController ,Events,Content,Platform} from 'ionic-angular';
import { GroupsProvider } from '../../providers/groups/groups';

@IonicPage()
@Component({
  selector: 'page-search-group-chat',
  templateUrl: 'search-group-chat.html',
})
export class SearchGroupChatPage {
  @ViewChild('content') content: Content;
  groupName;
  allgroupmsgs;
  imgornot;
  allgroupMessage=[];
  alignuid;
  tempChat=[];
  showheader:boolean = true;
  isNoRecord;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl:ViewController,
    private groupService:GroupsProvider,
    private events:Events,
    private zone:NgZone,
    public platform:Platform
    ) {
      this.alignuid = this.groupService.userId;
  }
  ionViewWillLeave() {
     this.showheader = false;
  }
  ionViewCanEnter() {
    if (this.groupService.currentgroupname != undefined) {
      this.groupName = this.groupService.currentgroupname;
    } else {
      this.groupName = this.navParams.get('groupName');
      this.groupService.currentgroupname != this.groupName
    }
    this.events.subscribe('newgroupmsg', () => {
      this.events.unsubscribe('newgroupmsg');
      this.allgroupmsgs = [];
      this.imgornot = [];
      this.zone.run(() => {
        this.allgroupmsgs = this.groupService.groupmsgs;
      });
      for (var key in this.allgroupmsgs) {
        var d = new Date(this.allgroupmsgs[key].timestamp);
        var hours = d.getHours();
        var minutes = "0" + d.getMinutes();
        var month = d.getMonth(); 1
        var da = d.getDate();
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var formattedTime = monthNames[month] + "-" + da + "-" + hours + ":" + minutes.substr(-2);
        this.allgroupmsgs[key].timestamp = formattedTime;
        if (this.allgroupmsgs[key].multiMessage == true) {
          this.imgornot.push(true);
        } else {
            if (this.allgroupmsgs[key].message.substring(0, 4) === 'http') {
              this.imgornot.push(true);
            }else {
              this.imgornot.push(false);
            }
        }
      }
      this.allgroupMessage = [];
      this.allgroupMessage = this.allgroupmsgs;
      this.tempChat=this.allgroupMessage;
      if (this.allgroupMessage.length > 5) {
        this.scrollto();
      }
    });
    this.groupService.getgroupmsgs(this.groupName);
  }

  initializeContacts(){
    this.zone.run(() => {
			this.allgroupMessage = this.tempChat;
		});
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  search(ev:any){
    this.initializeContacts();
		let myArray = [];
		let isNoRecord: boolean = false;
    this.isNoRecord = false;
    let val = ev.target.value;
    if (val && val.trim() != '') {
      for (var i = 0; i < this.allgroupMessage.length; ++i) {
          if(this.allgroupMessage[i].multiMessage){
          }else if(this.allgroupMessage[i].type=='image'){
          }else if(this.allgroupMessage[i].type=='document'){
          }else{
            if(this.allgroupMessage[i].message.toLowerCase().indexOf(val.toLowerCase())> -1 ){
              isNoRecord = true;
              myArray.push(this.allgroupMessage[i]);
            }
          }
      }
      if (isNoRecord != true) {
				this.isNoRecord = true;
			}
      this.allgroupMessage = myArray;
    }
  }

  scrollto() {
    setTimeout(() => {
      if(this.content._scroll)  {
        this.content.scrollToBottom();
      }
    }, 3000);
  }

}
