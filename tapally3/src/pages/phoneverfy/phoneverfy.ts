// Angular core
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

// Ionic-angular
import { IonicPage, NavController, NavParams } from 'ionic-angular';

// Providers
import { UserProvider } from '../../providers/user/user';
import { AuthProvider }from  '../../providers/auth/auth';
import { LoadingProvider } from '../../providers/loading/loading'

// Firebse javascript
import { apiKey } from '../../app/app.angularfireconfig';

const timeDurection = 120;

@IonicPage()
@Component({
  selector: 'page-phoneverfy',
  templateUrl: 'phoneverfy.html',
})
export class PhoneverfyPage {

  otpNumber: any
  authForm : FormGroup;
  otpTemp: AbstractControl;
  verificationCredential;
  mobileNumber;
  message;
  timer;
  maxTime:any = timeDurection;
  hidevalue = true;
  InvalidNumErr='';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    public userService: UserProvider,
    public loading: LoadingProvider,
    public authService: AuthProvider)
  {

    this.authForm = this.fb.group({
      'otpTemp' : [null, Validators.compose([Validators.required])]
    });

    this.otpTemp = this.authForm.controls['otpTemp'];

    this.verificationCredential = this.navParams.get('code');
    this.mobileNumber = this.navParams.get('number');
  }

  ionViewDidLoad() {
    this.message=""
    this.StartTimer();
  }

  StartTimer(){
   this.timer = setTimeout(x => {
      if(this.maxTime <= 0) { }
      this.maxTime -= 1;
      if(this.maxTime > 0){
        this.hidevalue = false;
        this.StartTimer();
      }
      else {
        this.hidevalue = true;
        this.maxTime = timeDurection;
      }
    }, 1000);
  }

  verify(code){
    this.loading.presentLoading();
	console.log(this.mobileNumber);
	console.log(this.verificationCredential);
	
      this.authService.otpVerify('phones/verification/check?api_key='+apiKey+'&country_code='+this.verificationCredential+'&phone_number='+this.mobileNumber+'&verification_code='+code)
      .then(res => {
        if(res.status == 200){
           this.userService.addmobileUserIOS(this.verificationCredential,this.mobileNumber)
           .then((data:any) => {
             this.loading.dismissMyLoading();

             //if i am invieted. lets make friends
             //this.tpStorageService.getItem('invited_by').then((userId__: any) => {

             //}).catch(e => { });

             if (data.success) {
               this.navCtrl.setRoot("DisplaynamePage");
             }else{
               this.message=data.msg;
             }
           });
        }
      }).catch(err => {
        this.loading.dismissMyLoading();
        if(err.status == 401){
         this.message = "Enter Valid Verification Code";
        }
        if(err.status == 429){
         this.message = "You has tried to check too many codes. Try to enter valid verification code.";
        }
      });
  }

  resend(){
    this.loading.presentLoading();
    let data = {
      "via":"sms",
      "phone_number":this.mobileNumber,
      "country_code":this.verificationCredential,
      "api_key":apiKey
    }
    this.authService.sendVerificationCode(data,'phones/verification/start')
    .then(res => {
      this.loading.dismissMyLoading();
      let data = JSON.parse(res['data']);
      if (res.status == 200) {
        this.loading.presentToast(data['message']);
      }else{
        this.loading.presentToast(data['message']);
      }
      this.StartTimer();
    }).catch(err => {
      this.loading.dismissMyLoading();
    });
  }

}
