import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events,Platform } from 'ionic-angular';
import { GroupsProvider } from '../../providers/groups/groups';

@IonicPage()
@Component({
  selector: 'page-groupmembers',
  templateUrl: 'groupmembers.html',
})
export class GroupmembersPage {
  groupmembers;
  tempgrpmembers;
  showheader:boolean = true;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public groupservice: GroupsProvider,
    public events: Events,
    public platform:Platform
  ) {

  }

  backButtonClick() {
      this.navCtrl.pop({animate: true, direction: "forward"});
  }
  ionViewWillEnter() {
    this.groupmembers = this.groupservice.currentgroup;
    this.tempgrpmembers = this.groupmembers;
    this.events.subscribe('gotintogroup', () => {
      this.events.unsubscribe('gotintogroup');
      this.groupmembers = this.groupservice.currentgroup;
      this.tempgrpmembers = this.groupmembers;
    })
  }
  ionViewWillLeave() {
  }
  ionViewDidLoad() {
  }
  initializeItems() {
    this.groupmembers = this.tempgrpmembers;
  }

  searchuser(ev: any) {
    this.initializeItems();

    const val = ev.target.value;

    if (val && val.trim() != '') {
      this.groupmembers = this.groupmembers.filter((item) => {
        return (item.displayName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
      });
    }
  }

  removemember(member) {
    var index = this.groupmembers.findIndex(function (o) {
      return o.uid === member.uid;
    });
    if (index !== -1) this.groupmembers.splice(index, 1);
    if (this.groupmembers.length == 0) {
      this.groupservice.deletegroup().then(() => {
        this.navCtrl.setRoot('TabsPage');
      }).catch((err) => {
      });
    } else {
      this.groupservice.deletemember(member);
    }
  }
  goBack() {
    this.navCtrl.setRoot("TabsPage")
  }
}
