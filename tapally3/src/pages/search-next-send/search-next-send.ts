import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-search-next-send',
  templateUrl: 'search-next-send.html',
})
export class SearchNextSendPage {
  showheader:boolean = true;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  ionViewWillLeave() {
     this.showheader = false;
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchNextSendPage');
  }

}
