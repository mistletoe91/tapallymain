import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import firebase from 'firebase';
import { UserProvider } from '../../providers/user/user';
import { RequestsProvider } from '../../providers/requests/requests';
import { env } from '../../app/app.angularfireconfig';
import { ChatProvider } from '../../providers/chat/chat';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';
@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {
  imgurl = env.userPic;
  isDisabled: boolean = false;
  isNotify: boolean = true;
  showheader:boolean = true;
  profileData: any = {
    username: '',
    disc: '',
    imgurl: env.userPic
  }
  buddy: any;
  userId: any;
  blockcounter: any = 0;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public zone: NgZone,
    public events: Events,
    public userservice: UserProvider,
    public requestservice: RequestsProvider,
    public chatservice: ChatProvider,
    public tpStorageService: TpstorageProvider,
  ) {

    console.log ("1");
    this.tpStorageService.getItem('userUID').then((userIDID: any) => {
       console.log ("userIDID");
       console.log (userIDID);
			 if(userIDID){
				 this.userId = userIDID;

         this.loaduserdetails();
         events.subscribe('block-users-counter', () => {
           this.events.unsubscribe('block-users-counter');
           this.blockcounter = this.userservice.blockUsersCounter;
         })
         this.userservice.initializeItem(this.userId).then((res) => {
           if (res == true) {
             this.isNotify = true;
           } else {
             this.isNotify = false;
           }
         });

			 }//endif
		}).catch(e => { });

  }
  ionViewDidLoad() {
    this.userservice.getAllBlockUsersCounter();
  }
  ionViewWillLeave() {
     this.showheader = false;
  }
  goBack() {
    this.navCtrl.setRoot('TabsPage');
  }
  backButtonClick() {
      this.navCtrl.pop({animate: true, direction: "forward"});
  }
  loaduserdetails() {
     console.log ("loaduserdetails");
    if (this.userId != undefined) {
      this.isDisabled = true;
      this.userservice.getuserdetails().then((res: any) => {
        console.log ("USER DETAILS");
        console.log (res);
        if (res) {
          let desc = res['disc'].replace(new RegExp('\n', 'g'), "<br />");
            if (res['displayName'] != undefined) {
              this.profileData.username = res['displayName'];
            }
            if (res['photoURL'] != undefined) {
              this.profileData.imgurl = res['photoURL']
            }
            if (desc != undefined) {
              this.profileData.disc = desc;
            }
        }
      }).catch(err => {
      });
    } else {
      this.profileData = {
        username: '',
        disc: '',
        imgurl: env.userPic
      }
    }
  }
  goBlockuser() {
    this.navCtrl.push('BlockUserPage');
  }
  openProfilePage() {
    this.navCtrl.push('ProfilepicPage')
  }
  inviteFriends() {
    this.navCtrl.push('InviteFriendPage');
  }
  notify() {
    this.isNotify != this.isNotify;
    this.userservice.notifyUser(this.isNotify);
  }
}
