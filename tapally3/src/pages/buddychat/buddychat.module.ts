import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuddychatPage } from './buddychat';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    BuddychatPage,
  ],
  imports: [
    IonicPageModule.forChild(BuddychatPage),
	IonicImageLoader
  ],
})
export class BuddychatPageModule {}
