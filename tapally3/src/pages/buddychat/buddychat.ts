import { Component, ViewChild, NgZone, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Content, LoadingController, ActionSheetController, PopoverController, AlertController, ModalController, Platform ,App} from 'ionic-angular';
import { ChatProvider } from '../../providers/chat/chat';
import { ImagehandlerProvider } from '../../providers/imagehandler/imagehandler';
import { LoadingProvider } from '../../providers/loading/loading';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { FcmProvider } from '../../providers/fcm/fcm';
import { UserProvider } from '../../providers/user/user';
//import { Storage } from '@ionic/storage';
import { RequestsProvider } from '../../providers/requests/requests';
import { ContactProvider } from '../../providers/contact/contact';
import { Subscription } from 'rxjs';
import { ImageViewerController } from 'ionic-img-viewer';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';
import { CatProvider } from '../../providers/cats/cats';
import { SQLite,SQLiteObject } from '@ionic-native/sqlite';
@IonicPage()
@Component({
    selector: 'page-buddychat',
    templateUrl: 'buddychat.html',
})
export class BuddychatPage {
    @ViewChild('content') content: Content;
    authForm: FormGroup;
    message: AbstractControl;
    buddy: any;
    newmessage = '';
    newmessageTmp = '';
    referralRequestPrefix;
    referralRequestPostfix;
    isRefresher:boolean = false;
    allmessages = [];
    filterallmessages = [];
    tempFilterallmessages = [];
    photoURL;
    imgornot;
    todaydate;
    myfriends;
    myfriendsList;
    tempmyfriendsList;
    allmessagesGroups = [];
    isData: boolean = false;
    buddyStatus: any;
    datacounter = 10;
    headercopyicon = true;
    catItems;
    showheader:boolean = true;
    selectAllMessage = [];
    selectCounter = 0;
    UnreadMSG: number;
    isuserBlock: boolean = false;
    msgstatus: boolean = false;
    backbuttonstatus: boolean = false;
    DeleteMsg;
    _imageViewerCtrl: ImageViewerController;
    //private onResumeSubscription: Subscription;
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        //public storage: Storage,
        public fcm: FcmProvider,
        public chatservice: ChatProvider,
        public events: Events,
        public catservice: CatProvider,
        public zone: NgZone,
        private sqlite: SQLite,
        public loadingCtrl: LoadingController,
        public imgstore: ImagehandlerProvider,
        public fb: FormBuilder,
        public loading: LoadingProvider,
        public app: App,
        public userservice: UserProvider,
        public actionSheetCtrl: ActionSheetController,
        public eleRef: ElementRef,
        public popoverCtrl: PopoverController,
        private alertCtrl: AlertController,
        public requestservice: RequestsProvider,
        public contactProvider: ContactProvider,
        public modalCtrl: ModalController,
        public platform: Platform,
        public imageViewerCtrl: ImageViewerController,
        public tpStorageService: TpstorageProvider
    ) {
        ////console.log ("Buddypage Constructor");
        this.platform.ready().then((readySource) => {
          this.catItems = this.catservice.getCats();
          this.referralRequestPrefix = 'Referral Request';
          this.referralRequestPostfix = '';
          this._imageViewerCtrl = imageViewerCtrl;
          this.authForm = this.fb.group({
              'message': [null, Validators.compose([Validators.required])]
          });
          //mytodo you can read the photoURL from cache as well , but use with caution as it may not be uptodate
          this.userservice.getuserdetails().then((res: any) => {
              this.photoURL = res['photoURL'];
          });
          this.message = this.authForm.controls['message'];
          this.buddy = this.chatservice.buddy;// buddy is receipient of chat

          //The resume event emits when the native platform pulls the application out from the background
          /*
          this.onResumeSubscription = platform.resume.subscribe(() => {
              this.chatservice.buddymessageRead();
          });
          */


          this.events.subscribe('newmessage', () => {
            ////console.log ("Buddypage Newmessage subscription ")
            this.zone.run(() => {
              this.MessageSubscription ();
            });
          })

        });
    }


    fnGetReferralDetail (item){
      if(null != item){
        this.navCtrl.push("IncentivedetailPage", { item: JSON.stringify(item) });
      }
    }


   fnSendToEarningsPage (){
      this.navCtrl.push("EarningsPage");
   }

    fnviewincentiverequst (item){
      this.tpStorageService.get('userUID').then((storage_res: any) => {
          this.app.getRootNav().push('ViewandredeemPage', {requestId:item.request_to_release_incentive,referral_send_by:item.sentby,referral_received_by:storage_res});
      }).catch(e => {
      });

    }

    MessageSubscription (){
        this.allmessages = [];
        this.allmessagesGroups = [];
        for (var msgKey in this.chatservice.buddymessages) {
          if(null == this.chatservice.buddymessages[msgKey].dateofmsg){
            continue;
          }
          this.chatservice.buddymessages[msgKey].selection = false;
          this.chatservice.buddymessages[msgKey].selectCatName = '';

          if (this.chatservice.buddymessages[msgKey].referral_type == null){
            this.chatservice.buddymessages[msgKey].referral_type = "";
          }

          if (this.chatservice.buddymessages[msgKey].selectCatId != null){
            for(let g=0;g<this.catItems.length;g++){
              if(this.catItems[g]['id']==this.chatservice.buddymessages[msgKey].selectCatId){
                this.chatservice.buddymessages[msgKey].selectCatName = this.catItems[g]['name'];
                break;
              }
            }
          }

          let tmpArr = {
            'dateofmsg' : this.chatservice.buddymessages[msgKey].dateofmsg,
            'message_item' : [this.chatservice.buddymessages[msgKey]]
          }

          //Check if data exists
          let foundKey:boolean = false;
          for (var myKey in this.allmessagesGroups) {
              if(this.allmessagesGroups[myKey].dateofmsg == this.chatservice.buddymessages[msgKey].dateofmsg){
                this.allmessagesGroups [myKey].message_item.push (this.chatservice.buddymessages[msgKey]);
                foundKey = true;
                break;
              }
          }//end for

          if(!foundKey){
            this.allmessagesGroups.push (tmpArr);
          }

          this.allmessages.push(this.chatservice.buddymessages[msgKey]);//this is used for search the chats
        }//end for
        ////////console.log ("Done ji");
        ////////console.log (this.allmessagesGroups);

        //this.tpStorageService.set('allBuddyChats', this.allmessages);//this is used for search the chats
      //});
    }

    ionViewWillLeave (){
      this.events.unsubscribe('newmessage');
      this.chatservice.buddymessages = [];
      //this.navCtrl.pop();
    }

    ionViewDidEnter() {
                ////console.log ("Buddypage ionViewDidEnter");
                this.todaydate = this.formatDate(new Date());
                this.chatservice.getbuddymessages(this.datacounter);
                this.scrollToBottom_ ();
    }

    saveContactInPhone (item){
      item.referral.phoneNumber = item.referral.mobile;
      this.contactProvider.addContact(item.referral).then((res) => {
        if (res) {
          this.chatservice.updateContactSaved (item.messageId);
          for(let x=0;x<this.allmessagesGroups.length;x++){
            let msgMy = this.allmessagesGroups[x].message_item;
            for(let y=0;y<msgMy.length;y++){
                if(msgMy[y].messageId == item.messageId){
                  this.allmessagesGroups[x].message_item[y].referralSavedOnPhone = true;
                  break;
                }
            }
          }
        }
      });
    }

    doRefresh(refresher) {
        this.isRefresher = true;
        this.UnreadMSG = 0;
        this.datacounter += 10;
        this.chatservice.getbuddymessages(this.datacounter);
        this.closeButtonClick()
        setTimeout(() => {
            refresher.complete();
        }, 500);
    }

        viewBuddy(name, mobile, disc, img, status) {
            this.navCtrl.push("ViewBuddyPage", { img: img, name: name, mobile: mobile, disc: disc, userstatus: status });


        }

        referralSendForward (myEvent){
            // I want to send buddy's referral to someone else
            this.navCtrl.push("SendReferralForwardPage", {source :"buddypage",buddy_uid : this.buddy.uid,buddy_displayName : this.buddy.displayName});

        }
        referralSendBackward (myEvent){
          this.navCtrl.push("SendReferralBackwardPage", {source :"buddypage", buddy_uid : this.buddy.uid,buddy_displayName : this.buddy.displayName});
        }

        sendReferral (friend_asking_referral, catid){
            this.navCtrl.push("FriendAskingReferralPage", {friend_asking_referral_uid : friend_asking_referral.uid,friend_asking_referral_displayName : friend_asking_referral.displayName, catid:catid});
        }
        formatDate(date) {
            var dd = date.getDate();
            var mm = date.getMonth() + 1;
            var yyyy = date.getFullYear();
            if (dd < 10)
                dd = '0' + dd;
            if (mm < 10)
                mm = '0' + mm;
            return dd + '/' + mm + '/' + yyyy;
        }

    addmessage_async (){
         this.newmessageTmp = this.newmessage;
         this.newmessage = '';
         ////////console.log ("addmessage_async");
         setTimeout(() => {
             this.addmessage ();
         }, 1);
    }//end async function


    addmessage() {
        //this.closeButtonClick()
        this.UnreadMSG = 0;
        this.selectAllMessage = [];
        this.selectCounter = 0;
        this.headercopyicon = true;
        this.userservice.getstatusblock(this.buddy).then((res) => {
            ////////console.log ("-getstatus");
            if (!res) {
                this.newmessageTmp = this.newmessageTmp.trim();
                if (this.newmessageTmp != '') {
                      ////////console.log ("-if message is not empty");
                      this.fcm.sendNotification(this.buddy, 'You have received a message', 'chatpage');
                      //this.events.unsubscribe('newmessage');//to avoid triggring multiple calls
                      //mytodo : above fix doesn`t work. Please try something else
                      this.chatservice.addnewmessage(this.newmessageTmp, 'message', this.buddyStatus).then(() => {
                        /*this.events.subscribe('newmessage', () => {
                            this.MessageSubscription ();
                        });
                        */
                        this.newmessage = '';
                        this.scrollto();
                        this.msgstatus = false;
                      });

                } else {
                    this.newmessage = this.newmessageTmp;
                    this.loading.presentToast('Please enter valid message...');
                }
            } else {
                //user might be blocked
                this.newmessage = this.newmessageTmp;
                let alert = this.alertCtrl.create({
                    message: 'Unblock ' + this.buddy.displayName + ' to send a message.',
                    buttons: [
                        {
                            text: 'CANCEL',
                            role: 'cancel',
                            handler: () => {
                            }
                        },
                        {
                            text: 'UNBLOCK',
                            handler: () => {
                                this.userservice.unblockUser(this.buddy).then((res) => {
                                    if (res) {
                                        this.addmessage();
                                    }
                                })
                            }
                        }
                    ]
                });
                alert.present();
            }
        })
        ////////console.log ("-End Function");
    }//end function
    scrollto() {
      /*
        setTimeout(() => {
            if(this.content._scroll)  {
              this.content.scrollToBottom();
            }
        }, 1000);
        */
    }

    scrollToBottom_() {
        setTimeout(() => {
            if(this.content._scroll)  {
              this.content.scrollToBottom();
            }
        }, 1);
    }

    sendPicMsg() {
        this.UnreadMSG = 0;
        this.userservice.getstatus(this.buddy).then((res) => {
            if (!res) {
                this.loading.presentLoading();
                this.imgstore.picmsgstore().then((imgurl) => {
                    this.chatservice.addnewmessage(imgurl, 'image', this.buddyStatus).then(() => {
                        this.loading.dismissMyLoading();
                        if (this.buddyStatus != 'online') {
                            let newMessage = 'You have received a message';
                            this.fcm.sendNotification(this.buddy, newMessage, 'chatpage');
                        }
                        this.scrollto();
                        this.newmessage = '';
                    })
                }).catch((err) => {
                    this.loading.dismissMyLoading();
                    alert(err);
                })
            } else {
                let alert = this.alertCtrl.create({
                    message: 'Unblock ' + this.buddy.displayName + ' to send a message.',
                    buttons: [
                        {
                            text: 'CANCEL',
                            role: 'cancel',
                            handler: () => {
                            }
                        },
                        {
                            text: 'UNBLOCK',
                            handler: () => {
                                this.userservice.unblockUser(this.buddy).then((res) => {
                                    if (res) {
                                        this.sendPicMsg();
                                    }
                                })
                            }
                        }
                    ]
                });
                alert.present();
            }
        })
    }

    getAllMessage(key) {
        ////////console.log ("getAllMessage CALLED");
        ////////console.log (key);
        ////////console.log (this.allmessages);

        let messagesAll = [];
        let counter = 0;
        // for(let i = 0; i<key;i++){
        for (let j = this.allmessages.length; j > 0; j--) {
            if (counter < key) {
                let msg = [];
                for (let k = this.allmessages[j - 1].messages.length; k > 0; k--) {
                    if (counter < key) {
                        msg.push(this.allmessages[j - 1].messages[k - 1])
                    } else {
                        break
                    }
                    counter++;
                }
                messagesAll.push({ date: this.allmessages[j - 1].date, messages: msg });
            } else {
                break;
            }
        }
        let filterallmessages = [];
        let sortDate = this.sortdata(messagesAll);
        ////////console.log ("Check h1");
        for (let i = 0; i < sortDate.length; i++) {
            let tempmsg = [];
            for (let j = sortDate[i].messages.length; j > 0; j--) {
                ////////console.log ("inside loop of sortDate");
                tempmsg.push(sortDate[i].messages[j - 1])
            }
            this.zone.run(() => {
                filterallmessages.push({ date: sortDate[i].date, messages: tempmsg })
            })
        }
        filterallmessages.sort(function (a, b) {
            var nameA = a.date; // ignore upper and lowercase
            var nameB = b.date; // ignore upper and lowercase
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        });
        this.filterallmessages = filterallmessages;
        // this.filterallmessages = [];
        // for (let i = filterallmessages.length - 1; i >= 0; i--) {
        //   this.filterallmessages.push(filterallmessages[i]);
        // }
        // this.filterallmessages = this.sortdata(this.filterallmessages);
        this.scrollto();
        // this.filterallmessages = this.sortdata(messagesAll);
        //   let tmpmessage = [];
        //   this.filterallmessages.forEach(element => {
        //     if (element.messages.length > 3) {
        //       let imgcter=0
        //       let imgcontainer=[];
        //       element.messages.forEach(ele => {

        //         if(ele.type == 'image'){
        //           imgcter++;
        //           imgcontainer.push(ele)
        //             if(imgcter>4){

        //             }
        //         }else{
        //           tmpmessage.push(ele);
        //         }
        //       });
        //     }
        //     tmpmessage.push(element);
        //   });
    }
    sortdata(data) {
        return data.sort(function (a, b) {
            var keyA = new Date(a.date),
                keyB = new Date(b.date);
            // Compare the 2 dates
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
        });
    }



    backButtonClick() {


        if(this.navParams.get('back_button_pop_on') || this.navParams.get('back_button_pop_on') == "true"){
                  this.navCtrl.pop({animate: true, direction: "right"});
        } else {
                  this.navCtrl.setRoot('TabsPage');
        }


        //
    }

    closeButtonClick() {
        this.selectAllMessage = [];
        this.selectCounter = 0;
        this.headercopyicon = true;
        this.backbuttonstatus = false;
        for (var i = 0; i < this.filterallmessages.length; i++) {
            for (var j = 0; j < this.filterallmessages[i].messages.length; j++) {
                this.filterallmessages[i].messages[j].selection = false;
            }
        }
    }

    popoverdialog(item, event) {
        let flag = 0;
        if (this.selectAllMessage.length != 0) {
            for (let i = 0; i < this.selectAllMessage.length; i++) {
                if (item.messageId == this.selectAllMessage[i].messageId) {
                    flag = 1;
                    //event.target.classList.remove('select-item');
                    item.selection = false;
                    this.selectAllMessage.splice(i, 1);

                    break;
                } else {
                }
            }
            if (flag == 1) {
            } else {
                this.popoveranothermsg(item, event);
            }
        } else {
            item.selection = true
            this.selectAllMessage.push(item);

            if (this.headercopyicon == true) {
                this.headercopyicon = false;
                this.backbuttonstatus = true;

            } else {
                this.headercopyicon = true;
                this.backbuttonstatus = false;
            }
        }
        this.selectCounter = this.selectAllMessage.length;
        if (this.selectAllMessage.length == 0) {
            this.headercopyicon = true;
            this.backbuttonstatus = false;
        }

    }
    popoveranothermsg(item, event) {
        let flag = 0;
        if (this.selectAllMessage.length >= 1) {

            for (let i = 0; i < this.selectAllMessage.length; i++) {
                if (item.messageId == this.selectAllMessage[i].messageId) {
                    item.selection = false;
                    flag = 1;
                    this.selectAllMessage.splice(i, 1);
                    break;
                } else {
                    flag = 0;
                }
            }
            if (flag == 1) {
            } else {
                item.selection = true
                this.selectAllMessage.push(item);
            }
        }
        this.selectCounter = this.selectAllMessage.length;
        if (this.selectAllMessage.length == 0) {
            this.headercopyicon = true;
            this.backbuttonstatus = false;
        }
    }

    deleteMessage() {
        if(this.selectAllMessage.length > 1){
            this.DeleteMsg="Do you want to delete these messages?"
        }else{
            this.DeleteMsg="Do you want to delete this message?"
        }
        this.loading.presentLoading();
        let alert = this.alertCtrl.create({
            title: 'Delete Message',
            message: this.DeleteMsg,
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: () => {
                        this.closeButtonClick();
                        this.loading.dismissMyLoading();
                    }
                },
                {
                    text: 'Yes',
                    handler: () => {
                        if (this.selectAllMessage.length != 0) {
                            this.chatservice.deleteMessages(this.selectAllMessage).then((res) => {
                                if (res) {
                                    this.selectAllMessage = [];
                                    if (this.selectAllMessage.length == 0) {
                                        this.headercopyicon = true;
                                        this.backbuttonstatus = false;
                                        this.loading.dismissMyLoading();
                                    }
                                }
                            }).catch((err) => {
                            })
                        }
                    }
                }
            ]
        });
        alert.present().then(() => {
        });
    }
    viewPopover(myEvent) {
        let popover = this.popoverCtrl.create('MenuBlockPage', { buddy: this.buddy });
        popover.present({ ev: myEvent });
        popover.onDidDismiss(data => {
            this.userservice.getstatusblockuser(this.buddy);
        });
    }
    //newkk
    callFunctionScroll() {
      if(this.content._scroll && !this.isRefresher)  {
        this.content.scrollToBottom(0);
      }
    }
    ionViewWillEnter() {
       ////console.log ("BUDDYPAGE ionViewWillEnter");
    }


    openAttachment (myEvent){
      this.openAttachment_(myEvent);
      /*
      //mytodo : disabling "Media Libary " on ios because i could not make it work. But we need to make it work
      if (this.platform.is('ios')) {
        this.sendPicMsg_ ();
      } else {
        this.openAttachment_(myEvent);
      }
      */
    }

        //duplicating from attachment.ts
        sendPicMsg_() {
            this.imgstore.cameraPicmsgStore().then((imgurl) => {
                   this.chatservice.addnewmessage(imgurl, 'image', this.buddyStatus).then(() => {
                            let newMessage = 'You have received a message';
                            this.fcm.sendNotification(this.buddy, newMessage, 'chatpage');
                        })
                    }).catch((err) => {
                    })
        }//end function


    openAttachment_(myEvent) {
        let popover = this.popoverCtrl.create('AttachmentsPage', { buddy: this.buddy });
        popover.present({
            ev: myEvent
        });
        popover.onDidDismiss((pages) => {
            if (pages != 0 && pages != undefined && pages != "undefined"  && pages != null ) {
                if(pages.page){
                  this.navCtrl.push(pages.page, { buddy: pages.buddy, flag: pages.flag });
                }
            }else{
            }
        });
    }
    presentImage(myImage) {
        if (this.selectAllMessage.length == 0) {
            const imageViewer = this._imageViewerCtrl.create(myImage);
            imageViewer.present();
            //imageViewer.onDidDismiss(() => ));
        }
    }

    openAllImg(items, $event) {
        if (this.selectAllMessage.length == 0) {
            this.navCtrl.push("AllimgPage", { allimg: items });
        }
    }

    openDoc(path) {
        if (this.selectAllMessage.length == 0) {
            this.imgstore.openDocument(path).then(res => {
            }).catch(err => {
            })
        }
    }

    openContacts(contacts) {
        if (this.selectAllMessage.length == 0) {
            this.navCtrl.push("ShowContactPage", { contacts: contacts });
        }
    }
    showLocation(location) {
        if (this.selectAllMessage.length == 0) {
            this.navCtrl.push("MapPage", { location: location })
        }
    }
    onChange(e) {
        if (this.newmessage.trim()) {
            this.msgstatus = true;
        }
        else {
            this.msgstatus = false;
        }
    }
}
