import { Component, NgZone, ElementRef } from '@angular/core';

import { IonicPage, NavController, Platform, NavParams, Events, AlertController, PopoverController, LoadingController, ModalController,App ,normalizeURL } from 'ionic-angular';

import { RequestsProvider } from '../../providers/requests/requests';
import { ChatProvider } from '../../providers/chat/chat';
import { UserProvider } from '../../providers/user/user';
import { ContactProvider } from '../../providers/contact/contact';
import { LoadingProvider } from '../../providers/loading/loading';
import { FcmProvider } from '../../providers/fcm/fcm';
import { GroupsProvider } from '../../providers/groups/groups';
import { ImagehandlerProvider } from '../../providers/imagehandler/imagehandler';
import { SQLite,SQLiteObject } from '@ionic-native/sqlite';
import { CONST} from '../../app/const';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';
import firebase from 'firebase';
import { Http } from '@angular/http';
//import { File } from '@ionic-native/file/ngx';
declare let cordova: any;
@IonicPage()
@Component({
    selector: 'page-chats',
    templateUrl: 'chats.html',
})
export class ChatsPage {
    myrequests = [];
    myfriends;
    tmp_StringName ;
    showfooter:boolean = true;
    myfriendsList = [];
    tempmyfriendsList = [];
    debugMe:boolean = true;
    showPaidSign:boolean = false;
    showheader:boolean = true;
    today;
    isData: boolean = false;
    isNoRecord: boolean = false;
    requestcounter = null;
    useremail;
    searchActive:boolean = false;
    username: string = "";
    avatar: string;
    selectContact = false;
    selectedContact = [];
    invited_by;
    selectCounter = 0
    allmygroups = [];
    tempallmygroups = [];
    allChatListing = [];
    allChatListing_HTML = [];
    allChatListing_HTML_ForSearch = [];
    archiveChats = [];
    counter: any = 0;
    messages = [];
    whitelabelObj:any = {};
    whitelabelObj_ShowNoBusiness:boolean = false;
    virginityBroken_ChatPage:boolean = true;
    businessName;
    testMode:boolean = false;

    //public file: File,
    constructor(public fcm: FcmProvider,
        public platform: Platform,
        public loadingProvider: LoadingProvider,
        public contactProvider: ContactProvider,
        public navCtrl: NavController,
        public app: App,
        private sqlite: SQLite,
        public navParams: NavParams,
        public requestservice: RequestsProvider,
        public events: Events,
        public alertCtrl: AlertController,
        public chatservice: ChatProvider,
        public zone: NgZone,
        public http:Http,
        public popoverCtrl: PopoverController,
        public imghandler: ImagehandlerProvider,
        public userservice: UserProvider,
        public loadingCtrl: LoadingController,
        public eleRef: ElementRef,
        public modalCtrl: ModalController,
        public groupservice: GroupsProvider,
        public tpStorageService: TpstorageProvider
    ) {
        this.tmp_StringName = '';
        this.events.unsubscribe('friends');
        this.initiateMe();
    }

    onCancelSearchbar (){
      this.searchActive = false;
    }

    showSearchBar (){
      if(this.searchActive){
        this.searchActive = false;
      } else {
        this.searchActive = true;
      }
    }

    formatPhoneNumber(phoneNumberString) {
      var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
      var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/)
      if (match) {
        var intlCode = (match[1] ? '+1 ' : '')
        return [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('')
      }
      return ""
    }


    getFromDbi (){
      this.userservice.getuserdetails().then((res)=>{
        this.whitelabelObj.mobile = this.formatPhoneNumber(res['mobile']);
        this.whitelabelObj.displayName = res['displayName'];
      }).catch(err=>{
        this.cleanWhiteLabelCache();
      });
    }

    getWhileLabelObj (showMyBusiness){
      this.whitelabelObj.buddy_uid = 0;
      if(showMyBusiness){
        this.whitelabelObj.showMyBusiness = true;
        this.tpStorageService.get('mymobile').then((storage_myphone: any) => {
            this.tpStorageService.get('displayName').then((storage_myname: any) => {
              this.whitelabelObj.mobile = this.formatPhoneNumber(storage_myphone);
              this.whitelabelObj.displayName = storage_myname;
            }).catch(e => {
              this.getFromDbi ();
            });
        }).catch(e => {
            this.getFromDbi ();
        });

        this.tpStorageService.get('userUID').then((storage_res: any) => {
          this.whitelabelObj.buddy_uid = storage_res;
          this.tpStorageService.setItem('whitelabelObj_uid', storage_res);
        }).catch(e => {
        });

        this.tpStorageService.get('business_my_referral_type_id').then((storage_res: any) => {
          this.whitelabelObj.business_my_referral_type_id = storage_res;
        }).catch(e => {
          this.whitelabelObj.business_my_referral_type_id = "";
        });

        this.tpStorageService.get('business_my_referral_detail').then((storage_res: any) => {
          this.whitelabelObj.business_my_referral_detail = storage_res;
        }).catch(e => {
          this.whitelabelObj.business_my_referral_detail = "";
        });

        this.tpStorageService.get('businessName').then((storage_res: any) => {
          this.tpStorageService.setItem('whitelabelObj_sponseredbusines_name',storage_res);
          this.whitelabelObj.sponseredbusines_businessName = storage_res;
        }).catch(e => {
        });


      } else {
        this.whitelabelObj.showMyBusiness = false;
        //use cache until we get correct result from  internet
        this.tpStorageService.get('whitelabelObj_phone').then((storage_myphone: any) => {
            this.tpStorageService.get('whitelabelObj_displayName').then((storage_myname: any) => {
              this.whitelabelObj.mobile = this.formatPhoneNumber(storage_myphone);
              this.whitelabelObj.displayName = storage_myname;
            }).catch(e => {
            });
        }).catch(e => {
        });

        this.tpStorageService.get('whitelabelObj_sponseredbusines_name').then((storage_res: any) => {
          this.whitelabelObj.sponseredbusines_businessName = storage_res;
        }).catch(e => {
        });

        //invited_by exists so lets call business details
        //mytodo : check once in a month : because businesses are on monthly subscription so don`t need to check everytime
        //console.log ("CHECKING INVITED BY <<<<<<<<<<:::::::<<<<<<<<");
        this.tpStorageService.getItem('invited_by').then((storage_res: any) => {
           //console.log (storage_res);
           if(storage_res){
               this.whitelabelObj.buddy_uid = storage_res;
               this.userservice.getBusinessDetailsOther(storage_res).then((res)=>{
                 if(res){

                   if(res['paid']){
                     this.whitelabelObj.sponseredbusines_businessName = res['displayName'];
                     this.tpStorageService.setItem('whitelabelObj_sponseredbusines_name', res['displayName']);

                      //replace it with my.friends to update :mytodo : save one ajax call
                       this.userservice.getAnotherUser(storage_res).then((res_user)=>{
                               this.whitelabelObj.mobile = this.formatPhoneNumber(res_user['mobile']);
                               this.whitelabelObj.displayName = res_user['displayName'];
                               this.tpStorageService.setItem('whitelabelObj_uid', storage_res);
                               this.tpStorageService.setItem('whitelabelObj_phone', res_user['mobile']);
                               this.tpStorageService.setItem('whitelabelObj_displayName', res_user['displayName']);

                       }).catch(err=>{});

                   } else {
                     this.cleanWhiteLabelCache();
                   }//endif
                 } else {
                   this.cleanWhiteLabelCache();
                 }//endif
               }).catch(err=>{
                 this.cleanWhiteLabelCache();
               });



           }//endif
        }).catch(e => {
        });



        //Check if there is something to be shown at bottom bar
        /*setTimeout(() => {
            this.tpStorageService.get('whitelabelObj_sponseredbusines_name').then((r: any) => {

            }).catch(e => {
            });
        }, 500);
        */
        this.whitelabelObj_ShowNoBusiness = true;

        }//endif




    }//end function

    cleanWhiteLabelCache (){
      this.tpStorageService.setItem('whitelabelObj_uid', "");
      this.tpStorageService.setItem('whitelabelObj_phone','');
      this.tpStorageService.setItem('whitelabelObj_displayName','');
      this.tpStorageService.setItem('whitelabelObj_sponseredbusines_name','');
      this.whitelabelObj.displayName = '';
      this.whitelabelObj.sponseredbusines_businessName = '';
      this.whitelabelObj.mobile = '';
    }

    tpInitilizeFromStorage (){

      this.tpStorageService.getItem('businessName').then((res: any) => {
         if(res){
           this.businessName = res;
         }
      }).catch(e => {
      });

       this.tpStorageService.getItem('virginityBroken_ChatPage').then((res: any) => {
       }).catch(e => {this.virginityBroken_ChatPage = false;});
    }

    ionViewWillLeave (){
      this.events.unsubscribe('friends');
    }

    ngOnDestroy (){
      this.events.unsubscribe('friends');
    }

/*
ionViewWillEnter
ionViewDidLoad
*/

    makeid(length) {
       var result           = '';
       var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
       var charactersLength = characters.length;
       for ( var i = 0; i < length; i++ ) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
       }
       return result;
    }

    /*
    getMyLink (){
      return 'https://tapally.com/handleotherwise.php?action=l&e='+ this.makeid(3) + btoa(this.useremail) + this.makeid(5);
    }
    Sample :
    https://tapally.com/handleotherwise.php?action=l&e=123bWlzdGxldG9lOTFAZ21haWwuY29t12345
    */

    //amFzQGlubG93cHJpY2UuY29t
    openLink (email){
      let str = this.makeid(3) + btoa(email) + this.makeid(5);
      window.open('https://tapally.com/handleotherwise.php?action=l&e='+str, '_system', 'location=yes');
    }

    ionViewDidLoad() {
        this.today = new Date();
    }
    ionViewWillEnter() {
      //Run Async function
      setTimeout(() => {
        this.fnGetLocalCache ();
        this.whitelabelObj = {};
        this.tpInitilizeFromStorage ();
        this.paidOrNot ();
        this.fcm.getToken();
        this.events.publish('checkUserStatus');
      }, 1);

    }

        initiateMe() {
            ////console.log ("initiateMe ()");
            this.requestservice.getmyfriends();
            this.myfriends = [];

            //if I am business
            this.tpStorageService.get('useremail').then((useremail_res: any) => {
              this.useremail = useremail_res;
            }).catch(e => {
              this.useremail = "";
            });


            this.events.subscribe('friends', () => {
                ////console.log ("[[[[[[Subscribed to Firebase Friends]]]]]]");
                this.tempallmygroups = [];
                this.allmygroups = [];
                this.myfriends = [];
                this.myfriendsList = [];
                this.myfriends = this.sortdatalist(this.requestservice.myfriends);
                if (this.myfriends.length > 0) {

                    //Friend found
                    this.tpStorageService.setItem('virginityBroken_ChatPage', '1');
                    this.isData = false;
                    //this.convertWithContacts(this.myfriends).then((res: any) => {
                        //if (res.length > 0) {
                            //////console.log ("Removing Duplicate Friends");
                            this.removeDuplicates(this.myfriends, 'mobile').then((responce: any) => {
                                //////console.log ("Duplicate Friends Removed");
                                ////////console.log ("H6");
                                this.myfriendsList = [];
                                this.myfriendsList = responce;
                                this.doRefresh(0);//indivial

                                /*this.events.subscribe('allmygroups', () => {
                                    ////////console.log ("H7");
                                    this.events.unsubscribe('allmygroups');
                                    this.allmygroups = [];
                                    this.allmygroups = this.groupservice.mygroups;
                                    this.doRefresh(1);//groups
                                });
                                this.groupservice.getmygroups();
                                */
                            })
                        //}
                  //  })
                } else {
                    this.isData = true;
                    //there are no friends so let's update the database
                    //but first check for the groups .
                    /*
                    this.events.subscribe('allmygroups', () => {
                    ////////console.log ("H8");
                        this.events.unsubscribe('allmygroups');
                        this.allmygroups = [];
                        this.allmygroups = this.groupservice.mygroups;
                        this.doRefresh(1);//
                    });
                    ////////console.log ("H9");
                    this.groupservice.getmygroups();
                    */
                    //this.loadingProvider.dismissMyLoading();
                }
            });

        }



    referralSendForward (buddy){
        // I want to send buddy's referral to someone else
        this.app.getRootNav().push("SendReferralForwardPage", {source :"chatpage",buddy_uid : buddy.uid,buddy_displayName :buddy.displayName});

    }
    referralSendBackward (buddy){
      this.app.getRootNav().push("SendReferralBackwardPage", {source :"chatpage",buddy_uid : buddy.uid,buddy_displayName : buddy.displayName});//
    }




    //no need to send insert statement because contacts page will do that
    updateContactsTable (db, allChatListing__){
        if(!allChatListing__.mobile || allChatListing__.mobile =="undefined"){
          return ;
        }
        if(allChatListing__.photoURL == null){
          allChatListing__.photoURL = ""; //mytodo set default pic
        }

        db.executeSql("UPDATE cache_tapally_contacts SET uid='"+allChatListing__.uid+"',photoURL='"+allChatListing__.photoURL+"' where mobile = '"+allChatListing__.mobile+"'", {})
        .then(res => {}).catch(e => { });

        //https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8193%20-%20Leopard%20Face.png?alt=media&token=ded0bd0d-5400-451e-8ee7-a4a43d1329e5


        //download the images

        setTimeout(() => {
            //console.log (allChatListing__.photoURL);
            //this.downloadFile(allChatListing__.photoURL, 'mytestfile.jpg');
        }, 1000);

        //this.platform.ready().then( _ => {
        //});
    }//end function


    /*
    https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-file/#where-to-store-files
    */
    downloadFile(pictureUrl,fileName) {
            fileName = pictureUrl.replace(/[^a-zA-Z0-9]/g, '');
            //pictureUrl = 'https://firebasestorage.googleapis.com/v0/b/tapallytest/o/tmp%2FA-Tiger-After-Marriage-Funny-Image.jpg?alt=media&token=357bca54-7372-4754-9fc8-21ebfcf84dd5';
            var oReq = new XMLHttpRequest();
            oReq.responseType = "blob";
            oReq.open("GET", pictureUrl, true);
            oReq.onload = function (oEvent) {
              var DataBlob = oReq.response; // Note: not oReq.responseText
              if (DataBlob) {
                      let myCordova:any;
                      let defaultLocation_:String = '';
                      myCordova = cordova;
                      if(myCordova && myCordova.file){
                         defaultLocation_ = myCordova.file.externalDataDirectory;
                      }//endif

                      var folderPath = defaultLocation_; //cordova.file.dataDirectory;
                      (<any>window).resolveLocalFileSystemURL(folderPath, function(dir) {
                          dir.getFile(fileName, {create:true}, function(file) {
                              file.createWriter(function(fileWriter) {
                                  //fileWriter.seek(fileWriter.length);
                                  fileWriter.write(DataBlob );
                                  fileWriter.onwrite = function(){
                                  }
                                  fileWriter.onwriteend = function() {
                                      //console.log(">>!!>>>>>>>>>>>>>>>>Successful file write...");
                                      //read and verify
                                      /*
                                      file.file(function (file_) {
                                          var reader = new FileReader();
                                          reader.onloadend = function() {
                                              ////console.log("Successful file read: " + this.result);
                                              ////console.log("file.fullPath" + ": " + file.fullPath);
                                          };
                                          reader.readAsText(file_);
                                      });
                                      */
                                  };
                                  fileWriter.onerror = function (e) {
                                      //console.log("!!!!!!!!!!!!!!!!Failed file write: " + e.toString());
                                  };

                              }, function(){
                                  alert('Unable to save file in path '+ folderPath);
                              });
                          });
                      });

                } else {
                  console.error('we didnt get an XHR response!');
                }//endif
            };
            oReq.send(null);


    }//end function
    referralSendForward_ (buddy){
        // I want to send buddy's referral to someone else
        if(!buddy.displayName && buddy.sponseredbusines_businessName){
           this.app.getRootNav().push("SendReferralForwardPage", {source :"chatpage",buddy_uid : buddy.buddy_uid,buddy_displayName :buddy.sponseredbusines_businessName});
        } else {
           this.app.getRootNav().push("SendReferralForwardPage", {source :"chatpage",buddy_uid : buddy.buddy_uid,buddy_displayName :buddy.displayName});
        }
    }
    referralSendBackward_ (buddy){
      if(!buddy.displayName && buddy.sponseredbusines_businessName){
         this.app.getRootNav().push("SendReferralBackwardPage", {source :"chatpage",buddy_uid : buddy.buddy_uid,buddy_displayName : buddy.sponseredbusines_businessName});
      } else {
         this.app.getRootNav().push("SendReferralBackwardPage", {source :"chatpage",buddy_uid : buddy.buddy_uid,buddy_displayName : buddy.displayName});//
      }
    }

    sendToEarningsPage (){
      this.app.getRootNav().push("EarningsPage");
    }


    sendToBroadcastPage (){
      this.app.getRootNav().push("BroadcastPage", {broadcast_retry : false});
    }


    //KEep it same as broadcast.ts :
    //If any thing change in this function make sure it is same as broadcast.ts
    paidOrNot (){
        this.showPaidSign = false;
        //////////console.log ("01923i01923");
        this.tpStorageService.getItem('business_created').then((business_created_raw: any) => {
            //business is registered

            //mytodo : uncomment following later
            this.getWhileLabelObj(true);

            //For now this check if disabled (mytodo). But later we need to call below logic like once a day or so.
            //to avoid clogging of network.
           if(!business_created_raw){
               business_created_raw = 1;
           }

    			 if(true){
             //////////console.log ("Going to check paidOrNot");
    				  /* start */
                  //Before we show warning to business that they have not paid yet
                  let AcceptableDelayInMicroseconds:number = 1000 * 60 * 1;//1 minute from registration
                  let timeStamp:number = Date.now();
                  let business_created:number = parseInt(business_created_raw);
                  //Do not check for first few minutes/hours/days ( as specified in AcceptableDelayInMicroseconds )
                  //But check every single time afterwards
                  if((business_created+AcceptableDelayInMicroseconds)<timeStamp){
                            if(true){
                                    //////console.log ("Paidornot : calling getBusinessDetails");
                                    this.userservice.getBusinessDetails().then((res)=>{
                                           this.tpStorageService.getItem('paid').then((paid_raw: any) => {
                                                //Paid flag set : but does not mean if user indeed paid
                                                if(paid_raw && paid_raw>0){
                                                  //paid
                                                  this.showPaidSign = false;
                                                } else {
                                                  this.showPaidSign = true;
                                                }
                                            }).catch(e => {
                                                //Business did not paid yet
                                                this.showPaidSign = true;
                                            });
                                            //mytodo : need a better place to put it
                                            if(res['paid']){
                                                this.showPaidSign = false;
                                            } else {
                                                this.showPaidSign = true;
                                            }
                                    }).catch(err=>{
                                       //could not get user info
                                    });
                            }//endif

                  } else {
                  }//endif
              /* end */
    			 }
    		}).catch(e => {
            //no business registered for this business
            this.getWhileLabelObj(false);
        });
    }//end function



  fnDeleteAndCreateCacheOfList( ) {
          this.sqlite.create({
            name: 'gr.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            //Update  Local Cahce
            db.executeSql(CONST.create_table_statement_tapally_friends, {})
            .then(res => {
                ////console.log ("DELETE  FROM cache_tapally_friends   ");
                db.executeSql("DELETE  FROM cache_tapally_friends", {}).then(res => {
                                     this.isNoRecord = false;
                                     let uniqueFriends = {};
                                     for (var i = 0; i < this.allChatListing.length; i++) {

                                       let recGotInserted = false;
                                       if(this.allChatListing[i].gpflag>0){
                                              //Group

                                              //do not insert duplicate
                                              /*

                                              MY TODO : When enabling group, please uncomment this

                                              if(uniqueFriends ["group"+this.allChatListing[i].groupName]){
                                                continue;
                                              } else {
                                                uniqueFriends ["group"+this.allChatListing[i].groupName] = true;
                                              }

                                               db.executeSql("INSERT INTO cache_tapally_friends (gpflag       , lastMessage_message  ,lastMessage_dateofmsg  ,groupimage  ,groupName  , lastmsg  ,dateofmsg,timeofmsg,selectCatId,referral_type  ) VALUES ('"+this.allChatListing[i].gpflag+"','"+this.allChatListing[i].lastmsg+"','"+this.allChatListing[i].dateofmsg+"','"+this.allChatListing[i].groupimage+"','"+this.allChatListing[i].groupName+"','"+this.allChatListing[i].lastmsg+"','"+this.allChatListing[i].dateofmsg+"','"+this.allChatListing[i].timeofmsg+"','"+this.allChatListing[i].selectCatId+"','"+this.allChatListing[i].referral_type+"')", {})
                                               .catch(e => {
                                                    //////////console.log ("ERROR inserting in cache_tapally_friends");
                                                    //////////console.log (e);
                                               });
                                               //////////console.log ("Done");

                                               //Update in ARray as well
                                               //mytodo : use uid in groups as well. right now its done by name
                                               for(let g=0;g<this.allChatListing_HTML.length;g++){
                                                   if(this.allChatListing_HTML[g].groupName == this.allChatListing[i].groupName){

                                                      //update photo url
                                                      if(this.allChatListing_HTML[g].groupimage != this.allChatListing[i].groupimage){
                                                           this.allChatListing_HTML[g].groupimage = this.allChatListing[i].groupimage;
                                                      }

                                                      this.allChatListing_HTML[g].lastmsg = this.allChatListing[i].lastmsg;
                                                      this.allChatListing_HTML[g].dateofmsg = this.allChatListing[i].dateofmsg;
                                                      this.allChatListing_HTML[g].timeofmsg = this.allChatListing[i].timeofmsg;
                                                      this.allChatListing_HTML[g].selectCatId = this.allChatListing[i].selectCatId;
                                                      this.allChatListing_HTML[g].referral_type = this.allChatListing[i].referral_type;

                                                      recGotInserted = true;
                                                      break;
                                                   }
                                               }//endfor
                                               */

                                       } else {
                                            //Individual
                                               //do not insert duplicate
                                               if(uniqueFriends ["indivial"+this.allChatListing[i].uid]){
                                                 continue;
                                               } else {
                                                 uniqueFriends ["indivial"+this.allChatListing[i].uid] = true;
                                               }

                                              ////////console.log ("this.allChatListing[i].gpflag"+this.allChatListing[i].gpflag);
                                              //Individual
                                              let isactive_ = 0;
                                              if(this.allChatListing[i].isActive){
                                                isactive_ = 1;
                                              }
                                              let isBlock_ = 0;
                                              if(this.allChatListing[i].isBlock){
                                                isBlock_ = 1;
                                              }
                                              let selection_ = 0;
                                              if(this.allChatListing[i].selection){
                                                selection_ = 1;
                                              }

                                              //console.log ("INSERTING INTO cache_tapally_friends");
                                               db.executeSql("INSERT INTO cache_tapally_friends (isactive,uid,gpflag,selection,isBlock, unreadmessage  , displayName  ,mobile  ,photoURL   ,                                      lastMessage_message  ,lastMessage_type  ,lastMessage_dateofmsg  ,groupimage  ,groupName  , lastmsg  ,dateofmsg,timeofmsg,selectCatId,referral_type,deviceToken  ) VALUES ("+isactive_+",'"+this.allChatListing[i].uid+"','"+this.allChatListing[i].gpflag+"','"+selection_+"',"+isBlock_+",0,'"+this.allChatListing[i].displayName+"','"+this.allChatListing[i].mobile+"','"+this.allChatListing[i].photoURL+"','"+this.allChatListing[i].lastMessage.message+"','"+this.allChatListing[i].lastMessage.type+"','"+this.allChatListing[i].lastMessage.dateofmsg+"','"+this.allChatListing[i].groupimage+"','"+this.allChatListing[i].groupName+"','"+this.allChatListing[i].lastmsg+"','"+this.allChatListing[i].dateofmsg+"','"+this.allChatListing[i].timeofmsg+"','"+this.allChatListing[i].selectCatId+"','"+this.allChatListing[i].referral_type+"','"+this.allChatListing[i].deviceToken+"')", {})
                                               .catch(e => {
                                                    ////////console.log ("ERROR inserting in cache_tapally_friends");
                                                    ////////console.log (e);
                                               });

                                               //Also update cache_tapally_contacts
                                               this.updateContactsTable (db,this.allChatListing[i]);

                                               //Update in ARray as well
                                               for(let g=0;g<this.allChatListing_HTML.length;g++){
                                                   if(this.allChatListing_HTML[g].uid == this.allChatListing[i].uid){

                                                      //update photo url
                                                      if(this.allChatListing_HTML[g].photoURL != this.allChatListing[i].photoURL){
                                                           this.allChatListing_HTML[g].photoURL = this.allChatListing[i].photoURL;
                                                      }//endif
                                                      //this.allChatListing_HTML[g].localPhotoURL = '';
                                                      //this.getLocalImageIfAvial(res.rows.item(i).photoURL,g);

                                                      this.allChatListing_HTML[g].displayName = this.allChatListing[i].displayName;
                                                      this.allChatListing_HTML[g].lastMessage = this.allChatListing[i].lastMessage;

                                                      recGotInserted = true;
                                                      break;
                                                   }
                                               }//endfor

                                       }//endif

                                       if(!recGotInserted) {
                                          //console.log ("Pushing in allChatListing_HTML");
                                          this.allChatListing_HTML.push ( this.allChatListing[i]);
                                       }

                                     }//end for

                                     ////console.log ("this.allChatListing_HTML.length");
                                     ////console.log (this.allChatListing_HTML.length);

                                     if (this.allChatListing_HTML.length == 0) {
                                         this.isNoRecord = true;
                                     }

                                     //Subscribe to new msg  jaswinder
                                     this.requestservice.subscribeToReadingLastMsg();

                                     this.events.subscribe('new_chat_message', () => {
                                       ////console.log ("new_chat_messagenew_chat_messagenew_chat_messagenew_chat_messagenew_chat_message");
                                       //mytodo : this event get called too many times. Try switching between tabs "My contacts" & "Get Referral"

                                          for ( var x in this.requestservice.lastMsgReader ){
                                            for ( var g=0;g<this.allChatListing_HTML.length;g++){
                                              //////console.log (this.allChatListing_HTML[g]['uid'] + ":"+x);
                                              if (this.allChatListing_HTML[g]['uid'] == x ){
                                                    //mytodo : why this is so slow. It takes forever to show on html page

                                                    if(this.requestservice.lastMsgReader[x].request_to_release_incentive){
                                                      this.requestservice.lastMsgReader[x].message = "Request For Incentive";
                                                    }
                                                    ////console.log ("GOING TO SET LAST MSG<<<<<<<<<<<");
                                                    ////console.log (this.requestservice.lastMsgReader[x].message);

                                                    this.allChatListing_HTML[g].lastMessage = this.requestservice.lastMsgReader[x];
                                                    break;
                                              }//endif
                                            }//end for
                                          }//end for
                                     });

                }).catch(e => {
                    //Error in operation
                });
            })
            .catch(e => {
            });//create table

          });//create database
  }//end function




      fnGetLocalCache() {


          this.sqlite.create({
            name: 'gr.db',
            location: 'default'
          }).then((db: SQLiteObject) => {

            //Get Friends
            db.executeSql(CONST.create_table_statement_tapally_friends, {})
            .then(res => {
                db.executeSql("SELECT * FROM cache_tapally_friends", {})
                .then(res => {
                          ////console.log ("Going to read from  cache_tapally_friends <--------<<<<<-------------");
                          ////console.log (res.rows.length + " Records");
                          if(res.rows.length>0){

                                 //if row exists
                                 ////console.log ("RESETTING allChatListing_HTML = []");
                                 this.allChatListing_HTML = [];
                                 let uniqueFriends = {};
                                 this.isNoRecord = false;
                                 for (var i = 0; i < res.rows.length; i++) {
                                            ////////console.log (res.rows.item(i));
                                           let isBlock_ = false ;
                                           if(res.rows.item(i).isBlock){
                                             isBlock_ = true ;
                                           }
                                           let isActive_ = true ;
                                           if(!res.rows.item(i).isactive){
                                             isActive_ = false ;
                                           }


                                           let  record = {};
                                           if(res.rows.item(i).gpflag>0){
                                               //Group

                                               let lastMssg = '';
                                               if(res.rows.item(i).lastmsg && res.rows.item(i).lastmsg!= "undefined"){
                                                 lastMssg = res.rows.item(i).lastmsg;
                                               }
                                               //Group
                                               let tmofmsg = '';
                                               if(res.rows.item(i).timeofmsg){
                                                 tmofmsg = res.rows.item(i).timeofmsg;
                                               }
                                               record = {
                                                   uid : res.rows.item(i).uid,
                                                   displayName : res.rows.item(i).displayName,
                                                   gpflag : res.rows.item(i).gpflag,
                                                   isActive : isActive_,
                                                   isBlock : isBlock_,
                                                   lastmsg : lastMssg,
                                                   mobile : res.rows.item(i).mobile,
                                                   selection : res.rows.item(i).selection,
                                                   unreadmessage : 0,
                                                   photoURL : res.rows.item(i).photoURL,
                                                   groupimage : res.rows.item(i).groupimage,
                                                   groupName : res.rows.item(i).groupName,
                                                   dateofmsg : res.rows.item(i).dateofmsg,
                                                   timeofmsg : tmofmsg
                                               };
                                           } else {
                                             //////////console.log ("gpflaggpflaggpflaggpflag= 0");
                                              //Individual
                                              let lastMssg = '';
                                              if(res.rows.item(i).lastMessage_message && res.rows.item(i).lastMessage_message!= "undefined"){
                                                lastMssg = res.rows.item(i).lastMessage_message;
                                              }
                                              let tmofmsg = '';
                                              if(res.rows.item(i).timeofmsg){
                                                tmofmsg = res.rows.item(i).timeofmsg;
                                              }
                                              record = {
                                                  uid : res.rows.item(i).uid,
                                                  displayName : res.rows.item(i).displayName,
                                                  gpflag : res.rows.item(i).gpflag,
                                                  isActive : isActive_,
                                                  isBlock : isBlock_,
                                                  lastMessage : {
                                                    message: lastMssg,
                                                    type: res.rows.item(i).lastMessage_type,
                                                    dateofmsg: res.rows.item(i).lastMessage_dateofmsg,
                                                    selectCatId : res.rows.item(i).selectCatId,
                                                    referral_type : res.rows.item(i).referral_type,
                                                    timeofmsg: tmofmsg,
                                                    isRead: false,
                                                    isStarred: false
                                                  },
                                                  mobile : res.rows.item(i).mobile,
                                                  selection : res.rows.item(i).selection,
                                                  unreadmessage : 0,
                                                  photoURL : res.rows.item(i).photoURL,
                                                  localphotoURL : '',
                                                  groupimage : res.rows.item(i).groupimage,
                                                  groupName : res.rows.item(i).groupName,
                                                  deviceToken:res.rows.item(i).deviceToken
                                              };
                                           }

                                           ////console.log ("Check1 ");
                                           if(res.rows.item(i).gpflag>1){
                                             //group
                                             if(!uniqueFriends ["group"+res.rows.item(i).groupimage]){
                                               ////console.log ("Pushing in this.allChatListing_HTML <");
                                               this.allChatListing_HTML.push ( record ) ;
                                               uniqueFriends ["group"+res.rows.item(i).groupimage] = true;
                                             }
                                           } else {
                                             //individual
                                             if(!uniqueFriends ["individual"+res.rows.item(i).uid]){
                                               ////console.log ("Pushing in this.allChatListing_HTML <");




                                               var lengthOfArr = this.allChatListing_HTML.push ( record ) ;
                                               //this.getLocalImageIfAvial(res.rows.item(i).photoURL,lengthOfArr-1);
                                               uniqueFriends ["individual"+res.rows.item(i).uid] = true;
                                             }
                                           }

                                           //

                                           //tmp.push (record);

                                 }//end for

                                 if (this.allChatListing_HTML.length == 0) {
                                     this.isNoRecord = true;
                                 }

                                 //just for testing

                                 if(this.testMode){
                                    this.fnMakeSomeTestData ();
                                 }
                                 //this.loadingProvider.dismissMyLoading();
                                 //console.log ("------++++++--+> The HTML should load by now ");
                                // ////////console.log (tmp );
                                 //console.log ("this.allChatListing_HTML");
                                 ////console.log (this.allChatListing_HTML);
                          }
                })
                .catch(e => {
                    //Error in operation
                });
            })
            .catch(e => {
            });//create table

          });//create database
     }//end function

     //mytodo : test if this load the photourl onload or not.
     //the whole is to read file from localstorage and not use internet to download profile pics
     //but if it still does it then its bad
     getMyPhotoURL (item){
       return item.photoURL;
     }

     getLocalImageIfAvial (remoteImageUrl, index){
       let localImageUrl = remoteImageUrl;
       let fileName = remoteImageUrl.replace(/[^a-zA-Z0-9]/g, '');

       //these type of logic is required to escape build errors when cordova.file does not exist
       let myCordova:any;
       let defaultLocation_:String = '';
       myCordova = cordova;
       if(myCordova && myCordova.file){
          defaultLocation_ = myCordova.file.externalDataDirectory;
       }//endif

       //fileName = "mytestfile.jpg";

       let loc_ = defaultLocation_ + "" + fileName;
       loc_ =  normalizeURL(loc_);

       /*
       file:///storage/emulated/0/Android/data/com.tapally.tapally/files/mytestfile.jpg
       */
       this.allChatListing_HTML[index].localPhotoURL  =  loc_ ;
       //console.log ("*****this.allChatListing_HTML[g].localPhotoURL");
       //console.log (this.allChatListing_HTML[index].localPhotoURL);

       //The following line works perfectly
       //(<any>window).resolveLocalFileSystemURL(defaultLocation_ + "/"+fileName, this.fileExists, this.fileDoesNotExist);
       //return localImageUrl;
     }
     fileExists(fileEntry){

       ////console.log (">>>>>>>>File EXISTS : " );
       //this.allChatListing_HTML[g].localPhotoURL = defaultLocation_ + "" +fileEntry.fullPath;
       ////console.log (this.allChatListing_HTML[g].localPhotoURL );

         ////console.log (">>File " + fileEntry.fullPath + " exists!");
     }
     fileDoesNotExist(){
         //console.log(">>file does not exist");
     }

     fnMakeSomeTestData (){
       this.allChatListing_HTML = [];
       /*Some Test Data */
       let record1 = {
           uid : "test1",
           displayName : "John Wilson",
           gpflag : 0,
           isActive : 1,
           isBlock : 0,
           lastMessage : {
             message: "Can I send you referral",
             type: "message",
             dateofmsg: "05-02-2019",
             timeofmsg: "",
             isRead: false,
             isStarred: false
           },
           mobile : "1231231234",
           selection : "",
           unreadmessage : 0,
           photoURL : "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8198%20-%20Octopus%20Face.png?alt=media&token=8ad94d18-0ef1-4cc0-8bb0-7e319be9c11b"
       };
       let record2 = {
           uid : "test2",
           displayName : "Jack Wong",
           gpflag : 0, isActive : 1, isBlock : 0,
           lastMessage : {
             message: "Send me draft tomorrow",
             type: "message",
             dateofmsg: "", timeofmsg: "",  isRead: false,  isStarred: false
           },
           mobile : "",      selection : "",    unreadmessage : 0,
           photoURL : "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8189%20-%20Horse%20Face.png?alt=media&token=f68d155d-9ca7-46d3-881c-30d0f41103dc"
        };
        let record3 = {
            uid : "test2",
            displayName : "Amy Johnson",
            gpflag : 0, isActive : 1, isBlock : 0,
            lastMessage : {
              message: "I got a new lead and she seems intersting",
              type: "message",
              dateofmsg: "", timeofmsg: "",  isRead: false,  isStarred: false
            },
            mobile : "",      selection : "",    unreadmessage : 0,
            photoURL : "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8185%20-%20Giraffe%20Face.png?alt=media&token=6a7d3c69-89ce-411d-8649-cfb7211cc9b6"
         };
         let record4 = {
             uid : "test2",
             displayName : "Peter Jack",
             gpflag : 0, isActive : 1, isBlock : 0,
             lastMessage : {
               message: "Come at 7pm next thursday",
               type: "message",
               dateofmsg: "", timeofmsg: "",  isRead: false,  isStarred: false
             },
             mobile : "",      selection : "",    unreadmessage : 0,
             photoURL : "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8209%20-%20Turtle%20Face.png?alt=media&token=bdaf2962-5a6a-4823-8388-4f7b9d1ab831"
          };
          let record5 = {
              uid : "test2",
              displayName : "Michael Williams",
              gpflag : 0, isActive : 1, isBlock : 0,
              lastMessage : {
                message: "I have question regarding your product",
                type: "message",
                dateofmsg: "", timeofmsg: "",  isRead: false,  isStarred: false
              },
              mobile : "",      selection : "",    unreadmessage : 0,
              photoURL : "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8211%20-%20Wolf%20Face.png?alt=media&token=d8654953-fdd6-46f1-b50a-cbceaf3d5cca"
           };
       this.allChatListing_HTML.push ( record1 ) ;
       this.allChatListing_HTML.push ( record2 ) ;
       this.allChatListing_HTML.push ( record3 ) ;
       this.allChatListing_HTML.push ( record4 ) ;
       this.allChatListing_HTML.push ( record5 ) ;

       this.allChatListing_HTML.push ( record1 ) ;
       this.allChatListing_HTML.push ( record2 ) ;
       this.allChatListing_HTML.push ( record3 ) ;
       this.allChatListing_HTML.push ( record4 ) ;
       this.allChatListing_HTML.push ( record5 ) ;
       this.allChatListing_HTML.push ( record1 ) ;
       this.allChatListing_HTML.push ( record2 ) ;
       this.allChatListing_HTML.push ( record3 ) ;
       this.allChatListing_HTML.push ( record4 ) ;
       this.allChatListing_HTML.push ( record5 ) ;

       console.log ("TEST DATA POPULATED");
       //this.testData.company = 'Home4You Realtor';
       //this.testData.person = 'John Wilson';
       //this.testData.phone = '416-123-1234';
     }


     sendto_invite (){
       if(this.businessName){
         this.app.getRootNav().push("OutreachForBusinessPage");
       } else {
         this.app.getRootNav().push("RegisterbusinessPage", {treatNewInstall : 0});
       }
     }

     goToAInfo (){
        this.app.getRootNav().push("BusinessinfoPage", {showPaidSign:this.showPaidSign});
     }
     goToBInfo (){
        this.app.getRootNav().push("BusinessinfootherPage");
     }
     goToConsumerInfo (){
        this.app.getRootNav().push("ConsumerinfoPage");
     }
     checkReferralIncentive (){
        this.app.getRootNav().push("CheckreferralincentivePage", {whitelabelObj:this.whitelabelObj});
     }
    removeDuplicates(originalArray, prop) {
        return new Promise((resolve) => {
            var newArray = [];
            var lookupObject = {};
            for (var i in originalArray) {
                lookupObject[originalArray[i][prop]] = originalArray[i];
            }
            for (i in lookupObject) {
                newArray.push(lookupObject[i]);
            }
            resolve(newArray);
        }).catch((error) => {
          ////////console.log ("Error 2938");////////console.log (error)
        });
    }

    doRefresh(gpFlag__) {
      //////console.log ("doRefresh ()");
      /*this.tpStorageService.getItem('invited_by').then((invited_by_raw: any) => {
  		  this.doRefreshRaw(gpFlag__,invited_by_raw);
  		}).catch(e => {
        this.doRefreshRaw(gpFlag__,"");
      });
      */
      this.doRefreshRaw(gpFlag__);

    }

    doRefreshRaw(gpFlag__) {
        ////console.log ("doRefreshRaw");
        ////////console.log ("doRefreshRaw gpFlag__ = "+gpFlag__);
        //////////console.log ("Do Refersh() - one for contacts and other for groups");
        let tmpalllist: any = [];

        if (this.allmygroups.length > 0) {
            for (let i = 0; i < this.allmygroups.length; i++) {
                this.allmygroups[i].gpflag = 1;
                tmpalllist.push(this.allmygroups[i]);
            }
        }
        ////////console.log ("this.myfriendsListthis.myfriendsList");
        ////////console.log (this.myfriendsList);
        if (this.myfriendsList.length > 0) {
            for (let i = 0; i < this.myfriendsList.length; i++) {
                this.myfriendsList[i].gpflag = 0;
                this.myfriendsList[i].whitelabelcss = '';

                /*
                if(invited_by_var){
                    if( this.myfriendsList[i].uid == invited_by_var){
                        //this.tpStorageService.setItem('sponseredbusines_name',this.myfriendsList[i].displayName);
                        //this.tpStorageService.setItem('sponseredbusines_mobile',this.myfriendsList[i].mobile);
                    }
                }
                */
                tmpalllist.push(this.myfriendsList[i]);
            }
        }
        if (this.selectedContact.length > 0) {
            this.selectedContact = [];
            this.selectCounter = 0;
            this.selectContact = false;
            for (var i = 0; i < this.allChatListing.length; i++) {
                this.allChatListing[i].selection = false;
            }
        }

        setTimeout(() => {
            //////console.log ("Settimeout inside refersh function ");
            if (this.myfriendsList.length > 0 || this.allmygroups.length > 0) {
                this.zone.run(() => {
                    this.archiveChats = [];
                    this.allChatListing = [];
                    this.tempallmygroups = [];
                    this.counter = [];
                    ////////console.log ("tmpalllist");
                    ////////console.log (tmpalllist);

                    //jaswinder
                    for(let x=0;x<tmpalllist.length;x++){
                            if(tmpalllist[x].gpflag > 0  ){
                              let ifMatched = false;
                              for(let y=0;y<this.allChatListing.length;y++){
                                 //Group
                                 if(tmpalllist [x].groupName == this.allChatListing[y].groupName){
                                     //this.allChatListing[y].lastmsg = tmpalllist [x].lastmsg;
                                     this.allChatListing[y] = tmpalllist [x];
                                     ifMatched = true;
                                     break;
                                 }
                              }//endfor
                              if(!ifMatched){
                                this.allChatListing.push (tmpalllist[x]);
                              }
                            } else {
                              let ifMatched = false;
                              for(let y=0;y<this.allChatListing.length;y++){
                                 //Individual
                                 if(tmpalllist [x].uid == this.allChatListing[y].uid){
                                     //this.allChatListing[y].lastMessage = tmpalllist [x].lastMessage;
                                     this.allChatListing[y] = tmpalllist [x];
                                     ifMatched = true;
                                     break;
                                 }
                               }//endfor
                               if(!ifMatched){
                                 this.allChatListing.push (tmpalllist[x]);
                               }
                            }//endif
                    }//end for
                    this.tempallmygroups = this.allChatListing;
                    this.fnDeleteAndCreateCacheOfList ();

                    //this.loadingProvider.dismissMyLoading();
                });
            } else {
              //so no group or friend found. Might be first time or deleted friends/groups. LEts make sure we put that in database
              this.fnDeleteAndCreateCacheOfList ();
            }
        }, 1000);
    }
    closeButtonClick() {
        //////////console.log ("CloseButton Click");
        if (this.selectedContact.length > 0) {
            this.selectedContact = [];
            this.selectCounter = 0;
            this.selectContact = false;
            for (var i = 0; i < this.allChatListing.length; i++) {
                this.allChatListing[i].selection = false;
            }
        }
    }
    CheckLastMSG(lastmsg, msg) {
        if (!(msg instanceof Array) && msg.match("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?")) {
            return false
        } else if (!(msg instanceof Array) && lastmsg) {
            return true
        } else {
            return false
        }
    }
    CheckLastMsgUrl(url) {

        if (!(url instanceof Array) && url.match("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?")) {

            return true;
        } else if ((url instanceof Array) && url.length > 0) {

            return true;
        }
    }
    sortdatalist(data) {
        return data.sort((a, b) => {
            let result = null;
            if (a.userprio != null) {
                var dateA = new Date(a.userprio).getTime();
                var dateB = new Date(b.userprio).getTime();
            }
            result = dateA < dateB ? 1 : -1;
            return result;
        });
    }
    refreshPage(refresher) {
        this.ionViewWillEnter();
        setTimeout(() => {
            refresher.complete();
        }, 500);
    }
    SortByDate(array) {

        array.sort((a: any = [], b: any = []) => {
            if (a.lastMessage.timestamp > b.lastMessage.timestamp) {
                return -1;
            } else if (a.lastMessage.timestamp < b.lastMessage.timestamp) {
                return 1;
            } else {
                return 0;
            }

        });
        return array;
    }
    viewPofile(url, name) {
        if (this.selectedContact.length == 0) {
            let modal = this.modalCtrl.create("ProfileViewPage", { img: url, name: name });
            modal.present();
        }
    }
    initializeContacts() {

        //initize the array
    		if(this.allChatListing_HTML_ForSearch.length<=0){
    			this.allChatListing_HTML_ForSearch = this.allChatListing_HTML;
    		} else {
    			this.allChatListing_HTML = this.allChatListing_HTML_ForSearch;
    		}//endif

    }

    searchuserBlur (){
        this.showfooter = true;
    }

    searchuserFocus (){
        this.showfooter  = false;
    }

    searchuser(ev: any) {
        this.showfooter  = false;
        this.initializeContacts();
        this.isNoRecord = false;
        let val = ev.target.value;
        if (val && val.trim() != '') {
            this.allChatListing_HTML = this.allChatListing_HTML.filter((item) => {

              if (item.displayName) {
                  return (item.displayName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
              }//endif

              /*
                if (item.displayName) {
                    return (item.displayName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
                }
                else if (item.groupName) {
                    return (item.groupName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
                }
                */
            });
            if (this.allChatListing_HTML.length == 0) {
                this.isNoRecord = true;
            }
        } else {
            this.allChatListing_HTML = this.allChatListing_HTML_ForSearch;
        }
    }
    setStatus() {
        this.chatservice.setstatusUser().then((res) => {
            if (res) {
            }
        }).catch((err) => {
        })
    }
    presentPopover(myEvent) {

        this.app.getRootNav().push("NotificationPage");
        //this.requestcounter = this.myrequests.length;
    }
    viewPopover(myEvent) {
        let popover = this.popoverCtrl.create('PopoverChatPage', { page: '0' });
        popover.present({
            ev: myEvent
        });
        popover.onDidDismiss((pages) => {
            if (pages != 0 && pages != undefined && pages != "undefined"  && pages != null ) {
              if(pages.page){
                this.app.getRootNav().push(pages.page);
              }

            }
        });
    }

    addbuddy() {
        this.app.getRootNav().push('ContactPage', {source:"ImportpPage"});
    }

    /*
    checkInvitedBy (invited_by_business_paid_flag_checked_date_raw){
      this.userservice.getuserdetails().then((res: any) => {
          if(res){
            this.username = res.displayName;
            //////////console.log ("resresresresresresresresresresresresresresresresres");
            //////////console.log (res);
            this.tpStorageService.setItem('invited_by', res.invited_by);
            if(res.invited_by){
                  let AcceptableDelayInMicroseconds:number = 1000; //1 day : Before we check if another business is paid member or not
                  let invited_by_business_paid_flag_checked_date:number = parseInt(invited_by_business_paid_flag_checked_date_raw);
                  let timeStamp:number = Date.now();
                  if(!invited_by_business_paid_flag_checked_date) {
                    invited_by_business_paid_flag_checked_date = 0;
                  }

                  if((invited_by_business_paid_flag_checked_date+AcceptableDelayInMicroseconds)<timeStamp){
                      if(!res.invited_by || res.invited_by != "none"){
                          this.userservice.checkIfBusinessIsPaid(res.invited_by).then((res_invitedby: any) => {
                                  ////////////console.log ("---->");
                                  ////////////console.log (res_invitedby);
                                  this.tpStorageService.setItem('sponseredbusines_businessName',res_invitedby.displayName);
                                  this.tpStorageService.setItem('invited_by_business_paid_flag_checked_date', String(Date.now()));//jaswinder
                                  if(res_invitedby.paid && res_invitedby.paid == "1" ){
                                      this.tpStorageService.setItem('invited_by', res.invited_by);
                                  } else {
                                      //member is not paid so lets not do whitelabeling
                                      this.tpStorageService.setItem('invited_by', "none");
                                      this.tpStorageService.setItem('sponseredbusines_businessName', "");

                                  }
                          });
                      } else {
                              this.tpStorageService.setItem('invited_by', "none");
                              this.tpStorageService.setItem('sponseredbusines_businessName', "");
                      }
                  }//endif
            }
            this.zone.run(() => {
                this.avatar = res.photoURL;
            })
          }
      })
    }
    */

    //profile data
    loaduserdetails() {
        /*
        this.tpStorageService.getItem('invited_by_business_paid_flag_checked_date').then((invited_by_business_paid_flag_checked_date_raw: any) => {
            this.checkInvitedBy (invited_by_business_paid_flag_checked_date_raw);
    		}).catch(e => { this.checkInvitedBy (0); });
        */
    }
    logout() {
        this.chatservice.setStatusOffline().then((res) => {
            if (res) {
                firebase.auth().signOut().then(() => {
                    this.navCtrl.setRoot('LoginPage');
                })
            }
        })
    }
    editname() {
        let statusalert = this.alertCtrl.create({
            buttons: ['okay']
        });
        let alert = this.alertCtrl.create({
            title: 'Edit Nickname',
            inputs: [{
                name: 'nickname',
                placeholder: 'Nickname'
            }],
            buttons: [{
                text: 'Cancel',
                role: 'cancel',
                handler: data => {

                }
            },
            {
                text: 'Edit',
                handler: data => {
                    if (data.nickname) {
                        this.userservice.updatedisplayname(data.nickname).then((res: any) => {
                            if (res.success) {
                                statusalert.setTitle('Updated');
                                statusalert.setSubTitle('Your username has been changed successfully!!');
                                statusalert.present();
                                this.zone.run(() => {
                                    this.username = data.nickname;
                                })
                            }
                            else {
                                statusalert.setTitle('Failed');
                                statusalert.setSubTitle('Your username was not changed');
                                statusalert.present();
                            }

                        })
                    }
                }

            }]
        });
        alert.present();
    }
    editimage() {
        let statusalert = this.alertCtrl.create({
            buttons: ['okay']
        });
        this.imghandler.uploadimage().then((url: any) => {
            this.userservice.updateimage(url).then((res: any) => {
                if (res.success) {
                    statusalert.setTitle('Updated');
                    statusalert.setSubTitle('Your profile pic has been changed successfully!!');
                    statusalert.present();
                    this.zone.run(() => {
                        this.avatar = url;
                    })
                }
            }).catch((err) => {
                statusalert.setTitle('Failed');
                statusalert.setSubTitle('Your profile pic was not changed');
                statusalert.present();
            })
        })
    }
    deleteAllMessage() {
        let title;
        let message;

        if (this.selectedContact[0].uid) {
            title = 'Clear Chat';
            message = 'Do you want to clear this chat ?';
        } else {
            title = 'Delete Group';
            message = 'Do you want to Delete this group ?';
        }
        let alert = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                    }
                },
                {
                    text: 'Ok',
                    handler: () => {
                        if (this.selectedContact.length > 0) {
                            for (let j = 0; j < this.selectedContact.length; j++) {
                                if (this.selectedContact[j].uid) {
                                    for (let i = 0; i < this.selectedContact.length; i++) {
                                        this.chatservice.deleteUserMessages(this.selectedContact).then((res) => {
                                                if (res) {
                                                    this.selectedContact = []
                                                    if (this.selectedContact.length == 0) {
                                                        this.selectContact = false;
                                                    }
                                                    for (let k = 0; k < this.allChatListing.length; k++) {
                                                        this.allChatListing[k].selection = false;
                                                    }
                                                }
                                        })
                                    }
                                } else {
                                    for (let i = 0; i < this.selectedContact.length; i++) {
                                        let groupname = this.selectedContact[i].groupName;

                                        this.groupservice.getgroupInfo(groupname).then((ress: any) => {

                                            let owners = this.groupservice.converanobj(ress.owner)
                                            if (owners.length == 1) {
                                                this.groupservice.getownership(groupname).then((res) => {
                                                    if (res) {

                                                        this.groupservice.deletegroups(groupname).then((res) => {
                                                            if (res) {
                                                                this.selectedContact = []
                                                                if (this.selectedContact.length == 0) {
                                                                    this.selectContact = false;
                                                                }
                                                                for (let k = 0; k < this.allChatListing.length; k++) {
                                                                    this.allChatListing[k].selection = false;
                                                                }
                                                            }
                                                        });
                                                    } else {
                                                        this.groupservice.leavegroups(groupname).then((res) => {
                                                            if (res) {
                                                                this.selectedContact = []
                                                                if (this.selectedContact.length == 0) {
                                                                    this.selectContact = false;
                                                                }
                                                                for (var m = 0; m < this.allChatListing.length; m++) {
                                                                    this.allChatListing[m].selection = false;
                                                                }
                                                            }

                                                        });
                                                    }
                                                });
                                            } else {
                                                this.groupservice.getownership(groupname).then((res) => {
                                                    if (res) {
                                                        this.groupservice.leavegroupsowner(groupname).then((res) => {
                                                            if (res) {
                                                                this.selectedContact = []
                                                                if (this.selectedContact.length == 0) {
                                                                    this.selectContact = false;
                                                                }
                                                                for (var p = 0; p < this.allChatListing.length; p++) {
                                                                    this.allChatListing[p].selection = false;
                                                                }
                                                            }

                                                        });
                                                    } else {
                                                        this.groupservice.leavegroups(groupname).then((res) => {
                                                            if (res) {
                                                                this.selectedContact = []
                                                                if (this.selectedContact.length == 0) {
                                                                    this.selectContact = false;
                                                                }
                                                                for (var o = 0; o < this.allChatListing.length; o++) {
                                                                    this.allChatListing[o].selection = false;
                                                                }
                                                            }

                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            ]
        });
        alert.present();
    }

    //(press)="popoveroncontact(item,$event)"
    popoveroncontact(item, event) {
        if (this.selectedContact.length == 0) {
            item.selection = true;
            this.selectedContact.push(item)
            if (this.selectContact == true) {
                this.selectContact = false;
            } else {
                this.selectContact = true;
            }
        }
        this.selectCounter = this.selectedContact.length;
        if (this.selectedContact.length == 0) {
            this.selectContact = false;
            item.selection = false;
        }
    }

    testfunction (){
      //////console.log ("test123");
    }
    buddychat(item, event, UnreadCount) {
      //////console.log ("BuddyChat Clicked");
      const animationsOptions = {
        animate: true, direction: "forward"
      }

        if (event.target.classList.contains('avtar-class') || event.target.classList.contains('avtar-img-list')) {
        } else {
            if (this.selectedContact.length == 0) {
                this.tpStorageService.setItem('UnreadCount', UnreadCount);
                this.chatservice.initializebuddy(item);
                this.app.getRootNav().push('BuddychatPage',{back_button_pop_on : true}, animationsOptions);//animationsOptions
            }
            else if (this.selectedContact.length == 1) {
                if (this.selectedContact[0].uid == item.uid) {
                    item.selection = false;
                    this.selectedContact = [];
                    this.selectCounter = 0;
                    this.selectContact = false;
                } else {
                    this.selectedContact = [];
                    this.selectCounter = 0;
                    this.selectContact = false;
                    for (var i = 0; i < this.allChatListing.length; i++) {
                        this.allChatListing[i].selection = false;
                    }
                    item.selection = true;
                    this.selectedContact.push(item);
                    this.selectCounter = this.selectedContact.length;
                    this.selectContact = true;
                }
            } else {
                this.popoveroncontact(item, event);
            }

        }
    }
    openchat(item, event) {
        //////////console.log ("Open Chat");

        if (this.selectedContact.length == 0) {
            this.groupservice.getintogroup(item.groupName);
            this.app.getRootNav().push('GroupchatPage', { groupName: item.groupName, groupImage: item.groupimage });
        } else if (this.selectedContact.length == 1) {
            if (this.selectedContact[0].groupName == item.groupName) {
                item.selection = false;
                this.selectedContact = [];
                this.selectCounter = 0;
                this.selectContact = false;
            } else {
                this.selectedContact = [];
                this.selectCounter = 0;
                this.selectContact = false;
                for (var i = 0; i < this.allChatListing.length; i++) {
                    this.allChatListing[i].selection = false;
                }
                item.selection = true;
                this.selectedContact.push(item);
                this.selectCounter = this.selectedContact.length;
                this.selectContact = true;
            }
        } else {
            this.popoveroncontact(item, event);
        }
    }

    /*
    convertWithContacts(data) {
        return new Promise((resolve, reject) => {
            this.contactProvider.getSimJsonContacts().then(res => {
                if (res['contacts'].length > 0) {
                    for (let i = 0; i < res['contacts'].length; ++i) {
                        for (let j = 0; j < data.length; ++j) {
                            if (res['contacts'][i].mobile == data[j].mobile) {
                                data[j].displayName = res['contacts'][i].displayName;
                            }
                        }
                    }
                    let islastMessage: any = [];
                    let islastMessageNot: any = [];
                    for (let i = 0; i < data.length; ++i) {
                        if (data[i].lastMessage) {
                            islastMessage.push(data[i]);
                        } else {
                            islastMessageNot.push(data[i]);
                        }
                    }
                    data = this.SortByDate(islastMessage);
                    data = data.concat(islastMessageNot);
                    resolve(data);
                } else {
                    resolve(data);
                }
            }).catch(err => {
                if (err == 20) {
                    this.platform.exitApp();
                }
                resolve(data);
            });
        }).catch((error) => {
          ////////console.log ("Error 8757");////////console.log (error);
        });

    }*/
}
/*
function checkIfFileExists(path){
    // path is the full absolute path to the file.
    (<any>window).resolveLocalFileSystemURL(path, fileExists, fileDoesNotExist);
}
function fileExists(fileEntry){
    //console.log ("File " + fileEntry.fullPath + " exists!");
}
function fileDoesNotExist(){
    //console.log("file does not exist");
}
*/
