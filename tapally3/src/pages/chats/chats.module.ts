import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatsPage } from './chats';
//import {  MatCardModule, MatTabsModule, MatChipsModule, MatIconModule, MatToolbarModule, MatDatepickerModule, MatFormFieldModule, MatNativeDateModule } from "@angular/material";
//import {MatButtonModule} from '@angular/material/button';
import { IonicImageLoader } from 'ionic-image-loader';


@NgModule({
  declarations: [
    ChatsPage,
  ],
  imports: [
    IonicPageModule.forChild(ChatsPage),
		IonicImageLoader
  ],
})
/*
MatButtonModule,
MatCardModule,
MatTabsModule,
MatChipsModule,
MatIconModule,
MatToolbarModule,
MatDatepickerModule,
MatFormFieldModule,
MatNativeDateModule
*/
export class ChatsPageModule {}
