import { Component, ViewChild, NgZone, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Content, LoadingController, ActionSheetController, PopoverController, AlertController, ModalController, Platform ,App} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { ChatProvider } from '../../providers/chat/chat';
import { ImagehandlerProvider } from '../../providers/imagehandler/imagehandler';
import { LoadingProvider } from '../../providers/loading/loading';

import { FcmProvider } from '../../providers/fcm/fcm';
import { UserProvider } from '../../providers/user/user';
import { RequestsProvider } from '../../providers/requests/requests';
import { ContactProvider } from '../../providers/contact/contact';
import { Subscription } from 'rxjs';
import { ImageViewerController } from 'ionic-img-viewer';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';
import { CatProvider } from '../../providers/cats/cats';
import { SQLite,SQLiteObject } from '@ionic-native/sqlite';
import { CONST} from '../../app/const';
@IonicPage()
@Component({
  selector: 'page-secondpage',
  templateUrl: 'secondpage.html',
})
export class SecondpagePage {
  @ViewChild('content') content: Content;
  authForm: FormGroup;
  message: AbstractControl;
  messageIds:any = [];
  buddy: any;
  newmessage = '';
  newmessageTmp = '';
  referralRequestPrefix;
  referralRequestPostfix;
  isRefresher:boolean = false;
  allmessages = [];
  filterallmessages = [];
  tempFilterallmessages = [];
  photoURL;
  imgornot;
  todaydate;
  myfriends;
  myfriendsList;
  tempmyfriendsList;
  allmessagesGroups = [];
  isData: boolean = false;
  buddyStatus: any;
  datacounter = 10;
  headercopyicon = true;
  catItems;
  db_:any;
  showheader:boolean = true;
  selectAllMessage = [];
  selectCounter = 0;
  buddymessages;
  UnreadMSG: number;
  isuserBlock: boolean = false;
  msgstatus: boolean = false;
  backbuttonstatus: boolean = false;
  DeleteMsg;
  userId;
  _imageViewerCtrl: ImageViewerController;
  firebuddychats = firebase.database().ref('/buddychats');
  constructor(
    public navCtrl: NavController,
        public navParams: NavParams,
        //public storage: Storage,
        public fcm: FcmProvider,
        public chatservice: ChatProvider,
        public events: Events,
        public catservice: CatProvider,
        public zone: NgZone,
        private sqlite: SQLite,
        public loadingCtrl: LoadingController,
        public imgstore: ImagehandlerProvider,
        public formBuilder: FormBuilder,
        public loading: LoadingProvider,
        public app: App,
        public userservice: UserProvider,
        public actionSheetCtrl: ActionSheetController,
        public eleRef: ElementRef,
        public popoverCtrl: PopoverController,
        private alertCtrl: AlertController,
        public requestservice: RequestsProvider,
        public contactProvider: ContactProvider,
        public modalCtrl: ModalController,
        public platform: Platform,
        public imageViewerCtrl: ImageViewerController,
        public tpStorageService: TpstorageProvider ) {

        this.platform.ready().then((readySource) => {

            this.authForm = this.formBuilder.group({ 
            });
            //this.message = this.authForm.controls['message'];

            this.catItems = this.catservice.getCats();
            this._imageViewerCtrl = imageViewerCtrl;
            this.buddyStatus = '';

            this.tpStorageService.getItem('userUID').then((userId__: any) => {
                  this.userId = userId__;
            }).catch(e => { });

            /*this.events.subscribe('newmessage_secondpage', (newMessageObj) => {
              this.zone.run(() => {
                if(!this.messageIds[newMessageObj.messageId]){
                    console.log ("RECIEVED event >  :"+newMessageObj.messageId);
                    //this.MessageSubscription (newMessageObj);
                    this.messageIds [newMessageObj.messageId] = true;
                }//endif
                //
                //console.log (newMessageObj.messageId);
              });
            })
            */

        });
  }


  ionViewWillLeave (){
  }

  ngOnDestroy (){
  }
  onChange(e) {
      if (this.newmessage.trim()) {
          this.msgstatus = true;
      }
      else {
          this.msgstatus = false;
      }
  }
  ionViewDidLoad() {
      this.allmessagesGroups = [];
      this.buddy  = {uid : '6FP0RtOoYhhvsOsjphTdCjTVIO62'};
      this.chatservice.buddy = this.buddy;
      this.todaydate = this.formatDate(new Date());
      this.fnGetCacheLocal ();
  }

  MessageSubscription (newMessageObj, pushInDb = false){
      let tmpArr = {
        'dateofmsg' : newMessageObj.dateofmsg,
        'message_item' : [newMessageObj]
      }

      newMessageObj.selection = false;
      newMessageObj.selectCatName = '';

      if (newMessageObj.referral_type == null){
        newMessageObj.referral_type = "";
      }

      if (newMessageObj.selectCatId != null){
        for(let g=0;g<this.catItems.length;g++){
          if(this.catItems[g]['id']==newMessageObj.selectCatId){
            newMessageObj.selectCatName = this.catItems[g]['name'];
            break;
          }
        }
      }//endif

      //Check if data exists
      let foundKey:boolean = false;
      for (var myKey in this.allmessagesGroups) {
          if(this.allmessagesGroups[myKey] && (this.allmessagesGroups[myKey].dateofmsg == newMessageObj.dateofmsg)){
            this.allmessagesGroups [myKey].message_item.push (newMessageObj);
            if(pushInDb){
              this.fnInsertInDatabase (newMessageObj);
            }
            foundKey = true;
            break;
          }
      }//end for

      if(!foundKey){
        this.allmessagesGroups.push (tmpArr);
        if(pushInDb){
          this.fnInsertInDatabase (newMessageObj);
        }
      }
  }
  presentImage(myImage) {
      if (this.selectAllMessage.length == 0) {
          const imageViewer = this._imageViewerCtrl.create(myImage);
          imageViewer.present();
          //imageViewer.onDidDismiss(() => ));
      }
  }
  MessageSubscriptionCache (res){
      this.allmessages = [];
      this.allmessagesGroups = [];
      console.log ("READING FROM CACHE");
      for (var i = 0; i < res.rows.length; i++) {
        this.MessageSubscription ( JSON.parse( res.rows.item(i).val ));
      }//end for
  }

  fnInsertInDatabase (newMessageObj){
      this.db_.executeSql("INSERT INTO cache_buddychat (uid,key,val) VALUES ('"+this.buddy.uid+"','"+newMessageObj.messageId+"','"+JSON.stringify(newMessageObj)+"')", {}).catch(e => {
              //Error in operation
              console.log ("ERROR 95934: " + e);
      });
  }//end function

  fnGetCacheLocal (){
    this.sqlite.create({
      name: 'gr.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      this.db_ = db;
      db.executeSql(CONST.create_table_buddychat, {})
      .then(res => {
          db.executeSql("SELECT * FROM cache_buddychat where uid like '"+this.buddy.uid+"'", {})
          .then(res => {
                    if(res.rows.length>0){
                         //Something is in cache
                         console.log ("Something is in cache ALREADY");
                         this.MessageSubscriptionCache (res);
                    } else {
                         //Nothing in cache so lets get last 100 messages
                         console.log (" Nothing in cache so lets read messages");
                         this.getbuddymessagesForSecondaPage(100);
                    }
          }).catch(e => {
              //Error in operation
          });
      }).catch(e => {
          //Error in operation
      });
  }).catch(e => {
      //Error in operation
  });
}//end function

formatDate(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    if (dd < 10)
        dd = '0' + dd;
    if (mm < 10)
        mm = '0' + mm;
    return dd + '/' + mm + '/' + yyyy;
}


  getbuddymessagesForSecondaPage ( limit ){
    if(this.userId) {
       this.getbuddymessagesForSecondaPage_ ( limit );
    } else {
        this.tpStorageService.getItem('userUID').then((userId__: any) => {
              this.userId = userId__;
              this.getbuddymessagesForSecondaPage_ ( limit );
        }).catch(e => { });
    }//endif
  }

  getbuddymessagesForSecondaPage_(limit) {
    this.buddymessages = [];
    this.firebuddychats.child(this.userId).child(this.buddy.uid).limitToLast(limit).on('child_added', (snapshot) => {
      //Check for unique value
      let tmpObj = snapshot.val();
      let isMatch  = false;
      for (var myKey in this.buddymessages) {
          if( this.buddymessages[myKey]['dateofmsg'] == tmpObj.dateofmsg &&
          this.buddymessages[myKey]['isStarred'] == tmpObj.isStarred &&
          this.buddymessages[myKey]['message'] == tmpObj.message &&
          this.buddymessages[myKey]['messageId'] == tmpObj.messageId &&
          this.buddymessages[myKey]['multiMessage'] == tmpObj.multiMessage &&
          this.buddymessages[myKey]['sentby'] == tmpObj.sentby &&
          this.buddymessages[myKey]['timeofmsg'] == tmpObj.timeofmsg &&
          this.buddymessages[myKey]['timestamp'] == tmpObj.timestamp &&
          this.buddymessages[myKey]['type'] == tmpObj.type
        ){
            //This is redundant message
            isMatch = true;
            break;
        }
       }//end for
       if(!isMatch){
         this.buddymessages.push(tmpObj);
         console.log ("GOING TO MESSAGE SUBSCRIPTION");
         this.MessageSubscription (tmpObj, true);
       }
    })
  }
  openAttachment (myEvent){
    this.openAttachment_(myEvent);
    /*
    //mytodo : disabling "Media Libary " on ios because i could not make it work. But we need to make it work
    if (this.platform.is('ios')) {
      this.sendPicMsg_ ();
    } else {
      this.openAttachment_(myEvent);
    }
    */
  }

  referralSendForward (myEvent){
      // I want to send buddy's referral to someone else
      this.navCtrl.push("SendReferralForwardPage", {source :"buddypage",buddy_uid : this.buddy.uid,buddy_displayName : this.buddy.displayName});

  }
  referralSendBackward (myEvent){
    this.navCtrl.push("SendReferralBackwardPage", {source :"buddypage", buddy_uid : this.buddy.uid,buddy_displayName : this.buddy.displayName});
  }

  sendReferral (friend_asking_referral, catid){
      this.navCtrl.push("FriendAskingReferralPage", {friend_asking_referral_uid : friend_asking_referral.uid,friend_asking_referral_displayName : friend_asking_referral.displayName, catid:catid});
  }

      openAttachment_(myEvent) {
          let popover = this.popoverCtrl.create('AttachmentsPage', { buddy: this.buddy });
          popover.present({
              ev: myEvent
          });
          popover.onDidDismiss((pages) => {
              if (pages != 0 && pages != undefined && pages != "undefined"  && pages != null ) {
                  if(pages.page){
                    this.navCtrl.push(pages.page, { buddy: pages.buddy, flag: pages.flag });
                  }
              }else{
              }
          });
      }

  scrollto() {
  }

      addmessage_async (){
           this.newmessageTmp = this.newmessage;
           this.newmessage = '';
           ////////console.log ("addmessage_async");
           setTimeout(() => {
               this.addmessage ();
           }, 1);
      }//end async function


      addmessage() {
          this.UnreadMSG = 0;
          this.selectAllMessage = [];
          this.selectCounter = 0;
          this.headercopyicon = true;
          this.userservice.getstatusblock(this.buddy).then((res) => {
              ////////console.log ("-getstatus");
              if (!res) {
                  this.newmessageTmp = this.newmessageTmp.trim();
                  if (this.newmessageTmp != '') {
                        ////////console.log ("-if message is not empty");
                        this.fcm.sendNotification(this.buddy, 'You have received a message', 'chatpage');
                        //this.events.unsubscribe('newmessage');//to avoid triggring multiple calls
                        //mytodo : above fix doesn`t work. Please try something else
                        this.msgstatus = false;
                        this.chatservice.addnewmessage(this.newmessageTmp, 'message', this.buddyStatus).then(() => {
                          this.newmessage = '';
                          this.msgstatus = false;
                        });

                  } else {
                      this.newmessage = this.newmessageTmp;
                      this.loading.presentToast('Please enter valid message...');
                  }
              } else {
                  //user might be blocked
                  this.newmessage = this.newmessageTmp;
                  let alert = this.alertCtrl.create({
                      message: 'Unblock ' + this.buddy.displayName + ' to send a message.',
                      buttons: [
                          {
                              text: 'CANCEL',
                              role: 'cancel',
                              handler: () => {
                              }
                          },
                          {
                              text: 'UNBLOCK',
                              handler: () => {
                                  this.userservice.unblockUser(this.buddy).then((res) => {
                                      if (res) {
                                          this.addmessage();
                                      }
                                  })
                              }
                          }
                      ]
                  });
                  alert.present();
              }
          })
          ////////console.log ("-End Function");
      }//end function

}
