import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DiscPage } from './disc';

@NgModule({
  declarations: [
    DiscPage,
  ],
  imports: [
    IonicPageModule.forChild(DiscPage),
  ],
})


export class DiscPageModule {}
