import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserProvider } from '../../providers/user/user';
import { CatProvider } from '../../providers/cats/cats';
import { ReferralincentiveTypeProvider } from '../../providers/referralincentive-type/referralincentive-type';
import firebase from 'firebase';
import { Http } from '@angular/http';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';
@IonicPage()
@Component({
  selector: 'page-registerbusiness',
  templateUrl: 'registerbusiness.html',
})
export class RegisterbusinessPage {
  authForm : FormGroup;
  businessname;
  businessadd;
  useremail;
  cats;
  showheader:boolean = true;
  bcats;
  bcatName;
  userId;
  nextPage;
  submittedAndEmailisWrong:boolean = false;
  bcatsObj;
  showincentivedetail:boolean = false;
  rtlistComplete;
  referral_incentive_detail_txt;
  rtlist;
  referral_incentive_detail;
  referral_incentive_detail_placeholder;
  doIHaveRegisteredBusinessAlready:boolean=false;
  submitted:boolean=false;
  msgstatus: boolean = false;
  msgstatus_referraldetail: boolean = false;
  treatNewInstall:boolean = false;
  selectOptions;
  selectOptionsTP;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public fb: FormBuilder,
    public http:Http,
    public userservice: UserProvider,
    public tpStorageService: TpstorageProvider,
    public catservice: CatProvider,
    public rtService : ReferralincentiveTypeProvider
  ) {
      this.referral_incentive_detail = '';
      this.referral_incentive_detail_placeholder = '';


      this.nextPage = "ImportpPage";
      if(this.navParams.get('treatNewInstall')>0){
        this.treatNewInstall = true;
        this.doIHaveRegisteredBusinessAlready = false;
      }
      this.cats = this.catservice.getCats();
      this.rtlistComplete = this.rtService.getList();
      //this.assignCatName ();

      this.selectOptions = {
       title: 'Business Category',
       subTitle: 'Please select an appropriate category'
     };
     this.selectOptionsTP = {
      title: 'Referral Incentive Type',
      subTitle: 'If your customer send you referral, what type of incentive would you offer to them ?'
    };
     //businessadd
     /*
    this.authForm=this.fb.group({
      businessname:['',Validators.compose([Validators.required])],
      useremail: ['', Validators.compose([Validators.required, Validators.email])],
      bcats:['',Validators.compose([Validators.required])],
      rtlist:[''],
      referral_incentive_detail_txt:['']
    })
    useremail: ['', Validators.compose([Validators.required, Validators.email])],
    */

    this.authForm=this.fb.group({
      businessname:['',Validators.compose([Validators.required])],
      useremail: ['', Validators.compose([Validators.required])],
      bcats:['',Validators.compose([Validators.required])],
      rtlist:[0],
      referral_incentive_detail_txt:['']
    })
    this.rtlist = 0;

  }
  tpInitilizeFromStorage (){
    this.tpStorageService.getItem('userUID').then((res: any) => {
       if(res){
         this.userId = res;
       }
    }).catch(e => { });
    this.tpStorageService.getItem('bcats').then((res: any) => {
       if(res){
         this.bcatsObj = res;
       }
    }).catch(e => { });
    this.tpStorageService.getItem('useremail').then((res: any) => {
       if(res){
         this.useremail=res;
       }
    }).catch(e => { });
    this.tpStorageService.getItem('businessName').then((res: any) => {
       if(res){
         this.doIHaveRegisteredBusinessAlready = true;
       } else {
         this.doIHaveRegisteredBusinessAlready = false;
       }
    }).catch(e => { });
  }
  ionViewWillEnter() {
    this.tpInitilizeFromStorage ();
  }

  ionViewWillLeave() {
     this.showheader = false;
  }
  assignCatName (){
    if(!this.bcatsObj){
      this.bcatName = "";
      return;
    }
    for(var x=0;x<=this.cats.length;x++){
       if(this.cats[x].id == this.bcatsObj){
          this.bcatName = this.cats[x].name;
       }
    }//end for
  }

  backButtonClick() {
      this.navCtrl.pop({animate: true, direction: "forward"});
  }
  btnSkipStep (){
    //mytodo mytodonow : check if contacts are already imported then just go to tabspage
    this.navCtrl.setRoot(this.nextPage);
  }

  ionViewDidLoad() {
    this.tpStorageService.getItem('userUID').then((res: any) => {
        if (res) {
            this.userId = res;
            this.loadbusinessname();
        }

    }).catch(e => {
      if (firebase && firebase.auth() && firebase.auth().currentUser) {
        this.userId = firebase.auth().currentUser.uid;
        this.loadbusinessname();
      }
    });

  }

  validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
  }

  loadbusinessname(){

       //Validation pass
       this.userservice.getBusinessDetails().then((res)=>{
         //jaswinder
         if(res){
           this.msgstatus=true;
           this.businessname=res['displayName'];
           this.tpStorageService.setItem('businessName', this.businessname);
           this.bcats=res['bcats'];
           this.rtlist = res['referral_type_id'];
           this.referral_incentive_detail_txt = res['referral_detail'];
           this.onChange_incentivetype(this.rtlist);
           this.doIHaveRegisteredBusinessAlready = true;
           //this.assignCatName ();
         }else{
           this.tpStorageService.setItem('businessName', '');
           this.businessname='';
           this.useremail='';
           this.bcats = 0;
           this.doIHaveRegisteredBusinessAlready = false ;
           this.rtlist = 0;
           this.referral_incentive_detail_txt = "";
         }
       }).catch(err=>{
       });

       //Get user email : mytodo put this in sqlite as well and get other user info as well
       if(this.useremail == "null"){
         this.useremail = "";
       }
       if(!this.useremail){
         this.userservice.getuserdetails().then((res)=>{
           if(res && res['useremail']!= "null"){
               this.useremail=res['useremail'];
           }else{
               this.useremail= "";
           }
         }).catch(err=>{
         });
       }//endif


}//end function
  onChange(e) {
    if (this.businessname.trim()) {
        this.msgstatus = true;
    }
    else {
        this.msgstatus = false;
    }
  }
  onChange_incentivetype(e) {
    this.referral_incentive_detail = 'Incentive Detail';
    this.referral_incentive_detail_placeholder = '';
    if (e) {

      switch (e ) {
          case '1':
          case '2':
              //cash
              //Gift Card
              this.referral_incentive_detail = 'Enter Referral Amount (Numbers Only)';
              this.referral_incentive_detail_placeholder = 'E.g. 10 For $10';
              this.showincentivedetail = true;
              break;
          case '3':
              //Extra service
              this.referral_incentive_detail = 'Enter Service Detail'
              this.referral_incentive_detail_placeholder = 'E.g. 2 Hours Extra Serivce'
              this.showincentivedetail = true;
              break;
          case '4':
              //Other
              this.referral_incentive_detail = 'Enter Incentive Detail'
              this.referral_incentive_detail_placeholder = 'E.g. Any Other Incentive Detail'
              this.showincentivedetail = true;
              break;
          default:
             this.showincentivedetail = false;
      }//end switch
    }
    else {
      this.showincentivedetail = false;
    }
  }

  save (){

        console.log ("this.validateEmail(this.useremail)");
        console.log (this.validateEmail(this.useremail));
        console.log ( this.useremail );
        if (!this.validateEmail(this.useremail)) {
           //error on email
           this.submittedAndEmailisWrong = true;
        }  else {
            //email is good
           this.save_ ();
        }

  }

  save_(){
    //console.log ("tp3123");
    this.submitted=true;
    if(this.authForm.valid){
      //console.log ("tp3124");
      if(this.businessname.trim()!=null || this.businessname.trim()!= ""){
        //console.log ("tp3125");
        this.businessname=this.businessname.trim();
        this.useremail=this.useremail.trim();
        //console.log ("tp3126");

        if(this.doIHaveRegisteredBusinessAlready){
          //I am updating business

        } else {
          //I am registering business
          this.tpStorageService.getItem('userUID').then((res_uid: any) => {
            this.tpStorageService.getItem('code').then((res_phone: any) => {
              this.tpStorageService.getItem('verificationCredential').then((res_countryCode: any) => {
                this.tpStorageService.getItem('myDisplayName').then((res_myDisplayName: any) => {

                   this.http.get('https://tapally.com/wp-json/log/v1/createbiz?phone='+res_phone+'&countrycode='+res_countryCode+'&uid='+res_uid+'&businessname='+this.businessname+'&email='+this.useremail+'&bcat='+this.bcats+'&displayname='+res_myDisplayName+'').subscribe(dddd => {
                  },
                err => {
                });

                 }).catch(e => { });
              }).catch(e => { });
            }).catch(e => { });
          }).catch(e => { });



        }//endif



        this.userservice.registerBusiness(this.businessname,this.useremail,this.bcats,this.rtlist,this.referral_incentive_detail_txt).then((res: any) => {
          //console.log ("tp3127");
          if (true) {
            if(this.doIHaveRegisteredBusinessAlready && !this.treatNewInstall){
              //I am updating business
              //this.navCtrl.popToRoot ();
              this.navCtrl.setRoot('TabsPage');
            } else {
              //I am registering business
              this.navCtrl.setRoot(this.nextPage);
            }

          }
        }).catch((err) => {
          //console.log ("Error tp3130");
        }) ;
        } else {

        }
    }

    }

}
