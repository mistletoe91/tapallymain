import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImportpPage } from './importp';

@NgModule({
  declarations: [
    ImportpPage,
  ],
  imports: [
    IonicPageModule.forChild(ImportpPage),
  ],
})
export class ImportpPageModule {}
