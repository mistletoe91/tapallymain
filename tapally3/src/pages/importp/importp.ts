import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
@IonicPage()
/**
 * Generated class for the ImportpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-importp',
  templateUrl: 'importp.html',
})
export class ImportpPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
  }

  addbuddy() {
    //this.navCtrl.push('InvitemanuallyPage');
    this.navCtrl.push("ContactimportprogressPage" , {source : 'importp'});
  }

}
