import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GpattachmentsPage } from './gpattachments';

@NgModule({
  declarations: [
    GpattachmentsPage,
  ],
  imports: [
    IonicPageModule.forChild(GpattachmentsPage),
  ],
})
export class GpattachmentsPageModule {


}
