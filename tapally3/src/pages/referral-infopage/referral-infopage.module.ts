import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReferralInfopagePage } from './referral-infopage';

@NgModule({
  declarations: [
    ReferralInfopagePage,
  ],
  imports: [
    IonicPageModule.forChild(ReferralInfopagePage),
  ],
})
export class ReferralInfopagePageModule {}
