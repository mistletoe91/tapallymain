import { Component } from '@angular/core';
import { NavController, NavParams,App } from 'ionic-angular';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';
@IonicPage()
@Component({
  selector: 'page-referral-infopage',
  templateUrl: 'referral-infopage.html',
})
export class ReferralInfopagePage {
  iamABusinessOwner:boolean = false;
  showheader:boolean = true;
  constructor(public app: App,public navCtrl: NavController, public navParams: NavParams,public tpStorageService: TpstorageProvider) {
  }
  ionViewWillLeave() {
  }
  tpInitilizeFromStorage (){
    this.tpStorageService.getItem('business_created').then((res: any) => {
       if(res){
         this.iamABusinessOwner = true;
       }
    }).catch(e => { });
  }
  sendToEarningsPage (){
    this.app.getRootNav().push("EarningsPage");
  }  
  presentPopover(myEvent) {
      this.navCtrl.push("NotificationPage");
      //this.requestcounter = this.myrequests.length;
  }
  sendToBroadcastPage (){
    this.app.getRootNav().push("BroadcastPage", {broadcast_retry : false});
  }
  ionViewWillEnter() {
    this.showheader = true;
    this.tpInitilizeFromStorage();
  }
  sendreferraldirectly (){
    this.navCtrl.push("ReferralPage");
  }

  sendToRegisterBuesiness (){
    this.navCtrl.push("RegisterbusinessPage", {treatNewInstall : 0});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReferralInfopagePage');
  }

}
