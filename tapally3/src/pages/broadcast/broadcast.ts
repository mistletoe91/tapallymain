import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { ChatProvider } from '../../providers/chat/chat';
import { UserProvider } from '../../providers/user/user';
import { FcmProvider } from '../../providers/fcm/fcm';
import { RequestsProvider } from '../../providers/requests/requests';
import { LoadingProvider } from '../../providers/loading/loading';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';
@IonicPage()
@Component({
  selector: 'page-broadcast',
  templateUrl: 'broadcast.html',
})
export class BroadcastPage {
  myrequests = [];
  requestcounter = null;
  isenabled:boolean=true;
  buddy: any;
  newmessage = '';
  debugMe:boolean = true;
  showPaidSign:boolean = false;
  doIhavebusinessAccount = false;
  showheader:boolean = true;
  cntCounter;
  myfriends;
  myfriendsList;
  tempmyfriendsList;
  isData: boolean = false;
  authForm: FormGroup;
  message: AbstractControl;
  constructor(public fcm: FcmProvider,public loading: LoadingProvider,public requestservice: RequestsProvider,public fb: FormBuilder,public events: Events,public chatservice: ChatProvider,public userservice: UserProvider,public navCtrl: NavController, public toastCtrl: ToastController, public navParams: NavParams,public tpStorageService: TpstorageProvider) {

    this.authForm = this.fb.group({
        'message': [null, Validators.compose([Validators.required])]
    });
    this.message = this.authForm.controls['message'];

  }

  /*
  Test message
   value="Father`s Day. Big Red Weekend. Our lowest prices of the season. Stop everything. Up to 20% Off Now"

   */
  backButtonClick() {
    ////console.log ("dasdasdasdasdas");
    ////console.log (this.navParams.get('broadcast_retry'));
    if(this.navParams.get('broadcast_retry')){
      this.navCtrl.setRoot("TabsPage");
    } else {
      this.navCtrl.pop({animate: true, direction: "forward"});
    }

  }
  tpInitilizeFromStorage (){
     this.doIhavebusinessAccount = false;
     this.tpStorageService.getItem('businessName').then((res: any) => {
        if(res){
           this.doIhavebusinessAccount = true;
        }
     }).catch(e => { });
  }
  ionViewWillEnter() {
      this.tpInitilizeFromStorage ();
      this.paidOrNot ();
      this.requestservice.getmyfriends();
      this.myfriends = [];
      this.events.subscribe('friends', () => {
        this.events.unsubscribe('friends');
        this.myfriends = [];
        this.myfriends = this.requestservice.myfriends;
      });

      /*this.requestservice.getmyrequests();
      this.events.subscribe('gotrequests', () => {
          this.myrequests = [];
          this.myrequests = this.requestservice.userdetails;
          if (this.myrequests) {
              this.requestcounter = this.myrequests.length;
          }
      })
      */
  }

  //KEep it same as chats.ts :
  //If any thing change in this function make sure it is same as chats.ts
  paidOrNot (){
      this.showPaidSign = false;
      this.tpStorageService.getItem('business_created').then((business_created_raw: any) => {
         if(business_created_raw){
            /* start */
                let AcceptableDelayInMicroseconds:number = 1000; //Before we show warning to business that they have not paid yet
                let timeStamp:number = Date.now();
                let business_created:number = parseInt(business_created_raw);
                //Do not check for first few minutes/hours/days ( as specified in AcceptableDelayInMicroseconds )
                //But check every single time afterwards
                if((business_created+AcceptableDelayInMicroseconds)<timeStamp){

                      this.tpStorageService.getItem('paid').then((paid_raw: any) => {
                          let paid:number = parseInt(paid_raw);
                          //check API everytime for now
                          if(true){
                                  this.userservice.getBusinessDetails().then((res)=>{
                                          //this.tpStorageService.setItem('businessName', res['displayName']);
                                          //this.tpStorageService.setItem('bcats', res['bcats']);
                                          if(!paid || paid<=0){
                                              //console.log ("Indeed Business has not paid yet");
                                              this.showPaidSign = true;
                                          }
                                  }).catch(err=>{
                                  });
                          }//endif
                      }).catch(e => { });

                } else {

                }//endif
            /* end */
         }
      }).catch(e => { });
  }//end function

  ionViewWillLeave() {
     this.showheader = false;
  }

  broadcast() {
      //console.log ("Broadcasting..1.");
      this.newmessage = this.newmessage.trim();
      if (this.newmessage != '') {
      } else {
        this.loading.presentToast('Please enter a valid message...');
        return;
      }
      this.isenabled = false;

      this.cntCounter = 0;
      if(this.myfriends.length>0){
          for (var key=0;key<this.myfriends.length;key++) {
            this.addmessage(this.myfriends[key]);
          }//end for
      } else {
          //You have no friends
          this.loading.presentToast('It appears like your contact list is empty. Invite your friends and let`s get started');
          this.navCtrl.setRoot('TabsPage');
      }


  }
  presentPopover(myEvent) {
      this.navCtrl.push("NotificationPage");
      //this.requestcounter = this.myrequests.length;
  }
  sendToNextPage (){
      this.cntCounter++;
      if(this.cntCounter>=this.myfriends.length){
        this.cntCounter = 0;
        this.navCtrl.setRoot("BroadcastSubmittedPage");
      }
  }

  sendToBusinessReg (){
    this.navCtrl.push("RegisterbusinessPage", {treatNewInstall : 0});
  }

  addmessage(buddy) {
    this.newmessage = this.newmessage.trim();

    //Send Push notification : mytodo check if the buddy is online or not. if online then no need to send push notification
    let newMessage = 'You have received a message';
    this.fcm.sendNotification(buddy, newMessage, 'chatpage');

    //Send chat message
    this.chatservice.addnewmessageBroadcast(this.newmessage, 'message', buddy , 0, true ).then(() => {
        this.newmessage = '';
        this.sendToNextPage ();
    });
  }

  ionViewDidLoad() {
  }

}
