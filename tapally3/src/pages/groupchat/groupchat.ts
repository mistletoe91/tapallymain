import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, ModalController, AlertController, NavParams, ActionSheetController, Content, Events, PopoverController, Platform } from 'ionic-angular';
import { GroupsProvider } from '../../providers/groups/groups';
import { ImagehandlerProvider } from '../../providers/imagehandler/imagehandler';
import { UserProvider } from '../../providers/user/user';
import { LoadingProvider } from '../../providers/loading/loading';
import { ImageViewerController } from 'ionic-img-viewer';
import { RequestsProvider } from '../../providers/requests/requests';
import {env} from '../../app/app.angularfireconfig'
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';
@IonicPage()
@Component({
    selector: 'page-groupchat',
    templateUrl: 'groupchat.html',
})
export class GroupchatPage {

    @ViewChild('content') content: Content;
    owner: boolean = false;
    userpic=env.userPic;
    groupName;
    groupImage;
    newmessage: any = '';
    allgroupmsgs;
    allgroupMessage = [];
    alignuid;
    photoURL;
    imgornot;
    isRefresher:boolean = false;
    loginuserInfo;
    msgstatus: boolean = false;
    showheader:boolean = true;
    imgmulti: any[];
    selectedMessage = [];
    _imageViewerCtrl: ImageViewerController;
    headerSelectionIcon = true;
    selectCounter = 0;
    groupMember = [];
    groupMemberStatus = [];
    backbuttonstatus = false;
    DeleteMsg;
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public imghandler: ImagehandlerProvider,
        public groupservice: GroupsProvider,
        public events: Events,
        public userservice: UserProvider,
        public actionSheet: ActionSheetController,
        public zone: NgZone,
        public popoverCtrl: PopoverController,
        public loading: LoadingProvider,
        public modalCtrl: ModalController,
        public imageViewerCtrl: ImageViewerController,
        public requestservice: RequestsProvider,
        public platform: Platform,
        public alertCtrl: AlertController,
        public tpStorageService: TpstorageProvider
    ) {
        this._imageViewerCtrl = imageViewerCtrl;
        if (this.groupservice.currentgroupname != undefined) {
            this.groupName = this.groupservice.currentgroupname;
        } else {
            this.groupName = this.navParams.get('groupName');
            this.groupservice.currentgroupname = this.groupName;
        }
        this.groupImage = this.navParams.get('groupImage');
        this.alignuid = this.groupservice.userId;
        this.groupservice.getownership(this.groupName).then((res) => {
            this.groupservice.getgroupimage();
            this.groupservice.getgroupmembers();
            if (res) {
                this.owner = true;
            }
        }).catch((err) => {
        })
    }
    backButtonClick() {
        this.navCtrl.pop({animate: true, direction: "forward"});
    }
    ionViewCanEnter() {
        if (this.groupservice.currentgroupname != undefined) {
            this.groupName = this.groupservice.currentgroupname;
        } else {
            this.groupName = this.navParams.get('groupName');
            this.groupservice.currentgroupname = this.groupName
        }
        this.groupservice.getgroupInfo(this.groupName).then((res: any) => {
            this.groupImage = res.groupimage;
        })
        this.groupservice.getgroupmsgs(this.groupName);
        this.getGroupMembers();
        this.events.subscribe('newgroupmsg', () => {
          //Do not unsubscribe this. it is necessary to read real-time messages
          //this.events.unsubscribe('newgroupmsg');

            this.tpStorageService.getItem('userUID').then((res: any) => {
        			 if(res){
        				  let currentuser = res;

                  this.allgroupmsgs = [];
                  this.imgornot = [];
                  this.zone.run(() => {
                      this.allgroupmsgs = this.groupservice.groupmsgs;
                  });
                  for (let key = 0; key < this.allgroupmsgs.length; key++) {
                      if (this.allgroupmsgs[key].sentby != currentuser) {
                          this.userservice.getmsgblock(this.allgroupmsgs[key].sentby).then((res) => {
                              this.allgroupmsgs[key].blockby = res;
                          })
                      }
                      else {
                          this.allgroupmsgs[key].blockby = false;
                      }
                      var d = new Date(this.allgroupmsgs[key].timestamp);
                      var hours = d.getHours();
                      var minutes = "0" + d.getMinutes();
                      var month = d.getMonth(); 1
                      var da = d.getDate();
                      var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                      var formattedTime = monthNames[month] + "-" + da + "-" + hours + ":" + minutes.substr(-2);
                      this.allgroupmsgs[key].timestamp = formattedTime;
                      if (this.allgroupmsgs[key].multiMessage == true) {
                          this.imgornot.push(true);
                      } else {
                          if (this.allgroupmsgs[key].message.substring(0, 4) === 'http') {
                              this.imgornot.push(true);
                          }
                          else {
                              this.imgornot.push(false);
                          }
                      }
                  }
                  this.allgroupMessage = [];
                  this.allgroupMessage = this.allgroupmsgs;
                  if (this.allgroupMessage.length > 5) {
                      this.scrollto();
                  }

        			 }//end res
        		}).catch(e => { });



        });
    }
    ionViewDidLoad() {
        this.groupservice.getgroupmsgs(this.groupName);
        this.userservice.getuserdetails().then((res: any) => {
            this.loginuserInfo = res;
        }).catch(err => {
        });
    }
    ionViewWillLeave() {
        this.showheader = false;
        this.groupservice.getintogroup(null);
        this.groupservice.getmygroups();
        this.requestservice.getmyfriends();
    }
    addmessage_async (){
         setTimeout(() => {
             this.addgroupmsg ();
         }, 1);
    }//end async function
    addgroupmsg() {
        this.closeButtonClick()
        this.getGroupMembers();
        if (this.newmessage.trim()) {
            this.groupservice.addgroupmsg(this.newmessage, this.loginuserInfo, 'message', this.groupName).then((res) => {
                this.newmessage = '';
                this.scrollto();
                this.msgstatus = false;
            })
        } else {
            this.loading.presentToast('Please enter valid message...');
        }

    }
    scrollto() {
        /*setTimeout(() => {
          if(this.content._scroll)  {
            this.content.scrollToBottom();
          }
        }, 500);
        */
    }
    callFunctionScroll() {
      if(this.content._scroll && !this.isRefresher)  {
        this.content.scrollToBottom(0);
      }
    }
    presentOwnerSheet() {
        let sheet = this.actionSheet.create({
            title: 'Group Actions',
            buttons: [
                {
                    text: 'Remove member',
                    icon: 'remove-circle',
                    handler: () => {
                        this.navCtrl.push('GroupmembersPage');
                    }
                }, {
                    text: 'Delete Group',
                    icon: 'trash',
                    handler: () => {
                        this.groupservice.deletegroup().then(() => {
                            this.navCtrl.pop();
                        }).catch((err) => {
                        });
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    icon: 'cancel',
                    handler: () => {
                    }
                }
            ]
        })
        sheet.present();
    }

    presentMemberSheet() {
        let sheet = this.actionSheet.create({
            title: 'Group Actions',
            buttons: [
                {
                    text: 'Leave Group',
                    icon: 'log-out',
                    handler: () => {
                        this.groupservice.leavegroup().then(() => {
                            this.navCtrl.pop();
                        }).catch((err) => {
                        })
                    }
                }, {
                    text: 'Cancel',
                    role: 'cancel',
                    icon: 'cancel',
                    handler: () => {
                    }
                }
            ]
        })
        sheet.present();
    }

    openGroupDetail() {
        this.navCtrl.push('GroupinfoPage', { groupName: this.groupName });
    }
    openAttachment(myEvent) {
        let popover = this.popoverCtrl.create('GpattachmentsPage', { gname: this.groupName });
        popover.present({
            ev: myEvent
        });
        popover.onDidDismiss((pages) => {
            if (pages != 0 && pages != undefined && pages != "undefined"  && pages != null ) {
                if(pages.page){
                  this.navCtrl.push(pages.page, { gpname: pages.gpname, flag: pages.flag });
                }
            }
        });
    }
    onChange(e) {
        if (this.newmessage.trim()) {
            this.msgstatus = true;
        }
        else {
            this.msgstatus = false;
        }
    }
    openAllImg(items) {
        if (this.selectedMessage.length == 0) {
            this.navCtrl.push("AllimgPage", { allimg: items });
        }
    }
    openDoc(path) {
        this.imghandler.openDocument(path).then(res => {
        }).catch(err => {
        })
    }
    openContacts(contacts) {
        this.navCtrl.push("ShowContactPage", { contacts: contacts });
    }
    showLocation(location) {
        this.navCtrl.push("MapPage", { location: location })
    }
    presentImage(myImage) {
        const imageViewer = this._imageViewerCtrl.create(myImage);
        imageViewer.present();
    }
    goBack() {
        this.navCtrl.pop();
    }
    viewPopover(myevent) {
        let popove = this.popoverCtrl.create('GroupChatPopoverPage', { groupname: this.groupName });
        popove.present({ ev: myevent });
        popove.onDidDismiss(() => {
        });
    }

    pushMsg(item, event) {
        let flag = 0;
        if (this.selectedMessage.length != 0) {
            for (let i = 0; i < this.selectedMessage.length; i++) {
                if (item.messageId == this.selectedMessage[i].messageId) {
                    flag = 1;
                    item.selection = false;
                    this.selectedMessage.splice(i, 1);
                    break;
                } else {
                }
            }
            if (flag == 1) {
            } else {
                this.pushanotherMsg(item, event);
            }
        } else {
            item.selection = true;
            this.selectedMessage.push(item);
            if (this.headerSelectionIcon == true) {
                this.headerSelectionIcon = false;
                this.backbuttonstatus = true;
            } else {
                this.headerSelectionIcon = true;
                this.backbuttonstatus = false;
            }
        }
        this.selectCounter = this.selectedMessage.length;
        if (this.selectedMessage.length == 0) {
            this.headerSelectionIcon = true;
            this.backbuttonstatus = false;
        }
    }
    pushanotherMsg(item, event) {
        let flag = 0;
        if (this.selectedMessage.length >= 1) {
            for (let i = 0; i < this.selectedMessage.length; i++) {
                if (item.messageId == this.selectedMessage[i].messageId) {
                    item.selection = false;
                    flag = 1;
                    this.selectedMessage.splice(i, 1);
                    break;
                } else {
                    flag = 0;
                }
            }
            if (flag == 1) {
            } else {
                item.selection = true;
                this.selectedMessage.push(item);
            }
        }
        this.selectCounter = this.selectedMessage.length;
        if (this.selectedMessage.length == 0) {
            this.headerSelectionIcon = true;
            this.backbuttonstatus = false;
        }

    }


    deleteMessage() {
        if(this.selectedMessage.length > 1){
            this.DeleteMsg="Do you want to delete these messages?"
        }else{
            this.DeleteMsg="Do you want to delete this message?"
        }
        this.loading.presentLoading();
        let alert = this.alertCtrl.create({
            title: 'Delete Message',
            message: this.DeleteMsg,
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: () => {
                        this.closeButtonClick();
                        this.loading.dismissMyLoading();
                    }
                },
                {
                    text: 'Yes',
                    handler: () => {
                        if (this.selectedMessage.length != 0) {
                            this.groupservice.deleteMessages(this.selectedMessage).then((res) => {
                                if (res) {
                                    this.selectedMessage = [];
                                    if (this.selectedMessage.length == 0) {
                                        this.headerSelectionIcon = true;
                                        this.backbuttonstatus = false;
                                        this.loading.dismissMyLoading();
                                    }
                                }
                            }).catch((err) => {
                            });

                        }
                    }
                }
            ]
        });
        alert.present().then(() => {
        });

    }
    closeButtonClick() {
        this.selectedMessage = [];
        this.selectCounter = 0;
        this.headerSelectionIcon = true;
        this.backbuttonstatus = false;
        for (var i = 0; i < this.allgroupMessage.length; i++) {
            this.allgroupMessage[i].selection = false;
        }
    }

    getGroupMembers() {
        this.groupservice.getgroupmembers();
        this.events.subscribe('gotmembers', () => {
            this.events.unsubscribe('gotmembers');
            this.groupMember = this.groupservice.currentgroup;
            this.groupservice.getgroupmemebrstatus(this.groupMember);
            this.events.subscribe('groupmemberstatus', () => {
              this.events.unsubscribe('groupmemberstatus');
              this.groupMemberStatus = this.groupservice.groupmemeberStatus;
            });
        });
    }

}
