import { Component, NgZone, ViewChild } from '@angular/core';
import { IonicPage, NavController,Content, Platform, NavParams, AlertController, Events, ModalController, ToastController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { ContactProvider } from '../../providers/contact/contact';
import { RequestsProvider } from '../../providers/requests/requests';
import { connreq } from '../../models/interfaces/request';
import { LoadingProvider } from '../../providers/loading/loading';
import { ChatProvider } from '../../providers/chat/chat';
import { SmsProvider } from '../../providers/sms/sms';
import { FcmProvider } from '../../providers/fcm/fcm';
import { GroupsProvider } from '../../providers/groups/groups';
import { env } from '../../app/app.angularfireconfig';
//import { LZString } from '../../app/compress';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';

import { SQLite,SQLiteObject } from '@ionic-native/sqlite';
import { CONST} from '../../app/const';

@IonicPage()
@Component({
	selector: 'page-contact',
	templateUrl: 'contact.html',
})
export class ContactPage {
  @ViewChild('content') content: Content;
	contactList: any = [];
	allregisteUserData: any = [];
	metchContact: any = [];
	mainArray: any = [];
	tempArray: any = [];
	myfriends;
	OnIsSearchBarActive:boolean = false;
	sourceImportpPage_:boolean = false;
	userpic = env.userPic;
	showheader:boolean = true;
	readContactsFromCache:boolean = false;
	cssClassChecking = 'hide';
	cssClassContacts = 'show';
	isData: boolean = false;
	reqStatus = false;
	isInviteArray: any = [];

	newrequest = {} as connreq;
	temparr = [];
	filteredusers = [];
	updatedRef:String;
	contactCounter = 0;
	userId;
	constructor(
		public events: Events,
		private platform: Platform,
		public loadingProvider: LoadingProvider,
		public contactProvider: ContactProvider,
		private sqlite: SQLite,
		public userservice: UserProvider,
		public navCtrl: NavController,
		public navParams: NavParams,
		public alertCtrl: AlertController,
		public requestservice: RequestsProvider,
		public chatservice: ChatProvider,
		public zone: NgZone,
		public http:Http,
		public smsservice: SmsProvider,
		public fcm: FcmProvider,
		public modalCtrl: ModalController,
		private toastCtrl: ToastController,
		public groupservice: GroupsProvider
		, public tpStorageService: TpstorageProvider
	) {

	}
	tpInitilizeFromStorage (){
		this.tpStorageService.getItem('userUID').then((res: any) => {
			 if(res){
				 this.userId = res;
			 }
		}).catch(e => { });
		this.tpStorageService.getItem('isInvitedContact').then((res: string) => {
			 if(res){
				 this.isInviteArray = JSON.parse(res);

			 }
		}).catch(e => { });
		this.tpStorageService.getItem('updated').then((res: any) => {
			 if(res){
				 this.updatedRef = res;
			 }
		}).catch(e => { });
	}


	  backButtonClick() {
			if(this.sourceImportpPage_){
					this.navCtrl.setRoot('TabsPage');
			} else {
				  this.navCtrl.pop({animate: true, direction: "forward"});
			}//endif

	  }

	ionViewDidLoad() {
				this.loadingProvider.presentLoading();
				if(this.navParams.get('source') == "ImportpPage"){
					this.sourceImportpPage_ = true;
				} else {
					this.sourceImportpPage_ = false;
				}
				this.tpInitilizeFromStorage ();
				this.contactProvider.getSimJsonContacts()
				.then(res => {
						Object.keys(res).forEach(ele => {


							 if(ele == "fromcache"){

								 if(!res["fromcache"]){
									  //came from device
									  this.readContactsFromCache = true;
									  this.cssClassChecking = 'show';
								  	this.cssClassContacts = 'hide';

										this.events.subscribe('contactInsertingDone', () => {
 							 			  this.events.unsubscribe('contactInsertingDone');
 							 			  if(!this.contactProvider.contactInsertingDone){
												//still inserting
 							 					this.readContactsFromCache = true;
 							 					this.cssClassChecking = 'show';
 							 					this.cssClassContacts = 'hide';
 							 				} else {
												//done inserting
 							 					//this.readContactsFromCache = false;
 							 					//this.cssClassChecking = 'hide';
 							 					//this.cssClassContacts = 'show';
												this.navCtrl.setRoot('TabsPage');
 							 				}
 							 	    });

								 } else {
 								   //came from database
									 this.readContactsFromCache = false;
									 this.cssClassChecking = 'hide';
									 this.cssClassContacts = 'show';

								 }
							 }
						   if(ele == "contacts"){

								 this.mainArray = [];
								 this.contactCounter = 0;


								 	Object.keys(res["contacts"] as any).forEach(key  => {
										this.mainArray.push ({
											"displayName": res["contacts"][key].displayName,
											"mobile":  res["contacts"][key].mobile,
											"mobile_formatted":  res["contacts"][key].mobile_formatted,
											"status": "",
											"isBlock":false,
											"isUser":0,
											"isdisable":false,
											"dim" : false ,
											"photoURL":res["contacts"][key].photoURL,
											"uid":res["contacts"][key].uid
										});
									});

									this.loadingProvider.dismissMyLoading();

									//check and fix the invited one
									this.tpStorageService.getItem('isInvitedContact').then((res: string) => {
										 if(res){
											 this.isInviteArray = JSON.parse(res);
											 for(let g=0;g<this.isInviteArray.length;g++){
												 for(let x=0;x<this.mainArray.length;x++){
													  if( this.mainArray[x].mobile == this.isInviteArray[g] ||  this.mainArray[x].mobile_formatted == this.isInviteArray[g] ){
															this.mainArray [x].isdisable = true;
															break;
														}
												 }
											 }
										 }
									}).catch(e => { });

							 //Only check contct history and build friend - only once
						     this.tpStorageService.getItem('importedContacts').then((t: any) => {
															  if(t!= "1") {
																	this.importContactHistoryAndBuildFriends ();
																}
														}).catch(e => {
																this.importContactHistoryAndBuildFriends ();
														});

							 }//endif
					  });




				}).catch(e => { });





	}//end function

	// Get All contact from the device
	getAllDeviceContacts__(myfriends) {
		this.contactProvider.getSimJsonContacts()
			.then(res => {
				this.loadingProvider.dismissMyLoading();

				//Now async check for any updated contact
				this.contactProvider.CheckForUpdatedContacts ().then(res => {}).catch(err => {});
				//if(this.loadingProvider){
				//	this.loadingProvider.loading.dismiss ();
				//}


				if (this.updatedRef == "show") {
						this.tpStorageService.setItem("updated","");
						let toast = this.toastCtrl.create({
							message: 'Contacts updated successfully.',
							duration: 2500,
							position: 'bottom'
						});
						toast.present();
				}
				if (res['contacts'].length > 0) {
					let newConatctList = [];
					newConatctList = res['contacts'];
					this.contactList = this.removeDuplicates(newConatctList, 'mobile');
					for (let i = 0; i < this.contactList.length; i++) {
						this.contactList[i].isBlock = false;
						if (this.contactList[i].displayName != undefined) {
							this.contactList[i].displayName = this.contactList[i].displayName.trim();
						}
					}
					this.metchContact = this.allregisteUserData.filter((item) =>
						this.contactList.some(data => item.mobile == data.mobile));

					for (let i = 0; i < this.contactList.length; i++) {
						for (let j = 0; j < this.metchContact.length; j++) {
							if (this.contactList[i] != undefined && this.metchContact[j] != undefined) {
								if (this.contactList[i].mobile == this.metchContact[j].mobile) {
									this.metchContact[j].isUser = "1";
									this.metchContact[j].isBlock = this.contactList[i].isBlock;
									this.metchContact[j].displayName = this.contactList[i].displayName;
								}
							}

						}
					}
					for (let i = 0; i < this.metchContact.length; i++) {
						for (let j = 0; j < myfriends.length; j++) {
							if (this.metchContact[i] != undefined && myfriends[j] != undefined) {
								if (this.metchContact[i].mobile == myfriends[j].mobile) {
									this.metchContact[i].status = 'friend';
									this.metchContact[i].isBlock = myfriends[j].isBlock;
								}
							}
						}
					}
					this.contactList = this.contactList.filter(o1 => {
						return !this.metchContact.some(o2 => {
							return o1.mobile === o2.mobile;          // assumes unique id
						});
					});
					this.metchContact = this.metchContact.sort(function (a, b) {
						if (a.displayName != undefined) {
							var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
						}
						if (b.displayName != undefined) {
							var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
						}
						if (nameA < nameB) {
							return -1;
						}
						if (nameA > nameB) {
							return 1;
						}

						// names must be equal
						return 0;
					});
					// sort by name
					this.contactList = this.contactList.sort(function (a, b) {
						if (a.displayName != undefined) {
							var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
						}
						if (b.displayName != undefined) {
							var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
						}
						if (nameA < nameB) {
							return -1;
						}
						if (nameA > nameB) {
							return 1;
						}
						// names must be equal
						return 0;
					});
					let myArray = this.metchContact.concat(this.contactList);
					if (this.isInviteArray.length > 0) {
						for (let i = 0; i < myArray.length; i++) {
							for (let j = 0; j < this.isInviteArray.length; j++) {
								if (myArray[i].mobile == this.isInviteArray[j]) {
									myArray[i].isdisable = true;
									break;
								} else {
									myArray[i].isdisable = false;
								}
							}
						}
					}

					//Find my friends
					for (let i = 0; i < myArray.length; i++) {
						for (let j = 0; j < myfriends.length; j++) {
							if (myArray[i] != undefined && myfriends[j] != undefined) {

								if (myArray[i].mobile == myfriends[j].mobile) {
									myArray[i].displayName = myfriends[j].displayName;
									myArray[i].status = 'friend';
									myArray[i].photoURL = myfriends[j].photoURL;
									myArray[i].isBlock = myfriends[j].isBlock;
								}
							}
						}
					}

					//this.zone.run(() => {
					//	this.mainArray = myArray;
					//})
					this.tempArray = this.mainArray;






				} else {
					this.isData = true;
				}
			}).catch(err => {
				this.loadingProvider.dismissMyLoading();
				if (err == 20) {
					this.platform.exitApp();
				}
			});
	}

	importContactHistoryAndBuildFriends (){
				this.tpStorageService.setItem('importedContacts', "1");

				//mytodo : when user add more in phone : upload them only the delta
				//mytodo : also add compression
				let strAllPhoneNumbers = "";
				let counterContacts = 0;
				for(let b=0;b<this.mainArray.length;b++){
					if(counterContacts++ >= 500 ) {

						//send the url once for every 500 contacts
						this.http.get('https://us-central1-tapallyionic3.cloudfunctions.net/updateContactList?inputuid='+this.userId+'&inputStr='+strAllPhoneNumbers)
							.map(res => res.json()).subscribe(dddd => {
													dddd.unsubscribe();
							},
						err => {
						});
						strAllPhoneNumbers = '';
						counterContacts = 0;
					}//endif

					if(this.mainArray[b].mobile && this.mainArray[b].mobile!= undefined){
						strAllPhoneNumbers += ","+this.mainArray[b].mobile;
					}
				}//end for




	}//end function

	removeDuplicates(originalArray: any[], prop) {
		let newArray = [];
		let lookupObject = {};

		originalArray.forEach((item, index) => {
			lookupObject[originalArray[index][prop]] = originalArray[index];
		});

		Object.keys(lookupObject).forEach(element => {
			newArray.push(lookupObject[element]);
		});
		return newArray;
	}

	ionViewWillLeave() {
		//this.showheader = false;
		//this.groupservice.getmygroups();
		//this.requestservice.getmyfriends();
		this.loadingProvider.dismissMyLoading();
	}
	refreshPage() {
		//this.ionViewDidLoad();
		//this.tpStorageService.setItem('updated', 'show');
		const confirm = this.alertCtrl.create({
      title: 'Sync All Contacts',
      message: 'It may take few minutes to re-sync all contacts from your device. Do you agree ? ',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            //console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            //console.log('Agree clicked');
						this.contactProvider.deleteFromTable() ;
						this.navCtrl.setRoot('ContactimportprogressPage', {source : 'ContactPage'});
          }
        }
      ]
    });
    confirm.present();

	}

	searchuser(ev: any) {
			if (this.platform.is('ios')) {
				return this.searchuser_ios(ev);
			} else {
				return this.searchuser_android (ev);
			}
	}

	searchuser_ios(ev: any) {
		//initize the array
		if(this.tempArray.length<=0){
			this.tempArray = this.mainArray
		} else {
			this.mainArray = this.tempArray;
		}//endif

		let val = ev.target.value;
		if (val && val.trim() != '') {

			this.mainArray.forEach((item, index) => {
				if (this.mainArray[index].displayName.toLowerCase().indexOf(val.toLowerCase())> -1){
					this.mainArray[index].dim = false;
				} else {
					this.mainArray[index].dim = true;
				}//endif
			});
		} else {
			this.mainArray.forEach((item, index) => {
					this.mainArray[index].dim = false;
			});
			/*
			this.mainArray = this.mainArray.filter((item) => {
				if (item.displayName != undefined) {
					return (  (item.displayName.toLowerCase().indexOf(val.toLowerCase())) > -1);
				}
			});
			*/

			//mytodo_critical mytodo : reload virtualScroll
			//this.content.scrollToTop();
			//console.log ("Going to scroll to top ");
			//this.content.scrollTo(0, 0, 0);
		}//endif
	}//end function

	searchuser_android(ev: any) {
		//initize the array
		if(this.tempArray.length<=0){
			this.tempArray = this.mainArray
		} else {
			this.mainArray = this.tempArray;
		}//endif

		let val = ev.target.value;
		if (val && val.trim() != '') {
				this.tempArray = this.mainArray.filter((item) => {
					if (item.displayName != undefined) {
						return (  (item.displayName.toLowerCase().indexOf(val.toLowerCase())) > -1);
					}
				});
		}//endif
	}//end function

	inviteReq(recipient) {
		let alert = this.alertCtrl.create({
			title: 'Invitation',
			subTitle: 'Invitation already sent to ' + recipient.displayName + '.',
			buttons: ['Ok']
		});
		alert.present();
	}


  searchuserBlur (){
		//if (this.platform.is('ios')) {
		console.log ("Blure...");
	   //this.OnIsSearchBarActive = false;
	  //}
  }

  searchuserFocus (){
		 console.log ("Active");
	   //this.OnIsSearchBarActive  = true;
  }

	sendreq(recipient) {
		this.reqStatus = true;
		this.newrequest.sender = this.userId;
		this.newrequest.recipient = recipient.uid;
		if (this.newrequest.sender === this.newrequest.recipient)
			alert('You are your friend always');
		else {
			let successalert = this.alertCtrl.create({
				title: 'Request sent',
				subTitle: 'Your request has been sent to ' + recipient.displayName + '.',
				buttons: ['ok']
			});

			let userName: any;

			this.userservice.getuserdetails().then((res: any) => {
				userName = res.displayName;

				let newMessage = userName + " has sent you friend request.";

				this.fcm.sendNotification(recipient, newMessage, 'sendreq');
			});

			this.requestservice.sendrequest(this.newrequest).then((res: any) => {
				if (res.success) {
					this.reqStatus = false;
					successalert.present();
					let sentuser = this.mainArray.indexOf(recipient);
					this.mainArray[sentuser].status = "pending";
				}
			}).catch((err) => {
			})
		}
	}

	buddychat(buddy) {
			this.chatservice.initializebuddy(buddy);
			this.navCtrl.push('BuddychatPage', {back_button_pop_on : true});
	}


	sendSMS(item) {
		console.log ("SEND SMS <-----");
		this.loadingProvider.presentLoading();

		this.smsservice.sendSms(item.mobile).then((res) => {
			this.loadingProvider.dismissMyLoading();
			if (res) {

				for (var i = 0; i < this.mainArray.length; ++i) {
					if (this.mainArray[i].mobile == item.mobile) {
						this.mainArray[i].isdisable = true
					}
				}
				this.isInviteArray.push(item.mobile);

				//log it
				this.requestservice.logInvitation (item.mobile);

				//set  invited_by or make friends
				this.requestservice.checkPhones(item.mobile).then((res_) => {
					 //if there is record then it means it cannot be invited
					 if (!res_){
						 this.requestservice.setInvitedBy(item.mobile);
					 } else {
						 if(null != res_["uid"]){
						 		// the person already have tapally account so lets make friends
								 //jaswinder
								 this.userservice.getbuddydetails(res_["uid"]).then((buddy: any) => {
								      this.requestservice.acceptrequest(buddy, false).then((res__) => {
											}).catch((error) => {});
								 }).catch((error) => {});
					   }//endif

					 }
				}).catch((error) => {
		       //mobile # does not exists
					   //add invited_by setInvitedBy
						 this.requestservice.setInvitedBy(item.mobile);
		    });



				this.tpStorageService.set('isInvitedContact', this.isInviteArray);
			} else {
				/*
				let alert = this.alertCtrl.create({
					title: 'Message Send',
					subTitle: 'Error for sending message to ' + item.displayName + '.',
					buttons: ['Ok']
				});
				alert.present();
				*/
			}
		}).catch(err => {
			this.loadingProvider.dismissMyLoading();
			/*
			let alert = this.alertCtrl.create({
				title: 'Message Send',
				subTitle: 'Error for sending message to ' + item.displayName + '.',
				buttons: ['Ok']
			});
			alert.present();
			*/
		});
	}
	addnewContact(myEvent) {
		let profileModal = this.modalCtrl.create("AddContactPage");
		profileModal.present({
			ev: myEvent
		});
		profileModal.onDidDismiss((pages) => {
		})
	}
	goBack() {
		this.navCtrl.setRoot('TabsPage');
	}
}
