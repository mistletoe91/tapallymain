import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-request-submitted',
  templateUrl: 'request-submitted.html',
})
export class RequestSubmittedPage {
  showheader:boolean = true;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
  }
  gotoTabsPage (){
    this.navCtrl.setRoot("TabsPage");
  }
  gotoChatsPage() {
      this.navCtrl.setRoot("ReferralPage");
  }
  ionViewWillLeave() {
     this.showheader = false;
  }

}
