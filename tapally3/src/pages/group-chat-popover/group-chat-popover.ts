import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { GroupsProvider } from '../../providers/groups/groups';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';
@IonicPage()
@Component({
  selector: 'page-group-chat-popover',
  templateUrl: 'group-chat-popover.html',
})
export class GroupChatPopoverPage {
  groupName: any;
  groupMembers = [];
  CurrentUser;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private groupService: GroupsProvider,
    private modelCtrl: ModalController,
    public viewCtrl: ViewController,
    public tpStorageService: TpstorageProvider
  ) {
    this.tpStorageService.getItem('userUID').then((res: any) => {
			 if(res){
				 this.CurrentUser = res;
			 }
		}).catch(e => { });

    if (this.groupService.currentgroupname != undefined) {
      this.groupName = this.groupService.currentgroupname;
    } else {
      this.groupName = this.navParams.get('groupName');
      this.groupService.currentgroupname = this.groupName;
    }

  }

  searchMessage(myEvent) {
    let searchChatModal = this.modelCtrl.create('SearchGroupChatPage');
    searchChatModal.present({
      ev: myEvent
    });
    searchChatModal.onDidDismiss(() => {
      this.viewCtrl.dismiss();
    })
  }
}
