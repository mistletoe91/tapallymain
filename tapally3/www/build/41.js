webpackJsonp([41],{

/***/ 797:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeAfterINeedTodayPageModule", function() { return HomeAfterINeedTodayPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_after_i_need_today__ = __webpack_require__(871);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HomeAfterINeedTodayPageModule = (function () {
    function HomeAfterINeedTodayPageModule() {
    }
    HomeAfterINeedTodayPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__home_after_i_need_today__["a" /* HomeAfterINeedTodayPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home_after_i_need_today__["a" /* HomeAfterINeedTodayPage */]),
            ],
        })
    ], HomeAfterINeedTodayPageModule);
    return HomeAfterINeedTodayPageModule;
}());

//# sourceMappingURL=home-after-i-need-today.module.js.map

/***/ }),

/***/ 871:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeAfterINeedTodayPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_chat_chat__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_fcm_fcm__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_requests_requests__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_sqlite__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_const__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var HomeAfterINeedTodayPage = (function () {
    function HomeAfterINeedTodayPage(app, fcm, sqlite, loading, requestservice, fb, events, chatservice, userservice, navCtrl, tpStorageService, toastCtrl, navParams) {
        this.app = app;
        this.fcm = fcm;
        this.sqlite = sqlite;
        this.loading = loading;
        this.requestservice = requestservice;
        this.fb = fb;
        this.events = events;
        this.chatservice = chatservice;
        this.userservice = userservice;
        this.navCtrl = navCtrl;
        this.tpStorageService = tpStorageService;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.newmessage = '';
        this.isenabled = true;
        this.showheader = true;
        this.isData = false;
        this.selectCatId = navParams.get('cat_id');
        this.authForm = this.fb.group({
            'message': [null, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].required])]
        });
        this.message = this.authForm.controls['message'];
    }
    HomeAfterINeedTodayPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.requestservice.getmyfriends();
        this.myfriends = [];
        this.events.subscribe('friends', function () {
            _this.events.unsubscribe('friends');
            _this.myfriends = [];
            _this.myfriends = _this.requestservice.myfriends;
        });
    };
    HomeAfterINeedTodayPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    HomeAfterINeedTodayPage.prototype.broadcast = function () {
        this.cntCounter = 0;
        //console.log ("[this.myfriends.length]:"+this.myfriends.length);
        //console.log (this.myfriends);
        this.isenabled = false;
        if (this.myfriends.length > 0) {
            for (var key = 0; key < this.myfriends.length; key++) {
                this.addmessage(this.myfriends[key]);
            } //end for
        }
        else {
            //You have no friends
            this.loading.presentToast('It appears like your contact list is empty. Invite your friends and let`s get started');
            this.navCtrl.setRoot('TabsPage');
        }
    };
    HomeAfterINeedTodayPage.prototype.sendToNextPage = function () {
        this.cntCounter++;
        if (this.cntCounter >= this.myfriends.length) {
            this.cntCounter = 0;
            //this.navCtrl.setRoot("RequestSubmittedPage");//jaswinder
            this.app.getRootNav().push("RequestSubmittedPage");
        }
    };
    HomeAfterINeedTodayPage.prototype.addmessage = function (buddy) {
        var _this = this;
        this.newmessage = this.newmessage.trim();
        //Send Push notification : mytodo check if the buddy is online or not. if online then no need to send push notification
        var newMessage = 'You have received a message';
        this.fcm.sendNotification(buddy, newMessage, 'chatpage');
        //Send chat message
        var msgA = '';
        if (this.newmessage.length) {
            msgA = this.newmessage;
        }
        this.chatservice.addnewmessageBroadcast(msgA, 'message', buddy, this.selectCatId, false).then(function () {
            _this.newmessage = '';
            _this.updateFlagInDb();
        });
    };
    HomeAfterINeedTodayPage.prototype.updateFlagInDb = function () {
        var _this = this;
        this.sqlite.create({
            name: 'gr.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql(__WEBPACK_IMPORTED_MODULE_9__app_const__["a" /* CONST */].create_table_statement_key_val, {})
                .then(function (res) {
                db.executeSql("SELECT val FROM key_val where key like 'ever_referral_sent'", {})
                    .then(function (res) {
                    _this.tpStorageService.setItem('ever_referral_sent', "1");
                    if (res.rows.length > 0) {
                        //Record exists
                        //console.log ("ever_referral_sent record exists");
                        _this.sendToNextPage();
                    }
                    else {
                        //Record does not exist
                        //console.log ("ever_referral_sent record DOES NOT exists");
                        db.executeSql("INSERT INTO key_val(key,val) VALUES ('ever_referral_sent','1')", {}).then(function (res) {
                            _this.sendToNextPage();
                        }).catch(function (e) {
                            //it's ok . cutomer experience is important
                            _this.sendToNextPage();
                        });
                    } //endif
                })
                    .catch(function (e) {
                    _this.sendToNextPage();
                });
            })
                .catch(function (e) {
                _this.sendToNextPage();
            }); //create table
        }); //create database
    }; //end function
    HomeAfterINeedTodayPage.prototype.ionViewDidLoad = function () {
    };
    HomeAfterINeedTodayPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home-after-i-need-today',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\home-after-i-need-today\home-after-i-need-today.html"*/'<ion-header >\n  <ion-navbar color="primary" >\n    <ion-title>\n      Submit Request\n    </ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name=\'menu\'></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <div class="pull-center givepadding">\n        <h3>Get Trusted Referrals</h3>\n        <p>Ask for a referral from your friends, family and other known ones</p>\n    </div>\n\n\n    <ion-grid>\n      <ion-row>\n        <ion-col col-10>\n          <ion-textarea  [(ngModel)]="newmessage" [formControl]="message"  type="text"  rows="7" name="message" class="newmsg" placeholder="Request Details (Optional)" ></ion-textarea>\n        </ion-col>\n        <ion-col col-2>\n          <button [disabled]="!isenabled"  (click)="broadcast()" ion-button class="btnbroadcast"  color="primary"  >\n          <ion-icon  ios="md-send"  md="md-send">  </ion-icon>\n          </button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n\n\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\home-after-i-need-today\home-after-i-need-today.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */], __WEBPACK_IMPORTED_MODULE_4__providers_fcm_fcm__["a" /* FcmProvider */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_sqlite__["a" /* SQLite */], __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_5__providers_requests_requests__["a" /* RequestsProvider */], __WEBPACK_IMPORTED_MODULE_7__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */], __WEBPACK_IMPORTED_MODULE_2__providers_chat_chat__["a" /* ChatProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_10__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */]])
    ], HomeAfterINeedTodayPage);
    return HomeAfterINeedTodayPage;
}());

//# sourceMappingURL=home-after-i-need-today.js.map

/***/ })

});
//# sourceMappingURL=41.js.map