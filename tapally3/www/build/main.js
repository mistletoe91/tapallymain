webpackJsonp([74],{

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CONST; });
var CONST = {
    create_table_statement: 'CREATE TABLE IF NOT EXISTS cache_users_local (id INT, uid TEXT, user_name TEXT,user_email TEXT,business_name TEXT,paid INT, phone_number INT, catid INT , invited_by INT, lastupdated TIMESTAMP)',
    create_table_statement_tapally_contacts: 'CREATE TABLE IF NOT EXISTS cache_tapally_contacts (id TEXT, displayName TEXT,mobile TEXT,photoURL TEXT,isUser INT,mobile_formatted TEXT,uid TEXT)',
    create_table_statement_tapally_friends: 'CREATE TABLE IF NOT EXISTS cache_tapally_friends  (uid TEXT,gpflag INT, selection INT,isBlock INT, unreadmessage TEXT, displayName TEXT,mobile TEXT,photoURL TEXT ,lastMessage_message TEXT,lastMessage_type TEXT,lastMessage_dateofmsg TEXT,groupimage TEXT,groupName TEXT, lastmsg TEXT,dateofmsg TEXT,timeofmsg TEXT,isactive INT,selectCatId TEXT,referral_type TEXT,deviceToken TEXT)',
    create_table_statement_key_val: 'CREATE TABLE IF NOT EXISTS key_val (id TEXT, key TEXT,val TEXT)',
    create_table_buddychat: 'CREATE TABLE IF NOT EXISTS cache_buddychat (uid TEXT, key TEXT,val TEXT)'
};
//# sourceMappingURL=const.js.map

/***/ }),

/***/ 186:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoadingProvider = (function () {
    function LoadingProvider(toastCtrl, loadingCtrl) {
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
    }
    // For present loading
    LoadingProvider.prototype.presentLoading = function () {
        //https://medium.muz.li/top-30-most-captivating-preloaders-for-your-website-95ed1beff99d
        this.loading = this.loadingCtrl.create({
            content: 'Please wait, we are working hard on it'
        });
        this.loading.present().then(function () {
            //setTimeout(()=>{
            // this.loading.dismiss();
            //},300);
        });
    };
    //dismiss
    LoadingProvider.prototype.dismissMyLoading = function () {
        //console.log ("Trying dismiising loading"); 
        if (this.loading) {
            this.loading.dismiss();
            this.loading = null;
        }
    };
    // Toast message
    LoadingProvider.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    LoadingProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* LoadingController */]])
    ], LoadingProvider);
    return LoadingProvider;
}());

//# sourceMappingURL=loading.js.map

/***/ }),

/***/ 199:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 199;

/***/ }),

/***/ 241:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/add-contact/add-contact.module": [
		765,
		73
	],
	"../pages/allbuddy-contacts/allbuddy-contacts.module": [
		766,
		72
	],
	"../pages/allcontacts/allcontacts.module": [
		767,
		71
	],
	"../pages/allimg/allimg.module": [
		768,
		70
	],
	"../pages/attachments/attachments.module": [
		769,
		69
	],
	"../pages/block-user/block-user.module": [
		770,
		68
	],
	"../pages/broadcast-submitted/broadcast-submitted.module": [
		771,
		67
	],
	"../pages/broadcast/broadcast.module": [
		772,
		66
	],
	"../pages/buddies/buddies.module": [
		773,
		65
	],
	"../pages/buddychat/buddychat.module": [
		774,
		64
	],
	"../pages/businessinfo/businessinfo.module": [
		775,
		63
	],
	"../pages/businessinfoother/businessinfoother.module": [
		776,
		62
	],
	"../pages/chats/chats.module": [
		778,
		61
	],
	"../pages/checkreferralincentive/checkreferralincentive.module": [
		777,
		60
	],
	"../pages/consumerinfo/consumerinfo.module": [
		779,
		59
	],
	"../pages/contact/contact.module": [
		780,
		58
	],
	"../pages/contactimportprogress/contactimportprogress.module": [
		781,
		57
	],
	"../pages/country-list/country-list.module": [
		782,
		56
	],
	"../pages/disc/disc.module": [
		783,
		55
	],
	"../pages/displayname/displayname.module": [
		784,
		54
	],
	"../pages/earnings/earnings.module": [
		785,
		53
	],
	"../pages/extra/extra.module": [
		786,
		52
	],
	"../pages/friend-asking-referral/friend-asking-referral.module": [
		787,
		51
	],
	"../pages/friend-list/friend-list.module": [
		788,
		50
	],
	"../pages/gpattachments/gpattachments.module": [
		789,
		49
	],
	"../pages/group-chat-popover/group-chat-popover.module": [
		790,
		48
	],
	"../pages/groupbuddies/groupbuddies.module": [
		791,
		47
	],
	"../pages/groupbuddiesadd/groupbuddiesadd.module": [
		792,
		46
	],
	"../pages/groupchat/groupchat.module": [
		793,
		45
	],
	"../pages/groupinfo/groupinfo.module": [
		794,
		44
	],
	"../pages/groupmembers/groupmembers.module": [
		795,
		43
	],
	"../pages/groups/groups.module": [
		796,
		42
	],
	"../pages/home-after-i-need-today/home-after-i-need-today.module": [
		797,
		41
	],
	"../pages/home/home.module": [
		798,
		40
	],
	"../pages/howcustomerseeadd/howcustomerseeadd.module": [
		799,
		39
	],
	"../pages/importp/importp.module": [
		800,
		38
	],
	"../pages/incentivedetail/incentivedetail.module": [
		801,
		37
	],
	"../pages/invite-friend/invite-friend.module": [
		802,
		36
	],
	"../pages/invitemanually/invitemanually.module": [
		803,
		35
	],
	"../pages/invitepeople/invitepeople.module": [
		804,
		34
	],
	"../pages/map/map.module": [
		805,
		33
	],
	"../pages/menu-block/menu-block.module": [
		806,
		32
	],
	"../pages/menu/menu.module": [
		807,
		31
	],
	"../pages/notification/notification.module": [
		808,
		30
	],
	"../pages/outreach-for-business/outreach-for-business.module": [
		809,
		29
	],
	"../pages/phone/phone.module": [
		810,
		28
	],
	"../pages/phoneverfy/phoneverfy.module": [
		811,
		27
	],
	"../pages/pick-contact-from/pick-contact-from.module": [
		812,
		26
	],
	"../pages/pick-contact-to/pick-contact-to.module": [
		813,
		25
	],
	"../pages/popover-chat/popover-chat.module": [
		814,
		24
	],
	"../pages/profile-view/profile-view.module": [
		815,
		23
	],
	"../pages/profilepic/profilepic.module": [
		816,
		22
	],
	"../pages/referral-infopage/referral-infopage.module": [
		817,
		21
	],
	"../pages/referral/referral.module": [
		818,
		20
	],
	"../pages/referralreceived/referralreceived.module": [
		819,
		19
	],
	"../pages/registerbusiness/registerbusiness.module": [
		820,
		18
	],
	"../pages/request-after-selected/request-after-selected.module": [
		821,
		17
	],
	"../pages/request-status/request-status.module": [
		822,
		16
	],
	"../pages/request-submitted/request-submitted.module": [
		823,
		15
	],
	"../pages/request/request.module": [
		824,
		14
	],
	"../pages/search-chats/search-chats.module": [
		825,
		13
	],
	"../pages/search-group-chat/search-group-chat.module": [
		826,
		12
	],
	"../pages/search-next-send/search-next-send.module": [
		827,
		11
	],
	"../pages/search-next/search-next.module": [
		828,
		10
	],
	"../pages/send-referral-backward/send-referral-backward.module": [
		829,
		9
	],
	"../pages/send-referral-filter/send-referral-filter.module": [
		830,
		8
	],
	"../pages/send-referral-forward/send-referral-forward.module": [
		831,
		7
	],
	"../pages/setting/setting.module": [
		832,
		6
	],
	"../pages/show-contact/show-contact.module": [
		833,
		5
	],
	"../pages/tabs/tabs.module": [
		834,
		4
	],
	"../pages/tutorial/tutorial.module": [
		835,
		0
	],
	"../pages/username/username.module": [
		836,
		3
	],
	"../pages/view-buddy/view-buddy.module": [
		837,
		2
	],
	"../pages/viewandredeem/viewandredeem.module": [
		838,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 241;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 32:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TpstorageProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_native_storage__ = __webpack_require__(256);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TpstorageProvider = (function () {
    /*
    storage
    localStorage,
    nativeStorage
    */
    function TpstorageProvider(nativeStorage, platform) {
        this.nativeStorage = nativeStorage;
        this.platform = platform;
    }
    TpstorageProvider.prototype.clear = function () {
        this.nativeStorage.clear();
    };
    TpstorageProvider.prototype.isArray = function (obj) {
        return Array.isArray(obj);
    };
    TpstorageProvider.prototype.removeItem = function (nam) {
        this.setItem(nam, "");
    };
    TpstorageProvider.prototype.set = function (nam, val) {
        if (this.isArray(val)) {
            val = JSON.stringify(val);
        }
        return this.setItem(nam, val);
    };
    TpstorageProvider.prototype.get = function (nam) {
        return this.getItem(nam);
    };
    TpstorageProvider.prototype.setItem = function (nam, val) {
        if (this.isArray(val)) {
            val = JSON.stringify(val);
        }
        this.nativeStorage.setItem(nam, val).then(function () { }, function (error) { });
    };
    TpstorageProvider.prototype.getItem = function (nam) {
        return this.nativeStorage.getItem(nam);
    };
    TpstorageProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_native_storage__["a" /* NativeStorage */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["x" /* Platform */]])
    ], TpstorageProvider);
    return TpstorageProvider;
}());

//# sourceMappingURL=tpstorage.js.map

/***/ }),

/***/ 403:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_firebase__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var RequestsProvider = (function () {
    function RequestsProvider(userservice, events, storage, platform, tpStorageService) {
        var _this = this;
        this.userservice = userservice;
        this.events = events;
        this.storage = storage;
        this.platform = platform;
        this.tpStorageService = tpStorageService;
        this.firebuddychats = __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.database().ref('/buddychats');
        this.firedata = __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.database().ref('/chatusers');
        this.firereq = __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.database().ref('/requests');
        this.firefriends = __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.database().ref('/friends');
        this.fireInvitationSent = __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.database().ref('/invitationsent');
        this.firePhones = __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.database().ref('/phones');
        //new_chat_message_flag:boolean = false;
        this.lastMsgReader = {};
        this.blockRequest = [];
        this.subscriptionAddedObj = {};
        this.subscriptionAddedFriendsObj = {};
        this.platform.ready().then(function (readySource) {
            ////console.log ("RequestsProvider constructor");
            _this.tpStorageService.getItem('userUID').then(function (userID) {
                ////console.log ("Getting userUIDuserUIDuserUIDuserUIDuserUIDuserUID");
                if (userID != undefined) {
                    ////console.log ("SETTING this.userId +++++++++++++++++++++++");
                    _this.userId = userID;
                }
                else {
                    //////console.log ("This is culprit");
                    //////console.log (firebase.auth().currentUser);
                    ////console.log ("SETTING this.userId ++++++++++--+++++++++++++");
                    _this.userId = __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.auth().currentUser.uid;
                }
            }).catch(function (e) {
            });
        });
    }
    /*
    Sample : of "requests"
    
    {
      "1bqhG3I2U5e6tMzkhTrIw8Ehozb2" : {
        "-Ld5sBkuU-eXYZ_LfbQS" : {
          "displayName" : "Kaztaaa",
          "isBlock" : false,
          "phone": "6471111212",
          "photoURL" : "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F0.png?alt=media&token=2c3594e5-a0ce-42a8-83af-c98a7a7827b9",
          "sender" : "46u6XlnVuIeBnofPrxkUZ8sRxem1"
        }
      }
    }
    
    
    */
    RequestsProvider.prototype.checkPhones = function (phoneNumber) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firePhones.child(phoneNumber).once('value', function (snapshot) {
                resolve(snapshot.val());
            }).catch(function (err) {
                reject(err);
            });
        }).catch(function (error) {
            //console.log ("Error 3652");//console.log (error);
        });
    };
    RequestsProvider.prototype.sendrequest = function (req) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.userservice.getuserdetails().then(function (myDetails) {
                _this.firereq.child(req.recipient).push({
                    sender: req.sender,
                    isBlock: false,
                    phone: myDetails.phone,
                    mobile: myDetails.phone,
                    displayName: myDetails.displayName,
                    photoURL: myDetails.photoURL
                }).then(function () {
                    resolve({ success: true });
                });
            });
        }).catch(function (error) {
            //console.log ("Error 6422");//console.log (error);
        });
        return promise;
    };
    RequestsProvider.prototype.setInvitedBy = function (phone) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firePhones.child(phone).set({
                invited_by: _this.userId,
                timestamp: __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.database.ServerValue.TIMESTAMP
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                resolve(false);
            });
        }).catch(function (error) {
            //console.log ("Error 8426");//console.log (error);
        });
    };
    RequestsProvider.prototype.logInvitation = function (phone) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.fireInvitationSent.child(_this.userId).child(phone).set({
                timestamp: __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.database.ServerValue.TIMESTAMP
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                resolve(false);
            });
        }).catch(function (error) {
            //console.log ("Error 8456");//console.log (error);
        });
    };
    RequestsProvider.prototype.getmyrequests_promise = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.userId = userId__;
                _this.firereq.child(_this.userId).once('value', function (snapshot) {
                    //check if user not blocked
                    _this.userdetails = [];
                    var myrequests = snapshot.val();
                    for (var j in myrequests) {
                        if (!myrequests[j].isBlock) {
                            _this.userdetails.push(myrequests[j]);
                        }
                    } //end for
                    resolve(_this.userdetails);
                });
            }).catch(function (e) { });
        }).catch(function (error) {
            //console.log ("Error 6234");//console.log (error)
        });
        return promise;
    }; //end function
    RequestsProvider.prototype.getmyrequests = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (userId__) {
            _this.userId = userId__;
            _this.getmyrequests_();
        }).catch(function (e) { });
    };
    RequestsProvider.prototype.getmyrequests_ = function () {
        var _this = this;
        var allmyrequests;
        var myrequests = [];
        //////console.log ("GET MY REQUESTS");
        //////console.log (this.userId);
        this.firereq.child(this.userId).once('value', function (snapshot) {
            //////console.log ("Found requests");
            //////console.log (snapshot.val());
            allmyrequests = snapshot.val();
            myrequests = [];
            for (var i in allmyrequests) {
                myrequests.push(allmyrequests[i]);
            }
            //Check if user is not blocked by this user
            _this.userdetails = [];
            for (var j in myrequests) {
                if (!myrequests[j].isBlock) {
                    _this.userdetails.push(myrequests[j]);
                }
            }
            ////console.log ("<<<>>>>");
            ////console.log (this.userdetails);
            _this.events.publish('gotrequests');
            /*
            this.userservice.getallusers_modified(myrequests).then((res) => {
              var allusers = res;
              this.userdetails = [];
              for (var j in myrequests)
                for (var key in allusers) {
                  if (myrequests[j].sender === allusers[key].uid && myrequests[j].isBlock == false) {
                    this.userdetails.push(allusers[key]);
                  }
                }
              this.events.publish('gotrequests');
            })
            */
        });
    };
    //duplicate function in userst.ts  but not entirly same 
    RequestsProvider.prototype.acceptrequest = function (buddy, deleteRequest) {
        var _this = this;
        if (deleteRequest === void 0) { deleteRequest = true; }
        var promise = new Promise(function (resolve, reject) {
            _this.userservice.getuserdetails().then(function (aboutMe) {
                _this.firefriends.child(_this.userId).child(buddy.uid).set({
                    uid: buddy.uid,
                    displayName: buddy.displayName,
                    photoURL: buddy.photoURL,
                    isActive: 1,
                    isBlock: false,
                    deviceToken: buddy.deviceToken,
                    countryCode: buddy.countryCode,
                    disc: buddy.disc,
                    mobile: buddy.mobile,
                    timestamp: buddy.timestamp
                }).then(function () {
                    _this.firefriends.child(buddy.uid).child(_this.userId).set({
                        uid: _this.userId,
                        displayName: aboutMe.displayName,
                        photoURL: aboutMe.photoURL,
                        isActive: 1,
                        isBlock: false,
                        deviceToken: aboutMe.deviceToken,
                        countryCode: aboutMe.countryCode,
                        disc: aboutMe.disc,
                        mobile: aboutMe.mobile,
                        timestamp: aboutMe.timestamp
                    }).then(function () {
                        if (deleteRequest) {
                            _this.deleterequest(buddy).then(function () {
                                resolve(true);
                            }).catch(function (err) {
                                ////console.log ("Error 3259");
                                reject(err);
                            });
                        } //endif
                    });
                });
            });
        }).catch(function (error) {
            //console.log ("Error 4563");//console.log (error)
        });
        return promise;
    }; //end function
    RequestsProvider.prototype.deleterequest = function (buddy) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firereq.child(_this.userId).orderByChild('sender').equalTo(buddy.uid).once('value', function (snapshot) {
                var somekey;
                for (var key in snapshot.val())
                    somekey = key;
                _this.firereq.child(_this.userId).child(somekey).remove().then(function () {
                    resolve(true);
                });
            })
                .then(function () {
            }).catch(function (err) {
                reject(err);
            });
        }).catch(function (error) {
            //console.log ("Error 1243");//console.log (error);
        });
        return promise;
    };
    RequestsProvider.prototype.getmyfriends = function () {
        var _this = this;
        console.log("getmyfriends ()");
        this.tpStorageService.getItem('userUID').then(function (userId__) {
            _this.userId = userId__;
            ////console.log ("LLLove ");
            _this.getmyfriends_();
        }).catch(function (e) { });
    };
    RequestsProvider.prototype.getmyfriends_ = function () {
        var _this = this;
        this.firefriends.child(this.userId).on('value', function (snapshot) {
            _this.myfriends = [];
            console.log("FIREFRIEND EVENT TRIGGERED");
            console.log(snapshot.val());
            var allfriends = snapshot.val();
            if (allfriends != null) {
                // I have some friends
                //console.log ("I have friends " );
                var CounterFriends_1 = 0;
                for (var i in allfriends) {
                    //console.log ("iiii ")     ;
                    var lastMsgActual__ = {
                        message: "",
                        type: "",
                        dateofmsg: "",
                        timeofmsg: "",
                        isRead: "",
                        isStarred: "",
                        referral_type: "",
                        selectCatId: ""
                    };
                    if (!allfriends[i].isBlock) {
                        allfriends[i].isBlock = false;
                    }
                    allfriends[i].unreadmessage = lastMsgActual__;
                    allfriends[i].lastMessage = lastMsgActual__;
                    _this.myfriends.push(allfriends[i]);
                    CounterFriends_1++;
                } //end for
                ////console.log ("this.cnter "+ this.cnter);
                console.log("Now Check last message one by one for each friend. CounterFriends_:" + CounterFriends_1);
                var friendsSoFar_1 = 0;
                var _loop_1 = function (y) {
                    if (true) {
                        _this.firebuddychats.child(_this.userId).child(_this.myfriends[y].uid).limitToLast(1).once('value', function (snapshot_last_msg) {
                            var lastMsg__ = snapshot_last_msg.val();
                            if (lastMsg__) {
                                for (var key in lastMsg__) {
                                    var lastMsgActual__ = {
                                        message: lastMsg__[key].message,
                                        type: lastMsg__[key].type,
                                        dateofmsg: lastMsg__[key].dateofmsg,
                                        timeofmsg: lastMsg__[key].timeofmsg,
                                        referral_type: lastMsg__[key].referral_type,
                                        selectCatId: lastMsg__[key].selectCatId,
                                        isRead: false,
                                        isStarred: false
                                    };
                                    _this.myfriends[y].unreadmessage = lastMsgActual__;
                                    _this.myfriends[y].lastMessage = lastMsgActual__;
                                    break;
                                } //end for
                            } //endif
                            if (++friendsSoFar_1 >= CounterFriends_1) {
                                //Make sure it run only on last one and only once
                                console.log("Publish Friends Event in Chat.ts ");
                                _this.events.publish('friends'); //jaswinder
                            }
                        });
                        _this.subscriptionAddedFriendsObj[_this.userId + "/" + _this.myfriends[y].uid] = true;
                    }
                    else {
                        //this friend has already been subscripted
                    } //endif
                };
                for (var y in _this.myfriends) {
                    _loop_1(y);
                } //end for
            }
            else {
                // I dont have any friend (:
                //if(null == this.subscriptionAddedFriendsObj [this.userId + "/" + this.myfriends[y].uid]) {
                _this.events.publish('friends');
                //  this.subscriptionAddedFriendsObj [this.userId + "/" + this.myfriends[y].uid] = true;
                //  }//endif
            } //endif
        }); //get all friends
    };
    RequestsProvider.prototype.subscribeToReadingLastMsg = function () {
        var _this = this;
        this.lastMsgReader = {};
        console.log("subscribeToReadingLastMsg");
        var _loop_2 = function (y) {
            console.log("Friend " + y);
            if (null == this_1.subscriptionAddedObj[this_1.userId + "/" + this_1.myfriends[y].uid]) {
                console.log("->Add Subscription To : " + this_1.userId + "/" + this_1.myfriends[y].uid);
                this_1.firebuddychats.child(this_1.userId).child(this_1.myfriends[y].uid).limitToLast(1).on('child_added', function (snapshot_last_msg) {
                    console.log("Inside firebuddychats ()");
                    var lastMsg__ = snapshot_last_msg.val();
                    console.log(lastMsg__);
                    //if(lastMsg__){
                    _this.lastMsgReader[_this.myfriends[y].uid] = ({
                        message: lastMsg__.message,
                        type: lastMsg__.type,
                        sentby: lastMsg__.sentby,
                        dateofmsg: lastMsg__.dateofmsg,
                        timeofmsg: lastMsg__.timeofmsg,
                        request_to_release_incentive: lastMsg__.request_to_release_incentive,
                        isRead: false,
                        isStarred: false
                    });
                    //if(!this.new_chat_message_flag){
                    _this.events.publish('new_chat_message');
                    console.log("Publishing event new_chat_message");
                    //this.new_chat_message_flag = true;
                    //}
                    //}//endif
                });
                this_1.subscriptionAddedObj[this_1.userId + "/" + this_1.myfriends[y].uid] = true;
            } //endif
        };
        var this_1 = this;
        for (var y in this.myfriends) {
            _loop_2(y);
        } //end for
    };
    /*
      getmyfriends_NOTEUSED() {
        let friendsuid = [];
        this.firefriends.child(this.userId).once('value', (snapshot) => {
          let allfriends = snapshot.val();
          if (allfriends != null) {
            for (var i in allfriends) {
              friendsuid.push(allfriends[i]);
            }
          }
    
          this.userservice.getallusers_modified(friendsuid).then((users) => {
            this.myfriends = [];
            let counter = 0;
            if (friendsuid.length > 0) {
              for (var j in friendsuid) {
                for (var key in users) {
                  if (friendsuid[j].uid === users[key].uid) {
                    users[key].isActive = friendsuid[j].isActive;
                    users[key].userprio = friendsuid[j].userprio ? friendsuid[j].userprio : null;
                    let userKey;
                    userKey = users[key];
                    let flag1 = false;
                    let flag2 = false;
                    let flag3 = false;
                    this.userservice.getuserblock(users[key]).then((res) => {
                      userKey.isBlock = res;
                      flag1 = true;
                    })
                    this.get_last_unreadmessage(users[key]).then(responce => {
                      userKey.unreadmessage = responce;
                      flag2 = true;
                    })
                    this.get_last_msg(users[key]).then(resp => {
                      userKey.lastMessage = resp;
                      flag3 = true;
                      this.myfriends.push(userKey);
                      counter++;
                      if (counter == friendsuid.length) {
                      }
                    })
                  }
                }
              }
            } else {
              if (friendsuid.length == 0) {
              }
            }
          }).catch((err) => {
          })
    
        })
      }
    */
    RequestsProvider.prototype.get_last_msg = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firebuddychats.child(_this.userId).child(buddy.uid).orderByChild('uid').once('value', function (snapshot) {
                var allmsg = snapshot.val();
                if (allmsg != null) {
                    var lastmsg = allmsg[Object.keys(allmsg)[Object.keys(allmsg).length - 1]];
                    resolve(lastmsg);
                }
                else {
                    resolve('');
                }
            });
        }).catch(function (error) {
            //console.log ("Error 7423");//console.log (error);
        });
    };
    RequestsProvider.prototype.get_last_unreadmessage = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var allunreadmessage = [];
            _this.firebuddychats.child(buddy.uid).child(_this.userId).orderByChild('uid').on('value', function (snapshot) {
                var allmsg = snapshot.val();
                if (allmsg != null) {
                    for (var tmpkey in allmsg) {
                        if (allmsg[tmpkey].isRead == false) {
                            allunreadmessage.push(allmsg);
                        }
                    }
                }
                if (allunreadmessage.length > 0) {
                    resolve(allunreadmessage.length);
                }
                else {
                    resolve(allunreadmessage.length);
                }
            });
        }).catch(function (error) {
            //console.log ("Error 7842");//console.log (error);
        });
    };
    RequestsProvider.prototype.pendingetmy__requests = function (userd) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firereq.child(userd).once('value', function (snapshot) {
                resolve(snapshot.val());
            });
        }).catch(function (error) {
            //console.log ("Error 7234");//console.log (error);
        });
    };
    RequestsProvider.prototype.pendingetmyrequests = function (userd) {
        ////console.log ("pendingetmyrequests");
        ////console.log (userd);
        ////console.log (this.userId);
        ////console.log ("pendingetmyrequests END");
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.userId = userId__;
                if (!userd || userd == undefined) {
                    userd = _this.userId;
                }
                var allmyrequests;
                var myrequests = [];
                _this.firereq.child(userd).once('value', function (snapshot) {
                    allmyrequests = snapshot.val();
                    myrequests = [];
                    for (var i in allmyrequests) {
                        myrequests.push(allmyrequests[i].sender);
                    }
                    _this.userservice.getallusers_modified(myrequests).then(function (allusers) {
                        var pendinguserdetails = [];
                        for (var j in myrequests)
                            for (var key in allusers) {
                                if (myrequests[j] == allusers[key].uid) {
                                    pendinguserdetails.push(allusers[key]);
                                }
                            }
                        resolve(pendinguserdetails);
                    });
                });
            }).catch(function (e) { });
        }).catch(function (error) {
            //console.log ("Error 1652");//console.log (error);
        });
    };
    RequestsProvider.prototype.blockuserprof = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firereq.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var requestFriends = snapshot.val();
                for (var tmpkey in requestFriends) {
                    if (requestFriends[tmpkey].sender == buddy.uid) {
                        _this.firereq.child(_this.userId).child(tmpkey).update({ isBlock: true });
                        resolve(true);
                    }
                }
            });
        }).catch(function (error) {
            //console.log ("Error 9645");//console.log (error);
        });
    };
    RequestsProvider.prototype.getAllBlockRequest = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (userId__) {
            _this.userId = userId__;
            _this.getAllBlockRequest_();
        }).catch(function (e) { });
    };
    RequestsProvider.prototype.getAllBlockRequest_ = function () {
        var _this = this;
        this.firereq.child(this.userId).orderByChild('uid').once('value', function (snapshot) {
            var allrequest = snapshot.val();
            var blockuser = [];
            for (var key in allrequest) {
                if (allrequest[key].isBlock) {
                    _this.firedata.child(allrequest[key].sender).once('value', function (snapsho) {
                        blockuser.push(snapsho.val());
                    }).catch(function (err) {
                    });
                }
                _this.blockRequest = [];
                _this.blockRequest = blockuser;
            }
            _this.events.publish('block-request');
        });
    };
    RequestsProvider.prototype.unblockRequest = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firereq.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                for (var key in allfriends) {
                    if (allfriends[key].sender == buddy.uid) {
                        _this.firereq.child(_this.userId).child(key).update({ isBlock: false });
                        resolve(true);
                    }
                }
            });
        }).catch(function (error) {
            //console.log ("Error 1745");//console.log (error);
        });
    };
    RequestsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_6__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], RequestsProvider);
    return RequestsProvider;
}());

//# sourceMappingURL=requests.js.map

/***/ }),

/***/ 404:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__fcm_fcm__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var GroupsProvider = (function () {
    function GroupsProvider(http, events, userservice, fcm, tpStorageService) {
        var _this = this;
        this.http = http;
        this.events = events;
        this.userservice = userservice;
        this.fcm = fcm;
        this.tpStorageService = tpStorageService;
        this.firedata = __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database().ref('/chatusers');
        this.firegroup = __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database().ref('/groups');
        this.fireuserStatus = __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database().ref('/userstatus');
        this.mygroups = [];
        this.currentgroup = [];
        this.flag = 0;
        this.groupmemeberStatus = [];
        this.tpStorageService.getItem('userUID').then(function (res) {
            if (res) {
                _this.userId = res;
            }
            else {
                _this.userId = __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.auth().currentUser.uid;
            }
        }).catch(function (e) { });
    }
    GroupsProvider.prototype.formatAMPM = function (date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    };
    GroupsProvider.prototype.formatDate = function (date) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        if (dd < 10)
            dd = '0' + dd;
        if (mm < 10)
            mm = '0' + mm;
        return dd + '/' + mm + '/' + yyyy;
    };
    GroupsProvider.prototype.getmygroups = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (userId__) {
            _this.userId = userId__;
            _this.firegroup.child(_this.userId).once('value', function (snapshot) {
                _this.mygroups = [];
                if (snapshot.val() != null) {
                    var temp = snapshot.val();
                    for (var key in temp) {
                        var newgroup = {
                            groupName: key,
                            groupimage: temp[key].groupimage,
                            timestamp: temp[key].timestamp,
                            userprio: temp[key].userprio ? temp[key].userprio : null,
                            lastmsg: temp[key].lastmsg,
                            dateofmsg: temp[key].dateofmsg,
                            timeofmsg: temp[key].timeofmsg,
                        };
                        _this.mygroups.push(newgroup);
                    }
                }
                _this.events.publish('allmygroups');
            });
        }).catch(function (e) { });
    };
    GroupsProvider.prototype.deletegroup = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.userId = userId__;
                _this.firegroup.child(_this.userId).child(_this.currentgroupname).child('members').once('value', function (snapshot) {
                    var tempmembers = snapshot.val();
                    for (var key in tempmembers) {
                        _this.firegroup.child(tempmembers[key].uid).child(_this.currentgroupname).remove();
                    }
                    _this.firegroup.child(_this.userId).child(_this.currentgroupname).remove().then(function () {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            }).catch(function (e) { });
        });
    };
    GroupsProvider.prototype.addMeaasge = function (newmessage, loginUser, tempowner, type, groupname) {
        var _this = this;
        var date = this.formatDate(new Date());
        var timeofmsg = this.formatAMPM(new Date());
        return new Promise(function (resolve) {
            _this.firegroup.child(tempowner).child(groupname).child('msgboard').push({
                sentby: _this.userId,
                displayName: loginUser.displayName,
                photoURL: loginUser.photoURL,
                message: newmessage,
                timestamp: __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database.ServerValue.TIMESTAMP,
                multiMessage: false,
                type: type,
                messageId: '',
                timeofmsg: timeofmsg,
                dateofmsg: date,
                userprio: '',
            }).then(function (res) {
                _this.firegroup.child(tempowner).child(groupname).child('msgboard').child(res.key).update({ messageId: res.key, userprio: new Date() });
                var notifyMessage = 'You have received a message in ' + groupname + ' by ' + loginUser.displayName;
                console.log("this.userIdthis.userIdthis.userIdthis.userIdthis.userId");
                console.log(_this.groupmemeberStatus[tempowner]);
                console.log(":" + _this.userId);
                if (_this.groupmemeberStatus[tempowner].uid != _this.userId) {
                    //if (this.groupmemeberStatus[tempowner].status == 'offline') {
                    _this.userservice.getbuddydetails(tempowner).then(function (res) {
                        var buddydata = res;
                        _this.fcm.sendNotification(buddydata, notifyMessage, 'Grouppage');
                    });
                    //  }
                }
                resolve(true);
            });
        });
    };
    GroupsProvider.prototype.addMultiMeaasge = function (newmessage, loginUser, tempowner, type, cgroupname) {
        var _this = this;
        var timeofmsg = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        return new Promise(function (resolve) {
            _this.firegroup.child(tempowner).child(cgroupname).child('msgboard').push({
                sentby: _this.userId,
                displayName: loginUser.displayName,
                photoURL: loginUser.photoURL,
                message: newmessage,
                messageId: '',
                timestamp: __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database.ServerValue.TIMESTAMP,
                multiMessage: true,
                type: type,
                timeofmsg: timeofmsg,
                dateofmsg: date,
                userprio: '',
            }).then(function (res) {
                _this.firegroup.child(tempowner).child(cgroupname).child('msgboard').child(res.key).update({ messageId: res.key, userprio: new Date() });
                var notifyMessage = 'You have received a message in ' + _this.currentgroupname + ' by ' + loginUser.displayName;
                if (_this.groupmemeberStatus[tempowner].uid != _this.userId) {
                    if (_this.groupmemeberStatus[tempowner].status == 'offline') {
                        _this.userservice.getbuddydetails(tempowner).then(function (res) {
                            var buddydata = res;
                            _this.fcm.sendNotification(buddydata, notifyMessage, 'Grouppage');
                        });
                    }
                }
                resolve(true);
            });
        });
    };
    GroupsProvider.prototype.addgroupmsg = function (newmessage, loginUser, type, groupname) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        var cgroupname = '';
        if (this.currentgroupname == undefined || this.currentgroupname == null) {
            cgroupname = groupname;
        }
        else {
            cgroupname = this.currentgroupname;
        }
        return new Promise(function (resolve) {
            _this.firegroup.child(_this.userId).child(cgroupname).child('owner').once('value', function (snapshots) {
                var own = _this.converanobj(snapshots.val());
                _this.firegroup.child(own[0].uid).child(cgroupname).child('members').once('value', function (snapshot) {
                    var tempowners = _this.converanobj(snapshot.val());
                    var flag = 0;
                    var _loop_1 = function (i) {
                        _this.addMeaasge(newmessage, loginUser, tempowners[i].uid, type, cgroupname).then(function (res) {
                            var msgtype = type;
                            var msg = newmessage;
                            var lastmsg_ = msg;
                            if (msgtype == 'message') {
                            }
                            else {
                                lastmsg_ = msgtype;
                            }
                            _this.firegroup.child(tempowners[i].uid).child(cgroupname).update({
                                lastmsg: lastmsg_,
                                userprio: new Date(),
                                dateofmsg: date,
                                timeofmsg: time
                            });
                            flag++;
                            if (tempowners.length == flag) {
                                resolve(true);
                            }
                        });
                    };
                    for (var i = 0; i < tempowners.length; i++) {
                        _loop_1(i);
                    }
                });
            });
        });
    };
    GroupsProvider.prototype.deleteMessages = function (item) {
        var _this = this;
        var cgroupname = this.currentgroupname;
        return new Promise(function (resolve, reject) {
            for (var i = 0; i < item.length; i++) {
                _this.firegroup.child(_this.userId).child(cgroupname).child('msgboard').child(item[i].messageId).remove().then(function (res) {
                    _this.firegroup.child(_this.userId).child(cgroupname).child('msgboard').once('value', function (snapshot) {
                        var tempData = _this.converanobj(snapshot.val());
                        if (tempData.length > 0) {
                            var lastMsg = tempData[tempData.length - 1];
                            _this.firegroup.child(_this.userId).child(cgroupname).update({
                                timeofmsg: lastMsg.timeofmsg,
                                timestamp: lastMsg.timestamp,
                                userprio: lastMsg.userprio,
                                dateofmsg: lastMsg.dateofmsg,
                            });
                            if (lastMsg.type == 'message') {
                                _this.firegroup.child(_this.userId).child(cgroupname).update({ lastmsg: lastMsg.message });
                            }
                            else {
                                _this.firegroup.child(_this.userId).child(cgroupname).update({ lastmsg: lastMsg.type });
                            }
                        }
                        else {
                            _this.firegroup.child(_this.userId).child(cgroupname).update({
                                timeofmsg: '',
                                timestamp: '',
                                userprio: '',
                                lastmsg: '',
                                dateofmsg: '',
                            });
                        }
                    }).then(function (res) {
                        resolve(true);
                    }, function (err) {
                        reject(err);
                    });
                }, function (err) {
                    reject(err);
                });
            }
        });
    };
    GroupsProvider.prototype.addgroupmsgmultiple = function (newmessage, loginUser, type, groupname) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        var cgroupname = '';
        if (this.currentgroupname == undefined || this.currentgroupname == null) {
            cgroupname = groupname;
        }
        else {
            cgroupname = this.currentgroupname;
        }
        return new Promise(function (resolve) {
            _this.firegroup.child(_this.userId).child(cgroupname).child('owner').once('value', function (snapshots) {
                var own = _this.converanobj(snapshots.val());
                _this.firegroup.child(_this.userId).child(cgroupname).child('msgboard').once('value', function (snapshots) {
                });
                _this.firegroup.child(own[0].uid).child(cgroupname).child('members').once('value', function (snapshot) {
                    var tempowners = _this.converanobj(snapshot.val());
                    var flag = 0;
                    for (var i = 0; i < tempowners.length; i++) {
                        _this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ userprio: new Date() });
                        _this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ dateofmsg: date });
                        _this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ timeofmsg: time });
                        if (type == 'message') {
                            _this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ lastmsg: newmessage });
                        }
                        else {
                            _this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ lastmsg: type });
                        }
                        _this.addMultiMeaasge(newmessage, loginUser, tempowners[i].uid, type, cgroupname).then(function (res) {
                            if (res) {
                                flag++;
                            }
                            if (tempowners.length == flag) {
                                resolve(true);
                            }
                        });
                    }
                });
            });
        });
    };
    GroupsProvider.prototype.postmsgs = function (member, msg, cb, loginUser) {
        this.firegroup.child(member.uid).child(this.currentgroupname).child('msgboard').push({
            sentby: this.userId,
            displayName: loginUser.displayName,
            photoURL: loginUser.photoURL,
            message: msg,
            timestamp: __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database.ServerValue.TIMESTAMP
        }).then(function () {
            cb();
        });
    };
    GroupsProvider.prototype.converanobj = function (obj) {
        var tmp = [];
        for (var key in obj) {
            tmp.push(obj[key]);
        }
        return tmp;
    };
    GroupsProvider.prototype.getownership = function (groupname) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.userId = userId__;
                _this.firegroup.child(_this.userId).child(groupname).once('value', function (snapshot) {
                    var temp = _this.converanobj(snapshot.val().owner);
                    if (temp.length > 0) {
                        var flag = 0;
                        for (var i = 0; i < temp.length; i++) {
                            if (temp[i].uid == _this.userId) {
                                flag = 1;
                                break;
                            }
                        }
                        if (flag == 1) {
                            resolve(true);
                        }
                        else {
                            resolve(false);
                        }
                    }
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (e) { });
        });
        return promise;
    };
    GroupsProvider.prototype.getgroupimage = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.userId = userId__;
                _this.firegroup.child(_this.userId).child(_this.currentgroupname).once('value', function (snapshot) {
                    if (snapshot.val() != null) {
                        _this.grouppic = snapshot.val().groupimage;
                        resolve(true);
                    }
                    else {
                        resolve(true);
                    }
                });
            }).catch(function (e) { });
        });
    };
    GroupsProvider.prototype.getintogroup = function (groupname) {
        var _this = this;
        if (groupname != null) {
            this.currentgroupname = groupname;
            this.firegroup.child(this.userId).child(groupname).once('value', function (snapshot) {
                if (snapshot.val() != null) {
                    var temp = snapshot.val().members;
                    _this.currentgroup = [];
                    for (var key in temp) {
                        _this.currentgroup.push(temp[key]);
                    }
                    _this.currentgroupname = groupname;
                    _this.events.publish('gotintogroup');
                }
            });
        }
        else {
            this.currentgroupname = undefined;
        }
    };
    GroupsProvider.prototype.getgroupmember = function (gpname) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.userId = userId__;
                _this.firegroup.child(_this.userId).child(gpname).once('value', function (snap) {
                    var groupqwner = _this.converanobj(snap.val().owner);
                    _this.firegroup.child(groupqwner[0].uid).child(gpname).once('value', function (snapshot) {
                        if (snapshot.val() != null) {
                            var temp = snapshot.val().members;
                            var currentgroup = [];
                            for (var key in temp) {
                                currentgroup.push(temp[key]);
                            }
                            resolve(currentgroup);
                        }
                    });
                });
            }).catch(function (e) { });
        });
    };
    GroupsProvider.prototype.addgroup = function (newGroup, members) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.userservice.getbuddydetails(_this.userId).then(function (owendetail) {
                _this.firegroup.child(_this.userId).child(newGroup.groupName).set({
                    timestamp: __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database.ServerValue.TIMESTAMP,
                    groupimage: newGroup.groupPic,
                    msgboard: '',
                    owner: '',
                }).then(function () {
                    _this.firegroup.child(_this.userId).child(newGroup.groupName).child('owner').push(owendetail);
                    _this.tempAddMember(members, newGroup).then(function (res) {
                        if (res == true) {
                            _this.firegroup.child(_this.userId).child(newGroup.groupName).child('members').push(owendetail);
                            resolve(true);
                        }
                    });
                }).catch(function (err) {
                    reject(err);
                });
            });
        });
    };
    GroupsProvider.prototype.tempAddMember = function (members, newGroup) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.flag = 0;
            for (var i = 0; i < members.length; i++) {
                _this.addmember(members[i], newGroup.groupName).then(function (res) {
                    if (res == true) {
                        _this.flag++;
                        if (_this.flag == members.length) {
                            resolve(true);
                        }
                    }
                });
            }
        });
    };
    GroupsProvider.prototype.tempAddMembers = function (members, groupName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.flag = 0;
            for (var i = 0; i < members.length; i++) {
                _this.tempaddmember(members[i], groupName).then(function (res) {
                    if (res == true) {
                        _this.flag++;
                        if (_this.flag == members.length) {
                            resolve(true);
                        }
                    }
                });
            }
        });
    };
    GroupsProvider.prototype.tempaddmember = function (newmember, groupName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firegroup.child(_this.userId).child(groupName).child('owner').once('value', function (snapshot) {
                var owners = snapshot.val();
                for (var key in owners) {
                    _this.firegroup.child(owners[key].uid).child(groupName).child('members').push(newmember).then(function () {
                        _this.getgroupimage().then(function () {
                            _this.firegroup.child(newmember.uid).child(groupName).set({
                                timestamp: __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database.ServerValue.TIMESTAMP,
                                groupimage: _this.grouppic,
                                owner: owners,
                                msgboard: ''
                            }).then(function () {
                                resolve(true);
                            }).catch(function (err) {
                                resolve(false);
                            });
                        });
                    });
                }
            });
        });
    };
    GroupsProvider.prototype.addmember = function (newmember, groupName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.currentgroupname = groupName;
            _this.userservice.getbuddydetails(_this.userId).then(function (owendetail) {
                _this.firegroup.child(_this.userId).child(groupName).child('members').push(newmember).then(function () {
                    _this.getgroupimage().then(function () {
                        _this.firegroup.child(newmember.uid).child(groupName).set({
                            timestamp: __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database.ServerValue.TIMESTAMP,
                            groupimage: _this.grouppic,
                            owner: '',
                            msgboard: ''
                        }).then(function () {
                            _this.firegroup.child(newmember.uid).child(groupName).child('owner').push(owendetail);
                            resolve(true);
                        }).catch(function (err) {
                            resolve(false);
                        });
                    });
                });
            });
        });
    };
    GroupsProvider.prototype.deletemember = function (member) {
        var _this = this;
        this.firegroup.child(this.userId).child(this.currentgroupname).child('owner').once('value', function (snapshot) {
            var owners = snapshot.val();
            var ownersmember = [];
            for (var keys in owners) {
                ownersmember.push(owners[keys]);
            }
            var flag = 0;
            var _loop_2 = function (i) {
                _this.firegroup.child(ownersmember[i].uid).child(_this.currentgroupname).child('members').once('value', function (snapshot) {
                    var buddy = snapshot.val();
                    for (var kk in buddy) {
                        if (buddy[kk].uid == member.uid) {
                            _this.firegroup.child(ownersmember[i].uid).child(_this.currentgroupname).child('members').child(kk).remove();
                            flag++;
                            if (flag == ownersmember.length) {
                                _this.firegroup.child(member.uid).child(_this.currentgroupname).remove().then(function () {
                                    _this.getintogroup(_this.currentgroupname);
                                    _this.getgroupmembers();
                                });
                            }
                        }
                    }
                });
            };
            for (var i = 0; i < ownersmember.length; i++) {
                _loop_2(i);
            }
        });
    };
    GroupsProvider.prototype.getgroupmsgs = function (groupname) {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (userId__) {
            _this.userId = userId__;
            _this.firegroup.child(_this.userId).child(groupname).child('msgboard').on('value', function (snapshot) {
                var tempmsgholder = snapshot.val();
                _this.groupmsgs = [];
                for (var key in tempmsgholder)
                    _this.groupmsgs.push(tempmsgholder[key]);
                _this.events.publish('newgroupmsg');
            });
        }).catch(function (e) { });
    };
    GroupsProvider.prototype.getgroupmembers = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (userId__) {
            _this.userId = userId__;
            _this.firegroup.child(_this.userId).child(_this.currentgroupname).once('value', function (snapshots) {
                var tempdata = _this.converanobj(snapshots.val().owner);
                _this.firegroup.child(tempdata[0].uid).child(_this.currentgroupname).child('members').once('value', function (snapshot) {
                    var tempvar = snapshot.val();
                    _this.currentgroup = [];
                    for (var key in tempvar) {
                        _this.currentgroup.push(tempvar[key]);
                    }
                    _this.events.publish('gotmembers');
                });
            });
        }).catch(function (e) { });
    };
    GroupsProvider.prototype.getgroupmemebrstatus = function (member) {
        var _this = this;
        var _loop_3 = function (i) {
            var tmpStatus;
            this_1.fireuserStatus.child(member[i].uid).on('value', function (statuss) {
                tmpStatus = statuss.val();
                if (tmpStatus.status == 1) {
                    _this.groupmemeberStatus[member[i].uid] = {
                        uid: member[i].uid,
                        status: tmpStatus.data,
                        sdate: tmpStatus.timestamp
                    };
                }
                else {
                    _this.groupmemeberStatus[member[i].uid] = {
                        uid: member[i].uid,
                        status: tmpStatus.data,
                        sdate: tmpStatus.timestamp
                    };
                }
            });
        };
        var this_1 = this;
        for (var i = 0; i < member.length; i++) {
            _loop_3(i);
        }
        this.events.publish('groupmemberstatus');
    };
    GroupsProvider.prototype.leavegroup = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firegroup.child(_this.userId).child(_this.currentgroupname).once('value', function (snapshot) {
                var tempowner = _this.converanobj(snapshot.val().owner);
                _this.firegroup.child(tempowner[0].uid).child(_this.currentgroupname).child('members').once('value', function (snapshots) {
                    var cuser = snapshots.val();
                    for (var key in cuser) {
                        if (cuser[key].uid == _this.userId) {
                            _this.firegroup.child(tempowner[0].uid).child(_this.currentgroupname).child('members').child(key).remove();
                        }
                    }
                    _this.firegroup.child(_this.userId).child(_this.currentgroupname).remove().then(function () {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            });
        });
    };
    GroupsProvider.prototype.getgroupInfo = function (groupname) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.userId = userId__;
                _this.firegroup.child(_this.userId).child(groupname).once('value', function (snapshot) {
                    resolve(snapshot.val());
                });
            }).catch(function (e) { });
        });
    };
    GroupsProvider.prototype.updategroupname = function (oldgpname, newgpname) {
        var _this = this;
        var _loop_4 = function (i) {
            setTimeout(function () {
                var child = _this.firegroup.child(_this.currentgroup[i].uid).child(oldgpname);
                _this.firegroup.child(_this.currentgroup[i].uid).child(oldgpname).once('value', function (snapshot) {
                    _this.firegroup.child(_this.currentgroup[i].uid).child(newgpname).set(snapshot.val());
                    child.remove();
                });
            }, 1000);
        };
        for (var i = 0; i < this.currentgroup.length; i++) {
            _loop_4(i);
        }
    };
    GroupsProvider.prototype.updategroupImage = function (res, gpname) {
        for (var i = 0; i < this.currentgroup.length; i++) {
            this.firegroup.child(this.currentgroup[i].uid).child(gpname).update({ groupimage: res });
        }
        this.firegroup.child(this.userId).child(gpname).update({ groupimage: res });
    };
    GroupsProvider.prototype.addGroupAdmin = function (buddy, gpname) {
        var _this = this;
        for (var i = 0; i < this.currentgroup.length; i++) {
            this.firegroup.child(this.currentgroup[i].uid).child(gpname).child('owner').push(buddy);
        }
        this.firegroup.child(this.userId).child(gpname).child('members').once('value', function (snapshot) {
            _this.firegroup.child(buddy.uid).child(gpname).child('members').set(snapshot.val());
        });
    };
    GroupsProvider.prototype.removefromAdmin = function (member) {
        var _this = this;
        this.firegroup.child(member.uid).child(this.currentgroupname).child('members').once('value', function (snapshots) {
            var members = snapshots.val();
            var allmember = [];
            for (var keys in members) {
                allmember.push(members[keys]);
            }
            if (allmember.length > 1) {
                var flag_1 = 0;
                var _loop_5 = function (i) {
                    _this.firegroup.child(allmember[i].uid).child(_this.currentgroupname).child('owner').once('value', function (snapshot) {
                        var owner = snapshot.val();
                        flag_1++;
                        var keyofmember;
                        for (var keyss in owner) {
                            if (owner[keyss].uid == member.uid) {
                                keyofmember = keyss;
                            }
                        }
                        _this.firegroup.child(allmember[i].uid).child(_this.currentgroupname).child('owner').child(keyofmember).remove();
                        if (flag_1 == allmember.length) {
                            _this.firegroup.child(member.uid).child(_this.currentgroupname).child('members').remove();
                            _this.getgroupmembers();
                        }
                    });
                };
                for (var i = 0; i < allmember.length; i++) {
                    _loop_5(i);
                }
            }
        });
    };
    GroupsProvider.prototype.removeAdmin = function (member) {
        this.deletemember(member);
        this.removefromAdmin(member);
    };
    GroupsProvider.prototype.deletegroups = function (groupname) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firegroup.child(_this.userId).child(groupname).child('members').once('value', function (snapshot) {
                var tempmembers = snapshot.val();
                for (var key in tempmembers) {
                    _this.firegroup.child(tempmembers[key].uid).child(groupname).remove();
                }
                _this.firegroup.child(_this.userId).child(groupname).remove().then(function () {
                    resolve(true);
                }).catch(function (err) {
                    reject(err);
                });
            });
        });
    };
    GroupsProvider.prototype.leavegroups = function (groupname) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firegroup.child(_this.userId).child(groupname).once('value', function (snapshot) {
                var tempowner = _this.converanobj(snapshot.val().owner);
                _this.firegroup.child(tempowner[0].uid).child(groupname).child('members').once('value', function (snapshots) {
                    var cuser = snapshots.val();
                    for (var key in cuser) {
                        if (cuser[key].uid == _this.userId) {
                            _this.firegroup.child(tempowner[0].uid).child(groupname).child('members').child(key).remove();
                        }
                    }
                    _this.firegroup.child(_this.userId).child(groupname).remove().then(function () {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            });
        });
    };
    GroupsProvider.prototype.leavegroupsowner = function (groupname) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firegroup.child(_this.userId).child(groupname).once('value', function (snapshot) {
                _this.firegroup.child(_this.userId).child(groupname).child('members').once('value', function (snapshot) {
                    var members = snapshot.val();
                    var allmember = [];
                    for (var keys in members) {
                        allmember.push(members[keys]);
                    }
                    if (allmember.length > 1) {
                        var flag_2 = 0;
                        var _loop_6 = function (i) {
                            _this.firegroup.child(allmember[i].uid).child(groupname).child('owner').once('value', function (snapshot) {
                                var owner = snapshot.val();
                                flag_2++;
                                var keyofmember;
                                for (var keyss in owner) {
                                    if (owner[keyss].uid == _this.userId) {
                                        keyofmember = keyss;
                                    }
                                }
                                _this.firegroup.child(allmember[i].uid).child(groupname).child('owner').child(keyofmember).remove();
                                if (flag_2 == allmember.length) {
                                    _this.firegroup.child(_this.userId).child(groupname).child('members').remove();
                                }
                            });
                            _this.firegroup.child(allmember[i].uid).child(groupname).child('members').once('value', function (snapshots) {
                                var cuser = snapshots.val();
                                for (var key in cuser) {
                                    if (cuser[key].uid == _this.userId) {
                                        _this.firegroup.child(allmember[i].uid).child(groupname).child('members').child(key).remove();
                                    }
                                }
                            });
                            _this.firegroup.child(_this.userId).child(groupname).remove().then(function () {
                                resolve(true);
                            }).catch(function (err) {
                                reject(err);
                            });
                        };
                        for (var i = 0; i < allmember.length; i++) {
                            _loop_6(i);
                        }
                    }
                });
            });
        });
    };
    GroupsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_5__fcm_fcm__["a" /* FcmProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], GroupsProvider);
    return GroupsProvider;
}());

//# sourceMappingURL=groups.js.map

/***/ }),

/***/ 405:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ChatProvider = (function () {
    function ChatProvider(events, tpStorageService, storage, userservice) {
        var _this = this;
        this.events = events;
        this.tpStorageService = tpStorageService;
        this.storage = storage;
        this.userservice = userservice;
        this.firedata = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/chatusers');
        this.firebuddychats = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/buddychats');
        this.firebuddymessagecounter = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/buddychats');
        this.fireuserStatus = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/userstatus');
        this.fireStar = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/starredmessage');
        this.firefriends = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/friends');
        this.fireReferral_sent = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/referral_sent');
        this.fireReferral_received = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/referral_received');
        this.fireBusiness = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/business');
        this.buddymessages = [];
        this.msgcount = 0;
        this.tpStorageService.getItem('userUID').then(function (userID) {
            if (userID != undefined) {
                _this.userId = userID;
            }
            else {
                _this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
            }
        }).catch(function (e) { });
    }
    //jaswinder probbaly not used
    ChatProvider.prototype.buddymessageRead = function (limit) {
        var _this = this;
        this.firebuddychats.child(this.userId).child(this.buddy.uid)
            .limitToLast(limit).once('value', function (snapshot) {
            var allmessahes = snapshot.val();
            for (var key in allmessahes) {
                if (allmessahes[key].isRead == false) {
                    _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).child(key).update({ isRead: true });
                }
            }
        });
    };
    ChatProvider.prototype.initializebuddy = function (buddy) {
        var _this = this;
        if (this.userId) {
            this.initializebuddy_(buddy);
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.userId = userId__;
                _this.initializebuddy_(buddy);
            }).catch(function (e) { });
        } //endif
    };
    ChatProvider.prototype.initializebuddy_ = function (buddy) {
        this.buddy = buddy;
        //console.log ("initializebuddy_ Done");
        //console.log (this.buddy);
        //this.buddymessageRead(10);
    };
    ChatProvider.prototype.formatAMPM = function (date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    };
    ChatProvider.prototype.formatDate = function (date) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        if (dd < 10)
            dd = '0' + dd;
        if (mm < 10)
            mm = '0' + mm;
        return dd + '/' + mm + '/' + yyyy;
    };
    ChatProvider.prototype.addnewmessageDirectReferral = function (msg, type, buddy, referral, referralType) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        if (buddy) {
            var promise = new Promise(function (resolve, reject) {
                _this.tpStorageService.getItem('userUID').then(function (userId__) {
                    _this.userId = userId__;
                    _this.firebuddychats.child(_this.userId).child(buddy.uid).push({
                        sentby: _this.userId,
                        message: msg,
                        type: type,
                        timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                        timeofmsg: time,
                        dateofmsg: date,
                        messageId: '',
                        selectCatId: 0,
                        multiMessage: false,
                        request_to_release_incentive: '',
                        isStarred: false,
                        referral_type: referralType,
                        referral: referral,
                        referralSavedOnPhone: false,
                        userprio: ''
                    })
                        .then(function (item) {
                        //Log info
                        _this.fireReferral_sent.child(_this.userId).child(buddy.uid).push({
                            sent_by: _this.userId,
                            received_by: buddy.uid,
                            referral_type: referralType,
                            referral: referral,
                            timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                            timeofmsg: time,
                            dateofmsg: date,
                            redeemStatus: 0,
                            incentiveType: "",
                            incentiveDetail: "",
                            business_referral_detail: "",
                            business_referral_type_id: ""
                        }).then(function (referral_sent) {
                            _this.fireReferral_received.child(buddy.uid).child(_this.userId).child(referral_sent.key).set({
                                sent_by: _this.userId,
                                received_by: buddy.uid,
                                referral_type: referralType,
                                referral: referral,
                                timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                                timeofmsg: time,
                                dateofmsg: date,
                                redeemStatus: 0,
                                incentiveType: "",
                                incentiveDetail: "",
                                business_referral_detail: "",
                                business_referral_type_id: ""
                            }).then(function (referral_received) {
                                //Now check and see if the guy who recieved referral is actually busienss owner at that time
                                _this.fireBusiness.child(buddy.uid).once('value', function (snapshot_business_of_referral_rec) {
                                    var businessGuy = snapshot_business_of_referral_rec.val();
                                    if (businessGuy) {
                                        _this.fireReferral_sent.child(_this.userId).child(buddy.uid).child(referral_sent.key).update({
                                            business_referral_detail: businessGuy.referral_detail,
                                            business_referral_type_id: businessGuy.referral_type_id
                                        });
                                        _this.fireReferral_received.child(buddy.uid).child(_this.userId).child(referral_sent.key).update({
                                            business_referral_detail: businessGuy.referral_detail,
                                            business_referral_type_id: businessGuy.referral_type_id
                                        });
                                    } //endif
                                }).catch(function (err) {
                                });
                            });
                        });
                        _this.firefriends.child(_this.userId).child(buddy.uid).update({ isActive: 1, userprio: new Date() });
                        _this.firebuddychats.child(_this.userId).child(buddy.uid).child(item.key).update({ messageId: item.key, userprio: new Date() });
                        _this.userservice.getstatusblock(buddy).then(function (res) {
                            ////console.log ("going_to_call gotstatusblock ");
                            if (res == false) {
                                //mytodo : check if buddystatus is online
                                //if (buddystatus == "online") {
                                if (false) {
                                    //this.firebuddychats.child(this.userId).child(buddy.uid).child(item.key).update({ isRead: true });
                                }
                                else {
                                    ////console.log ("going_to_call firebuddychats 1");
                                    _this.firebuddychats.child(_this.userId).child(buddy.uid).child(item.key).update({ isRead: false });
                                }
                                //Also add to buddy
                                _this.firebuddychats.child(buddy.uid).child(_this.userId).push({
                                    sentby: _this.userId,
                                    message: msg,
                                    type: type,
                                    timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                                    timeofmsg: time,
                                    dateofmsg: date,
                                    messageId: '',
                                    isRead: true,
                                    isStarred: false,
                                    selectCatId: 0,
                                    request_to_release_incentive: '',
                                    referral_type: referralType,
                                    referral: referral,
                                    referralSavedOnPhone: false,
                                    userprio: ''
                                }).then(function (items) {
                                    _this.firebuddychats.child(buddy.uid).child(_this.userId).child(items.key).update({ messageId: items.key, userprio: new Date() }).then(function () {
                                        _this.firefriends.child(buddy.uid).child(_this.userId).update({ isActive: 1, userprio: new Date() });
                                        resolve(true);
                                    });
                                });
                            }
                            else {
                                //Buddy has blocked us
                                ////console.log ("going_to_call firebuddychats 2");
                                _this.firebuddychats.child(_this.userId).child(buddy.uid).child(item.key).update({ isRead: false });
                                resolve(true);
                            }
                        });
                    });
                }).catch(function (e) { });
            });
            return promise;
        }
    };
    //buddy is receipient
    ChatProvider.prototype.addnewmessageBroadcast = function (msg, type, buddy, selectCatId, bothways) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        if (buddy) {
            var promise = new Promise(function (resolve, reject) {
                _this.tpStorageService.getItem('userUID').then(function (userId__) {
                    _this.userId = userId__;
                    _this.userservice.getstatusblock(buddy).then(function (res) {
                        if (res == false) {
                            //user has not blocked buddy
                            _this.firebuddychats.child(buddy.uid).child(_this.userId).push({
                                sentby: _this.userId,
                                message: msg,
                                type: type,
                                timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                                timeofmsg: time,
                                dateofmsg: date,
                                request_to_release_incentive: '',
                                messageId: '',
                                selectCatId: selectCatId,
                                multiMessage: false,
                                isStarred: false,
                                userprio: ''
                            })
                                .then(function (item) {
                                if (bothways) {
                                    //This is both way broadcast so include me as whitelabeled
                                    _this.firebuddychats.child(_this.userId).child(buddy.uid).push({
                                        sentby: _this.userId,
                                        message: msg,
                                        type: type,
                                        timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                                        timeofmsg: time,
                                        dateofmsg: date,
                                        request_to_release_incentive: '',
                                        messageId: '',
                                        selectCatId: selectCatId,
                                        multiMessage: false,
                                        isStarred: false,
                                        userprio: ''
                                    }).then(function (item) { });
                                    //mytodo : broadcast in groups too
                                }
                                else {
                                    //A referral request has been sent
                                    //somebody who posted referral request
                                }
                                _this.firefriends.child(buddy.uid).child(_this.userId).update({ isActive: 1, userprio: new Date() });
                                _this.firebuddychats.child(buddy.uid).child(_this.userId).child(item.key).update({ messageId: item.key, userprio: new Date() });
                                resolve(true);
                                //mytodo : check if buddystatus is online
                                //if (buddystatus == "online") {
                                if (false) {
                                    //Send push notification
                                    //this.firebuddychats.child(this.userId).child(buddy.uid).child(item.key).update({ isRead: true });
                                }
                                else {
                                    //Need this because we did not send push notification
                                    _this.firebuddychats.child(buddy.uid).child(_this.userId).child(item.key).update({ isRead: false });
                                }
                            });
                        } //endif
                    }); //getstatusblock
                }).catch(function (e) { });
            });
            return promise;
        }
    };
    //under dev : not used
    ChatProvider.prototype.addnewmessageAskForIncentives = function (msg, type, buddystatus, referral_id, referral_send_by, referral_received_by) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        if (this.buddy) {
            var promise = new Promise(function (resolve, reject) {
                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).push({
                    sentby: _this.userId,
                    message: msg,
                    type: type,
                    request_to_release_incentive: referral_id,
                    referral_send_by: referral_send_by,
                    referral_received_by: referral_received_by,
                    timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    messageId: '',
                    multiMessage: false,
                    isStarred: false,
                    userprio: ''
                })
                    .then(function (item) {
                    _this.firefriends.child(_this.userId).child(_this.buddy.uid).update({ isActive: 1, userprio: new Date() });
                    _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ messageId: item.key, userprio: new Date() });
                    _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).push({
                        sentby: _this.userId,
                        message: msg,
                        type: type,
                        timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                        timeofmsg: time,
                        dateofmsg: date,
                        messageId: '',
                        request_to_release_incentive: referral_id,
                        isRead: true,
                        isStarred: false,
                        userprio: ''
                    }).then(function (items) {
                        _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).child(items.key).update({ messageId: items.key, userprio: new Date() });
                        _this.firefriends.child(_this.buddy.uid).child(_this.userId).update({ isActive: 1, userprio: new Date() });
                        resolve(true);
                    });
                });
            });
            return promise;
        }
    };
    ChatProvider.prototype.addnewmessageRedeemDone = function (msg, type, buddystatus, referral_id, referral_send_by, referral_received_by) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        if (this.buddy) {
            var promise = new Promise(function (resolve, reject) {
                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).push({
                    sentby: _this.userId,
                    message: msg,
                    type: type,
                    referral_redeemed: 1,
                    referral_send_by: referral_send_by,
                    referral_received_by: referral_received_by,
                    timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    messageId: '',
                    multiMessage: false,
                    isStarred: false,
                    userprio: ''
                })
                    .then(function (item) {
                    _this.firefriends.child(_this.userId).child(_this.buddy.uid).update({ isActive: 1, userprio: new Date() });
                    _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ messageId: item.key, userprio: new Date() });
                    _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).push({
                        sentby: _this.userId,
                        message: msg,
                        type: type,
                        timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                        timeofmsg: time,
                        dateofmsg: date,
                        messageId: '',
                        referral_redeemed: 1,
                        referral_send_by: referral_send_by,
                        referral_received_by: referral_received_by,
                        isRead: true,
                        isStarred: false,
                        userprio: ''
                    }).then(function (items) {
                        _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).child(items.key).update({ messageId: items.key, userprio: new Date() });
                        _this.firefriends.child(_this.buddy.uid).child(_this.userId).update({ isActive: 1, userprio: new Date() });
                        resolve(true);
                    });
                });
            });
            return promise;
        }
    };
    ChatProvider.prototype.addnewmessageRedeemRequest = function (msg, type, buddystatus, referral_id, referral_send_by, referral_received_by) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        if (this.buddy) {
            var promise = new Promise(function (resolve, reject) {
                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).push({
                    sentby: _this.userId,
                    message: msg,
                    type: type,
                    request_to_release_incentive: referral_id,
                    referral_send_by: referral_send_by,
                    referral_received_by: referral_received_by,
                    timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    messageId: '',
                    multiMessage: false,
                    isStarred: false,
                    userprio: ''
                })
                    .then(function (item) {
                    _this.firefriends.child(_this.userId).child(_this.buddy.uid).update({ isActive: 1, userprio: new Date() });
                    _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ messageId: item.key, userprio: new Date() });
                    _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).push({
                        sentby: _this.userId,
                        message: msg,
                        type: type,
                        timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                        timeofmsg: time,
                        dateofmsg: date,
                        messageId: '',
                        request_to_release_incentive: referral_id,
                        referral_send_by: referral_send_by,
                        referral_received_by: referral_received_by,
                        isRead: true,
                        isStarred: false,
                        userprio: ''
                    }).then(function (items) {
                        _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).child(items.key).update({ messageId: items.key, userprio: new Date() });
                        _this.firefriends.child(_this.buddy.uid).child(_this.userId).update({ isActive: 1, userprio: new Date() });
                        resolve(true);
                    });
                });
            });
            return promise;
        }
    };
    ChatProvider.prototype.addnewmessage = function (msg, type, buddystatus) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        if (this.buddy) {
            var promise = new Promise(function (resolve, reject) {
                //console.log ("GGGGGGGGGGGGG1");
                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).push({
                    sentby: _this.userId,
                    message: msg,
                    type: type,
                    timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    request_to_release_incentive: '',
                    messageId: '',
                    multiMessage: false,
                    isStarred: false,
                    userprio: ''
                })
                    .then(function (item) {
                    //console.log ("GGGGGGGGGGGGG2");
                    _this.firefriends.child(_this.userId).child(_this.buddy.uid).update({ isActive: 1, userprio: new Date() });
                    //console.log ("GGGGGGGGGGGGG3");
                    _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ messageId: item.key, userprio: new Date() });
                    //console.log ("GGGGGGGGGGGGG4");
                    _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).push({
                        sentby: _this.userId,
                        message: msg,
                        type: type,
                        timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                        timeofmsg: time,
                        dateofmsg: date,
                        request_to_release_incentive: '',
                        messageId: '',
                        isRead: true,
                        isStarred: false,
                        userprio: ''
                    }).then(function (items) {
                        //console.log ("GGGGGGGGGGGGG5");
                        _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).child(items.key).update({ messageId: items.key, userprio: new Date() });
                        //console.log ("GGGGGGGGGGGGG6");
                        _this.firefriends.child(_this.buddy.uid).child(_this.userId).update({ isActive: 1, userprio: new Date() });
                        resolve(true);
                    });
                });
            });
            return promise;
        }
    };
    ChatProvider.prototype.addnewmessagemultiple = function (msg, type, buddystatus) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        if (this.buddy) {
            var promise = new Promise(function (resolve, reject) {
                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).push({
                    sentby: _this.userId,
                    message: msg,
                    type: type,
                    timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    request_to_release_incentive: '',
                    messageId: '',
                    isRead: true,
                    multiMessage: true,
                    isStarred: false,
                    userprio: ''
                })
                    .then(function (item) {
                    _this.getfirendlist(_this.userId).then(function (res) {
                        var friends = res;
                        for (var tmpkey in friends) {
                            if (friends[tmpkey].uid == _this.buddy.uid) {
                                _this.firefriends.child(_this.userId).child(tmpkey).update({ isActive: 1, userprio: new Date() });
                                resolve(true);
                            }
                        }
                    });
                    _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ messageId: item.key, userprio: new Date() });
                    if (buddystatus == "online") {
                        _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ isRead: true });
                    }
                    else {
                        _this.userservice.getstatusblock(_this.buddy).then(function (res) {
                            if (res == false) {
                                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ isRead: false });
                            }
                            else {
                                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ isRead: true });
                            }
                        });
                    }
                    _this.userservice.getstatusblock(_this.buddy).then(function (res) {
                        if (res == false) {
                            _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).push({
                                sentby: _this.userId,
                                message: msg,
                                type: type,
                                timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                                timeofmsg: time,
                                dateofmsg: date,
                                request_to_release_incentive: '',
                                messageId: '',
                                multiMessage: true,
                                isStarred: false,
                                userprio: ''
                            }).then(function (items) {
                                _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).child(items.key).update({ messageId: items.key, userprio: new Date() }).then(function () {
                                    if (buddystatus == "online") {
                                        _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).child(items.key).update({ isRead: true });
                                    }
                                    else {
                                    }
                                    _this.getfirendlist(_this.buddy.uid).then(function (res) {
                                        var friends = res;
                                        for (var tmpkey in friends) {
                                            if (friends[tmpkey].uid == _this.userId) {
                                                _this.firefriends.child(_this.buddy.uid).child(tmpkey).update({ isActive: 1, userprio: new Date() });
                                                resolve(true);
                                            }
                                        }
                                    });
                                });
                            });
                        }
                        else {
                            resolve(true);
                        }
                    });
                });
            });
            return promise;
        }
    };
    ChatProvider.prototype.deleteMessages = function (items) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            for (var i = 0; i < items.length; i++) {
                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(items[i].messageId).remove()
                    .then(function (res) {
                    _this.getfirendlist(_this.userId).then(function (res) {
                        var friends = res;
                        for (var tmpkey in friends) {
                            if (friends[tmpkey].uid == _this.buddy.uid) {
                                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).once('value', function (snapshot) {
                                    var tempdata = _this.converanobj(snapshot.val());
                                    if (tempdata.length > 0) {
                                        var lastMsg = tempdata[tempdata.length - 1];
                                        _this.firefriends.child(_this.userId).child(tmpkey).update({ userprio: lastMsg.userprio });
                                        resolve(true);
                                    }
                                    else {
                                        _this.firefriends.child(_this.userId).child(tmpkey).update({ userprio: '' });
                                        resolve(true);
                                    }
                                });
                            }
                        }
                    });
                }).catch(function (err) {
                    reject(false);
                });
            }
        });
    };
    ChatProvider.prototype.deleteUserMessages = function (allbuddyuser) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (allbuddyuser.length > 0) {
                var _loop_1 = function (i) {
                    _this.firebuddychats.child(_this.userId).child(allbuddyuser[i].uid).remove()
                        .then(function (res) {
                        _this.getfirendlist(_this.userId).then(function (res) {
                            var friends = res;
                            for (var tmpkey in friends) {
                                if (friends[tmpkey].uid == allbuddyuser[i].uid) {
                                    _this.firefriends.child(_this.userId).child(tmpkey).update({ isActive: 0 }).then(function (res) {
                                        resolve(true);
                                        _this.events.publish('friends');
                                    });
                                }
                            }
                        });
                    });
                };
                for (var i = 0; i < allbuddyuser.length; i++) {
                    _loop_1(i);
                }
            }
        });
    };
    ChatProvider.prototype.getfirendlist = function (uid) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            ////console.log ("<<<<<<<<Finding Frenids in DB>>>>>>>>");
            _this.firefriends.child(uid).on('value', function (snapshot) {
                var friendsAll = snapshot.val();
                resolve(friendsAll);
            });
        });
    };
    ChatProvider.prototype.updateContactSaved = function (msgId) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(msgId).update({
                referralSavedOnPhone: true,
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    ChatProvider.prototype.getbuddymessages = function (limit) {
        var _this = this;
        console.log("getbuddymessages function called");
        if (this.userId) {
            this.getbuddymessages_(limit);
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.userId = userId__;
                _this.getbuddymessages_(limit);
            }).catch(function (e) { });
        } //endif
    };
    ChatProvider.prototype.getbuddymessages_ = function (limit) {
        var _this = this;
        this.buddymessages = [];
        this.firebuddychats.child(this.userId).child(this.buddy.uid).limitToLast(limit).on('child_added', function (snapshot) {
            //Check for unique value
            console.log("Inside firebuddychats :");
            var tmpObj = snapshot.val();
            var isMatch = false;
            for (var myKey in _this.buddymessages) {
                if (_this.buddymessages[myKey]['dateofmsg'] == tmpObj.dateofmsg &&
                    _this.buddymessages[myKey]['isStarred'] == tmpObj.isStarred &&
                    _this.buddymessages[myKey]['message'] == tmpObj.message &&
                    _this.buddymessages[myKey]['messageId'] == tmpObj.messageId &&
                    _this.buddymessages[myKey]['multiMessage'] == tmpObj.multiMessage &&
                    _this.buddymessages[myKey]['sentby'] == tmpObj.sentby &&
                    _this.buddymessages[myKey]['timeofmsg'] == tmpObj.timeofmsg &&
                    _this.buddymessages[myKey]['timestamp'] == tmpObj.timestamp &&
                    _this.buddymessages[myKey]['type'] == tmpObj.type) {
                    //This is redundant message
                    isMatch = true;
                    break;
                }
            } //end for
            if (!isMatch) {
                _this.buddymessages.push(tmpObj);
                console.log("Going to publish newmessage");
                _this.events.publish('newmessage');
            }
        });
    };
    ChatProvider.prototype.getbuddyStatus = function () {
        var _this = this;
        var tmpStatus;
        this.fireuserStatus.child(this.buddy.uid).on('value', function (statuss) {
            tmpStatus = statuss.val();
            if (tmpStatus.status == 1) {
                _this.buddyStatus = tmpStatus.data;
            }
            else {
                var date = tmpStatus.timestamp;
                _this.buddyStatus = date;
            }
            _this.events.publish('onlieStatus');
        });
    };
    ChatProvider.prototype.setstatusUser = function () {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        var promise = new Promise(function (resolve, reject) {
            _this.fireuserStatus.child(_this.userId).set({
                status: 1,
                data: 'online',
                timestamp: date + ' at ' + time
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    ChatProvider.prototype.setStatusOffline = function () {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        var promise = new Promise(function (resolve, reject) {
            _this.fireuserStatus.child(_this.userId).update({
                status: 0,
                data: 'offline',
                timestamp: date + ' at ' + time
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    ChatProvider.prototype.converanobj = function (obj) {
        var tmp = [];
        for (var key in obj) {
            tmp.push(obj[key]);
        }
        return tmp;
    };
    ChatProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */], __WEBPACK_IMPORTED_MODULE_5__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_4__user_user__["a" /* UserProvider */]])
    ], ChatProvider);
    return ChatProvider;
}());

//# sourceMappingURL=chat.js.map

/***/ }),

/***/ 406:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_contacts__ = __webpack_require__(242);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_sqlite__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_const__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ContactProvider = (function () {
    function ContactProvider(events, contacts, sqlite) {
        this.events = events;
        this.contacts = contacts;
        this.sqlite = sqlite;
        this.contactInsertCounter = 0;
        this.contactErrorCounter = 0;
        this.contactTotalCounter = 0;
        this.contactInsertingDone = false;
        this.ContactCounterMargin = 40; //margin of error when calculating insert/error and total counter
        //this.deleteFromTable ();
    }
    ContactProvider.prototype.deleteFromTable = function () {
        this.sqlite.create({
            name: 'gr.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql(__WEBPACK_IMPORTED_MODULE_4__app_const__["a" /* CONST */].create_table_statement_tapally_contacts, {})
                .then(function (res) {
                db.executeSql("DELETE FROM cache_tapally_contacts", {})
                    .then(function (res) {
                    console.log("table cache_tapally_contacts dropped ");
                })
                    .catch(function (e) {
                    //Error in operation
                    //console.log ("ERROR 101");
                    //console.log (e);
                });
            })
                .catch(function (e) {
                //Error in operation
                //console.log ("ERROR 102");
                //console.log (e);
            }); //create table
        }); //create database
    }; //end function
    ContactProvider.prototype.getSimJsonContacts = function () {
        var _this = this;
        var options = {
            filter: "",
            multiple: true,
            hasPhoneNumber: true
        };
        return new Promise(function (resolve, reject) {
            _this.sqlite.create({
                name: 'gr.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql(__WEBPACK_IMPORTED_MODULE_4__app_const__["a" /* CONST */].create_table_statement_tapally_contacts, {})
                    .then(function (res) {
                    db.executeSql("SELECT *  FROM cache_tapally_contacts order by displayName", {})
                        .then(function (res) {
                        if (res.rows.length > 0) {
                            var mainArray = {};
                            for (var i = 0; i < res.rows.length; i++) {
                                mainArray[res.rows.item(i).displayName + '_' + res.rows.item(i).mobile] = res.rows.item(i);
                            } //end for
                            resolve({ contacts: mainArray, fromcache: true });
                        }
                        else {
                            //No contact found. LEts get from contact list
                            _this.contacts.find(["phoneNumbers", "displayName"], options).then(function (phone_contacts) {
                                if (phone_contacts != null) {
                                    resolve(_this.onSuccessSqlLite(phone_contacts, db));
                                }
                            }).catch(function (err) {
                                reject(err);
                            });
                        } //endif
                    }).catch(function (e) { reject(); });
                }).catch(function (e) { reject(); });
            }).catch(function (e) { reject(); });
        }).catch(function (error) {
            console.log("Error 81273 ");
        });
    }; //end function
    ContactProvider.prototype.CheckForUpdatedContacts = function () {
        var _this = this;
        var options = {
            filter: "",
            multiple: true,
            hasPhoneNumber: true
        };
        return new Promise(function (resolve, reject) {
            _this.sqlite.create({
                name: 'gr.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql(__WEBPACK_IMPORTED_MODULE_4__app_const__["a" /* CONST */].create_table_statement_tapally_contacts, {})
                    .then(function (res) {
                    db.executeSql("SELECT *  FROM cache_tapally_contacts", {})
                        .then(function (res) {
                        if (res.rows.length > 0) {
                            ////console.log ("Finding contacts in phone ")
                            _this.contacts.find(["phoneNumbers", "displayName"], options).then(function (phone_contacts) {
                                if (phone_contacts.length > res.rows.length) {
                                    //If contact on phone are more than what in our DB
                                    //console.log (">>>>contact on phone are more than what in our DB");
                                    //console.log (phone_contacts.length+"::::::"+res.rows.length);
                                    _this.onSuccessSqlLite(phone_contacts, db);
                                    //Uncommend following if returning value
                                    //resolve(this.onSuccessSqlLite(phone_contacts,db));
                                }
                                else {
                                    console.log("Resolve : No new contacts found  ");
                                    resolve(true);
                                    //Uncommend following if returning value
                                    //resolve({contacts: phoneContactsJson});
                                }
                            }).catch(function (err) {
                                resolve(true);
                                //Uncommend following if returning value
                                //resolve({contacts: phoneContactsJson});
                            });
                        } //endif
                    }).catch(function (e) { });
                }).catch(function (e) { });
            }).catch(function (e) { });
        }).catch(function (error) { console.log("Error 2371"); });
    }; //end function
    ContactProvider.prototype.formatPhoneNumber = function (phoneNumberString) {
        var cleaned = ('' + phoneNumberString).replace(/\D/g, '');
        var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
        if (match) {
            var intlCode = (match[1] ? '+1 ' : '');
            return [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('');
        }
        return "";
    };
    // Call   contact res
    ContactProvider.prototype.onSuccessSqlLite = function (contacts, db) {
        var _this = this;
        //console.log ("Came hereee : " + contacts.length);
        var phoneContactsJson = {};
        //sort by name
        /*
        contacts = contacts.sort(function (a, b) {
          if (a.displayName != undefined) {
            var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
          }
          if (b.displayName != undefined) {
            var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
          }
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          // names must be equal
          return 0;
        });
        */
        for (var i = 0; i < contacts.length; i++) {
            ////console.log ("Itetrating "+i);
            var no = "";
            if (contacts[i].name && contacts[i].name != null && contacts[i].name != undefined) {
                no = contacts[i].name.formatted;
            }
            var phonenumber = contacts[i].phoneNumbers;
            if (phonenumber != null) {
                for (var x = 0; x < phonenumber.length; x++) {
                    if (true) {
                        var phone = phonenumber[x].value;
                        var tempMobile = void 0;
                        tempMobile = phone.replace(/[^0-9]/g, "");
                        var mobile = void 0;
                        if (tempMobile.length > 10) {
                            mobile = tempMobile.substr(tempMobile.length - 10);
                        }
                        else {
                            mobile = tempMobile;
                        }
                        var contactData = {
                            "_id": mobile,
                            "displayName": no,
                            "mobile": mobile,
                            "mobile_formatted": this.formatPhoneNumber(mobile),
                            "status": "",
                            "isBlock": false,
                            "isUser": 0,
                            "isdisable": false,
                            "photoURL": "",
                            "uid": ""
                        };
                        if (contactData.displayName && contactData.displayName != "undefined" && contactData.mobile && contactData.mobile != "undefined") {
                            phoneContactsJson[mobile] = contactData;
                        }
                    } //endif
                } //end for
            } //endif
        } //end for
        //Now insert in db
        this.contactInsertCounter = 0;
        this.contactTotalCounter = 0;
        this.contactErrorCounter = 0;
        this.contactInsertingDone = false;
        Object.keys(phoneContactsJson).forEach(function (element) {
            phoneContactsJson[element].mobile = phoneContactsJson[element].mobile.replace(/'/g, "`"); //replace the '
            db.executeSql("INSERT INTO cache_tapally_contacts (id,displayName,mobile,mobile_formatted,isUser) VALUES('" + phoneContactsJson[element].mobile + "','" + phoneContactsJson[element].displayName + "','" + phoneContactsJson[element].mobile + "','" + phoneContactsJson[element].mobile_formatted + "',0)", {})
                .then(function (res1) {
                _this.contactInsertCounter++;
                _this.calculateAndMarkDone();
            }).catch(function (err) {
                _this.contactErrorCounter++;
                _this.calculateAndMarkDone();
            });
            _this.contactTotalCounter++;
        }); //end loop
        this.calculateAndMarkDone();
        var collection = {
            contacts: phoneContactsJson,
            fromcache: false
        };
        return collection;
    };
    ContactProvider.prototype.calculateAndMarkDone = function () {
        if (this.contactInsertCounter + this.contactErrorCounter + this.ContactCounterMargin >= this.contactTotalCounter) {
            this.contactInsertingDone = true;
            //console.log ("Going to publish contactInsertingDone : "+this.contactTotalCounter);
            //console.log (this.contactInsertCounter+this.contactErrorCounter+this.ContactCounterMargin);
            //console.log ("----------------");
            this.events.publish('contactInsertingDone');
        }
    };
    ContactProvider.prototype.removeDuplicates = function (originalArray, prop) {
        var newArray = [];
        var lookupObject = {};
        originalArray.forEach(function (item, index) {
            lookupObject[originalArray[index][prop]] = originalArray[index];
        });
        Object.keys(lookupObject).forEach(function (element) {
            newArray.push(lookupObject[element]);
        });
        return newArray;
    };
    // Call onSuccess contact res : might be unused
    ContactProvider.prototype.onSuccess = function (contacts) {
        var phoneContactsJson = [];
        for (var i = 0; i < contacts.length; i++) {
            var no = void 0;
            if (contacts[i].name != null) {
                no = contacts[i].name.formatted;
            }
            var phonenumber = contacts[i].phoneNumbers;
            if (phonenumber != null) {
                var type = phonenumber[0].type;
                if (type == 'mobile' || type == 'work' || type == 'home') {
                    var phone = phonenumber[0].value;
                    var tempMobile = void 0;
                    tempMobile = phone.replace(/[^0-9]/g, "");
                    var mobile = void 0;
                    if (tempMobile.length > 10) {
                        mobile = tempMobile.substr(tempMobile.length - 10);
                    }
                    else {
                        mobile = tempMobile;
                    }
                    var contactData = {
                        "_id": mobile,
                        "displayName": no,
                        "mobile": mobile,
                        "isUser": '0'
                    };
                    phoneContactsJson.push(contactData);
                }
            }
        }
        var collection = {
            contacts: phoneContactsJson
        };
        return collection;
    };
    ContactProvider.prototype.addContact = function (newContact) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var contact = _this.contacts.create();
            contact.displayName = newContact.displayName;
            var field = new __WEBPACK_IMPORTED_MODULE_1__ionic_native_contacts__["a" /* ContactField */]();
            field.type = 'mobile';
            field.value = newContact.phoneNumber;
            field.pref = true;
            var numberSection = [];
            numberSection.push(field);
            contact.phoneNumbers = numberSection;
            contact.save().then(function (value) {
                resolve(true);
            }, function (error) {
                reject(error);
            });
        }).catch(function (error) { });
    };
    ContactProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* Events */], __WEBPACK_IMPORTED_MODULE_1__ionic_native_contacts__["b" /* Contacts */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_sqlite__["a" /* SQLite */]])
    ], ContactProvider);
    return ContactProvider;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 407:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImagehandlerProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_image_picker__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_media_capture__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_tpstorage_tpstorage__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_chooser__ = __webpack_require__(268);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var ImagehandlerProvider = (function () {
    function ImagehandlerProvider(loadingCtrl, loadingProvider, camera, actionSheetCtrl, toastCtrl, imagePicker, pf, iab, mediaCapture, chooser, tpStorageService) {
        var _this = this;
        this.loadingCtrl = loadingCtrl;
        this.loadingProvider = loadingProvider;
        this.camera = camera;
        this.actionSheetCtrl = actionSheetCtrl;
        this.toastCtrl = toastCtrl;
        this.imagePicker = imagePicker;
        this.pf = pf;
        this.iab = iab;
        this.mediaCapture = mediaCapture;
        this.chooser = chooser;
        this.tpStorageService = tpStorageService;
        this.firestore = __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.storage();
        this.tpStorageService.getItem('userUID').then(function (res) {
            if (res) {
                _this.userId = res;
            }
            else {
                _this.userId = __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.auth().currentUser.uid;
            }
        }).catch(function (e) { });
    }
    ImagehandlerProvider.prototype.uploadimage = function () {
        if (this.pf.is('ios')) {
            return this.uploadimage_android();
        }
        else {
            return this.uploadimage_android();
        } //endif
    };
    ImagehandlerProvider.prototype.uploadimage_ios = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var actionSheet = _this.actionSheetCtrl.create({
                title: 'Select Image From',
                buttons: [{
                        text: 'Camera',
                        handler: function () {
                            _this.takePicture(_this.camera.PictureSourceType.CAMERA, _this.camera.DestinationType.FILE_URI).then(function (data) {
                                resolve(data);
                            }).catch(function (err) {
                                reject(err);
                            });
                        }
                    }, {
                        text: 'Cancel',
                        role: 'cancel'
                    }
                ]
            });
            actionSheet.present();
        });
    };
    ImagehandlerProvider.prototype.uploadimage_android = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var actionSheet = _this.actionSheetCtrl.create({
                title: 'Select Image From',
                buttons: [{
                        text: 'Load from Gallery',
                        handler: function () {
                            _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY, _this.camera.DestinationType.FILE_URI).then(function (data) {
                                resolve(data);
                            }).catch(function (err) {
                                reject(err);
                            });
                        }
                    }, {
                        text: 'Camera',
                        handler: function () {
                            _this.takePicture(_this.camera.PictureSourceType.CAMERA, _this.camera.DestinationType.FILE_URI).then(function (data) {
                                resolve(data);
                            }).catch(function (err) {
                                reject(err);
                            });
                        }
                    }, {
                        text: 'Cancel',
                        role: 'cancel'
                    }
                ]
            });
            actionSheet.present();
        });
    };
    ImagehandlerProvider.prototype.takePicture = function (sourceType, destinationType) {
        var _this = this;
        var options = {
            quality: 100,
            sourceType: sourceType,
            destinationType: destinationType,
            saveToPhotoAlbum: true,
            allowEdit: true,
            targetWidth: 500,
            targetHeight: 500,
            correctOrientation: true
        };
        return new Promise(function (resolve, reject) {
            _this.camera.getPicture(options).then(function (url) {
                _this.loadingProvider.presentLoading();
                window.resolveLocalFileSystemURL(url, function (res) {
                    console.log("local url");
                    console.log(url);
                    res.file(function (resFile) {
                        var reader = new FileReader();
                        reader.readAsArrayBuffer(resFile);
                        reader.onloadend = function (evt) {
                            var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                            var imageStore = _this.firestore.ref('/profileimages').child(_this.userId);
                            imageStore.put(imgBlob).then(function (res) {
                                _this.firestore.ref('/profileimages').child(_this.userId).getDownloadURL().then(function (url) {
                                    console.log("Uploaded to firestore");
                                    resolve(url);
                                }).catch(function (err) {
                                    reject(err);
                                });
                            }).catch(function (err) {
                                reject(err);
                            });
                        };
                    });
                });
            }, function (err) {
                _this.presentToast('Error while selecting image.');
                reject(err);
            });
        });
    };
    ImagehandlerProvider.prototype.getAllIomage = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firestore.ref('/picmsgs').child(_this.userId).getDownloadURL().then(function (url) {
            });
        });
    };
    ImagehandlerProvider.prototype.picmsgstore = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var options = {
                quality: 100,
                sourceType: _this.camera.PictureSourceType.PHOTOLIBRARY,
                destinationType: _this.camera.DestinationType.FILE_URI,
                saveToPhotoAlbum: true,
                allowEdit: true,
                targetWidth: 500,
                targetHeight: 500,
                correctOrientation: true
            };
            _this.camera.getPicture(options).then(function (results_in) {
                var results = [];
                results[0] = results_in;
                console.log(results);
                //this.imagePicker.getPictures(options).then((results) => {
                var imageName = '';
                var imageCounter = 0;
                var responseArr = [];
                if (results.length > 0) {
                    _this.loadingProvider.presentLoading();
                    for (var i = 0; i < results.length; i++) {
                        if (_this.pf.is('ios')) {
                            imageName = 'file://' + results[i];
                        }
                        else {
                            imageName = results[i];
                        }
                        //jaswinder
                        window.resolveLocalFileSystemURL(imageName, function (res) {
                            res.file(function (resFile) {
                                var reader = new FileReader();
                                reader.readAsArrayBuffer(resFile);
                                reader.onloadend = function (evt) {
                                    var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                                    var uuid = _this.guid();
                                    var imageStore = _this.firestore.ref('/picmsgs').child(_this.userId).child('picmsg' + uuid);
                                    imageStore.put(imgBlob).then(function (res) {
                                        imageCounter++;
                                        responseArr.push(res.downloadURL);
                                        if (results.length == imageCounter) {
                                            _this.loadingProvider.dismissMyLoading();
                                            resolve(responseArr);
                                        }
                                    }).catch(function (err) {
                                        console.log("Error 92123");
                                        _this.loadingProvider.dismissMyLoading();
                                        reject(err);
                                    }).catch(function (err) {
                                        console.log("Error 41232");
                                        _this.loadingProvider.dismissMyLoading();
                                        reject(err);
                                    });
                                };
                            });
                        }, function (err) {
                            console.log("Error 93823");
                            console.log(err);
                            _this.loadingProvider.dismissMyLoading();
                        });
                    }
                }
                else {
                    resolve(true);
                }
            }).catch(function (err) {
                console.log("ERROR");
                console.log(err);
                _this.loadingProvider.dismissMyLoading();
            });
        }).catch(function (err) {
        });
    };
    ImagehandlerProvider.prototype.cameraPicmsgStore = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // Create options for the Camera Dialog
            var options = {
                quality: 100,
                sourceType: _this.camera.PictureSourceType.CAMERA,
                destinationType: _this.camera.DestinationType.FILE_URI,
                saveToPhotoAlbum: true,
                allowEdit: true,
                targetWidth: 500,
                targetHeight: 500,
                correctOrientation: true
            };
            _this.camera.getPicture(options).then(function (url) {
                _this.loadingProvider.presentLoading();
                window.resolveLocalFileSystemURL(url, function (res) {
                    res.file(function (resFile) {
                        var reader = new FileReader();
                        reader.readAsArrayBuffer(resFile);
                        reader.onloadend = function (evt) {
                            var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                            var uuid = _this.guid();
                            var imageStore = _this.firestore.ref('/picmsgs').child(_this.userId).child('picmsg' + uuid);
                            imageStore.put(imgBlob).then(function (res) {
                                _this.loadingProvider.dismissMyLoading();
                                resolve(res.downloadURL);
                            }).catch(function (err) {
                                _this.loadingProvider.dismissMyLoading();
                                reject(err);
                            }).catch(function (err) {
                                _this.loadingProvider.dismissMyLoading();
                                reject(err);
                            });
                        };
                    });
                });
            }).catch(function (err) {
            });
            _this.loadingProvider.dismissMyLoading();
        });
    };
    ImagehandlerProvider.prototype.guid = function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    };
    ImagehandlerProvider.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    ImagehandlerProvider.prototype.gpuploadimage = function (groupname) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var actionSheet = _this.actionSheetCtrl.create({
                title: 'Select Image From',
                buttons: [{
                        text: 'Load from Gallery',
                        handler: function () {
                            _this.gptakePicture(_this.camera.PictureSourceType.PHOTOLIBRARY, _this.camera.DestinationType.FILE_URI, groupname).then(function (data) {
                                resolve(data);
                            }).catch(function (err) {
                                reject(err);
                            });
                        }
                    }, {
                        text: 'Camera',
                        handler: function () {
                            _this.gptakePicture(_this.camera.PictureSourceType.CAMERA, _this.camera.DestinationType.FILE_URI, groupname).then(function (data) {
                                resolve(data);
                            }).catch(function (err) {
                                reject(err);
                            });
                        }
                    }, {
                        text: 'Cancel',
                        role: 'cancel'
                    }
                ]
            });
            actionSheet.present();
        });
    };
    ImagehandlerProvider.prototype.gptakePicture = function (sourceType, destinationType, groupname) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 100,
            sourceType: sourceType,
            destinationType: destinationType,
            saveToPhotoAlbum: false,
            allowEdit: true,
            targetWidth: 500,
            targetHeight: 500,
            correctOrientation: true
        };
        return new Promise(function (resolve, reject) {
            _this.camera.getPicture(options).then(function (url) {
                _this.loadingProvider.presentLoading();
                window.resolveLocalFileSystemURL(url, function (res) {
                    res.file(function (resFile) {
                        var reader = new FileReader();
                        reader.readAsArrayBuffer(resFile);
                        reader.onloadend = function (evt) {
                            var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                            var imageStore = _this.firestore.ref('/groupimages').child(_this.userId).child(groupname);
                            imageStore.put(imgBlob).then(function (res) {
                                _this.firestore.ref('/groupimages').child(_this.userId).child(groupname).getDownloadURL().then(function (url) {
                                    resolve(url);
                                }).catch(function (err) {
                                    _this.loadingProvider.dismissMyLoading();
                                    reject(err);
                                });
                            }).catch(function (err) {
                                _this.loadingProvider.dismissMyLoading();
                                reject(err);
                            });
                        };
                    });
                });
            }, function (err) {
                reject(err);
            });
        });
    };
    ImagehandlerProvider.prototype.get_url_extension = function (url) {
        return url.split(/\#|\?/)[0].split('.').pop().trim();
    };
    ImagehandlerProvider.prototype.selectDocument = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.chooser.getFile('application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet').then(function (url) {
                if ((url.mediaType == 'application/octet-stream' && _this.get_url_extension(url.uri) == 'txt') || (url.mediaType == 'application/octet-stream' && _this.get_url_extension(url.uri) == 'xls') || (url.mediaType == 'application/octet-stream' && _this.get_url_extension(url.uri) == 'xlsx') ||
                    (url.mediaType == 'application/octet-stream' && _this.get_url_extension(url.uri) == 'doc') ||
                    (url.mediaType == 'application/octet-stream' && _this.get_url_extension(url.uri) == 'docx') ||
                    (url.mediaType == 'application/octet-stream' && _this.get_url_extension(url.uri) == 'pdf') || url.mediaType == 'application/pdf' || url.mediaType == 'application/msword' || url.mediaType == 'application/vnd.ms-excel' || url.mediaType == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || url.mediaType == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                    _this.loadingProvider.presentLoading();
                    window.resolveLocalFileSystemURL(url.uri, function (res) {
                        res.file(function (resFile) {
                            var reader = new FileReader();
                            reader.readAsArrayBuffer(resFile);
                            reader.onloadend = function (evt) {
                                var imgBlob = new Blob([evt.target.result], { type: 'document' });
                                var uuid = _this.guid();
                                var imageStore = _this.firestore.ref('/docs').child(_this.userId).child('doc' + uuid);
                                imageStore.put(imgBlob).then(function (res) {
                                    _this.loadingProvider.dismissMyLoading();
                                    resolve(res.downloadURL);
                                }).catch(function (err) {
                                    _this.loadingProvider.dismissMyLoading();
                                    reject(err);
                                }).catch(function (err) {
                                    _this.loadingProvider.dismissMyLoading();
                                    reject(err);
                                });
                            };
                        });
                    });
                }
                else {
                    _this.presentToast('Upload only document files.');
                }
            }).catch(function (err) {
                _this.loadingProvider.dismissMyLoading();
            });
        });
    };
    ImagehandlerProvider.prototype.openDocument = function (path) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var browser = _this.iab.create(path, "_system", { location: "yes" });
            browser.on('loadstop').subscribe(function (event) {
                resolve("File opened");
            });
        });
    };
    ImagehandlerProvider.prototype.recordAudio = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.mediaCapture.captureAudio().then(function (data) {
                _this.loadingProvider.presentLoading();
                window.resolveLocalFileSystemURL(data[0]["localURL"], function (res) {
                    res.file(function (resFile) {
                        var reader = new FileReader();
                        reader.readAsArrayBuffer(resFile);
                        reader.onloadend = function (evt) {
                            var imgBlob = new Blob([evt.target.result], { type: 'audio' });
                            var uuid = _this.guid();
                            var imageStore = _this.firestore.ref('/audios').child(_this.userId).child('audio' + uuid);
                            imageStore.put(imgBlob).then(function (res) {
                                _this.loadingProvider.dismissMyLoading();
                                resolve(res.downloadURL);
                            }).catch(function (err) {
                                _this.loadingProvider.dismissMyLoading();
                                reject(err);
                            }).catch(function (err) {
                                _this.loadingProvider.dismissMyLoading();
                                reject(err);
                            });
                        };
                    });
                });
            });
            _this.loadingProvider.dismissMyLoading();
        });
    };
    ImagehandlerProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_image_picker__["a" /* ImagePicker */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_media_capture__["a" /* MediaCapture */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_chooser__["a" /* Chooser */],
            __WEBPACK_IMPORTED_MODULE_8__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], ImagehandlerProvider);
    return ImagehandlerProvider;
}());

//# sourceMappingURL=imagehandler.js.map

/***/ }),

/***/ 408:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SmsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_sms__ = __webpack_require__(352);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



 //mytodo: this should not come here. remove it and put it in parent
var SmsProvider = (function () {
    function SmsProvider(platform, sms, tpStorageService) {
        this.platform = platform;
        this.sms = sms;
        this.tpStorageService = tpStorageService;
        this.iosDelay = 300;
        this.simulateSMS = false;
        this.options = {
            replaceLineBreaks: false,
            android: {
                intent: 'INTENT' // send SMS with the native android SMS messaging
                //intent: '' // send SMS without opening any other app
            }
        };
    }
    SmsProvider.prototype.sendSmsCustomMsg = function (mobile, msg) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.simulateSMS) {
                resolve(true);
            }
            else {
                try {
                    //timeout to prevent issue we are getting on IOS device. Keyboard dissapear : https://github.com/cordova-sms/cordova-sms-plugin/issues/22
                    if (_this.platform.is('ios')) {
                        setTimeout(function () {
                            this.sms.send(mobile, msg, this.options);
                        }, _this.iosDelay);
                        resolve(true);
                    }
                    else {
                        _this.sms.send(mobile, msg, _this.options).then(function (res) {
                            resolve(true);
                        }).catch(function (err) {
                            reject(err);
                        });
                    } //endif
                }
                catch (e) {
                }
            } //endif
        });
    }; //end function
    SmsProvider.prototype.sendSms = function (mobileIn) {
        var _this = this;
        var mobile = mobileIn.toString();
        return new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('business_created').then(function (business_created_raw) {
                //business created
                var msg = "Hi, I'm offering referral incentives. Please download my app at tapally.com and start earning";
                //timeout to prevent issue we are getting on IOS device. Keyboard dissapear : https://github.com/cordova-sms/cordova-sms-plugin/issues/22
                if (_this.platform.is('ios')) {
                    setTimeout(function () {
                        this.sms.send(mobile, msg, this.options);
                    }, _this.iosDelay);
                    resolve(true);
                }
                else {
                    _this.sms.send(mobile, msg, _this.options).then(function (res) {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                } //endif
            }).catch(function (e) {
                //no business registered for this user
                var msg = "Hi, please download app at TapAlly.com and start earning";
                //timeout to prevent issue we are getting on IOS device. Keyboard dissapear : https://github.com/cordova-sms/cordova-sms-plugin/issues/22
                if (_this.platform.is('ios')) {
                    setTimeout(function () {
                        this.sms.send(mobile, msg, this.options);
                    }, _this.iosDelay);
                    resolve(true);
                }
                else {
                    _this.sms.send(mobile, msg, _this.options).then(function (res) {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                } //endif
            });
        });
    }; //end function
    SmsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["x" /* Platform */], __WEBPACK_IMPORTED_MODULE_1__ionic_native_sms__["a" /* SMS */], __WEBPACK_IMPORTED_MODULE_3__tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], SmsProvider);
    return SmsProvider;
}()); //end class

//# sourceMappingURL=sms.js.map

/***/ }),

/***/ 410:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CatProvider; });
var CatProvider = (function () {
    function CatProvider() {
    }
    //Master sheet at
    //https://docs.google.com/spreadsheets/d/1M4vrnEUQYYV-AVcqTySOnE2zrQYEzJBP-wOUw7BAybg/edit?usp=sharing
    CatProvider.prototype.getCats = function () {
        return [
            { "id": 0, "name": "	Uncategorized	", "sort": 0 },
            { "id": 1, "name": "	Accountants	", "sort": 1 },
            { "id": 2, "name": "	Insurance Agents	", "sort": 2 },
            { "id": 3, "name": "	Real Estate Agents	", "sort": 3 },
            { "id": 4, "name": "	Mortgage/ Loan Agents	", "sort": 4 },
            { "id": 5, "name": "	Photographers/ Videographers/ DJs	", "sort": 5 },
            { "id": 6, "name": "	Lawyers	", "sort": 6 },
            { "id": 7, "name": "	Dentists	", "sort": 7 },
            { "id": 8, "name": "	Plumbers	", "sort": 8 },
            { "id": 9, "name": "	Construction/ Renovation	", "sort": 9 },
            { "id": 10, "name": "	Painters Paralegals/ Traffic Tickets	", "sort": 10 },
            { "id": 11, "name": "	Electricians	", "sort": 11 },
            { "id": 12, "name": "	Travel Tickets Agents	", "sort": 12 },
            { "id": 13, "name": "	Appliances Sales & Service	", "sort": 13 },
            { "id": 14, "name": "	Architect/ Engineer	", "sort": 14 },
            { "id": 15, "name": "	Auto Repair & Sales	", "sort": 15 },
            { "id": 16, "name": "	Ayurvedic, Herbal & Nutritional products	", "sort": 16 },
            { "id": 17, "name": "	Bankruptcy/ Debt Consolidation	", "sort": 17 },
            { "id": 18, "name": "	Banquet Halls & Convention Centre	", "sort": 18 },
            { "id": 19, "name": "	Beauty Parlor/ Salon/ Spa	", "sort": 19 },
            { "id": 20, "name": "	Car/ Truck Rental	", "sort": 20 },
            { "id": 21, "name": "	Carpet/ Flooring Stores	", "sort": 21 },
            { "id": 22, "name": "	Cheque Cashing/ Cash for Gold	", "sort": 22 },
            { "id": 23, "name": "	Chiropractor/ Physiotherapist	", "sort": 23 },
            { "id": 24, "name": "	Clothing Stores/ Boutiques	", "sort": 24 },
            { "id": 25, "name": "	Concrete/ Paving	", "sort": 25 },
            { "id": 26, "name": "	Drapery/ Blinds/ Shutters	", "sort": 26 },
            { "id": 27, "name": "	Duct/ Carpet Cleaning	", "sort": 27 },
            { "id": 28, "name": "	Foreign Exchange	", "sort": 28 },
            { "id": 29, "name": "	Furniture/ Mattress Stores	", "sort": 29 },
            { "id": 30, "name": "	Garage Doors	", "sort": 30 },
            { "id": 31, "name": "	Gift Stores	", "sort": 31 },
            { "id": 32, "name": "	Heating/ Air Conditioning	", "sort": 32 },
            { "id": 33, "name": "	Homeopathy	", "sort": 33 },
            { "id": 34, "name": "	Immigration Consultants	", "sort": 34 },
            { "id": 35, "name": "	Interpreters/ Translators	", "sort": 35 },
            { "id": 36, "name": "	Jewellery Shops	", "sort": 36 },
            { "id": 37, "name": "	Kitchen Cabinets/ Woodworking	", "sort": 37 },
            { "id": 38, "name": "	Landscaping/ Snow Removal	", "sort": 38 },
            { "id": 39, "name": "	Locksmith	", "sort": 39 },
            { "id": 40, "name": "	Limousine	", "sort": 40 },
            { "id": 41, "name": "	Media	", "sort": 41 },
            { "id": 42, "name": "	Moving/ Storage	", "sort": 42 },
            { "id": 43, "name": "	Optical Stores/ Opticians	", "sort": 43 },
            { "id": 44, "name": "	Pest Control	", "sort": 44 },
            { "id": 45, "name": "	Printers/ Signs	", "sort": 45 },
            { "id": 46, "name": "	Real Estate Brokers/ Agents	", "sort": 46 },
            { "id": 47, "name": "	Refrigeration Solutions	", "sort": 47 },
            { "id": 48, "name": "	Restaurants/ Sweet Shops	", "sort": 48 },
            { "id": 49, "name": "	Roofing	", "sort": 49 },
            { "id": 50, "name": "	Staffing/ Job/ Employment Agency	", "sort": 50 },
            { "id": 51, "name": "	Security Cameras/ Computers/ Networking	", "sort": 51 },
            { "id": 52, "name": "	Solar Panels	", "sort": 52 },
            { "id": 53, "name": "	Telephone /Cell/ Mobile Phones	", "sort": 53 },
            { "id": 54, "name": "	Tent Services	", "sort": 54 },
            { "id": 55, "name": "	Tile/ Granite/ Marble	", "sort": 55 },
            { "id": 56, "name": "	Towing	", "sort": 56 },
            { "id": 57, "name": "	Training Institutes/ Colleges	", "sort": 57 },
            { "id": 58, "name": "	Trucking Industry	", "sort": 58 },
            { "id": 59, "name": "	Tutors	", "sort": 59 },
            { "id": 60, "name": "	TV Repair/ Electronic Parts	", "sort": 60 },
            { "id": 61, "name": "	Wedding Planners	", "sort": 61 },
            { "id": 62, "name": "	Windows/ Doors	", "sort": 62 }
        ];
    };
    return CatProvider;
}());

//# sourceMappingURL=cats.js.map

/***/ }),

/***/ 411:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReferralincentiveTypeProvider; });
var ReferralincentiveTypeProvider = (function () {
    function ReferralincentiveTypeProvider() {
    }
    ReferralincentiveTypeProvider.prototype.getList = function () {
        return [
            { "id": 0, "name": "	None", "sort": 0 },
            { "id": 1, "name": "	Cash", "sort": 1 },
            { "id": 2, "name": "	Gift Card", "sort": 2 },
            { "id": 3, "name": "	Extra service", "sort": 3 },
            { "id": 4, "name": "	Other", "sort": 4 },
        ];
    };
    return ReferralincentiveTypeProvider;
}());

//# sourceMappingURL=referralincentive-type.js.map

/***/ }),

/***/ 413:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_http__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthProvider = (function () {
    function AuthProvider(nativeHttp) {
        this.nativeHttp = nativeHttp;
        this.apiUrl = "https://api.authy.com/protected/json/phones/verification/start";
    }
    AuthProvider.prototype.sendVerificationCode = function (data, type) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.nativeHttp.post(_this.apiUrl, data, {})
                .then(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    AuthProvider.prototype.otpVerify = function (type) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.nativeHttp.get("https://api.authy.com/protected/json/" + type, {}, {})
                .then(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
                console.log(err);
            });
        });
    };
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_http__["a" /* HTTP */]])
    ], AuthProvider);
    return AuthProvider;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 416:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(421);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 421:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_img_viewer__ = __webpack_require__(409);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser_animations__ = __webpack_require__(756);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(758);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_angularfireconfig__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_angularfire2_auth__ = __webpack_require__(250);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_angularfire2__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_angularfire2_firestore__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_sqlite__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_firebase__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_camera__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_contacts__ = __webpack_require__(242);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_firebase__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_sms__ = __webpack_require__(352);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_groups_groups__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_image_picker__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_in_app_browser__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_media_capture__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_geolocation__ = __webpack_require__(414);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_splash_screen__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_status_bar__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_storage__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_native_storage__ = __webpack_require__(256);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__providers_contact_contact__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__providers_loading_loading__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__providers_tpstorage_tpstorage__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__providers_auth_auth__ = __webpack_require__(413);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__providers_fcm_fcm__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__providers_imagehandler_imagehandler__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__providers_requests_requests__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__providers_chat_chat__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__providers_sms_sms__ = __webpack_require__(408);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__providers_cats_cats__ = __webpack_require__(410);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39_ionic_native_http_connection_backend__ = __webpack_require__(759);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__ionic_native_http__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__providers_gpmessage_gpmessage__ = __webpack_require__(764);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__ionic_native_chooser__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__providers_referralincentive_type_referralincentive_type__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44_ionic_image_loader__ = __webpack_require__(412);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// Ionic modules










//import { File } from '@ionic-native/file/ngx';
// Entry component

// Firebase config

// Angular fireauth




// Firebase javascript

// Ionic native













// Custom providers

















if (true) {
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_14" /* enableProdMode */])();
}
// Firebase config app initialize
__WEBPACK_IMPORTED_MODULE_14_firebase__["initializeApp"](__WEBPACK_IMPORTED_MODULE_9__app_angularfireconfig__["b" /* config */]);
var AppModule = (function () {
    // File
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */]],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_11_angularfire2__["a" /* AngularFireModule */].initializeApp(__WEBPACK_IMPORTED_MODULE_9__app_angularfireconfig__["b" /* config */]),
                __WEBPACK_IMPORTED_MODULE_12_angularfire2_firestore__["b" /* AngularFirestoreModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["c" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_http__["d" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["b" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_39_ionic_native_http_connection_backend__["c" /* NativeHttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */], {
                    tabsPlacement: 'top',
                    pageTransition: 'md-transition',
                    animate: true
                }, {
                    links: [
                        { loadChildren: '../pages/add-contact/add-contact.module#AddContactPageModule', name: 'AddContactPage', segment: 'add-contact', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/allbuddy-contacts/allbuddy-contacts.module#AllbuddyContactsPageModule', name: 'AllbuddyContactsPage', segment: 'allbuddy-contacts', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/allcontacts/allcontacts.module#AllcontactsPageModule', name: 'AllcontactsPage', segment: 'allcontacts', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/allimg/allimg.module#AllimgPageModule', name: 'AllimgPage', segment: 'allimg', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/attachments/attachments.module#AttachmentsPageModule', name: 'AttachmentsPage', segment: 'attachments', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/block-user/block-user.module#BlockUserPageModule', name: 'BlockUserPage', segment: 'block-user', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/broadcast-submitted/broadcast-submitted.module#BroadcastSubmittedPageModule', name: 'BroadcastSubmittedPage', segment: 'broadcast-submitted', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/broadcast/broadcast.module#BroadcastPageModule', name: 'BroadcastPage', segment: 'broadcast', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/buddies/buddies.module#BuddiesPageModule', name: 'BuddiesPage', segment: 'buddies', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/buddychat/buddychat.module#BuddychatPageModule', name: 'BuddychatPage', segment: 'buddychat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/businessinfo/businessinfo.module#BusinessinfoPageModule', name: 'BusinessinfoPage', segment: 'businessinfo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/businessinfoother/businessinfoother.module#BusinessinfootherPageModule', name: 'BusinessinfootherPage', segment: 'businessinfoother', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/checkreferralincentive/checkreferralincentive.module#CheckreferralincentivePageModule', name: 'CheckreferralincentivePage', segment: 'checkreferralincentive', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chats/chats.module#ChatsPageModule', name: 'ChatsPage', segment: 'chats', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/consumerinfo/consumerinfo.module#ConsumerinfoPageModule', name: 'ConsumerinfoPage', segment: 'consumerinfo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact/contact.module#ContactPageModule', name: 'ContactPage', segment: 'contact', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contactimportprogress/contactimportprogress.module#ContactimportprogressPageModule', name: 'ContactimportprogressPage', segment: 'contactimportprogress', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/country-list/country-list.module#CountryListPageModule', name: 'CountryListPage', segment: 'country-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/disc/disc.module#DiscPageModule', name: 'DiscPage', segment: 'disc', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/displayname/displayname.module#DisplaynamePageModule', name: 'DisplaynamePage', segment: 'displayname', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/earnings/earnings.module#EarningsPageModule', name: 'EarningsPage', segment: 'earnings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/extra/extra.module#ExtraPageModule', name: 'ExtraPage', segment: 'extra', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/friend-asking-referral/friend-asking-referral.module#FriendAskingReferralPageModule', name: 'FriendAskingReferralPage', segment: 'friend-asking-referral', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/friend-list/friend-list.module#FriendListPageModule', name: 'FriendListPage', segment: 'friend-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/gpattachments/gpattachments.module#GpattachmentsPageModule', name: 'GpattachmentsPage', segment: 'gpattachments', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/group-chat-popover/group-chat-popover.module#GroupChatPopoverPageModule', name: 'GroupChatPopoverPage', segment: 'group-chat-popover', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groupbuddies/groupbuddies.module#GroupbuddiesPageModule', name: 'GroupbuddiesPage', segment: 'groupbuddies', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groupbuddiesadd/groupbuddiesadd.module#GroupbuddiesaddPageModule', name: 'GroupbuddiesaddPage', segment: 'groupbuddiesadd', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groupchat/groupchat.module#GroupchatPageModule', name: 'GroupchatPage', segment: 'groupchat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groupinfo/groupinfo.module#GroupinfoPageModule', name: 'GroupinfoPage', segment: 'groupinfo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groupmembers/groupmembers.module#GroupmembersPageModule', name: 'GroupmembersPage', segment: 'groupmembers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groups/groups.module#GroupsPageModule', name: 'GroupsPage', segment: 'groups', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home-after-i-need-today/home-after-i-need-today.module#HomeAfterINeedTodayPageModule', name: 'HomeAfterINeedTodayPage', segment: 'home-after-i-need-today', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/howcustomerseeadd/howcustomerseeadd.module#HowcustomerseeaddPageModule', name: 'HowcustomerseeaddPage', segment: 'howcustomerseeadd', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/importp/importp.module#ImportpPageModule', name: 'ImportpPage', segment: 'importp', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/incentivedetail/incentivedetail.module#IncentivedetailPageModule', name: 'IncentivedetailPage', segment: 'incentivedetail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/invite-friend/invite-friend.module#InviteFriendPageModule', name: 'InviteFriendPage', segment: 'invite-friend', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/invitemanually/invitemanually.module#InvitemanuallyPageModule', name: 'InvitemanuallyPage', segment: 'invitemanually', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/invitepeople/invitepeople.module#InvitepeoplePageModule', name: 'InvitepeoplePage', segment: 'invitepeople', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/map/map.module#MapPageModule', name: 'MapPage', segment: 'map', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/menu-block/menu-block.module#MenuBlockPageModule', name: 'MenuBlockPage', segment: 'menu-block', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/menu/menu.module#MenuPageModule', name: 'MenuPage', segment: 'menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notification/notification.module#NotificationPageModule', name: 'NotificationPage', segment: 'notification', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/outreach-for-business/outreach-for-business.module#OutreachForBusinessPageModule', name: 'OutreachForBusinessPage', segment: 'outreach-for-business', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/phone/phone.module#PhonePageModule', name: 'PhonePage', segment: 'phone', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/phoneverfy/phoneverfy.module#PhoneverfyPageModule', name: 'PhoneverfyPage', segment: 'phoneverfy', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pick-contact-from/pick-contact-from.module#PickContactFromPageModule', name: 'PickContactFromPage', segment: 'pick-contact-from', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pick-contact-to/pick-contact-to.module#PickContactToPageModule', name: 'PickContactToPage', segment: 'pick-contact-to', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/popover-chat/popover-chat.module#PopoverChatPageModule', name: 'PopoverChatPage', segment: 'popover-chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile-view/profile-view.module#ProfileViewPageModule', name: 'ProfileViewPage', segment: 'profile-view', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profilepic/profilepic.module#ProfilepicPageModule', name: 'ProfilepicPage', segment: 'profilepic', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/referral-infopage/referral-infopage.module#ReferralInfopagePageModule', name: 'ReferralInfopagePage', segment: 'referral-infopage', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/referral/referral.module#ReferralPageModule', name: 'ReferralPage', segment: 'referral', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/referralreceived/referralreceived.module#ReferralreceivedPageModule', name: 'ReferralreceivedPage', segment: 'referralreceived', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/registerbusiness/registerbusiness.module#RegisterbusinessPageModule', name: 'RegisterbusinessPage', segment: 'registerbusiness', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/request-after-selected/request-after-selected.module#RequestAfterSelectedPageModule', name: 'RequestAfterSelectedPage', segment: 'request-after-selected', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/request-status/request-status.module#RequestStatusPageModule', name: 'RequestStatusPage', segment: 'request-status', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/request-submitted/request-submitted.module#RequestSubmittedPageModule', name: 'RequestSubmittedPage', segment: 'request-submitted', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/request/request.module#RequestPageModule', name: 'RequestPage', segment: 'request', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search-chats/search-chats.module#SearchChatsPageModule', name: 'SearchChatsPage', segment: 'search-chats', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search-group-chat/search-group-chat.module#SearchGroupChatPageModule', name: 'SearchGroupChatPage', segment: 'search-group-chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search-next-send/search-next-send.module#SearchNextSendPageModule', name: 'SearchNextSendPage', segment: 'search-next-send', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search-next/search-next.module#SearchNextPageModule', name: 'SearchNextPage', segment: 'search-next', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/send-referral-backward/send-referral-backward.module#SendReferralBackwardPageModule', name: 'SendReferralBackwardPage', segment: 'send-referral-backward', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/send-referral-filter/send-referral-filter.module#SendReferralFilterPageModule', name: 'SendReferralFilterPage', segment: 'send-referral-filter', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/send-referral-forward/send-referral-forward.module#SendReferralForwardPageModule', name: 'SendReferralForwardPage', segment: 'send-referral-forward', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/setting/setting.module#SettingPageModule', name: 'SettingPage', segment: 'setting', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/show-contact/show-contact.module#ShowContactPageModule', name: 'ShowContactPage', segment: 'show-contact', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tabs/tabs.module#TabsPageModule', name: 'TabsPage', segment: 'tabs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tutorial/tutorial.module#TutorialPageModule', name: 'TutorialPage', segment: 'tutorial', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/username/username.module#UsernamePageModule', name: 'UsernamePage', segment: 'username', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/view-buddy/view-buddy.module#ViewBuddyPageModule', name: 'ViewBuddyPage', segment: 'view-buddy', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/viewandredeem/viewandredeem.module#ViewandredeemPageModule', name: 'ViewandredeemPage', segment: 'viewandredeem', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_26__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_44_ionic_image_loader__["a" /* IonicImageLoader */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_3_ionic_img_viewer__["b" /* IonicImageViewerModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* IonicApp */]],
            entryComponents: [__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */]],
            providers: [
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_native_storage__["a" /* NativeStorage */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* IonicErrorHandler */] },
                { provide: __WEBPACK_IMPORTED_MODULE_13__ionic_native_sqlite__["a" /* SQLite */], useClass: __WEBPACK_IMPORTED_MODULE_13__ionic_native_sqlite__["a" /* SQLite */] },
                __WEBPACK_IMPORTED_MODULE_31__providers_auth_auth__["a" /* AuthProvider */],
                __WEBPACK_IMPORTED_MODULE_33__providers_user_user__["a" /* UserProvider */],
                __WEBPACK_IMPORTED_MODULE_29__providers_loading_loading__["a" /* LoadingProvider */],
                __WEBPACK_IMPORTED_MODULE_30__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */],
                __WEBPACK_IMPORTED_MODULE_42__ionic_native_chooser__["a" /* Chooser */],
                __WEBPACK_IMPORTED_MODULE_10_angularfire2_auth__["a" /* AngularFireAuth */],
                __WEBPACK_IMPORTED_MODULE_34__providers_imagehandler_imagehandler__["a" /* ImagehandlerProvider */],
                __WEBPACK_IMPORTED_MODULE_35__providers_requests_requests__["a" /* RequestsProvider */],
                __WEBPACK_IMPORTED_MODULE_36__providers_chat_chat__["a" /* ChatProvider */],
                __WEBPACK_IMPORTED_MODULE_28__providers_contact_contact__["a" /* ContactProvider */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_contacts__["b" /* Contacts */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_32__providers_fcm_fcm__["a" /* FcmProvider */],
                __WEBPACK_IMPORTED_MODULE_38__providers_cats_cats__["a" /* CatProvider */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_firebase__["a" /* Firebase */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_sms__["a" /* SMS */],
                __WEBPACK_IMPORTED_MODULE_37__providers_sms_sms__["a" /* SmsProvider */],
                __WEBPACK_IMPORTED_MODULE_19__providers_groups_groups__["a" /* GroupsProvider */],
                __WEBPACK_IMPORTED_MODULE_40__ionic_native_http__["a" /* HTTP */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_image_picker__["a" /* ImagePicker */],
                { provide: __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpBackend */], useClass: __WEBPACK_IMPORTED_MODULE_39_ionic_native_http_connection_backend__["b" /* NativeHttpFallback */], deps: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["x" /* Platform */], __WEBPACK_IMPORTED_MODULE_39_ionic_native_http_connection_backend__["a" /* NativeHttpBackend */], __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["g" /* HttpXhrBackend */]] },
                __WEBPACK_IMPORTED_MODULE_41__providers_gpmessage_gpmessage__["a" /* GpmessageProvider */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_media_capture__["a" /* MediaCapture */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_43__providers_referralincentive_type_referralincentive_type__["a" /* ReferralincentiveTypeProvider */],
            ]
        })
        // File
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 49:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(250);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_angularfireconfig__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_sqlite__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_const__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_tpstorage_tpstorage__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_http__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { Storage } from '@ionic/storage';





var UserProvider = (function () {
    function UserProvider(sqlite, afireAuth, 
        //public storage: Storage,
        platform, events, http, tpStorageService) {
        var _this = this;
        this.sqlite = sqlite;
        this.afireAuth = afireAuth;
        this.platform = platform;
        this.events = events;
        this.http = http;
        this.tpStorageService = tpStorageService;
        this.firedata = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/chatusers');
        this.firefriend = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/friends');
        this.firebuddychat = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/buddychats');
        this.users = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/users');
        this.userstatus = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/userstatus');
        this.firereq = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/requests');
        this.firenotify = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/notification');
        this.fireBusiness = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/business');
        //fireInvitationSent = firebase.database().ref('/invitationsent');
        this.firePhones = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/phones');
        this.fireWebsiteUsers = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/website_users');
        this.fireReferral_sent = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/referral_sent');
        this.fireReferral_received = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/referral_received');
        this.isUserExits = true;
        this.blockUsers = [];
        this.unblockUsers = [];
        this.isuserBlock = false;
        this.blockUsersCounter = 0;
        this.platform.ready().then(function (readySource) {
            _this.tpStorageService.getItem('userUID').then(function (res) {
                if (res) {
                    _this.userId = res;
                }
                if (!_this.userId) {
                    _this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                }
                ////console.log ("this.userIdthis.userIdthis.userIdthis.userId:" + this.userId);
            }).catch(function (e) {
                if (__WEBPACK_IMPORTED_MODULE_2_firebase___default.a && __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser) {
                    _this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                }
            });
            _this.tpStorageService.getItem('userName').then(function (res) {
                if (res) {
                    _this.userName = res;
                }
                else {
                    _this.userName = _this.afireAuth.auth.currentUser.displayName;
                }
            }).catch(function (e) {
                if (_this.afireAuth && _this.afireAuth.auth && _this.afireAuth.auth.currentUser) {
                    _this.userName = _this.afireAuth.auth.currentUser.displayName;
                }
            });
            _this.tpStorageService.getItem('userPic').then(function (res) {
                if (res) {
                    _this.userPic = res;
                }
                else {
                    _this.userPic = _this.afireAuth.auth.currentUser.photoURL;
                }
            }).catch(function (e) {
                if (_this.afireAuth && _this.afireAuth.auth && _this.afireAuth.auth.currentUser) {
                    _this.userPic = _this.afireAuth.auth.currentUser.photoURL;
                }
            });
        });
    }
    UserProvider.prototype.initializeItem = function (userID) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firenotify.child(userID).child('isNotify').once('value', function (snapshot) {
                _this.isNotify = snapshot.val();
                resolve(_this.isNotify);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    UserProvider.prototype.adduser = function (newuser) {
        var _this = this;
        if (this.userId) {
            return this.adduser_(newuser);
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (res) {
                if (res) {
                    _this.userId = res;
                }
                if (!_this.userId) {
                    _this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                }
                return _this.adduser_(newuser);
            }).catch(function (e) {
                if (__WEBPACK_IMPORTED_MODULE_2_firebase___default.a && __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser) {
                    _this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                }
                return _this.adduser_(newuser);
            });
        }
    };
    //mytodo : is this being used anywhere ?
    UserProvider.prototype.adduser_ = function (newuser) {
        var _this = this;
        var myuserPic = __WEBPACK_IMPORTED_MODULE_4__app_app_angularfireconfig__["e" /* userPicArr */][Math.floor(Math.random() * Object.keys(__WEBPACK_IMPORTED_MODULE_4__app_app_angularfireconfig__["e" /* userPicArr */]).length - 1)];
        var promise = new Promise(function (resolve, reject) {
            _this.afireAuth.auth.createUserWithEmailAndPassword(newuser.email, newuser.password).then(function () {
                _this.afireAuth.auth.currentUser.updateProfile({
                    displayName: newuser.username,
                    photoURL: myuserPic
                }).then(function () {
                    _this.firedata.child(_this.userId).set({
                        uid: _this.userId,
                        displayName: newuser.username,
                        photoURL: myuserPic,
                        invited_by: "none"
                    }).then(function () {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    //This doesnt look like being used
    UserProvider.prototype.addmobileUser = function (verificationCredential, code, mobile) {
        var _this = this;
        var myuserPic = __WEBPACK_IMPORTED_MODULE_4__app_app_angularfireconfig__["e" /* userPicArr */][Math.floor(Math.random() * Object.keys(__WEBPACK_IMPORTED_MODULE_4__app_app_angularfireconfig__["e" /* userPicArr */]).length - 1)];
        return new Promise(function (resolve, reject) {
            _this.tpStorageService.setItem('verificationCredential', verificationCredential);
            _this.tpStorageService.setItem('code', code);
            var signInData = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth.PhoneAuthProvider.credential(verificationCredential, code);
            ////console.log ("signInData");
            ////console.log (signInData);
            _this.afireAuth.auth.signInWithCredential(signInData).then(function (info) {
                var profileImage;
                if (_this.afireAuth.auth.currentUser.photoURL != null) {
                    profileImage = _this.afireAuth.auth.currentUser.photoURL;
                }
                else {
                    profileImage = myuserPic;
                }
                _this.afireAuth.auth.currentUser.updateProfile({ displayName: _this.afireAuth.auth.currentUser.displayName, photoURL: profileImage }).then(function () {
                    _this.firedata.child(_this.afireAuth.auth.currentUser.uid).set({
                        uid: _this.afireAuth.auth.currentUser.uid,
                        mobile: mobile,
                        countryCode: verificationCredential,
                        displayName: _this.afireAuth.auth.currentUser.displayName,
                        disc: '',
                        photoURL: profileImage,
                        deviceToken: ''
                    }).then(function () {
                        resolve({ success: true });
                    }).catch(function (err) {
                        reject(err);
                    });
                }).catch(function (err) {
                });
            }).catch(function (err) {
                resolve({ success: false, msg: JSON.stringify(err.message) });
            });
        });
    };
    UserProvider.prototype.addmobileUserIOS = function (countryCode, mobile) {
        var _this = this;
        var myuserPic = __WEBPACK_IMPORTED_MODULE_4__app_app_angularfireconfig__["e" /* userPicArr */][Math.floor(Math.random() * Object.keys(__WEBPACK_IMPORTED_MODULE_4__app_app_angularfireconfig__["e" /* userPicArr */]).length - 1)];
        return new Promise(function (resolve, reject) {
            _this.tpStorageService.setItem('verificationCredential', countryCode);
            _this.tpStorageService.setItem('code', mobile);
            _this.tpStorageService.setItem('fullPhoneNumber', countryCode + mobile);
            //this.firedata.once('value', (snep) => {
            /*let allData = snep.val();
            for (let tmpkey in allData) {
              if (allData[tmpkey].mobile == mobile) {
                this.isUserExits = false;
                this.tpStorageService.setItem('userUID', allData[tmpkey].uid);
              }
            }*/
            if (_this.isUserExits) {
                _this.afireAuth.auth.signInAnonymously().then(function (info) {
                    var profileImage;
                    if (_this.afireAuth.auth.currentUser.photoURL != null) {
                        profileImage = _this.afireAuth.auth.currentUser.photoURL;
                    }
                    else {
                        profileImage = myuserPic;
                    }
                    _this.tpStorageService.setItem('userUID', _this.afireAuth.auth.currentUser.uid);
                    _this.afireAuth.auth.currentUser.updateProfile({
                        displayName: _this.afireAuth.auth.currentUser.displayName,
                        photoURL: profileImage
                    }).then(function () {
                        _this.http.get('https://tapally.com/wp-json/log/v1/createuser?phone=' + mobile + '&countrycode=' + countryCode + '&uid=' + _this.afireAuth.auth.currentUser.uid).map(function (res) { return res.json(); }).subscribe(function (dddd) {
                        }, function (err) {
                            console.log("Error 89123");
                            console.log(err);
                        });
                        _this.firedata.child(_this.afireAuth.auth.currentUser.uid).set({
                            uid: _this.afireAuth.auth.currentUser.uid,
                            mobile: mobile,
                            countryCode: countryCode,
                            displayName: _this.afireAuth.auth.currentUser.displayName,
                            disc: '',
                            photoURL: profileImage,
                            deviceToken: '',
                            timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP
                        }).then(function () {
                            //Check if user is invited by someone else
                            _this.firePhones.child(mobile).once('value', function (snapshot) {
                                var phoneNumberOb = snapshot.val();
                                if (phoneNumberOb) {
                                    if (phoneNumberOb.uid) {
                                        //it already have uid so this means that this user is trying to register again perhaps he/she uninstalled the app
                                        //Lets remove some pieces of his old profile
                                        _this.removeOldProfile(phoneNumberOb.uid, _this.afireAuth.auth.currentUser.uid);
                                        //update business profile with new value
                                        _this.fireBusiness.child(phoneNumberOb.uid).once('value', function (snapshot_business) {
                                            _this.fireBusiness.child(_this.afireAuth.auth.currentUser.uid).set(snapshot_business.val());
                                            _this.fireBusiness.child(phoneNumberOb.uid).remove();
                                        });
                                    }
                                    else {
                                        if (phoneNumberOb.invited_by) {
                                            //I was actually invited by someone so update both cache and database
                                            _this.tpStorageService.setItem('invited_by', phoneNumberOb.invited_by);
                                            _this.firedata.child(_this.afireAuth.auth.currentUser.uid).update({
                                                invited_by: phoneNumberOb.invited_by
                                            });
                                            //if i am invieted. lets make friends
                                            _this.acceptrequest(_this.afireAuth.auth.currentUser.uid, phoneNumberOb.invited_by, _this.afireAuth.auth.currentUser.displayName, mobile, countryCode, profileImage, __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP);
                                        } //endif
                                    }
                                    //Object is not properly set so set it
                                    _this.firePhones.child(mobile).update({
                                        uid: _this.afireAuth.auth.currentUser.uid,
                                    });
                                }
                                else {
                                    //Object does not exist
                                    _this.firePhones.child(mobile).update({
                                        uid: _this.afireAuth.auth.currentUser.uid,
                                    });
                                }
                            }).catch(function (err) {
                                //Object does not exist
                                _this.firePhones.child(mobile).update({
                                    uid: _this.afireAuth.auth.currentUser.uid,
                                });
                            });
                            resolve({ success: true });
                        }).catch(function (err) {
                            reject(err);
                        });
                    }).catch(function (err) {
                    });
                }).catch(function (err) {
                    resolve({ success: false, msg: JSON.stringify(err.message) });
                });
            }
            else {
                resolve({ success: true });
            }
            //});
        });
    };
    //duplicate function in requests.ts  but not entirly same
    UserProvider.prototype.acceptrequest = function (me_uid, buddy_uid, displayName, mobile, countryCode, profileImage, timestamp_) {
        var _this = this;
        this.userId = me_uid;
        this.getbuddydetails(buddy_uid).then(function (buddy) {
            //mytodo : update  deviceToken
            //this.getbuddydetails(me_uid).then((aboutMe: any) => {
            _this.firefriend.child(_this.userId).child(buddy.uid).set({
                uid: buddy.uid,
                displayName: buddy.displayName,
                photoURL: buddy.photoURL,
                isActive: 1,
                isBlock: false,
                deviceToken: buddy.deviceToken,
                countryCode: buddy.countryCode,
                disc: buddy.disc,
                mobile: buddy.mobile,
                timestamp: buddy.timestamp
            }).then(function () {
                _this.firefriend.child(buddy.uid).child(_this.userId).set({
                    uid: _this.userId,
                    displayName: displayName,
                    photoURL: profileImage,
                    isActive: 1,
                    isBlock: false,
                    deviceToken: "",
                    countryCode: countryCode,
                    disc: '',
                    mobile: mobile,
                    timestamp: timestamp_
                }).then(function () {
                });
            });
            //});
        });
    }; //end function
    //it remove and re-attach
    UserProvider.prototype.removeOldProfile = function (oldUid, newuid) {
        var _this = this;
        this.firefriend.child(oldUid).once('value', function (snapshot) {
            var allFriendsWithOldUid = snapshot.val();
            for (var key in allFriendsWithOldUid) {
                _this.firefriend.child(key).once('value', function (snapshot_friends) {
                    var allFriendsWithOldUid_friends = snapshot_friends.val();
                    for (var key_friends in allFriendsWithOldUid_friends) {
                        if (key_friends == oldUid) {
                            var pathToRemove = snapshot_friends.ref.path.toString();
                            pathToRemove = pathToRemove.replace('/friends/', '');
                            //console.log("Adding : "+"(pathToRemove"+"/"+this.afireAuth.auth.currentUser.uid);
                            //console.log (allFriendsWithOldUid_friends [oldUid]);
                            allFriendsWithOldUid_friends[oldUid].uid = _this.afireAuth.auth.currentUser.uid;
                            _this.firefriend.child(pathToRemove + "/" + _this.afireAuth.auth.currentUser.uid).set(allFriendsWithOldUid_friends[oldUid]);
                            _this.firefriend.child(pathToRemove + "/" + oldUid).remove();
                            break;
                        }
                    } //end for
                });
            } //end for
            //now change the old friend record to new record
            _this.firefriend.child(_this.afireAuth.auth.currentUser.uid).set(allFriendsWithOldUid);
            _this.firefriend.child(oldUid).remove();
        });
    };
    UserProvider.prototype.updateimage = function (args) {
        var _this = this;
        if (this.userId) {
            return this.updateimage_(args);
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (res) {
                if (res) {
                    _this.userId = res;
                }
                if (!_this.userId) {
                    _this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                }
                return _this.updateimage_(args);
            }).catch(function (e) {
                if (__WEBPACK_IMPORTED_MODULE_2_firebase___default.a && __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser) {
                    _this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                }
                return _this.updateimage_(args);
            });
        }
    };
    UserProvider.prototype.updateimage_ = function (imageurl) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firedata.child(_this.userId).update({ photoURL: imageurl }).then(function () {
                __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/users/' + _this.userId).update({
                    photoURL: imageurl
                }).then(function () {
                    resolve({ success: true });
                }).catch(function (err) {
                    reject(err);
                });
            });
        });
        return promise;
    };
    /*getuserdetails (){
       if(this.userId){
         return this.getuserdetails_();
       } else {
         this.tpStorageService.getItem('userUID').then((res: any) => {
           if(res){
             this.userId = res;
           }
           if (!this.userId) {
             this.userId = firebase.auth().currentUser.uid;
           }
           return this.getuserdetails_();
         }).catch(e => {
           if(firebase && firebase.auth().currentUser){
             this.userId = firebase.auth().currentUser.uid;
           }
           return this.getuserdetails_();
         });
       }
    }
    */
    UserProvider.prototype.getuserdetails = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (res) {
                _this.firedata.child(_this.userId).once('value', function (snapshot) {
                    var res = snapshot.val();
                    _this.tpStorageService.setItem('mymobile', res['mobile']);
                    _this.tpStorageService.setItem('displayName', res['displayName']);
                    _this.tpStorageService.setItem('photoURL', res['photoURL']);
                    resolve(res);
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (e) {
            });
        });
        return promise;
    };
    UserProvider.prototype.getbuddydetails = function (buddyID) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firedata.child(buddyID).once('value', function (snapshot) {
                resolve(snapshot.val());
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    /*
      updatedisplayname (args){
         if(this.userId){
           return this.updatedisplayname_(args);
         } else {
           this.tpStorageService.getItem('userUID').then((res: any) => {
             if(res){
               this.userId = res;
             }
             if (!this.userId) {
               this.userId = firebase.auth().currentUser.uid;
             }
             return this.updatedisplayname_(args);
           }).catch(e => {
             if(firebase && firebase.auth().currentUser){
               this.userId = firebase.auth().currentUser.uid;
             }
             return this.updatedisplayname_(args);
           });
         }
      }
      */
    UserProvider.prototype.updatedisplayname = function (newname) {
        var _this = this;
        ////console.log ("trying to updaet display name");
        this.tpStorageService.setItem('myDisplayName', newname);
        var promise = new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (res) {
                _this.firedata.child(_this.userId).update({
                    displayName: newname
                }).then(function () {
                    _this.updatedisplayname_DB(newname); //do it in parellel
                    resolve({ success: true });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (e) {
                if (_this.userId) {
                }
                else {
                    if (__WEBPACK_IMPORTED_MODULE_2_firebase___default.a && __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser) {
                        _this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                    }
                } //endif
                _this.tpStorageService.setItem('userUID', _this.userId);
                _this.firedata.child(_this.userId).update({
                    displayName: newname
                }).then(function () {
                    _this.updatedisplayname_DB(newname); //do it in parellel
                    resolve({ success: true });
                }).catch(function (err) {
                    reject(err);
                });
            });
        });
        return promise;
    };
    UserProvider.prototype.updatedisplayname_DB = function (args) {
        var _this = this;
        if (this.userId) {
            return this.updatedisplayname_DB_(args);
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (res) {
                if (res) {
                    _this.userId = res;
                }
                if (!_this.userId) {
                    _this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                }
                return _this.updatedisplayname_DB_(args);
            }).catch(function (e) {
                if (__WEBPACK_IMPORTED_MODULE_2_firebase___default.a && __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser) {
                    _this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                }
                return _this.updatedisplayname_DB_(args);
            });
        }
    };
    UserProvider.prototype.saveUserIdInSqlLite = function (userId) {
        var _this = this;
        this.sqlite.create({
            name: 'gr.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql(__WEBPACK_IMPORTED_MODULE_6__app_const__["a" /* CONST */].create_table_statement, {})
                .then(function (res) {
                db.executeSql("SELECT * FROM cache_users_local", {})
                    .then(function (res) {
                    if (res.rows.length > 0) {
                    }
                    else {
                        //INSERT IN THE RECORD
                        db.executeSql("INSERT INTO cache_users_local (uid)   VALUES('" + _this.userId + "')", {})
                            .then(function (res1) {
                            ////console.log ("executeSql INSERT");//testa
                        }).catch(function (e) {
                        });
                    } //endif
                })
                    .catch(function (e) {
                    //Error in operation
                });
            })
                .catch(function (e) {
                //Error in operation
            }); //create table
        }); //create database
    }; //end function
    UserProvider.prototype.updatedisplayname_DB_ = function (newname) {
        var _this = this;
        ////console.log ("Querying DB");
        this.sqlite.create({
            name: 'gr.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql(__WEBPACK_IMPORTED_MODULE_6__app_const__["a" /* CONST */].create_table_statement, {})
                .then(function (res) {
                db.executeSql("SELECT * FROM cache_users_local", {})
                    .then(function (res) {
                    if (res.rows.length > 0) {
                        ////console.log ("user.ts record exists");
                        //UPDATE THE RECORD
                        db.executeSql("UPDATE cache_users_local SET uid='" + _this.userId + "',user_name='" + newname + "',user_email='" + res.rows.item(0).user_email + "'", {})
                            .then(function (res1) {
                            ////console.log ("executeSql UPDATE");
                        }).catch(function (e) {
                        });
                    }
                    else {
                        ////console.log ("user.ts record does not exists");
                        //INSERT IN THE RECORD
                        db.executeSql("INSERT INTO cache_users_local (uid,user_name)   VALUES('" + _this.userId + "','" + newname + "')", {})
                            .then(function (res1) {
                            ////console.log ("executeSql INSERT");//testa
                        }).catch(function (e) {
                        });
                    } //endif
                })
                    .catch(function (e) {
                    //Error in operation
                });
            })
                .catch(function (e) {
                //Error in operation
            }); //create table
        }); //create database
    }; //end function
    UserProvider.prototype.updateDeviceToken = function (token, userID) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            //Update the one who invited me
            //The google cloud function should do this job but sometime its lazy and take lot of time to run. So doing here quickly now
            //Do not run additional call to google cloud or something. That should happen through google cloud triggers function on cloud
            _this.tpStorageService.getItem('invited_by').then(function (invited_by_res) {
                _this.tpStorageService.getItem('myDisplayName').then(function (myDisplayName_res) {
                    _this.firefriend.child(invited_by_res).child(userID).update({
                        deviceToken: token,
                        displayName: myDisplayName_res
                    });
                }).catch(function (e) {
                });
            }).catch(function (e) {
            });
            _this.firedata.child(userID).update({
                deviceToken: token
            }).then(function () {
                resolve({ success: true });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider.prototype.updatedisc = function (args) {
        var _this = this;
        if (this.userId) {
            return this.updatedisc_(args);
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (res) {
                if (res) {
                    _this.userId = res;
                }
                if (!_this.userId) {
                    _this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                }
                return _this.updatedisc_(args);
            }).catch(function (e) {
                if (__WEBPACK_IMPORTED_MODULE_2_firebase___default.a && __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser) {
                    _this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                }
                return _this.updatedisc_(args);
            });
        }
    };
    UserProvider.prototype.updatedisc_ = function (disc) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firedata.child(_this.userId).update({
                disc: disc
            }).then(function () {
                resolve({ success: true });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    //We need to remove getallusers_modified and getallusers . (mytodo) This is just for placeholder
    UserProvider.prototype.getallusers_modified = function (obj) {
        var promise = new Promise(function (resolve, reject) {
            resolve(obj);
        });
        return promise;
    };
    UserProvider.prototype.getallusers = function () {
        var _this = this;
        ////console.log ("[[[[[[[[[[[[[[[[[GET ALL USERS]]]]]]]]]]]]]]]]]");
        var promise = new Promise(function (resolve, reject) {
            _this.firedata.orderByChild('uid').once('value', function (snapshot) {
                ////console.log (snapshot.val());
                //mytodo immediate : Why its getting all users. it should only get me
                var userdata = snapshot.val();
                var temparr = [];
                for (var key in userdata) {
                    temparr.push(userdata[key]);
                }
                resolve(temparr);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider.prototype.directLogin = function () {
        return new Promise(function (resolve, reject) {
            resolve({ success: true });
        });
    };
    UserProvider.prototype.deleteUser = function () {
        var _this = this;
        if (this.userId) {
            return this.deleteUser_();
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (res) {
                if (res) {
                    _this.userId = res;
                }
                if (!_this.userId) {
                    _this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                }
                return _this.deleteUser_();
            }).catch(function (e) {
                if (__WEBPACK_IMPORTED_MODULE_2_firebase___default.a && __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser) {
                    _this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                }
                return _this.deleteUser_();
            });
        }
    };
    UserProvider.prototype.deleteUser_ = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firefriend;
            _this.firedata.child(_this.userId).remove().then(function () {
                _this.deletefriend(_this.userId);
                _this.deletefirebuddychat(_this.userId);
                _this.deleteusers(_this.userId);
                _this.deleteuserstatus(_this.userId);
                _this.tpStorageService.clear();
                resolve({ success: true });
            }).catch(function (err) {
                reject({ success: true });
            });
        });
    };
    UserProvider.prototype.deletefriend = function (id) {
        this.firedata.child(id).remove().then(function (res) {
        });
    };
    UserProvider.prototype.deletefirebuddychat = function (id) {
        this.firebuddychat.child(id).remove().then(function (res) {
        });
    };
    UserProvider.prototype.deleteusers = function (id) {
        this.users.child(id).remove().then(function (res) {
        });
    };
    UserProvider.prototype.deleteuserstatus = function (id) {
        this.userstatus.child(id).remove().then(function (res) {
        });
    };
    UserProvider.prototype.blockUser = function (args) {
        var _this = this;
        if (this.userId) {
            return this.blockUser_(args);
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (res) {
                if (res) {
                    _this.userId = res;
                }
                if (!_this.userId) {
                    _this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                }
                return _this.blockUser_(args);
            }).catch(function (e) {
                if (__WEBPACK_IMPORTED_MODULE_2_firebase___default.a && __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser) {
                    _this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                }
                return _this.blockUser_(args);
            });
        }
    };
    UserProvider.prototype.blockUser_ = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firefriend.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                for (var key in allfriends) {
                    if (allfriends[key].uid == buddy.uid) {
                        _this.firefriend.child(_this.userId).child(key).update({ isBlock: true });
                        resolve(true);
                    }
                }
            });
        });
    };
    /*This is proxy function for unblockUser_ */
    UserProvider.prototype.unblockUser = function (args) {
        var _this = this;
        if (this.userId) {
            return this.unblockUser_(args);
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (res) {
                if (res) {
                    _this.userId = res;
                }
                if (!_this.userId) {
                    _this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                }
                return _this.unblockUser_(args);
            }).catch(function (e) {
                if (__WEBPACK_IMPORTED_MODULE_2_firebase___default.a && __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser) {
                    _this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                }
                return _this.unblockUser_(args);
            });
        }
    };
    UserProvider.prototype.unblockUser_ = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firefriend.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                for (var key in allfriends) {
                    if (allfriends[key].uid == buddy.uid) {
                        _this.firefriend.child(_this.userId).child(key).update({ isBlock: false });
                        resolve(true);
                    }
                }
            });
        });
    };
    //mytodo : create proxy functions for all the following (The one has this.userId arggument)
    //The reason for theses proxy functions is to make sure this.userId exists before they are called
    //Chances are rare but it still happens. if happen it throw error on user screen and its ugly error
    UserProvider.prototype.getstatus = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.userId = userId__;
                _this.firefriend.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                    var allfriends = snapshot.val();
                    for (var key in allfriends) {
                        if (allfriends[key].uid == buddy.uid) {
                            resolve(allfriends[key].isBlock);
                        }
                    }
                });
            }).catch(function (e) { });
        });
    };
    UserProvider.prototype.getstatusblock = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.userId = userId__;
                _this.firefriend.child(buddy.uid).child(_this.userId).once('value', function (snapshot) {
                    var buddyS = snapshot.val();
                    resolve(buddyS.isBlock);
                });
            }).catch(function (e) { });
        });
    };
    UserProvider.prototype.getuserblock = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firefriend.child(buddy.uid).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                for (var key in allfriends) {
                    if (allfriends[key].uid == _this.userId) {
                        resolve(allfriends[key].isBlock);
                    }
                }
            });
        });
    };
    UserProvider.prototype.getmsgblock = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firefriend.child(buddy).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                for (var key in allfriends) {
                    if (allfriends[key].uid == _this.userId) {
                        resolve(allfriends[key].isBlock);
                    }
                }
            });
        });
    };
    UserProvider.prototype.getstatusblockuser = function (buddy) {
        var _this = this;
        this.firefriend.child(buddy.uid).orderByChild('uid').once('value', function (snapshot) {
            var allfriends = snapshot.val();
            for (var key in allfriends) {
                if (allfriends[key].uid == _this.userId) {
                    _this.isuserBlock = allfriends[key].isBlock;
                }
            }
            _this.events.publish('isblock-user');
        });
    };
    UserProvider.prototype.getAllBlockUsers = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (userId__) {
            _this.userId = userId__;
            _this.firefriend.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                var blockuser = [];
                for (var key in allfriends) {
                    if (allfriends[key].isBlock) {
                        _this.firedata.child(allfriends[key].uid).once('value', function (snapsho) {
                            blockuser.push(snapsho.val());
                        }).catch(function (err) {
                        });
                    }
                    _this.blockUsers = [];
                    _this.blockUsers = blockuser;
                }
                _this.events.publish('block-users');
            });
        }).catch(function (e) { });
    };
    UserProvider.prototype.getAllunBlockUsers = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (userId__) {
            _this.userId = userId__;
            _this.firefriend.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                var unblockuser = [];
                for (var key in allfriends) {
                    if (allfriends[key].isBlock == false) {
                        _this.firedata.child(allfriends[key].uid).once('value', function (snapsho) {
                            unblockuser.push(snapsho.val());
                        }).catch(function (err) {
                        });
                    }
                    _this.unblockUsers = [];
                    _this.unblockUsers = unblockuser;
                }
                _this.events.publish('unblock-users');
            });
        }).catch(function (e) { });
    };
    UserProvider.prototype.getAllBlockUsersCounter = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (userId__) {
            _this.userId = userId__;
            // block-users-counter
            _this.firefriend.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                var blockalluser = [];
                for (var tmpkey in allfriends) {
                    if (allfriends[tmpkey].isBlock) {
                        blockalluser.push(allfriends[tmpkey]);
                    }
                }
                _this.firereq.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                    var allrequest = snapshot.val();
                    for (var tmp in allrequest) {
                        if (allrequest[tmp].isBlock) {
                            blockalluser.push(allrequest[tmp]);
                        }
                    }
                    _this.blockUsersCounter = 0;
                    _this.blockUsersCounter = blockalluser.length;
                    _this.events.publish('block-users-counter');
                });
            });
        }).catch(function (e) { });
    };
    UserProvider.prototype.notifyUser = function (isnotify) {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (userId__) {
            _this.userId = userId__;
            _this.firenotify.child(_this.userId).set({
                isNotify: isnotify
            });
        }).catch(function (e) { });
    };
    UserProvider.prototype.getNotifyStatus = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firenotify.child(buddy.uid).child('isNotify').once('value', function (snapshot) {
                var isNotify = snapshot.val();
                if (isNotify == true)
                    resolve(true);
                else
                    resolve(false);
            });
        });
    };
    UserProvider.prototype.registerBusiness = function (newname, useremail, bcats, rtlist, referral_incentive_detail_txt) {
        var _this = this;
        ////console.log (">registerBusiness");
        //update email
        this.tpStorageService.setItem('useremail', useremail);
        new Promise(function (resolve, reject) {
            _this.firedata.child(_this.userId).update({
                useremail: useremail
            }).then(function () {
                ////console.log ("Done1");
            }).catch(function (err) {
                ////console.log ("Tapally Error 9122");
                ////console.log(err);
            });
        });
        var timeStamp = String(Date.now()); // get timestamp
        //update/create business name
        var promise = new Promise(function (resolve, reject) {
            _this.fireBusiness.child(_this.userId).update({
                displayName: newname,
                timestamp_user_local_time_updated: timeStamp,
                bcats: bcats,
                referral_type_id: rtlist,
                referral_detail: referral_incentive_detail_txt
            }).then(function () {
                _this.tpStorageService.setItem('businessName', newname);
                _this.tpStorageService.getItem('business_created').then(function (business_created_raw) {
                }).catch(function (e) {
                    _this.tpStorageService.setItem('business_created', timeStamp);
                });
                _this.tpStorageService.setItem('bcats', bcats);
                _this.tpStorageService.setItem('business_my_referral_type_id', rtlist);
                _this.tpStorageService.setItem('business_my_referral_detail', referral_incentive_detail_txt);
                //Check for paid flag and update it with userid
                var clean_email = useremail.replace(/[^0-9A-Za-z]/gi, '');
                ////console.log ("clean_email:"+clean_email);
                new Promise(function (resolve, reject) {
                    clean_email = clean_email.toLowerCase(); //mytodo : please check on website if its lower case as well
                    _this.fireWebsiteUsers.child(clean_email).once('value', function (snapshot_websiteUser) {
                        ////console.log (snapshot_websiteUser.val());
                        //User register on website yet
                        var websiteData = snapshot_websiteUser.val();
                        var paidvalue = 0;
                        if (websiteData && websiteData.paid > 0) {
                            // User has paid business account
                            _this.tpStorageService.setItem('paid', "1");
                            paidvalue = 1;
                        }
                        else {
                            _this.tpStorageService.setItem('paid', "0");
                            paidvalue = 0;
                        } //endif
                        ////console.log ("websiteData.paid");
                        ////console.log (websiteData.paid);
                        _this.fireBusiness.child(_this.userId).update({
                            paid: paidvalue,
                        });
                        //now update websiteuserdata  it with our id
                        _this.fireWebsiteUsers.child(clean_email).update({
                            userId: _this.userId
                        }).then(function () {
                        }).catch(function (err) {
                        });
                    });
                });
                // end website_user
                ////console.log ("Promise Resolved 1");
                resolve(true);
            }).catch(function (err) {
                ////console.log ("Tapally Error 29832");
                ////console.log(err);
                resolve(false);
            });
        });
        return promise;
    }; //end function
    UserProvider.prototype.registerBusiness_old = function (newname, useremail, bcats) {
        var _this = this;
        //update email
        this.tpStorageService.setItem('useremail', useremail);
        new Promise(function (resolve, reject) {
            _this.firedata.child(_this.userId).update({
                useremail: useremail
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                resolve(false);
            });
        });
        var timeStamp = String(Date.now()); //
        //update/create business name
        var promise = new Promise(function (resolve, reject) {
            _this.fireBusiness.child(_this.userId).update({
                displayName: newname,
                timestamp_user_local_time_updated: timeStamp
            }).then(function () {
                if (_this.tpStorageService.getItem('businessName') || _this.tpStorageService.getItem('businessName') != newname) {
                    _this.tpStorageService.setItem('businessName', "");
                }
                if (!_this.tpStorageService.getItem('businessName')) {
                    //Business Just got registered
                    //var timestamp = firebase.database.ServerValue.TIMESTAMP;
                    new Promise(function (resolve, reject) {
                        _this.fireBusiness.child(_this.userId).update({
                            timestamp_user_local_time_created: timeStamp
                        }).then(function () {
                            resolve({ success: true });
                        }).catch(function (err) {
                        });
                    });
                    _this.tpStorageService.getItem('business_created').then(function (business_created_raw) {
                    }).catch(function (e) {
                        _this.tpStorageService.setItem('business_created', timeStamp);
                    });
                } //endif
                _this.tpStorageService.setItem('businessName', newname);
                resolve({ success: true });
            }).catch(function (err) {
                reject(err);
            });
        });
        //update cat
        new Promise(function (resolve, reject) {
            _this.fireBusiness.child(_this.userId).update({
                bcats: bcats
            }).then(function () {
                _this.tpStorageService.setItem('bcats', bcats);
            }).catch(function (err) {
            });
        });
        return promise;
    }; //end function
    UserProvider.prototype.checkIfBusinessIsPaid = function (businessId) {
        var _this = this;
        var promise_cibp = new Promise(function (resolve, reject) {
            _this.fireBusiness.child(businessId).once('value', function (snapshot) {
                resolve(snapshot.val());
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise_cibp;
    };
    UserProvider.prototype.getBusinessDetailsOther = function (uid) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.fireBusiness.child(uid).once('value', function (snapshot) {
                var res = snapshot.val();
                resolve(res);
            }).catch(function (err) {
                reject(err);
            });
        });
    }; //end function
    UserProvider.prototype.getAnotherUser = function (uid) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firedata.child(uid).once('value', function (snapshot) {
                var res = snapshot.val();
                resolve(res);
            }).catch(function (err) {
                reject(err);
            });
        });
    }; //end function
    UserProvider.prototype.getBusinessDetails = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.fireBusiness.child(_this.userId).once('value', function (snapshot) {
                    var res = snapshot.val();
                    if (res) {
                        if (Object.keys(res).length > 0) {
                            _this.tpStorageService.getItem('business_created').then(function (business_created_raw) {
                            }).catch(function (e) {
                                _this.tpStorageService.setItem('business_created', res['timestamp_user_local_time_created']);
                            });
                            _this.tpStorageService.setItem('businessName', res['displayName']);
                            _this.tpStorageService.setItem('bcats', res['bcats']);
                            if (res['paid']) {
                                _this.tpStorageService.setItem('paid', "1");
                            }
                            else {
                                _this.tpStorageService.setItem('paid', "-1");
                            }
                        }
                        else {
                            _this.tpStorageService.setItem('paid', "-1");
                        }
                    } //endif   .
                    resolve(res);
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (e) { });
        });
        return promise;
    }; //end function
    UserProvider.prototype.fnMarkIncentiveRedeemed_ = function (referral_send_by, referral_received_by, requestId) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.fireReferral_sent.child(referral_send_by).child(referral_received_by).child(requestId).update({
                redeemStatus: 2,
            });
            _this.fireReferral_received.child(referral_received_by).child(referral_send_by).child(requestId).update({
                redeemStatus: 2,
            });
            resolve(true);
        });
        return promise;
    }; //end function
    UserProvider.prototype.getReferralRecieved = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.fireReferral_sent.child(_this.userId).once('value', function (snapshot) {
                    resolve(snapshot.val());
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (e) { });
        });
        return promise;
    }; //end function
    UserProvider.prototype.getReferralRecievedByID = function (referral_send_by, referral_received_by, Id) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                if (referral_received_by == _this.userId) {
                    //i am reciever of this referral
                    _this.fireReferral_received.child(referral_received_by).child(referral_send_by).child(Id).once('value', function (snapshot) {
                        resolve(snapshot.val());
                    }).catch(function (err) {
                        reject(err);
                    });
                }
                else {
                    //following is not used right now
                    //somebody else is reciever ; I probably (might not necessarily) sent
                    _this.fireReferral_received.child(referral_send_by).child(referral_received_by).child(Id).once('value', function (snapshot) {
                        resolve(snapshot.val());
                    }).catch(function (err) {
                        reject(err);
                    });
                }
            }).catch(function (e) { });
        });
        return promise;
    }; //end function
    UserProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_8__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_7__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], UserProvider);
    return UserProvider;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 758:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_status_bar__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_splash_screen__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__const__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_fcm_fcm__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_sqlite__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// Ionic native


// Ionic-angular


// Providers




//import { tap } from 'rxjs/operators'; //uncomment for push notification
var MyApp = (function () {
    function MyApp(events, fcm, platform, statusBar, app, ionicApp, splashScreen, sqlite, tpStorageService, userService) {
        var _this = this;
        this.events = events;
        this.fcm = fcm;
        this.platform = platform;
        this.statusBar = statusBar;
        this.app = app;
        this.ionicApp = ionicApp;
        this.splashScreen = splashScreen;
        this.sqlite = sqlite;
        this.tpStorageService = tpStorageService;
        this.userService = userService;
        this.alertShown = false;
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        this.platform.ready().then(function (readySource) {
            // used for an example of ngFor and navigation
            _this.pages = [
                { title: 'Settings', component: "SettingPage" },
                { title: 'Business Settings', component: "RegisterbusinessPage" },
            ];
            //hardware back button
            _this.platform.registerBackButtonAction(function () {
                //console.log ("Hardware Back Button");
                var overlayView = _this.app._appRoot._overlayPortal._views[0];
                if (overlayView && overlayView.dismiss) {
                    overlayView.dismiss();
                    return;
                }
                if (_this.nav.canGoBack()) {
                    _this.nav.pop();
                }
                else {
                    var view = _this.nav.getActive();
                    //console.log ("viewview");
                    //console.log (view.component);
                } //endif
            });
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            _this.statusBar.overlaysWebView(false);
            // Listen to incoming messages .
            // Get a FCM token
            //this.fcm.getToken()
            // Listen to incoming messages
            //uncomment for push notification . Also uncomment above required line in header of this file :  for "pipe"
            /*  this.fcm.listenToNotifications().pipe(
                tap(msg => {
                  //console.log (" PUSH NOTIFICATION DETECTED");
                })
             );
             */
            //jaswinder disabled for now because of error
            /*
            this.fcm.listenToNotifications().subscribe(data => {
              if (data.wasTapped) {
                //Notification was received on device tray and tapped by the user.
                this.navCtrl.setRoot('ChatsPage');
              } else {
                //Notification was received in foreground. Maybe the user needs to be notified.
              }
            },(err) => {////console.log("error in fcm")});
            */
            _this.events.subscribe('checkUserStatus', function () {
                _this.onResumeSubscription = platform.resume.subscribe(function () {
                    _this.fcm.setstatusUser().then(function (data) {
                    }).catch(function (error) {
                    });
                }, function (err) { });
                _this.onPauseSubscription = platform.pause.subscribe(function () {
                    _this.fcm.setStatusOffline().then(function (data) {
                    }).catch(function (error) {
                    });
                }, function (err) { });
            });
            //this.dropAllStorage ();
            _this.checkUserAndRedirectUser();
        });
    }
    MyApp.prototype.dropAllStorage = function () {
        ////console.log("DROP Everything and start fresh ");
        this.dropTable('cache_users_local');
        this.dropTable('cache_tapally_contacts');
        this.dropTable('cache_tapally_friends');
        this.dropTable('key_val');
        this.tpStorageService.clear();
    };
    MyApp.prototype.dropTable = function (tbl) {
        this.sqlite.create({
            name: 'gr.db',
            location: 'default'
        }).then(function (db) {
            console.log("DROPPED TABLE " + tbl);
            db.executeSql("DROP TABLE " + tbl, {})
                .then(function (res) {
            })
                .catch(function (e) {
                //Error in operation
            }); //create table
        }); //create database
    }; //end function
    MyApp.prototype.deleteFromTable = function () {
        this.sqlite.create({
            name: 'gr.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql(__WEBPACK_IMPORTED_MODULE_4__const__["a" /* CONST */].create_table_statement, {})
                .then(function (res) {
                db.executeSql("DELETE   FROM cache_users_local", {})
                    .then(function (res) {
                })
                    .catch(function (e) {
                    //Error in operation
                });
            })
                .catch(function (e) {
                //Error in operation
            }); //create table
        }); //create database
    }; //end function
    MyApp.prototype.checkUserAndRedirectUser = function () {
        ////console.log ("CHECK1");
        var _this = this;
        this.sqlite.create({
            name: 'gr.db',
            location: 'default'
        }).then(function (db) {
            ////console.log ("CHECK2");
            db.executeSql(__WEBPACK_IMPORTED_MODULE_4__const__["a" /* CONST */].create_table_statement, {})
                .then(function (res) {
                db.executeSql("SELECT *  FROM cache_users_local", {})
                    .then(function (res) {
                    ////console.log ("CHECK 4");
                    if (res.rows.length > 0) {
                        ////console.log (res.rows.item(0));
                        //User exists so go to next page
                        if (!res.rows.item(0).uid || res.rows.item(0).uid == undefined) {
                            _this.rootPage = 'TutorialPage';
                        }
                        ////console.log ("App component Trying to get userUID");
                        _this.tpStorageService.getItem('userUID').then(function (res) {
                            ////console.log ("??Inside this.tpStorageService.getItem");
                            if (res && res != undefined) {
                                ////console.log ("userUID Found in tpStorageService");
                            }
                            else {
                                ////console.log ("userUID NOT Found in tpStorageService so setting to "+ res.rows.item(0).uid);
                                _this.tpStorageService.setItem('userUID', res.rows.item(0).uid);
                            }
                        }).catch(function (e) { });
                        _this.tpStorageService.getItem('newname').then(function (res) {
                            if (res && res != undefined) {
                            }
                            else {
                                _this.tpStorageService.setItem('newname', res.rows.item(0).user_name);
                            }
                        }).catch(function (e) { });
                        _this.tpStorageService.getItem('useremail').then(function (res) {
                            if (res && res != undefined) {
                            }
                            else {
                                _this.tpStorageService.setItem('useremail', res.rows.item(0).useremail);
                            }
                        }).catch(function (e) { });
                        //When everything is fine
                        //this.dropTable ('cache_tapally_contacts');
                        _this.rootPage = 'TabsPage'; //TabsPage  TutorialPage
                        /*
                        Workflow goes like this --->
                        PhonePage
                        PhoneverfyPage
                        DisplaynamePage
                        TabsPage
                        RegisterbusinessPage
                        */
                        ////console.log ("++++--->");
                        ////console.log (res.rows.item(0).user_name);
                        if (!res.rows.item(0).user_name || res.rows.item(0).user_name == undefined) {
                            _this.rootPage = 'DisplaynamePage';
                        }
                    }
                    else {
                        _this.tpStorageService.getItem('userUID').then(function (userId__) {
                            if (userId__) {
                                //A situation when we have userId in storage but not in sqlite
                                _this.userService.saveUserIdInSqlLite(userId__);
                                _this.rootPage = 'TabsPage';
                            }
                            else {
                                ////console.log ("RECORD DOES NOT EXISTS");
                                _this.rootPage = 'TutorialPage';
                            }
                        }).catch(function (e) {
                            ////console.log ("RECORD DOES NOT EXISTS");
                            _this.rootPage = 'TutorialPage'; //for_prod
                        });
                    } //endif
                })
                    .catch(function (e) {
                    //Error in operation
                    ////console.log ("ERROR11:");
                    ////console.log (e);
                });
            })
                .catch(function (e) {
                //Error in operation
                ////console.log ("ERROR12:");
                ////console.log (e);
            }); //create table
        }); //create database
    }; //end function
    MyApp.prototype.goStraightToRoot = function () {
        this.nav.setRoot("TabsPage");
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component == "TabsPage") {
            this.nav.setRoot(page.component);
        }
        else {
            if (page.component == "RegisterbusinessPage") {
                this.nav.push(page.component, { treatNewInstall: 0 });
            }
            else {
                this.nav.push(page.component);
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('myNav'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["u" /* NavController */])
    ], MyApp.prototype, "navCtrl", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["t" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["t" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\app\app.html"*/'<ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title menuClose>TapAlly</ion-title>\n    </ion-toolbar>\n  </ion-header>\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n    </ion-list>\n    <p class="leftbarinfo">\n      &copy; 2019 TapAlly Corporation, Canada  \n    </p>\n    <!--\n    <ion-footer  >\n        <ion-item nav-clear menu-close href="#" style="left:0;right:0;margin:0; width: 100%;position: fixed;"><button ion-button color="dark" clear>Legal Stuff</button></ion-item>\n    </ion-footer>\n\n        <div class="atthebottom">\n          <button ion-button color="dark" clear>&copy; 2019 TapAlly Corporation, Canada</button>\n        </div>\n  -->\n  </ion-content>\n\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_5__providers_fcm_fcm__["a" /* FcmProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* App */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* IonicApp */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_8__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_user_user__["a" /* UserProvider */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 764:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GpmessageProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GpmessageProvider = (function () {
    function GpmessageProvider(http) {
        this.http = http;
    }
    GpmessageProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */]])
    ], GpmessageProvider);
    return GpmessageProvider;
}());

//# sourceMappingURL=gpmessage.js.map

/***/ }),

/***/ 98:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FcmProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_firebase__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_firestore__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_tpstorage_tpstorage__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_app_angularfireconfig__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









var FcmProvider = (function () {
    function FcmProvider(userservice, http, firebaseNative, afs, platform, tpStorageService) {
        var _this = this;
        this.userservice = userservice;
        this.http = http;
        this.firebaseNative = firebaseNative;
        this.afs = afs;
        this.platform = platform;
        this.tpStorageService = tpStorageService;
        this.fireuserStatus = __WEBPACK_IMPORTED_MODULE_6_firebase___default.a.database().ref('/userstatus');
        this.platform.ready().then(function (readySource) {
            //console.log ("Trying to call tpStorageService.getItem('userUID') FROM FcmProvider");
            _this.tpStorageService.getItem('userUID').then(function (res) {
                //console.log (">>>>>>>>>>>>>>Inside getItem()");
                if (res) {
                    _this.userId = res;
                }
                if (!_this.userId) {
                    _this.userId = __WEBPACK_IMPORTED_MODULE_6_firebase___default.a.auth().currentUser.uid;
                }
            }).catch(function (e) {
                if (__WEBPACK_IMPORTED_MODULE_6_firebase___default.a && __WEBPACK_IMPORTED_MODULE_6_firebase___default.a.auth().currentUser) {
                    _this.userId = __WEBPACK_IMPORTED_MODULE_6_firebase___default.a.auth().currentUser.uid;
                }
                //console.log ("Tapally_Error_382731 FcmProvider");
            });
        });
    }
    // Get permission from the user
    FcmProvider.prototype.getToken = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var token;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.platform.is('android')) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.firebaseNative.getToken()];
                    case 1:
                        token = _a.sent();
                        this.firebaseNative.onTokenRefresh()
                            .subscribe(function (token) {
                            if (!_this.userId) {
                                if (__WEBPACK_IMPORTED_MODULE_6_firebase___default.a && __WEBPACK_IMPORTED_MODULE_6_firebase___default.a.auth().currentUser) {
                                    _this.userId = __WEBPACK_IMPORTED_MODULE_6_firebase___default.a.auth().currentUser.uid;
                                }
                            }
                            if (!_this.userId) {
                                _this.tpStorageService.getItem('userUID').then(function (res) {
                                    _this.userservice.updateDeviceToken(token, _this.userId);
                                }).catch(function (e) { });
                            }
                            else {
                                _this.userservice.updateDeviceToken(token, _this.userId);
                            }
                        });
                        _a.label = 2;
                    case 2:
                        if (!this.platform.is('ios')) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.firebaseNative.getToken()];
                    case 3:
                        token = _a.sent();
                        return [4 /*yield*/, this.firebaseNative.grantPermission()];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, this.firebaseNative.onTokenRefresh()
                                .subscribe(function (token) {
                                if (!_this.userId) {
                                    if (__WEBPACK_IMPORTED_MODULE_6_firebase___default.a && __WEBPACK_IMPORTED_MODULE_6_firebase___default.a.auth().currentUser) {
                                        _this.userId = __WEBPACK_IMPORTED_MODULE_6_firebase___default.a.auth().currentUser.uid;
                                    }
                                }
                                if (!_this.userId) {
                                    _this.tpStorageService.getItem('userUID').then(function (res) {
                                        _this.userservice.updateDeviceToken(token, _this.userId);
                                    }).catch(function (e) { });
                                }
                                else {
                                    _this.userservice.updateDeviceToken(token, _this.userId);
                                }
                            })];
                    case 5:
                        _a.sent();
                        _a.label = 6;
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    // Listen to incoming FCM messages
    FcmProvider.prototype.listenToNotifications = function () {
        //this.firebaseNative.grantPermission();
        return this.firebaseNative.onNotificationOpen();
    };
    FcmProvider.prototype.setstatusUser = function () {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        var promise = new Promise(function (resolve, reject) {
            _this.fireuserStatus.child(_this.userId).set({
                status: 1,
                data: 'online',
                timestamp: date + ' at ' + time
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    FcmProvider.prototype.setStatusOffline = function () {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        var promise = new Promise(function (resolve, reject) {
            _this.fireuserStatus.child(_this.userId).update({
                status: 0,
                data: 'offline',
                timestamp: date + ' at ' + time
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    FcmProvider.prototype.formatAMPM = function (date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    };
    FcmProvider.prototype.formatDate = function (date) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        if (dd < 10)
            dd = '0' + dd;
        if (mm < 10)
            mm = '0' + mm;
        return dd + '/' + mm + '/' + yyyy;
    };
    FcmProvider.prototype.sendNotification = function (buddy, msg, pagename) {
        var body = {};
        if (pagename == 'chatpage') {
            body = {
                "notification": {
                    "title": buddy.displayName,
                    "body": msg,
                    "sound": "default",
                    "forceStart": "1",
                    "click_action": "FCM_PLUGIN_ACTIVITY",
                    "icon": "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/common%2Ficon.png?alt=media&token=35726d75-69f6-4f50-bfb2-07079e28d67a"
                },
                "to": buddy.deviceToken,
                "priority": "high"
            };
        }
        else if (pagename == 'Grouppage') {
            body = {
                "notification": {
                    "title": buddy.displayName,
                    "body": msg,
                    "sound": "default",
                    "forceStart": "1",
                    "click_action": "FCM_PLUGIN_ACTIVITY",
                    "icon": "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/common%2Ficon.png?alt=media&token=35726d75-69f6-4f50-bfb2-07079e28d67a"
                },
                "to": buddy.deviceToken,
                "priority": "high"
            };
        }
        else if (pagename == 'sendreq') {
            body = {
                "notification": {
                    "title": '',
                    "body": msg,
                    "forceStart": "1",
                    "sound": "default",
                    "click_action": "FCM_PLUGIN_ACTIVITY",
                    "icon": "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/common%2Ficon.png?alt=media&token=35726d75-69f6-4f50-bfb2-07079e28d67a"
                },
                "to": buddy.deviceToken,
                "priority": "high"
            };
        }
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Headers */]({ 'Authorization': 'key=' + __WEBPACK_IMPORTED_MODULE_8__app_app_angularfireconfig__["d" /* server_key */].key, 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["f" /* RequestOptions */]({ headers: headers });
        console.log("FCM Sending");
        console.log(body);
        console.log(options);
        this.http.post("https://fcm.googleapis.com/fcm/send", body, options).subscribe(function (res) {
            console.log("RESPONSE");
            console.log(res);
        }, function (error) {
            console.log("FCM ERROR ");
            console.log(error);
        });
    };
    FcmProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_firebase__["a" /* Firebase */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_7__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], FcmProvider);
    return FcmProvider;
}());

//# sourceMappingURL=fcm.js.map

/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export myEnv */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return config; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return server_key; });
/* unused harmony export tapallyKey */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return apiKey; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return env; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return userPicArr; });
//------------------------------------//
/*
    Environment
    1 = Prod
    0 = Dev
*/
var myEnv = 0;
//------------------------------------//
var config = {};
if (myEnv) {
    //PROD
    //BEFORE PUBLISHING COPY AND PASTE THE PROD DATABASE 
}
else {
    //DEV
    config = {
        apiKey: "AIzaSyB9ZN4YPIdFZgLfHWo5HbsBp2gOiJeWKCI",
        authDomain: "tapallyionic3.firebaseapp.com",
        databaseURL: "https://tapallytest.firebaseio.com",
        projectId: "tapallyionic3",
        storageBucket: "tapallytest",
        messagingSenderId: "392933828452"
    };
} //endif
// Push Notification
var server_key = {
    key: "AAAAW3yuZ2Q:APA91bGpdKl6yizA9wV1ZBgfvWaVjn1bWeDa76yNb5HTJlUuIzR8IQcSHOmyoKgCXlbBfZiVBkyTbVTF-QwlG_V4bKtiuOdTZhp4q4hICeDiwB3FV9kKLbYR513vt1XyLoAh31v__RB0"
};
// TApAlly Key : not used right now
var tapallyKey = {
    key: "djoiasjdDKASKD21893DAJ"
};
// Twilio API key
var apiKey = "TKgzw8rHwnYloYWUQqhJDPhTVMpQMIk4";
//default image of profile image
var env = {
    userPic: "https://firebasestorage.googleapis.com/v0/b/fir-chat-1f142.appspot.com/o/notouch%2Fuser.png?alt=media&token=79404d85-679a-48d3-9f73-edc7e53b4c2f"
};
//Animal Pics for icons
var userPicArr = {
    0: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F0.png?alt=media&token=2c3594e5-a0ce-42a8-83af-c98a7a7827b9",
    1: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F1.png?alt=media&token=d718091f-5d10-4edc-80e1-a2a10ef574ff",
    2: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F2.png?alt=media&token=425fbd27-514c-486b-962e-0c1648897e60",
    3: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F3.png?alt=media&token=dba66456-cca8-404f-ad6f-22d85b5c87ab",
    4: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F4.png?alt=media&token=aa684de9-c64e-4f0f-ae44-6b816ac346ff",
    5: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F5.png?alt=media&token=bcfe7d7d-7959-4e02-b2f2-2a4b48531fbe",
    6: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F6.png?alt=media&token=b202b42f-540a-46d6-a204-0ac2f8d57038",
    7: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F7.png?alt=media&token=ffb1565c-8c14-446c-8126-02b0c4ababa1",
    8: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8.png?alt=media&token=39038611-f4f0-4871-b7ae-86e9e51ce284",
    9: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8174%20-%20Crocodile%20Face.png?alt=media&token=228f0333-9cce-41d2-bf1c-5732dc615e2b",
    10: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8175%20-%20Deer%20Face.png?alt=media&token=c2e983b3-584c-4ce9-ae6b-217003e08f89",
    11: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8176%20-%20Dinosaur%20Face.png?alt=media&token=fac6d998-22f7-4129-a117-fffee163c62e",
    12: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8177%20-%20Dog%20Face.png?alt=media&token=e8fb1752-ed1f-451d-a0e6-8bd1eb16d737",
    13: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8178%20-%20Donkey%20Face.png?alt=media&token=5173617a-fe87-4570-8090-e6c90fe5312a",
    14: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8179%20-%20Duckling%20Face.png?alt=media&token=e1c780b7-609c-4096-8eaf-cf1ace7af73b",
    15: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8180%20-%20Duck%20Face.png?alt=media&token=600b227e-aa3a-4e9c-b52d-f953989f3479",
    16: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8181%20-%20Eagle%20Face.png?alt=media&token=82a8904d-0755-45f9-b221-3575a672ed4c",
    17: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8182%20-%20Elephant%20Face.png?alt=media&token=72201d6f-f5d4-4f94-a27d-22bfc238b28a",
    18: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8183%20-%20Fox%20Face.png?alt=media&token=bc85c616-fe34-41cc-9f8f-895b2fd95d9c",
    19: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8184%20-%20Frog%20Face.png?alt=media&token=2d7d3978-3cfd-4164-af2e-575e02bf459a",
    20: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8185%20-%20Giraffe%20Face.png?alt=media&token=6a7d3c69-89ce-411d-8649-cfb7211cc9b6",
    21: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8186%20-%20Goat%20Face.png?alt=media&token=372fc443-48fe-451d-8098-94f67866f582",
    22: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8187%20-%20Hen%20Face.png?alt=media&token=5988a35e-4029-4268-a613-e2c144662054",
    23: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8188%20-%20Hippopotamus%20Face.png?alt=media&token=f23c42d8-0f9a-4210-bc4d-4f8deae0d011",
    24: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8189%20-%20Horse%20Face.png?alt=media&token=f68d155d-9ca7-46d3-881c-30d0f41103dc",
    25: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8190%20-%20Kangaroo%20Face.png?alt=media&token=5c27d5f0-96f1-426a-b00e-ace6a1dfb32c",
    26: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8191%20-%20Koala%20Bear%20Face.png?alt=media&token=ca5453ed-80a8-4873-896b-1e0928bea96d",
    27: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8192%20-%20Lamb%20Face.png?alt=media&token=51da019d-6247-41e7-928f-23eeafb7442c",
    28: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8193%20-%20Leopard%20Face.png?alt=media&token=ded0bd0d-5400-451e-8ee7-a4a43d1329e5",
    29: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8194%20-%20Lion%20Face.png?alt=media&token=5a6557f0-3967-4f29-9428-373276549f96",
    30: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8195%20-%20Monkey%20Face.png?alt=media&token=06844636-a82f-4478-a5c8-ac95b0a3168a",
    31: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8196%20-%20Moutain%20Goat%20Face.png?alt=media&token=e4a3d71c-aca9-4665-8ba3-e8e8511e9b2e",
    32: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8197%20-%20Mouse%20Face.png?alt=media&token=a3848a65-fbd8-4511-bbf8-b7e202b07a92",
    33: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8198%20-%20Octopus%20Face.png?alt=media&token=8ad94d18-0ef1-4cc0-8bb0-7e319be9c11b",
    34: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8199%20-%20Owl%20Face.png?alt=media&token=2fa15989-9d96-4684-b526-f5a185c23947",
    35: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8200%20-%20Panda%20Face.png?alt=media&token=87439c6b-a4d3-4e57-97d9-3d02303c6012",
    36: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8201%20-%20Parrot%20Face.png?alt=media&token=5ca706d4-54a9-4cfb-842d-9c120c8604c7",
    37: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8202%20-%20Pig%20Face.png?alt=media&token=2ab495b3-a291-4281-a317-a17d1255518d",
    38: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8203%20-%20Rabbit%20Face.png?alt=media&token=bf8ffc53-8786-4bc3-a60f-c42fb529bf6e",
    39: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8204%20-%20Rhino%20Face.png?alt=media&token=4ef8a4b5-237f-4afa-abab-cdd0c063e956",
    40: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8205%20-%20Sea%20Lion%20Face.png?alt=media&token=8c42ff62-fdb1-488e-9164-73e9ad15bdd3",
    41: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8206%20-%20Shark%20Face.png?alt=media&token=0a51ef3a-9385-4043-a32c-daa255bdbf04",
    42: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8207%20-%20Snake%20Face.png?alt=media&token=9ab811c2-bf5e-40a9-8ca1-9adb4d73292b",
    43: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8208%20-%20Tiger%20Face.png?alt=media&token=33b57683-9409-42b4-a1e9-76f4a78ee16c",
    44: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8209%20-%20Turtle%20Face.png?alt=media&token=bdaf2962-5a6a-4823-8388-4f7b9d1ab831",
    45: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8210%20-%20Whale%20Face.png?alt=media&token=6b0b2bae-f2c4-49e7-bf06-07725a43796d",
    46: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8211%20-%20Wolf%20Face.png?alt=media&token=d8654953-fdd6-46f1-b50a-cbceaf3d5cca",
    47: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8212%20-%20Zebra%20Face.png?alt=media&token=c2523721-0054-4054-9e23-bfb75412a967",
    48: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F9.png?alt=media&token=cf956513-c831-4e0e-8333-e46b080e970c"
};
//Object length
//console.log (Object.keys(userPicArr).length);
//# sourceMappingURL=app.angularfireconfig.js.map

/***/ })

},[416]);
//# sourceMappingURL=main.js.map