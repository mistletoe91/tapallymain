webpackJsonp([69],{

/***/ 769:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AttachmentsPageModule", function() { return AttachmentsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__attachments__ = __webpack_require__(843);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AttachmentsPageModule = (function () {
    function AttachmentsPageModule() {
    }
    AttachmentsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__attachments__["a" /* AttachmentsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__attachments__["a" /* AttachmentsPage */]),
            ],
        })
    ], AttachmentsPageModule);
    return AttachmentsPageModule;
}());

//# sourceMappingURL=attachments.module.js.map

/***/ }),

/***/ 843:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AttachmentsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_imagehandler_imagehandler__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_chat_chat__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_fcm_fcm__ = __webpack_require__(98);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AttachmentsPage = (function () {
    function AttachmentsPage(navCtrl, navParams, userservice, loading, imgstore, chatservice, fcm, alertCtrl, viewCtrl, app, platform) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.userservice = userservice;
        this.loading = loading;
        this.imgstore = imgstore;
        this.chatservice = chatservice;
        this.fcm = fcm;
        this.alertCtrl = alertCtrl;
        this.viewCtrl = viewCtrl;
        this.app = app;
        this.platform = platform;
        this.iosPlatformActive = false;
        this.buddy = this.navParams.get('buddy');
        this.buddyStatus = this.chatservice.buddyStatus;
        this.platform.ready().then(function (readySource) {
            if (_this.platform.is('ios')) {
                _this.iosPlatformActive = true;
            } //endif
        });
    }
    AttachmentsPage.prototype.sendPicMsg = function () {
        var _this = this;
        if (this.viewCtrl) {
            this.viewCtrl.dismiss();
        }
        this.UnreadMSG = 0;
        /*
        this.userservice.getstatus(this.buddy).then((res) => {
            if (!res) {
        */
        //Mytodo : Check if user is blocked or not
        this.imgstore.cameraPicmsgStore().then(function (imgurl) {
            _this.chatservice.addnewmessage(imgurl, 'image', _this.buddyStatus).then(function () {
                if (_this.buddyStatus != 'online') {
                    var newMessage = 'You have received a message';
                    _this.fcm.sendNotification(_this.buddy, newMessage, 'chatpage');
                }
                _this.newmessage = '';
            });
        }).catch(function (err) {
            if (err == 20) {
                _this.navCtrl.pop();
            }
            //alert(err);
        });
        /*
              } else {
                  let alert = this.alertCtrl.create({
                      message: 'Unblock ' + this.buddy.displayName + ' to send a message.',
                      buttons: [
                          {
                              text: 'CANCEL',
                              role: 'cancel',
                              handler: () => {
                              }
                          },
                          {
                              text: 'UNBLOCK',
                              handler: () => {
                                  this.userservice.unblockUser(this.buddy).then((res) => {
                                      if (res) {
                                          this.sendPicMsg();
                                      }
                                  })
                              }
                          }
                      ]
                  });
                  alert.present();
              }
          })
          */
    };
    AttachmentsPage.prototype.sendGalleryPicMsg = function () {
        var _this = this;
        if (this.viewCtrl) {
            this.viewCtrl.dismiss();
        }
        this.UnreadMSG = 0;
        /*
        this.userservice.getstatus(this.buddy).then((res) => {
            if (!res) {
            */
        //mytodo : Check if user has blocked or not
        this.imgstore.picmsgstore().then(function (imgurl) {
            if (imgurl.length > 4) {
                _this.chatservice.addnewmessagemultiple(imgurl, 'image', _this.buddyStatus).then(function () {
                    if (_this.buddyStatus != 'online') {
                        var newMessage = 'You have received a message';
                        _this.fcm.sendNotification(_this.buddy, newMessage, 'chatpage');
                    }
                    _this.newmessage = '';
                }).catch(function (err) {
                });
            }
            else {
                for (var i = 0; i < imgurl.length; i++) {
                    _this.chatservice.addnewmessage(imgurl[i], 'image', _this.buddyStatus).then(function () {
                        if (_this.buddyStatus != 'online') {
                            var newMessage = 'You have received a message';
                            _this.fcm.sendNotification(_this.buddy, newMessage, 'chatpage');
                        }
                        _this.newmessage = '';
                    }).catch(function (err) {
                    });
                }
            }
        }).catch(function (err) {
        });
        /*
    } else {
        let alert = this.alertCtrl.create({
            message: 'Unblock ' + this.buddy.displayName + ' to send a message.',
            buttons: [
                {
                    text: 'CANCEL',
                    role: 'cancel',
                    handler: () => {
                    }
                }, {
                    text: 'UNBLOCK',
                    handler: () => {
                        this.userservice.unblockUser(this.buddy).then((res) => {
                            if (res) {
                                this.sendPicMsg();
                            }
                        })
                    }
                }
            ]
        });
        alert.present();
    }
}).catch((err)=>{
})
*/
    };
    AttachmentsPage.prototype.sendDocument = function () {
        var _this = this;
        if (this.viewCtrl) {
            this.viewCtrl.dismiss();
        }
        this.UnreadMSG = 0;
        this.userservice.getstatus(this.buddy).then(function (res) {
            if (!res) {
                _this.imgstore.selectDocument().then(function (imgurl) {
                    _this.chatservice.addnewmessage(imgurl, 'document', _this.buddyStatus).then(function () {
                        _this.loading.dismissMyLoading();
                        if (_this.buddyStatus != 'online') {
                            var newMessage = 'You have received a message';
                            _this.fcm.sendNotification(_this.buddy, newMessage, 'chatpage');
                        }
                        _this.newmessage = '';
                    });
                }).catch(function (err) {
                    _this.loading.dismissMyLoading();
                    alert(err);
                });
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    message: 'Unblock ' + _this.buddy.displayName + ' to send a message.',
                    buttons: [
                        {
                            text: 'CANCEL',
                            role: 'cancel',
                            handler: function () {
                            }
                        },
                        {
                            text: 'UNBLOCK',
                            handler: function () {
                                _this.userservice.unblockUser(_this.buddy).then(function (res) {
                                    if (res) {
                                        _this.sendPicMsg();
                                    }
                                });
                            }
                        }
                    ]
                });
                alert_1.present();
            }
        });
    };
    AttachmentsPage.prototype.sendAudioMsg = function () {
        var _this = this;
        if (this.viewCtrl) {
            this.viewCtrl.dismiss();
        }
        this.userservice.getstatus(this.buddy).then(function (res) {
            if (!res) {
                _this.imgstore.recordAudio().then(function (imgurl) {
                    _this.chatservice.addnewmessage(imgurl, 'audio', _this.buddyStatus).then(function () {
                        if (_this.buddyStatus != 'online') {
                            var newMessage = 'You have received a message';
                            _this.fcm.sendNotification(_this.buddy, newMessage, 'chatpage');
                        }
                        _this.newmessage = '';
                    });
                }).catch(function (err) {
                    alert(err);
                });
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    message: 'Unblock ' + _this.buddy.displayName + ' to send a message.',
                    buttons: [
                        {
                            text: 'CANCEL',
                            role: 'cancel',
                            handler: function () {
                            }
                        },
                        {
                            text: 'UNBLOCK',
                            handler: function () {
                                _this.userservice.unblockUser(_this.buddy).then(function (res) {
                                    if (res) {
                                        _this.sendPicMsg();
                                    }
                                });
                            }
                        }
                    ]
                });
                alert_2.present();
            }
        });
    };
    AttachmentsPage.prototype.openContacts = function () {
        if (this.viewCtrl) {
            this.viewCtrl.dismiss({ page: "AllbuddyContactsPage", buddy: this.buddy });
        }
    };
    AttachmentsPage.prototype.openMap = function () {
        if (this.viewCtrl) {
            this.viewCtrl.dismiss({ page: "MapPage", flag: false, buddy: this.buddy });
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('content'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Content */])
    ], AttachmentsPage.prototype, "content", void 0);
    AttachmentsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-attachments',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\attachments\attachments.html"*/'<ion-item tappable (click)="sendPicMsg()">\n	<ion-label no-line >Camera</ion-label>\n</ion-item>\n<ion-item  tappable (click)="sendGalleryPicMsg()" no-line>\n	<ion-label no-line >Gallery</ion-label>\n</ion-item>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\attachments\attachments.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_imagehandler_imagehandler__["a" /* ImagehandlerProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_chat_chat__["a" /* ChatProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_fcm_fcm__["a" /* FcmProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */]])
    ], AttachmentsPage);
    return AttachmentsPage;
}());

/*
Original html
<ion-item no-line>
    <ion-label (click)="sendDocument()" no-line >Documents</ion-label>
</ion-item>
<ion-item (click)="sendPicMsg()">
    <ion-label no-line >Camera</ion-label>
</ion-item>
<ion-item (click)="sendGalleryPicMsg()" no-line>
    <ion-label no-line >Gallery</ion-label>
</ion-item>
<!-- <ion-item no-line>
    <ion-label no-line (click)="sendAudioMsg()" >Audio</ion-label>
</ion-item> -->
<ion-item no-line (click)="openContacts($event)">
    <ion-label no-line >Contact</ion-label>
</ion-item>
<ion-item no-line (click)="openMap($event)">
    <ion-label no-line >Current Location</ion-label>
</ion-item>
*/
//# sourceMappingURL=attachments.js.map

/***/ })

});
//# sourceMappingURL=69.js.map