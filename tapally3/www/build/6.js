webpackJsonp([6],{

/***/ 832:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingPageModule", function() { return SettingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__setting__ = __webpack_require__(906);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SettingPageModule = (function () {
    function SettingPageModule() {
    }
    SettingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__setting__["a" /* SettingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__setting__["a" /* SettingPage */]),
            ],
        })
    ], SettingPageModule);
    return SettingPageModule;
}());

//# sourceMappingURL=setting.module.js.map

/***/ }),

/***/ 906:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_requests_requests__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_angularfireconfig__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_chat_chat__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SettingPage = (function () {
    function SettingPage(navCtrl, navParams, zone, events, userservice, requestservice, chatservice, tpStorageService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.zone = zone;
        this.events = events;
        this.userservice = userservice;
        this.requestservice = requestservice;
        this.chatservice = chatservice;
        this.tpStorageService = tpStorageService;
        this.imgurl = __WEBPACK_IMPORTED_MODULE_4__app_app_angularfireconfig__["c" /* env */].userPic;
        this.isDisabled = false;
        this.isNotify = true;
        this.showheader = true;
        this.profileData = {
            username: '',
            disc: '',
            imgurl: __WEBPACK_IMPORTED_MODULE_4__app_app_angularfireconfig__["c" /* env */].userPic
        };
        this.blockcounter = 0;
        console.log("1");
        this.tpStorageService.getItem('userUID').then(function (userIDID) {
            console.log("userIDID");
            console.log(userIDID);
            if (userIDID) {
                _this.userId = userIDID;
                _this.loaduserdetails();
                events.subscribe('block-users-counter', function () {
                    _this.events.unsubscribe('block-users-counter');
                    _this.blockcounter = _this.userservice.blockUsersCounter;
                });
                _this.userservice.initializeItem(_this.userId).then(function (res) {
                    if (res == true) {
                        _this.isNotify = true;
                    }
                    else {
                        _this.isNotify = false;
                    }
                });
            } //endif
        }).catch(function (e) { });
    }
    SettingPage.prototype.ionViewDidLoad = function () {
        this.userservice.getAllBlockUsersCounter();
    };
    SettingPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    SettingPage.prototype.goBack = function () {
        this.navCtrl.setRoot('TabsPage');
    };
    SettingPage.prototype.backButtonClick = function () {
        this.navCtrl.pop({ animate: true, direction: "forward" });
    };
    SettingPage.prototype.loaduserdetails = function () {
        var _this = this;
        console.log("loaduserdetails");
        if (this.userId != undefined) {
            this.isDisabled = true;
            this.userservice.getuserdetails().then(function (res) {
                console.log("USER DETAILS");
                console.log(res);
                if (res) {
                    var desc = res['disc'].replace(new RegExp('\n', 'g'), "<br />");
                    if (res['displayName'] != undefined) {
                        _this.profileData.username = res['displayName'];
                    }
                    if (res['photoURL'] != undefined) {
                        _this.profileData.imgurl = res['photoURL'];
                    }
                    if (desc != undefined) {
                        _this.profileData.disc = desc;
                    }
                }
            }).catch(function (err) {
            });
        }
        else {
            this.profileData = {
                username: '',
                disc: '',
                imgurl: __WEBPACK_IMPORTED_MODULE_4__app_app_angularfireconfig__["c" /* env */].userPic
            };
        }
    };
    SettingPage.prototype.goBlockuser = function () {
        this.navCtrl.push('BlockUserPage');
    };
    SettingPage.prototype.openProfilePage = function () {
        this.navCtrl.push('ProfilepicPage');
    };
    SettingPage.prototype.inviteFriends = function () {
        this.navCtrl.push('InviteFriendPage');
    };
    SettingPage.prototype.notify = function () {
        this.isNotify != this.isNotify;
        this.userservice.notifyUser(this.isNotify);
    };
    SettingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-setting',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\setting\setting.html"*/'<ion-header  >\n  <ion-navbar hideBackButton>\n\n    <ion-buttons left>\n        <button ion-button (click)="backButtonClick()">\n             <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n\n    <ion-title>Settings</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content  class="bg-grey" padding>\n  <ion-list>\n      <div class="border-bottom" tappable (click)="openProfilePage()">\n          <ion-item no-lines>\n              <ion-avatar item-start >\n                <img src="{{ profileData.imgurl ||assets/imgs/user.png}}">\n              </ion-avatar>\n              <h2>{{profileData.username}}</h2>\n              <p>{{profileData.disc}}</p>\n          </ion-item>\n      </div>\n\n      <ion-item  tappable (click)="goBlockuser()">\n        <ion-avatar item-start >\n          <ion-icon name="people"></ion-icon>\n        </ion-avatar>\n        <h2>Blocked Contacts<span *ngIf="blockcounter > 0">:{{blockcounter}}</span></h2>\n      </ion-item>\n\n      <!-- <ion-item>\n        <ion-avatar item-start >\n          <ion-icon name="notifications"></ion-icon>\n        </ion-avatar>\n        <ion-label>Notification</ion-label>\n        <ion-toggle [(ngModel)]="isNotify" (ionChange)="notify()"></ion-toggle>\n      </ion-item>\n\n      <ion-item>\n        <ion-avatar item-start >\n          <ion-icon name="help"></ion-icon>\n        </ion-avatar>\n        <h2>Help</h2>\n      </ion-item>\n\n      <ion-item (click)="inviteFriends()">\n        <ion-avatar item-start >\n          <ion-icon name="add"></ion-icon>\n        </ion-avatar>\n        <h2>Invite Friend</h2>\n      </ion-item> -->\n\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\setting\setting.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_chat_chat__["a" /* ChatProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], SettingPage);
    return SettingPage;
}());

//# sourceMappingURL=setting.js.map

/***/ })

});
//# sourceMappingURL=6.js.map