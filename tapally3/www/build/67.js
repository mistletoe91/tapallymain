webpackJsonp([67],{

/***/ 771:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BroadcastSubmittedPageModule", function() { return BroadcastSubmittedPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__broadcast_submitted__ = __webpack_require__(845);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BroadcastSubmittedPageModule = (function () {
    function BroadcastSubmittedPageModule() {
    }
    BroadcastSubmittedPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__broadcast_submitted__["a" /* BroadcastSubmittedPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__broadcast_submitted__["a" /* BroadcastSubmittedPage */]),
            ],
        })
    ], BroadcastSubmittedPageModule);
    return BroadcastSubmittedPageModule;
}());

//# sourceMappingURL=broadcast-submitted.module.js.map

/***/ }),

/***/ 845:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BroadcastSubmittedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BroadcastSubmittedPage = (function () {
    function BroadcastSubmittedPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.showheader = true;
    }
    BroadcastSubmittedPage.prototype.gotBackToBroadcastPage = function () {
        this.navCtrl.setRoot("BroadcastPage", { broadcast_retry: true });
    };
    BroadcastSubmittedPage.prototype.gotoChatsPage = function () {
        this.navCtrl.setRoot("TabsPage");
    };
    BroadcastSubmittedPage.prototype.ionViewDidLoad = function () {
    };
    BroadcastSubmittedPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    BroadcastSubmittedPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-broadcast-submitted',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\broadcast-submitted\broadcast-submitted.html"*/'<ion-header >\n  <ion-navbar color="primary" >\n    <ion-title>\n      Broadcast Successful\n    </ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name=\'menu\'></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n\n  <div class="pull-center givepadding">\n      <h3>Your Broadcast Has Been Successfully Delivered</h3>\n      <p>You have unlimited broadcast in your business package however it is advisable to send it with care to avoid spamming the network\n      </p>\n<br />\n        <button ion-button  (click)="gotBackToBroadcastPage()" color="primary" block>Perform Another Broadcast</button>\n        <button ion-button clear  (click)="gotoChatsPage()"  >Close</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\broadcast-submitted\broadcast-submitted.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */]])
    ], BroadcastSubmittedPage);
    return BroadcastSubmittedPage;
}());

//# sourceMappingURL=broadcast-submitted.js.map

/***/ })

});
//# sourceMappingURL=67.js.map