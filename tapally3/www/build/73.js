webpackJsonp([73],{

/***/ 765:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddContactPageModule", function() { return AddContactPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_contact__ = __webpack_require__(839);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AddContactPageModule = (function () {
    function AddContactPageModule() {
    }
    AddContactPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__add_contact__["a" /* AddContactPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__add_contact__["a" /* AddContactPage */]),
            ],
        })
    ], AddContactPageModule);
    return AddContactPageModule;
}());

//# sourceMappingURL=add-contact.module.js.map

/***/ }),

/***/ 839:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_contact_contact__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddContactPage = (function () {
    function AddContactPage(navCtrl, navParams, contactProvider, viewCtrl, alertCtrl, fb) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.contactProvider = contactProvider;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.fb = fb;
        this.contactObject = {
            displayName: '',
            nickName: '',
            phoneNumber: '',
            phoneType: 'mobile'
        };
        this.submitAttempt = false;
        this.showheader = true;
        this.contacts = [];
        this.msgstatus = false;
        this.authForm = this.fb.group({
            'displayNam': [null, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required])],
            'phoneNumbe': [null, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required])],
            'nickName': [null],
        });
        this.displayNam = this.authForm.controls['displayNam'];
        this.phoneNumbe = this.authForm.controls['phoneNumbe'];
        this.nickName = this.authForm.controls['nickName'];
    }
    AddContactPage.prototype.ionViewDidLoad = function () {
        if (this.navParams.get('contacts')) {
            this.contacts = this.navParams.get('contacts');
            this.contactObject.displayName = this.contacts.displayName.trim();
            this.contactObject.phoneNumber = this.contacts.mobile;
        }
    };
    AddContactPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    AddContactPage.prototype.onChange = function (e) {
        if (this.contactObject.displayName.trim()) {
            this.msgstatus = true;
        }
        else {
            this.msgstatus = false;
        }
    };
    AddContactPage.prototype.saveContact = function (newContact) {
        var _this = this;
        this.submitAttempt = true;
        if (this.authForm.valid) {
            this.contactProvider.addContact(newContact).then(function (res) {
                if (res) {
                    _this.viewCtrl.dismiss();
                }
            });
        }
    };
    AddContactPage.prototype.closeContact = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Discard changes',
            message: 'Discard your changes?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                        _this.viewCtrl.dismiss();
                    }
                }
            ]
        });
        alert.present();
    };
    AddContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-add-contact',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\add-contact\add-contact.html"*/'<ion-header >\n  <ion-navbar color="dark" >\n    <div class="header-wrap" >\n          <ion-buttons class="close-left" left>\n            <button ion-button icon-only (click)="closeContact()"><ion-icon ios="md-close" md="md-close" ></ion-icon></button>\n          </ion-buttons>\n          <div class="header-title">Add Contact</div>\n          <ion-buttons class="right-arrow" right>\n              <button ion-button icon-only [disabled]="!authForm.valid || !this.msgstatus" (click)="saveContact(contactObject)"><ion-icon ios="md-checkmark" md="md-checkmark"></ion-icon></button>\n          </ion-buttons>\n    </div>\n  </ion-navbar>\n</ion-header>\n\n<ion-content  class="bg-grey">\n  <ion-card>\n    <ion-card-content>\n      <form  [formGroup]="authForm">\n      <ion-list>\n          <ion-item class="txt-input">\n              <ion-input type="text" (input)="onChange($event.keyCode)"[(ngModel)]="contactObject.displayName"  [formControl]="displayNam"  id="displayNam" placeholder="Enter Display Name"  ></ion-input>\n          </ion-item>\n          <div *ngIf="(!authForm.controls.displayNam.valid||!msgstatus)  && (authForm.controls.displayNam.dirty || submitAttempt)" class="validation-msg">\n            <p> Display name is Required.</p>\n          </div>\n\n          <ion-item class="txt-input">\n              <ion-input type="text" placeholder="Enter Nick Name" [(ngModel)]="contactObject.nickName" [formControl]="nickName"  id="nickName" ></ion-input>\n          </ion-item>\n\n          <ion-item class="txt-input">\n            <ion-input type="tel" [(ngModel)]="contactObject.phoneNumber" [formControl]="phoneNumbe" id="phoneNumbe" placeholder="Enter Phone Number"  ></ion-input>\n          </ion-item>\n          <div *ngIf="!authForm.controls.phoneNumbe.valid && (authForm.controls.phoneNumbe.dirty || submitAttempt)" class="validation-msg">\n            <p> Phone number is Required.</p>\n          </div>\n          <button ion-button  class="button-secondary" [disabled]="!authForm.valid || !this.msgstatus" (click)="saveContact(contactObject)">Submit</button>\n          <button ion-button  class="button-secondary" (click)="closeContact()">Close</button>\n      </ion-list>\n    </form>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\add-contact\add-contact.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_contact_contact__["a" /* ContactProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */]])
    ], AddContactPage);
    return AddContactPage;
}());

//# sourceMappingURL=add-contact.js.map

/***/ })

});
//# sourceMappingURL=73.js.map