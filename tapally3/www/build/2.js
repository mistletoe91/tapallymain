webpackJsonp([2],{

/***/ 837:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewBuddyPageModule", function() { return ViewBuddyPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__view_buddy__ = __webpack_require__(912);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ViewBuddyPageModule = (function () {
    function ViewBuddyPageModule() {
    }
    ViewBuddyPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__view_buddy__["a" /* ViewBuddyPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__view_buddy__["a" /* ViewBuddyPage */]),
            ],
        })
    ], ViewBuddyPageModule);
    return ViewBuddyPageModule;
}());

//# sourceMappingURL=view-buddy.module.js.map

/***/ }),

/***/ 912:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewBuddyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_img_viewer__ = __webpack_require__(409);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ViewBuddyPage = (function () {
    function ViewBuddyPage(navCtrl, navParams, imageViewerCtrl, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.imageViewerCtrl = imageViewerCtrl;
        this.viewCtrl = viewCtrl;
        this.showheader = true;
        this.name = navParams.get('name');
        this.img = navParams.get('img');
        this.mobile = navParams.get('mobile');
        this.disc = navParams.get('disc');
        this.userstatus = navParams.get('userstatus');
        this.disc = this.disc.replace(/<\/?[^>]+>/gi, "");
        this._imageViewerCtrl = imageViewerCtrl;
    }
    ViewBuddyPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    ViewBuddyPage.prototype.presentImage = function (myImage) {
        var imageViewer = this._imageViewerCtrl.create(myImage);
        imageViewer.present();
    };
    ViewBuddyPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    ViewBuddyPage.prototype.ionViewDidLoad = function () {
    };
    ViewBuddyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-view-buddy',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\view-buddy\view-buddy.html"*/'<ion-header  >\n	<ion-navbar>\n	</ion-navbar>\n</ion-header>\n<ion-content>\n<div class="main">\n	<div class="back">\n	</div>\n	<img *ngIf="userstatus == false" src="{{img}}" #myImage (click)="presentImage(myImage)"/>\n	<img *ngIf="userstatus == true" src="assets/imgs/user.png" #myImage (click)="presentImage(myImage)"/>\n	<div class="buddy">\n		<h3>{{name}}</h3>\n	</div>\n</div>\n<ion-list padding>\n	<h5 class="color-g" >About and Phone Number</h5>\n	<div class="disc" *ngIf="userstatus == false" [innerHTML]="disc" ></div><br>\n	<div class="seperater"></div>\n	<h6  class="color-g"><b class="color-b">{{mobile}}</b> <br>Mobile</h6>\n</ion-list>\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\view-buddy\view-buddy.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_ionic_img_viewer__["a" /* ImageViewerController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */]])
    ], ViewBuddyPage);
    return ViewBuddyPage;
}());

//# sourceMappingURL=view-buddy.js.map

/***/ })

});
//# sourceMappingURL=2.js.map