webpackJsonp([60],{

/***/ 777:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckreferralincentivePageModule", function() { return CheckreferralincentivePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__checkreferralincentive__ = __webpack_require__(851);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CheckreferralincentivePageModule = (function () {
    function CheckreferralincentivePageModule() {
    }
    CheckreferralincentivePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__checkreferralincentive__["a" /* CheckreferralincentivePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__checkreferralincentive__["a" /* CheckreferralincentivePage */]),
            ],
        })
    ], CheckreferralincentivePageModule);
    return CheckreferralincentivePageModule;
}());

//# sourceMappingURL=checkreferralincentive.module.js.map

/***/ }),

/***/ 851:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckreferralincentivePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_referralincentive_type_referralincentive_type__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CheckreferralincentivePage = (function () {
    function CheckreferralincentivePage(tpStorageService, userservice, navCtrl, navParams, rtService, app) {
        this.tpStorageService = tpStorageService;
        this.userservice = userservice;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.rtService = rtService;
        this.app = app;
        this.whitelabelObj = { 'buddy_uid': 0, 'displayName': '' };
        this.businessOwnerCheckingHisOwnPage = false;
        this.businessOwnerCheckingHisOwnPage_Checked = false;
        this.hasnotLoadedYet = false;
        this.rtlistComplete = this.rtService.getList();
    }
    CheckreferralincentivePage.prototype.referralSendBackward = function () {
        this.app.getRootNav().push("SendReferralBackwardPage", { source: "checkreferralpage", buddy_uid: this.whitelabelObj.buddy_uid, buddy_displayName: this.whitelabelObj.displayName }); //
    };
    CheckreferralincentivePage.prototype.ionViewWillLoad = function () {
        var _this = this;
        this.myIncentive = '';
        this.myIncentiveName = '';
        this.whitelabelObj = this.navParams.get('whitelabelObj');
        this.tpStorageService.get('userUID').then(function (myUID) {
            _this.businessOwnerCheckingHisOwnPage_Checked = true;
            if (myUID == _this.whitelabelObj.buddy_uid) {
                _this.businessOwnerCheckingHisOwnPage = true;
            }
        }).catch(function (e) {
            _this.businessOwnerCheckingHisOwnPage_Checked = true;
        });
        this.userservice.getBusinessDetailsOther(this.whitelabelObj.buddy_uid).then(function (res) {
            _this.hasnotLoadedYet = true;
            //jaswinder
            if (res) {
                for (var x = 0; x < _this.rtlistComplete.length; x++) {
                    if (_this.rtlistComplete[x].id == res['referral_type_id']) {
                        _this.myIncentiveName = _this.rtlistComplete[x].name;
                        break;
                    }
                } //end for
                if (res['referral_type_id'] && res['referral_type_id'] > 0 && res['referral_detail']) {
                    var referral_detail = _this.getDetail(res['referral_type_id'], res['referral_detail']);
                    _this.myIncentive = _this.myIncentiveName + " ( " + referral_detail + " ) ";
                }
                else {
                    //No Incentive
                    _this.myIncentive = '';
                }
            }
            else {
                _this.myIncentive = '';
            }
        }).catch(function (err) {
        });
    };
    CheckreferralincentivePage.prototype.getDetail = function (referral_type_id, referral_detail) {
        switch (referral_type_id) {
            case '1':
            case '2':
                //cash
                //Gift Card
                var onlyNumber = referral_detail.replace(/\D/g, '');
                ////console.log ("onlyNumber");
                ////console.log (onlyNumber);
                return "Amount " + onlyNumber;
            case '3':
            case '4':
            default:
                return referral_detail;
        } //end switch
    };
    CheckreferralincentivePage.prototype.backButtonClick = function () {
        this.navCtrl.pop({ animate: true, direction: "forward" });
    };
    CheckreferralincentivePage.prototype.sendToBusinessReg = function () {
        //this.app.getRootNav().push("RegisterbusinessPage", {treatNewInstall : 0});
        this.navCtrl.push("RegisterbusinessPage", { treatNewInstall: 0 });
    };
    CheckreferralincentivePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-checkreferralincentive',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\checkreferralincentive\checkreferralincentive.html"*/'<ion-header >\n  <ion-navbar hideBackButton>\n\n    <ion-buttons left>\n        <button ion-button (click)="backButtonClick()">\n             <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n\n    <ion-title>Referral Incentive</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <p text-center class="infoline"  *ngIf="businessOwnerCheckingHisOwnPage">Your Customer Will See Below</p>\n\n  <ion-card  *ngIf="!myIncentive && hasnotLoadedYet" class="maincard_bubble" text-center padding>\n    <ion-card-content >\n      <ion-card-title>\n          {{whitelabelObj.displayName}} has not specified any referral incentives\n        </ion-card-title>\n      <p>\n        When you reach out to {{whitelabelObj.displayName}} at later date, please remind {{whitelabelObj.displayName}} to update incentives in the app\n      </p>\n\n    </ion-card-content>\n </ion-card>\n\n  <ion-card  *ngIf="myIncentive && hasnotLoadedYet" class="maincard_bubble" text-center padding>\n    <ion-card-content >\n      <ion-card-title>\n          You Earn Incentive For Each Referral You Send Back To {{whitelabelObj.displayName}}\n        </ion-card-title>\n      <p>\n        For every referral you send back to {{whitelabelObj.displayName}} you recieve <ion-badge color="secondary">{{myIncentive}}</ion-badge>\n      </p>\n\n      <p style="color:#999;">Disclaimer : Your request to redeem incentives will be sent to {{whitelabelObj.displayName}} for approval. The incentives are being offered by {{whitelabelObj.displayName}} and not TapAlly</p>\n\n\n\n    </ion-card-content>\n </ion-card>\n\n\n<button  *ngIf="!businessOwnerCheckingHisOwnPage && hasnotLoadedYet && businessOwnerCheckingHisOwnPage_Checked" style="margin-top:15px;" ion-button (click)="referralSendBackward()" color="primary" block>Send Referral</button>\n\n<button  *ngIf="!businessOwnerCheckingHisOwnPage && hasnotLoadedYet" clear style="" ion-button (click)="sendToBusinessReg()"  block>Business Owner? Grow Sales & Revenue</button>\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\checkreferralincentive\checkreferralincentive.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_referralincentive_type_referralincentive_type__["a" /* ReferralincentiveTypeProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */]])
    ], CheckreferralincentivePage);
    return CheckreferralincentivePage;
}());

//# sourceMappingURL=checkreferralincentive.js.map

/***/ })

});
//# sourceMappingURL=60.js.map