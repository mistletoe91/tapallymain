webpackJsonp([58],{

/***/ 780:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactPageModule", function() { return ContactPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact__ = __webpack_require__(854);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ContactPageModule = (function () {
    function ContactPageModule() {
    }
    ContactPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__contact__["a" /* ContactPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__contact__["a" /* ContactPage */]),
            ],
        })
    ], ContactPageModule);
    return ContactPageModule;
}());

//# sourceMappingURL=contact.module.js.map

/***/ }),

/***/ 854:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_contact_contact__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_requests_requests__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_sms_sms__ = __webpack_require__(408);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_fcm_fcm__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_groups_groups__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_app_angularfireconfig__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_http__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_add_operator_map__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_tpstorage_tpstorage__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_sqlite__ = __webpack_require__(71);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











//import { LZString } from '../../app/compress';




var ContactPage = (function () {
    function ContactPage(events, platform, loadingProvider, contactProvider, sqlite, userservice, navCtrl, navParams, alertCtrl, requestservice, chatservice, zone, http, smsservice, fcm, modalCtrl, toastCtrl, groupservice, tpStorageService) {
        this.events = events;
        this.platform = platform;
        this.loadingProvider = loadingProvider;
        this.contactProvider = contactProvider;
        this.sqlite = sqlite;
        this.userservice = userservice;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.requestservice = requestservice;
        this.chatservice = chatservice;
        this.zone = zone;
        this.http = http;
        this.smsservice = smsservice;
        this.fcm = fcm;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.groupservice = groupservice;
        this.tpStorageService = tpStorageService;
        this.contactList = [];
        this.allregisteUserData = [];
        this.metchContact = [];
        this.mainArray = [];
        this.tempArray = [];
        this.OnIsSearchBarActive = false;
        this.sourceImportpPage_ = false;
        this.userpic = __WEBPACK_IMPORTED_MODULE_10__app_app_angularfireconfig__["c" /* env */].userPic;
        this.showheader = true;
        this.readContactsFromCache = false;
        this.cssClassChecking = 'hide';
        this.cssClassContacts = 'show';
        this.isData = false;
        this.reqStatus = false;
        this.isInviteArray = [];
        this.newrequest = {};
        this.temparr = [];
        this.filteredusers = [];
        this.contactCounter = 0;
    }
    ContactPage.prototype.tpInitilizeFromStorage = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (res) {
            if (res) {
                _this.userId = res;
            }
        }).catch(function (e) { });
        this.tpStorageService.getItem('isInvitedContact').then(function (res) {
            if (res) {
                _this.isInviteArray = JSON.parse(res);
            }
        }).catch(function (e) { });
        this.tpStorageService.getItem('updated').then(function (res) {
            if (res) {
                _this.updatedRef = res;
            }
        }).catch(function (e) { });
    };
    ContactPage.prototype.backButtonClick = function () {
        if (this.sourceImportpPage_) {
            this.navCtrl.setRoot('TabsPage');
        }
        else {
            this.navCtrl.pop({ animate: true, direction: "forward" });
        } //endif
    };
    ContactPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.loadingProvider.presentLoading();
        if (this.navParams.get('source') == "ImportpPage") {
            this.sourceImportpPage_ = true;
        }
        else {
            this.sourceImportpPage_ = false;
        }
        this.tpInitilizeFromStorage();
        this.contactProvider.getSimJsonContacts()
            .then(function (res) {
            Object.keys(res).forEach(function (ele) {
                if (ele == "fromcache") {
                    if (!res["fromcache"]) {
                        //came from device
                        _this.readContactsFromCache = true;
                        _this.cssClassChecking = 'show';
                        _this.cssClassContacts = 'hide';
                        _this.events.subscribe('contactInsertingDone', function () {
                            _this.events.unsubscribe('contactInsertingDone');
                            if (!_this.contactProvider.contactInsertingDone) {
                                //still inserting
                                _this.readContactsFromCache = true;
                                _this.cssClassChecking = 'show';
                                _this.cssClassContacts = 'hide';
                            }
                            else {
                                //done inserting
                                //this.readContactsFromCache = false;
                                //this.cssClassChecking = 'hide';
                                //this.cssClassContacts = 'show';
                                _this.navCtrl.setRoot('TabsPage');
                            }
                        });
                    }
                    else {
                        //came from database
                        _this.readContactsFromCache = false;
                        _this.cssClassChecking = 'hide';
                        _this.cssClassContacts = 'show';
                    }
                }
                if (ele == "contacts") {
                    _this.mainArray = [];
                    _this.contactCounter = 0;
                    Object.keys(res["contacts"]).forEach(function (key) {
                        _this.mainArray.push({
                            "displayName": res["contacts"][key].displayName,
                            "mobile": res["contacts"][key].mobile,
                            "mobile_formatted": res["contacts"][key].mobile_formatted,
                            "status": "",
                            "isBlock": false,
                            "isUser": 0,
                            "isdisable": false,
                            "dim": false,
                            "photoURL": res["contacts"][key].photoURL,
                            "uid": res["contacts"][key].uid
                        });
                    });
                    _this.loadingProvider.dismissMyLoading();
                    //check and fix the invited one
                    _this.tpStorageService.getItem('isInvitedContact').then(function (res) {
                        if (res) {
                            _this.isInviteArray = JSON.parse(res);
                            for (var g = 0; g < _this.isInviteArray.length; g++) {
                                for (var x = 0; x < _this.mainArray.length; x++) {
                                    if (_this.mainArray[x].mobile == _this.isInviteArray[g] || _this.mainArray[x].mobile_formatted == _this.isInviteArray[g]) {
                                        _this.mainArray[x].isdisable = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }).catch(function (e) { });
                    //Only check contct history and build friend - only once
                    _this.tpStorageService.getItem('importedContacts').then(function (t) {
                        if (t != "1") {
                            _this.importContactHistoryAndBuildFriends();
                        }
                    }).catch(function (e) {
                        _this.importContactHistoryAndBuildFriends();
                    });
                } //endif
            });
        }).catch(function (e) { });
    }; //end function
    // Get All contact from the device
    ContactPage.prototype.getAllDeviceContacts__ = function (myfriends) {
        var _this = this;
        this.contactProvider.getSimJsonContacts()
            .then(function (res) {
            _this.loadingProvider.dismissMyLoading();
            //Now async check for any updated contact
            _this.contactProvider.CheckForUpdatedContacts().then(function (res) { }).catch(function (err) { });
            //if(this.loadingProvider){
            //	this.loadingProvider.loading.dismiss ();
            //}
            if (_this.updatedRef == "show") {
                _this.tpStorageService.setItem("updated", "");
                var toast = _this.toastCtrl.create({
                    message: 'Contacts updated successfully.',
                    duration: 2500,
                    position: 'bottom'
                });
                toast.present();
            }
            if (res['contacts'].length > 0) {
                var newConatctList = [];
                newConatctList = res['contacts'];
                _this.contactList = _this.removeDuplicates(newConatctList, 'mobile');
                for (var i = 0; i < _this.contactList.length; i++) {
                    _this.contactList[i].isBlock = false;
                    if (_this.contactList[i].displayName != undefined) {
                        _this.contactList[i].displayName = _this.contactList[i].displayName.trim();
                    }
                }
                _this.metchContact = _this.allregisteUserData.filter(function (item) {
                    return _this.contactList.some(function (data) { return item.mobile == data.mobile; });
                });
                for (var i = 0; i < _this.contactList.length; i++) {
                    for (var j = 0; j < _this.metchContact.length; j++) {
                        if (_this.contactList[i] != undefined && _this.metchContact[j] != undefined) {
                            if (_this.contactList[i].mobile == _this.metchContact[j].mobile) {
                                _this.metchContact[j].isUser = "1";
                                _this.metchContact[j].isBlock = _this.contactList[i].isBlock;
                                _this.metchContact[j].displayName = _this.contactList[i].displayName;
                            }
                        }
                    }
                }
                for (var i = 0; i < _this.metchContact.length; i++) {
                    for (var j = 0; j < myfriends.length; j++) {
                        if (_this.metchContact[i] != undefined && myfriends[j] != undefined) {
                            if (_this.metchContact[i].mobile == myfriends[j].mobile) {
                                _this.metchContact[i].status = 'friend';
                                _this.metchContact[i].isBlock = myfriends[j].isBlock;
                            }
                        }
                    }
                }
                _this.contactList = _this.contactList.filter(function (o1) {
                    return !_this.metchContact.some(function (o2) {
                        return o1.mobile === o2.mobile; // assumes unique id
                    });
                });
                _this.metchContact = _this.metchContact.sort(function (a, b) {
                    if (a.displayName != undefined) {
                        var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
                    }
                    if (b.displayName != undefined) {
                        var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
                    }
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    // names must be equal
                    return 0;
                });
                // sort by name
                _this.contactList = _this.contactList.sort(function (a, b) {
                    if (a.displayName != undefined) {
                        var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
                    }
                    if (b.displayName != undefined) {
                        var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
                    }
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    // names must be equal
                    return 0;
                });
                var myArray = _this.metchContact.concat(_this.contactList);
                if (_this.isInviteArray.length > 0) {
                    for (var i = 0; i < myArray.length; i++) {
                        for (var j = 0; j < _this.isInviteArray.length; j++) {
                            if (myArray[i].mobile == _this.isInviteArray[j]) {
                                myArray[i].isdisable = true;
                                break;
                            }
                            else {
                                myArray[i].isdisable = false;
                            }
                        }
                    }
                }
                //Find my friends
                for (var i = 0; i < myArray.length; i++) {
                    for (var j = 0; j < myfriends.length; j++) {
                        if (myArray[i] != undefined && myfriends[j] != undefined) {
                            if (myArray[i].mobile == myfriends[j].mobile) {
                                myArray[i].displayName = myfriends[j].displayName;
                                myArray[i].status = 'friend';
                                myArray[i].photoURL = myfriends[j].photoURL;
                                myArray[i].isBlock = myfriends[j].isBlock;
                            }
                        }
                    }
                }
                //this.zone.run(() => {
                //	this.mainArray = myArray;
                //})
                _this.tempArray = _this.mainArray;
            }
            else {
                _this.isData = true;
            }
        }).catch(function (err) {
            _this.loadingProvider.dismissMyLoading();
            if (err == 20) {
                _this.platform.exitApp();
            }
        });
    };
    ContactPage.prototype.importContactHistoryAndBuildFriends = function () {
        this.tpStorageService.setItem('importedContacts', "1");
        //mytodo : when user add more in phone : upload them only the delta
        //mytodo : also add compression
        var strAllPhoneNumbers = "";
        var counterContacts = 0;
        for (var b = 0; b < this.mainArray.length; b++) {
            if (counterContacts++ >= 500) {
                //send the url once for every 500 contacts
                this.http.get('https://us-central1-tapallyionic3.cloudfunctions.net/updateContactList?inputuid=' + this.userId + '&inputStr=' + strAllPhoneNumbers)
                    .map(function (res) { return res.json(); }).subscribe(function (dddd) {
                    dddd.unsubscribe();
                }, function (err) {
                });
                strAllPhoneNumbers = '';
                counterContacts = 0;
            } //endif
            if (this.mainArray[b].mobile && this.mainArray[b].mobile != undefined) {
                strAllPhoneNumbers += "," + this.mainArray[b].mobile;
            }
        } //end for
    }; //end function
    ContactPage.prototype.removeDuplicates = function (originalArray, prop) {
        var newArray = [];
        var lookupObject = {};
        originalArray.forEach(function (item, index) {
            lookupObject[originalArray[index][prop]] = originalArray[index];
        });
        Object.keys(lookupObject).forEach(function (element) {
            newArray.push(lookupObject[element]);
        });
        return newArray;
    };
    ContactPage.prototype.ionViewWillLeave = function () {
        //this.showheader = false;
        //this.groupservice.getmygroups();
        //this.requestservice.getmyfriends();
        this.loadingProvider.dismissMyLoading();
    };
    ContactPage.prototype.refreshPage = function () {
        var _this = this;
        //this.ionViewDidLoad();
        //this.tpStorageService.setItem('updated', 'show');
        var confirm = this.alertCtrl.create({
            title: 'Sync All Contacts',
            message: 'It may take few minutes to re-sync all contacts from your device. Do you agree ? ',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                        //console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Agree',
                    handler: function () {
                        //console.log('Agree clicked');
                        _this.contactProvider.deleteFromTable();
                        _this.navCtrl.setRoot('ContactimportprogressPage', { source: 'ContactPage' });
                    }
                }
            ]
        });
        confirm.present();
    };
    ContactPage.prototype.searchuser = function (ev) {
        if (this.platform.is('ios')) {
            return this.searchuser_ios(ev);
        }
        else {
            return this.searchuser_android(ev);
        }
    };
    ContactPage.prototype.searchuser_ios = function (ev) {
        var _this = this;
        //initize the array
        if (this.tempArray.length <= 0) {
            this.tempArray = this.mainArray;
        }
        else {
            this.mainArray = this.tempArray;
        } //endif
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.mainArray.forEach(function (item, index) {
                if (_this.mainArray[index].displayName.toLowerCase().indexOf(val.toLowerCase()) > -1) {
                    _this.mainArray[index].dim = false;
                }
                else {
                    _this.mainArray[index].dim = true;
                } //endif
            });
        }
        else {
            this.mainArray.forEach(function (item, index) {
                _this.mainArray[index].dim = false;
            });
            /*
            this.mainArray = this.mainArray.filter((item) => {
                if (item.displayName != undefined) {
                    return (  (item.displayName.toLowerCase().indexOf(val.toLowerCase())) > -1);
                }
            });
            */
            //mytodo_critical mytodo : reload virtualScroll
            //this.content.scrollToTop();
            //console.log ("Going to scroll to top ");
            //this.content.scrollTo(0, 0, 0);
        } //endif
    }; //end function
    ContactPage.prototype.searchuser_android = function (ev) {
        //initize the array
        if (this.tempArray.length <= 0) {
            this.tempArray = this.mainArray;
        }
        else {
            this.mainArray = this.tempArray;
        } //endif
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.tempArray = this.mainArray.filter(function (item) {
                if (item.displayName != undefined) {
                    return ((item.displayName.toLowerCase().indexOf(val.toLowerCase())) > -1);
                }
            });
        } //endif
    }; //end function
    ContactPage.prototype.inviteReq = function (recipient) {
        var alert = this.alertCtrl.create({
            title: 'Invitation',
            subTitle: 'Invitation already sent to ' + recipient.displayName + '.',
            buttons: ['Ok']
        });
        alert.present();
    };
    ContactPage.prototype.searchuserBlur = function () {
        //if (this.platform.is('ios')) {
        console.log("Blure...");
        //this.OnIsSearchBarActive = false;
        //}
    };
    ContactPage.prototype.searchuserFocus = function () {
        console.log("Active");
        //this.OnIsSearchBarActive  = true;
    };
    ContactPage.prototype.sendreq = function (recipient) {
        var _this = this;
        this.reqStatus = true;
        this.newrequest.sender = this.userId;
        this.newrequest.recipient = recipient.uid;
        if (this.newrequest.sender === this.newrequest.recipient)
            alert('You are your friend always');
        else {
            var successalert_1 = this.alertCtrl.create({
                title: 'Request sent',
                subTitle: 'Your request has been sent to ' + recipient.displayName + '.',
                buttons: ['ok']
            });
            var userName_1;
            this.userservice.getuserdetails().then(function (res) {
                userName_1 = res.displayName;
                var newMessage = userName_1 + " has sent you friend request.";
                _this.fcm.sendNotification(recipient, newMessage, 'sendreq');
            });
            this.requestservice.sendrequest(this.newrequest).then(function (res) {
                if (res.success) {
                    _this.reqStatus = false;
                    successalert_1.present();
                    var sentuser = _this.mainArray.indexOf(recipient);
                    _this.mainArray[sentuser].status = "pending";
                }
            }).catch(function (err) {
            });
        }
    };
    ContactPage.prototype.buddychat = function (buddy) {
        this.chatservice.initializebuddy(buddy);
        this.navCtrl.push('BuddychatPage', { back_button_pop_on: true });
    };
    ContactPage.prototype.sendSMS = function (item) {
        var _this = this;
        console.log("SEND SMS <-----");
        this.loadingProvider.presentLoading();
        this.smsservice.sendSms(item.mobile).then(function (res) {
            _this.loadingProvider.dismissMyLoading();
            if (res) {
                for (var i = 0; i < _this.mainArray.length; ++i) {
                    if (_this.mainArray[i].mobile == item.mobile) {
                        _this.mainArray[i].isdisable = true;
                    }
                }
                _this.isInviteArray.push(item.mobile);
                //log it
                _this.requestservice.logInvitation(item.mobile);
                //set  invited_by or make friends
                _this.requestservice.checkPhones(item.mobile).then(function (res_) {
                    //if there is record then it means it cannot be invited
                    if (!res_) {
                        _this.requestservice.setInvitedBy(item.mobile);
                    }
                    else {
                        if (null != res_["uid"]) {
                            // the person already have tapally account so lets make friends
                            //jaswinder
                            _this.userservice.getbuddydetails(res_["uid"]).then(function (buddy) {
                                _this.requestservice.acceptrequest(buddy, false).then(function (res__) {
                                }).catch(function (error) { });
                            }).catch(function (error) { });
                        } //endif
                    }
                }).catch(function (error) {
                    //mobile # does not exists
                    //add invited_by setInvitedBy
                    _this.requestservice.setInvitedBy(item.mobile);
                });
                _this.tpStorageService.set('isInvitedContact', _this.isInviteArray);
            }
            else {
                /*
                let alert = this.alertCtrl.create({
                    title: 'Message Send',
                    subTitle: 'Error for sending message to ' + item.displayName + '.',
                    buttons: ['Ok']
                });
                alert.present();
                */
            }
        }).catch(function (err) {
            _this.loadingProvider.dismissMyLoading();
            /*
            let alert = this.alertCtrl.create({
                title: 'Message Send',
                subTitle: 'Error for sending message to ' + item.displayName + '.',
                buttons: ['Ok']
            });
            alert.present();
            */
        });
    };
    ContactPage.prototype.addnewContact = function (myEvent) {
        var profileModal = this.modalCtrl.create("AddContactPage");
        profileModal.present({
            ev: myEvent
        });
        profileModal.onDidDismiss(function (pages) {
        });
    };
    ContactPage.prototype.goBack = function () {
        this.navCtrl.setRoot('TabsPage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('content'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Content */])
    ], ContactPage.prototype, "content", void 0);
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\contact\contact.html"*/'<ion-header  >\n  <ion-navbar hideBackButton>\n    <ion-buttons left >\n        <button ion-button (click)="backButtonClick()">\n             <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n    <ion-title>Invite Contacts</ion-title>\n    <ion-buttons right class=\'rightsidebtncss\'>\n\n      <button *ngIf="true || !sourceImportpPage_" ion-button icon-only (click)="refreshPage()">\n        <ion-icon class="rightsidebtncss"  name="refresh-circle"></ion-icon>\n      </button>\n\n      <button  *ngIf="sourceImportpPage_" ion-button  (click)="backButtonClick()">\n        Done\n      </button>\n\n\n\n    </ion-buttons>\n  </ion-navbar>\n  <ion-searchbar tappable  (ionBlur)="searchuserBlur()" (ionFocus)="searchuserFocus()"   placeholder="Search" (ionInput)="searchuser($event)"></ion-searchbar>\n\n\n</ion-header>\n<ion-content> \n\n  <ion-list [virtualScroll]="mainArray" class="contactslistion " [approxItemHeight]="\'40px\'">\n    <ion-item *virtualItem="let item"  [ngClass]="item.dim?\'itemside_dim\':\'itemside\'"  >\n      <ion-avatar item-start >\n        <img *ngIf="item.isBlock == false" src="{{item.photoURL || userpic}}">\n        <img *ngIf="item.isBlock == true" src="{{\'assets/imgs/user.png\' || userpic }}">\n      </ion-avatar>\n      <ion-note class="itemnote" item-start>\n        <h2 class="displaynamecss" *ngIf="item.displayName">{{item.displayName}} </h2>\n        <p *ngIf="item.mobile_formatted"  [innerHTML]="item.mobile_formatted" ></p>\n      </ion-note>\n      <button item-end   (click)="sendSMS(item)" *ngIf="!item.isdisable">\n        <ion-icon class="mainiconcontact"  name="add-circle"></ion-icon>\n      </button>\n      <button item-end   (click)="sendSMS(item)" *ngIf="item.isdisable">\n        <ion-icon class="mainiconcontact" color="secondary"  name="add-circle"></ion-icon>\n      </button>\n    </ion-item>\n  </ion-list>\n\n <div *ngIf="isData" padding-horizontal>\n        No contacts found!\n </div>\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\contact\contact.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_contact_contact__["a" /* ContactProvider */],
            __WEBPACK_IMPORTED_MODULE_14__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__["a" /* ChatProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_11__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_7__providers_sms_sms__["a" /* SmsProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_fcm_fcm__["a" /* FcmProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_9__providers_groups_groups__["a" /* GroupsProvider */],
            __WEBPACK_IMPORTED_MODULE_13__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ })

});
//# sourceMappingURL=58.js.map