webpackJsonp([48],{

/***/ 790:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupChatPopoverPageModule", function() { return GroupChatPopoverPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__group_chat_popover__ = __webpack_require__(864);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GroupChatPopoverPageModule = (function () {
    function GroupChatPopoverPageModule() {
    }
    GroupChatPopoverPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__group_chat_popover__["a" /* GroupChatPopoverPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__group_chat_popover__["a" /* GroupChatPopoverPage */]),
            ],
        })
    ], GroupChatPopoverPageModule);
    return GroupChatPopoverPageModule;
}());

//# sourceMappingURL=group-chat-popover.module.js.map

/***/ }),

/***/ 864:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupChatPopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_groups_groups__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GroupChatPopoverPage = (function () {
    function GroupChatPopoverPage(navCtrl, navParams, groupService, modelCtrl, viewCtrl, tpStorageService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.groupService = groupService;
        this.modelCtrl = modelCtrl;
        this.viewCtrl = viewCtrl;
        this.tpStorageService = tpStorageService;
        this.groupMembers = [];
        this.tpStorageService.getItem('userUID').then(function (res) {
            if (res) {
                _this.CurrentUser = res;
            }
        }).catch(function (e) { });
        if (this.groupService.currentgroupname != undefined) {
            this.groupName = this.groupService.currentgroupname;
        }
        else {
            this.groupName = this.navParams.get('groupName');
            this.groupService.currentgroupname = this.groupName;
        }
    }
    GroupChatPopoverPage.prototype.searchMessage = function (myEvent) {
        var _this = this;
        var searchChatModal = this.modelCtrl.create('SearchGroupChatPage');
        searchChatModal.present({
            ev: myEvent
        });
        searchChatModal.onDidDismiss(function () {
            _this.viewCtrl.dismiss();
        });
    };
    GroupChatPopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-group-chat-popover',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\group-chat-popover\group-chat-popover.html"*/'<ion-item (click)="searchMessage($event)" no-line>\n	<ion-label no-line >Search</ion-label>\n</ion-item>\n<!-- <ion-item (click)="clearChat()"  no-line>\n	<ion-label no-line >Clear</ion-label>\n</ion-item> -->'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\group-chat-popover\group-chat-popover.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_groups_groups__["a" /* GroupsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], GroupChatPopoverPage);
    return GroupChatPopoverPage;
}());

//# sourceMappingURL=group-chat-popover.js.map

/***/ })

});
//# sourceMappingURL=48.js.map