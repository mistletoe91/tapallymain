webpackJsonp([34],{

/***/ 804:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvitepeoplePageModule", function() { return InvitepeoplePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__invitepeople__ = __webpack_require__(878);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var InvitepeoplePageModule = (function () {
    function InvitepeoplePageModule() {
    }
    InvitepeoplePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__invitepeople__["a" /* InvitepeoplePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__invitepeople__["a" /* InvitepeoplePage */]),
            ],
        })
    ], InvitepeoplePageModule);
    return InvitepeoplePageModule;
}());

//# sourceMappingURL=invitepeople.module.js.map

/***/ }),

/***/ 878:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InvitepeoplePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_contact_contact__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_requests_requests__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_sms_sms__ = __webpack_require__(408);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_fcm_fcm__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_groups_groups__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_app_angularfireconfig__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









//import { Storage } from '@ionic/storage';



var InvitepeoplePage = (function () {
    function InvitepeoplePage(events, platform, 
        //public storage: Storage,
        loadingProvider, contactProvider, userservice, navCtrl, navParams, alertCtrl, requestservice, chatservice, zone, smsservice, fcm, modalCtrl, toastCtrl, groupservice, tpStorageService) {
        this.events = events;
        this.platform = platform;
        this.loadingProvider = loadingProvider;
        this.contactProvider = contactProvider;
        this.userservice = userservice;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.requestservice = requestservice;
        this.chatservice = chatservice;
        this.zone = zone;
        this.smsservice = smsservice;
        this.fcm = fcm;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.groupservice = groupservice;
        this.tpStorageService = tpStorageService;
        this.contactList = [];
        this.allregisteUserData = [];
        this.metchContact = [];
        this.mainArray = [];
        this.tempArray = [];
        this.userpic = __WEBPACK_IMPORTED_MODULE_10__app_app_angularfireconfig__["c" /* env */].userPic;
        this.isData = false;
        this.reqStatus = false;
        this.isInviteArray = [];
        this.initialLook = true;
        this.importfinished = false;
        this.newrequest = {};
        this.temparr = [];
        this.filteredusers = [];
    }
    InvitepeoplePage.prototype.tpInitilizeFromStorage = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (res) {
            if (res) {
                _this.userId = res;
            }
        }).catch(function (e) { });
        this.tpStorageService.getItem('isInvitedContact').then(function (res) {
            if (res) {
                _this.isInviteArray = res;
            }
        }).catch(function (e) { });
        this.tpStorageService.getItem('updated').then(function (res) {
            if (res) {
                _this.updatedRef = res;
            }
        }).catch(function (e) { });
    };
    InvitepeoplePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.loadingProvider.presentLoading();
        this.tpInitilizeFromStorage();
        this.userservice.getallusers().then(function (res) {
            _this.allregisteUserData = res;
            var userID = _this.userId;
            var _loop_1 = function (i) {
                _this.requestservice.pendingetmyrequests(res[i].uid).then(function (data) {
                    for (var q = 0; q < data.length; q++) {
                        if (data[q].uid == userID) {
                            _this.allregisteUserData[i].disc = _this.allregisteUserData[i].disc.replace(new RegExp('<br />', 'g'), '');
                            _this.allregisteUserData[i].status = "pending";
                        }
                        else {
                            _this.allregisteUserData[i].status = null;
                        }
                    }
                });
            };
            for (var i = 0; i < res.length; i++) {
                _loop_1(i);
            }
            _this.requestservice.getmyfriends();
            _this.myfriends = [];
            _this.events.subscribe('friends', function () {
                _this.events.unsubscribe('friends');
                _this.myfriends = [];
                _this.myfriends = _this.requestservice.myfriends;
                _this.getAllDeviceContacts(_this.myfriends);
            });
        });
    };
    InvitepeoplePage.prototype.dismissMyLoadingLocal = function () {
        this.loadingProvider.dismissMyLoading();
        this.importfinished = true;
    };
    InvitepeoplePage.prototype.inviteAll = function () {
    };
    InvitepeoplePage.prototype.inviteManuall = function () {
        this.navCtrl.push('InvitemanuallyPage');
    };
    // Get All contact from the device
    InvitepeoplePage.prototype.getAllDeviceContacts = function (myfriends) {
        var _this = this;
        this.contactProvider.getSimJsonContacts()
            .then(function (res) {
            _this.dismissMyLoadingLocal();
            var ref = _this.updatedRef;
            if (ref == "show") {
                _this.tpStorageService.removeItem("updated");
                var toast = _this.toastCtrl.create({
                    message: 'Contacts updated successfully.',
                    duration: 2500,
                    position: 'bottom'
                });
                toast.present();
            }
            if (res['contacts'].length > 0) {
                var newConatctList = [];
                newConatctList = res['contacts'];
                _this.contactList = _this.removeDuplicates(newConatctList, 'mobile');
                for (var i = 0; i < _this.contactList.length; i++) {
                    _this.contactList[i].isBlock = false;
                    if (_this.contactList[i].displayName != undefined) {
                        _this.contactList[i].displayName = _this.contactList[i].displayName.trim();
                    }
                }
                _this.metchContact = _this.allregisteUserData.filter(function (item) {
                    return _this.contactList.some(function (data) { return item.mobile == data.mobile; });
                });
                for (var i = 0; i < _this.contactList.length; i++) {
                    for (var j = 0; j < _this.metchContact.length; j++) {
                        if (_this.contactList[i] != undefined && _this.metchContact[j] != undefined) {
                            if (_this.contactList[i].mobile == _this.metchContact[j].mobile) {
                                _this.metchContact[j].isUser = "1";
                                _this.metchContact[j].isBlock = _this.contactList[i].isBlock;
                                _this.metchContact[j].displayName = _this.contactList[i].displayName;
                            }
                        }
                    }
                }
                for (var i = 0; i < _this.metchContact.length; i++) {
                    for (var j = 0; j < myfriends.length; j++) {
                        if (_this.metchContact[i] != undefined && myfriends[j] != undefined) {
                            if (_this.metchContact[i].mobile == myfriends[j].mobile) {
                                _this.metchContact[i].status = 'friend';
                                _this.metchContact[i].isBlock = myfriends[j].isBlock;
                            }
                        }
                    }
                }
                _this.contactList = _this.contactList.filter(function (o1) {
                    return !_this.metchContact.some(function (o2) {
                        return o1.mobile === o2.mobile; // assumes unique id
                    });
                });
                _this.metchContact = _this.metchContact.sort(function (a, b) {
                    if (a.displayName != undefined) {
                        var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
                    }
                    if (b.displayName != undefined) {
                        var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
                    }
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    // names must be equal
                    return 0;
                });
                // sort by name
                _this.contactList = _this.contactList.sort(function (a, b) {
                    if (a.displayName != undefined) {
                        var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
                    }
                    if (b.displayName != undefined) {
                        var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
                    }
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    // names must be equal
                    return 0;
                });
                var myArray_1 = _this.metchContact.concat(_this.contactList);
                if (_this.isInviteArray.length > 0) {
                    for (var i = 0; i < myArray_1.length; i++) {
                        for (var j = 0; j < _this.isInviteArray.length; j++) {
                            if (myArray_1[i].mobile == _this.isInviteArray[j].mobile) {
                                myArray_1[i].isdisable = true;
                            }
                            else {
                                myArray_1[i].isdisable = false;
                            }
                        }
                    }
                }
                _this.zone.run(function () {
                    _this.mainArray = myArray_1;
                });
                _this.dismissMyLoadingLocal();
                _this.tempArray = _this.mainArray;
            }
            else {
                _this.isData = true;
            }
        }).catch(function (err) {
            _this.dismissMyLoadingLocal();
            if (err == 20) {
                _this.platform.exitApp();
            }
        });
    };
    InvitepeoplePage.prototype.removeDuplicates = function (originalArray, prop) {
        var newArray = [];
        var lookupObject = {};
        originalArray.forEach(function (item, index) {
            lookupObject[originalArray[index][prop]] = originalArray[index];
        });
        Object.keys(lookupObject).forEach(function (element) {
            newArray.push(lookupObject[element]);
        });
        return newArray;
    };
    InvitepeoplePage.prototype.ionViewWillLeave = function () {
        this.groupservice.getmygroups();
        this.requestservice.getmyfriends();
        this.dismissMyLoadingLocal();
    };
    InvitepeoplePage.prototype.initializeContacts = function () {
        this.mainArray = this.tempArray;
    };
    InvitepeoplePage.prototype.searchuser = function (ev) {
        this.initializeContacts();
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.mainArray = this.mainArray.filter(function (item) {
                if (item.displayName != undefined) {
                    return (item.displayName.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
            });
        }
    };
    InvitepeoplePage.prototype.inviteReq = function (recipient) {
        var alert = this.alertCtrl.create({
            title: 'Invitation',
            subTitle: 'Invitation already sent to ' + recipient.displayName + '.',
            buttons: ['Ok']
        });
        alert.present();
    };
    InvitepeoplePage.prototype.sendreq = function (recipient) {
        var _this = this;
        this.reqStatus = true;
        this.newrequest.sender = this.userId;
        this.newrequest.recipient = recipient.uid;
        if (this.newrequest.sender === this.newrequest.recipient)
            alert('You are your friend always');
        else {
            var successalert_1 = this.alertCtrl.create({
                title: 'Request sent',
                subTitle: 'Your request has been sent to ' + recipient.displayName + '.',
                buttons: ['ok']
            });
            var userName_1;
            this.userservice.getuserdetails().then(function (res) {
                userName_1 = res.displayName;
                var newMessage = userName_1 + " has sent you friend request.";
                _this.fcm.sendNotification(recipient, newMessage, 'sendreq');
            });
            this.requestservice.sendrequest(this.newrequest).then(function (res) {
                if (res.success) {
                    _this.reqStatus = false;
                    successalert_1.present();
                    var sentuser = _this.mainArray.indexOf(recipient);
                    _this.mainArray[sentuser].status = "pending";
                }
            }).catch(function (err) {
            });
        }
    };
    InvitepeoplePage.prototype.buddychat = function (buddy) {
        if (buddy.status == 'friend') {
            this.chatservice.initializebuddy(buddy);
            this.navCtrl.push('BuddychatPage', { back_button_pop_on: true });
        }
    };
    InvitepeoplePage.prototype.sendSMS = function (item) {
        var _this = this;
        this.loadingProvider.presentLoading();
        this.smsservice.sendSms(item.mobile).then(function (res) {
            _this.dismissMyLoadingLocal();
            if (res) {
                for (var i = 0; i < _this.mainArray.length; ++i) {
                    if (_this.mainArray[i].mobile == item.mobile) {
                        _this.mainArray[i].isdisable = true;
                    }
                }
                var alert_1 = _this.alertCtrl.create({
                    title: 'Message Send',
                    subTitle: 'Your message has been sent to ' + item.displayName + '.',
                    buttons: ['Ok']
                });
                alert_1.present();
                _this.isInviteArray.push(item);
                _this.tpStorageService.set('isInvitedContact', _this.isInviteArray);
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: 'Message Send',
                    subTitle: 'Error for sending message to ' + item.displayName + '.',
                    buttons: ['Ok']
                });
                alert_2.present();
            }
        }).catch(function (err) {
            _this.dismissMyLoadingLocal();
            var alert = _this.alertCtrl.create({
                title: 'Message Send',
                subTitle: 'Error for sending message to ' + item.displayName + '.',
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    InvitepeoplePage.prototype.addnewContact = function (myEvent) {
        var profileModal = this.modalCtrl.create("AddContactPage");
        profileModal.present({
            ev: myEvent
        });
        profileModal.onDidDismiss(function (pages) {
        });
    };
    InvitepeoplePage.prototype.goBack = function () {
        this.navCtrl.setRoot('TabsPage');
    };
    InvitepeoplePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-invitepeople',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\invitepeople\invitepeople.html"*/'<ion-content padding> \n\n    <ion-card *ngIf="initialLook" >\n    \n    </ion-card>\n    \n    <ion-card *ngIf="!initialLook && !importfinished" >\n      <ion-card-content>\n        <ion-card-title>\n          Processing\n        </ion-card-title>\n       </ion-card-content>\n    </ion-card>\n    \n    <ion-card *ngIf="importfinished" >\n      <ion-card-content>\n        <ion-card-title>\n          Expand your network exponitially\n        </ion-card-title>\n    \n        <button  ion-button  (click)="inviteAll()">\n          <ion-icon ios="md-person-add" md="md-person-add"> </ion-icon> Invite All\n        </button>\n        <button ion-button  (click)="inviteManuall()" clear>Invite Manually</button>\n    \n      </ion-card-content>\n    </ion-card> \n\n</ion-content>\n\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\invitepeople\invitepeople.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_contact_contact__["a" /* ContactProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__["a" /* ChatProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_7__providers_sms_sms__["a" /* SmsProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_fcm_fcm__["a" /* FcmProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_9__providers_groups_groups__["a" /* GroupsProvider */],
            __WEBPACK_IMPORTED_MODULE_11__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], InvitepeoplePage);
    return InvitepeoplePage;
}());

//# sourceMappingURL=invitepeople.js.map

/***/ })

});
//# sourceMappingURL=34.js.map