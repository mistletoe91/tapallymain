webpackJsonp([63],{

/***/ 775:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BusinessinfoPageModule", function() { return BusinessinfoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__businessinfo__ = __webpack_require__(849);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BusinessinfoPageModule = (function () {
    function BusinessinfoPageModule() {
    }
    BusinessinfoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__businessinfo__["a" /* BusinessinfoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__businessinfo__["a" /* BusinessinfoPage */]),
            ],
        })
    ], BusinessinfoPageModule);
    return BusinessinfoPageModule;
}());

//# sourceMappingURL=businessinfo.module.js.map

/***/ }),

/***/ 849:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BusinessinfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BusinessinfoPage = (function () {
    function BusinessinfoPage(app, navCtrl, navParams) {
        this.app = app;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.showPaidSign = false;
        if (navParams.get('showPaidSign')) {
            this.showPaidSign = true;
            this.myLink = navParams.get('myLink');
        }
    }
    BusinessinfoPage.prototype.backButtonClick = function () {
        this.navCtrl.pop({ animate: true, direction: "forward" });
    };
    BusinessinfoPage.prototype.addbuddy = function () {
        this.app.getRootNav().push('ContactPage', { source: "ImportpPage" });
    };
    BusinessinfoPage.prototype.sendToBusinessReg = function () {
        this.app.getRootNav().push("RegisterbusinessPage", { treatNewInstall: 0 });
    };
    BusinessinfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-businessinfo',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\businessinfo\businessinfo.html"*/'<ion-header >\n  <ion-navbar hideBackButton>\n\n    <ion-buttons left>\n        <button ion-button (click)="backButtonClick()">\n             <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n\n    <ion-title>Grow Your Brand</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content  >\n\n  <div *ngIf="showPaidSign" tappable class="notpaidbar fadeInAnimation_Slow" >In order for co-branding and other features to work, go back to finish your business registration</div>\n\n\n\n  <ion-card  class="maincard_bubble" text-center padding>\n    <ion-card-content >\n      <ion-card-title>\n        Turn your customers into your sales agents\n        </ion-card-title>\n      <p>\n        Everytime you send invite to your customer, a permanent ad will get embedded on front page of their app. It will enable them to easily send you referrals and grow you revenue with ease. \n      </p>\n\n        <p></p>\n    </ion-card-content>\n </ion-card>\n\n <button   style="margin-top:15px;" ion-button (click)="addbuddy()" color="primary" block>Let\'s Start Inviting </button>\n\n\n\n\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\businessinfo\businessinfo.html"*/,
        })
        //mytodo : Right now we are hardcoring the copy. Please use webview to pull content directly from website. Use a dedicated page in website to render it here
        ,
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */]])
    ], BusinessinfoPage);
    return BusinessinfoPage;
}());

//# sourceMappingURL=businessinfo.js.map

/***/ })

});
//# sourceMappingURL=63.js.map