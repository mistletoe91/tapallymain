webpackJsonp([36],{

/***/ 802:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InviteFriendPageModule", function() { return InviteFriendPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__invite_friend__ = __webpack_require__(876);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var InviteFriendPageModule = (function () {
    function InviteFriendPageModule() {
    }
    InviteFriendPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__invite_friend__["a" /* InviteFriendPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__invite_friend__["a" /* InviteFriendPage */]),
            ],
        })
    ], InviteFriendPageModule);
    return InviteFriendPageModule;
}());

//# sourceMappingURL=invite-friend.module.js.map

/***/ }),

/***/ 876:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InviteFriendPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_contact_contact__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_requests_requests__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_sms_sms__ = __webpack_require__(408);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_fcm_fcm__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









//import { Storage } from '@ionic/storage';

var InviteFriendPage = (function () {
    function InviteFriendPage(events, 
        //public storage: Storage,
        loadingProvider, contactProvider, userservice, navCtrl, navParams, alertCtrl, requestservice, chatservice, zone, smsservice, fcm, modalCtrl, toastCtrl, tpStorageService) {
        this.events = events;
        this.loadingProvider = loadingProvider;
        this.contactProvider = contactProvider;
        this.userservice = userservice;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.requestservice = requestservice;
        this.chatservice = chatservice;
        this.zone = zone;
        this.smsservice = smsservice;
        this.fcm = fcm;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.tpStorageService = tpStorageService;
        this.contactList = [];
        this.allregisteUserData = [];
        this.metchContact = [];
        this.mainArray = [];
        this.tempArray = [];
        this.isData = false;
        this.showheader = true;
        this.isInviteArray = [];
        this.newrequest = {};
        this.temparr = [];
        this.filteredusers = [];
    }
    InviteFriendPage.prototype.tpInitilizeFromStorage = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (res) {
            if (res) {
                _this.userId = res;
            }
        }).catch(function (e) { });
        this.tpStorageService.getItem('isInvitedContact').then(function (res) {
            if (res) {
                _this.isInviteArray = res;
            }
        }).catch(function (e) { });
        this.tpStorageService.getItem('updated').then(function (res) {
            if (res) {
                _this.updatedRef = res;
            }
        }).catch(function (e) { });
    };
    InviteFriendPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.loadingProvider.presentLoading();
        this.tpInitilizeFromStorage();
        this.requestservice.getmyfriends();
        this.myfriends = [];
        this.events.subscribe('friends', function () {
            _this.events.unsubscribe('friends');
            _this.myfriends = [];
            _this.myfriends = _this.requestservice.myfriends;
            _this.getAllDeviceContacts(_this.myfriends);
        });
    };
    InviteFriendPage.prototype.backButtonClick = function () {
        this.navCtrl.pop({ animate: true, direction: "forward" });
    };
    InviteFriendPage.prototype.getAllDeviceContacts = function (myfriends) {
        var _this = this;
        this.contactProvider.getSimJsonContacts()
            .then(function (res) {
            _this.loadingProvider.dismissMyLoading();
            if (_this.updatedRef == "show") {
                _this.tpStorageService.removeItem("updated");
                var toast = _this.toastCtrl.create({
                    message: 'Contacts updated successfully',
                    duration: 2500,
                    position: 'bottom'
                });
                toast.present();
            }
            if (res['contacts'].length > 0) {
                var newConatctList = [];
                newConatctList = res['contacts'];
                _this.contactList = _this.removeDuplicates(newConatctList, 'mobile');
                for (var i = 0; i < _this.contactList.length; i++) {
                    if (_this.contactList[i].displayName != undefined) {
                        _this.contactList[i].displayName = _this.contactList[i].displayName.trim();
                    }
                }
                _this.metchContact = _this.allregisteUserData.filter(function (item) {
                    return _this.contactList.some(function (data) { return item.mobile == data.mobile; });
                });
                for (var i = 0; i < _this.contactList.length; i++) {
                    for (var j = 0; j < _this.metchContact.length; j++) {
                        if (_this.contactList[i] != undefined && _this.metchContact[j] != undefined) {
                            if (_this.contactList[i].mobile == _this.metchContact[j].mobile) {
                                _this.metchContact[j].isUser = "1";
                                _this.metchContact[j].displayName = _this.contactList[i].displayName;
                            }
                        }
                    }
                }
                _this.contactList = _this.contactList.filter(function (o1) {
                    return !_this.metchContact.some(function (o2) {
                        return o1.mobile === o2.mobile; // assumes unique id
                    });
                });
                _this.metchContact = _this.metchContact.sort(function (a, b) {
                    if (a.displayName != undefined) {
                        var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
                    }
                    if (b.displayName != undefined) {
                        var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
                    }
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    return 0;
                });
                // sort by name
                _this.contactList = _this.contactList.sort(function (a, b) {
                    if (a.displayName != undefined) {
                        var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
                    }
                    if (b.displayName != undefined) {
                        var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
                    }
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    // names must be equal
                    return 0;
                });
                var myArray_1 = _this.metchContact.concat(_this.contactList);
                if (_this.isInviteArray.length > 0) {
                    for (var i = 0; i < myArray_1.length; i++) {
                        for (var j = 0; j < _this.isInviteArray.length; j++) {
                            if (myArray_1[i].mobile == _this.isInviteArray[j].mobile) {
                                myArray_1[i].isdisable = true;
                            }
                            else {
                                myArray_1[i].isdisable = false;
                            }
                        }
                    }
                }
                _this.zone.run(function () {
                    _this.mainArray = myArray_1;
                });
                _this.loadingProvider.dismissMyLoading();
                _this.tempArray = _this.mainArray;
            }
            else {
                _this.isData = true;
            }
        }).catch(function (err) {
            _this.loadingProvider.dismissMyLoading();
        });
    };
    InviteFriendPage.prototype.removeDuplicates = function (originalArray, prop) {
        var newArray = [];
        var lookupObject = {};
        originalArray.forEach(function (item, index) {
            lookupObject[originalArray[index][prop]] = originalArray[index];
        });
        Object.keys(lookupObject).forEach(function (element) {
            newArray.push(lookupObject[element]);
        });
        return newArray;
    };
    InviteFriendPage.prototype.goBack = function () {
        this.navCtrl.setRoot('TabsPage');
    };
    InviteFriendPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
        this.loadingProvider.dismissMyLoading();
    };
    InviteFriendPage.prototype.initializeContacts = function () {
        this.mainArray = this.tempArray;
    };
    InviteFriendPage.prototype.refreshPage = function () {
        this.ionViewDidLoad();
        this.tpStorageService.setItem('updated', 'show');
    };
    InviteFriendPage.prototype.searchuser = function (ev) {
        this.initializeContacts();
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.mainArray = this.mainArray.filter(function (item) {
                if (item.displayName != undefined) {
                    return (item.displayName.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
            });
        }
    };
    InviteFriendPage.prototype.inviteReq = function (recipient) {
        var alert = this.alertCtrl.create({
            title: 'Invitation',
            subTitle: 'Invitation already sent to ' + recipient.displayName,
            buttons: ['Ok']
        });
        alert.present();
    };
    InviteFriendPage.prototype.sendreq = function (recipient) {
        var _this = this;
        this.newrequest.sender = this.userId;
        this.newrequest.recipient = recipient.uid;
        if (this.newrequest.sender === this.newrequest.recipient)
            alert('You are your friend always');
        else {
            var successalert_1 = this.alertCtrl.create({
                title: 'Request sent',
                subTitle: 'Your request has been sent to ' + recipient.displayName,
                buttons: ['ok']
            });
            var userName_1;
            this.userservice.getuserdetails().then(function (res) {
                userName_1 = res.displayName;
                var newMessage = userName_1 + " has sent you friend request.";
                _this.fcm.sendNotification(recipient, newMessage, 'sendreq');
            });
            this.requestservice.sendrequest(this.newrequest).then(function (res) {
                if (res.success) {
                    successalert_1.present();
                    var sentuser = _this.mainArray.indexOf(recipient);
                    _this.mainArray[sentuser].status = "pending";
                }
            }).catch(function (err) {
            });
        }
    };
    InviteFriendPage.prototype.sendSMS = function (item) {
        var _this = this;
        this.loadingProvider.presentLoading();
        this.smsservice.sendSms(item.mobile).then(function (res) {
            _this.loadingProvider.dismissMyLoading();
            if (res == 'OK') {
                for (var i = 0; i < _this.mainArray.length; ++i) {
                    if (_this.mainArray[i].mobile == item.mobile) {
                        _this.mainArray[i].isdisable = true;
                    }
                }
                var alert_1 = _this.alertCtrl.create({
                    title: 'Message Send',
                    subTitle: 'Your message has been sent to ' + item.displayName,
                    buttons: ['Ok']
                });
                //alert.present();
                _this.isInviteArray.push(item);
                _this.tpStorageService.set('isInvitedContact', _this.isInviteArray);
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: 'Message Send',
                    subTitle: 'Error for sending message to ' + item.displayName,
                    buttons: ['Ok']
                });
                //alert.present();
            }
        }).catch(function (err) {
            _this.loadingProvider.dismissMyLoading();
            var alert = _this.alertCtrl.create({
                title: 'Message Send',
                subTitle: 'Error for sending message to ' + item.displayName,
                buttons: ['Ok']
            });
            //alert.present();
        });
    };
    InviteFriendPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-invite-friend',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\invite-friend\invite-friend.html"*/'<ion-header  >\n  <ion-navbar color="darkblue" hideBackButton="true">\n    <!-- <ion-buttons start  left><ion-icon name="arrow-back"></ion-icon></ion-buttons> -->\n    <ion-buttons left>\n      <button ion-button (click)="backButtonClick()">\n           <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="refreshPage()">\n        <ion-icon ios="md-refresh" md="md-refresh"></ion-icon>\n      </button>\n      <!-- <button ion-button icon-only (click)="addnewContact()">\n        <ion-icon ios="md-person-add" md="md-person-add"></ion-icon>\n      </button> -->\n    </ion-buttons>\n    <ion-title>Invite Friends</ion-title>\n  </ion-navbar>\n  <ion-searchbar placeholder="Search" (ionInput)="searchuser($event)"></ion-searchbar>\n</ion-header>\n<ion-content>\n <ion-list>\n   <ion-item-group>\n        <div *ngFor="let item of mainArray"  >\n          <div  class="border-bottom" *ngIf="userId != item.uid">\n            <ion-item no-lines class="contact-list">\n                <div >\n                  <ion-avatar item-start>\n                  <!-- <ion-icon name="person"></ion-icon> -->\n                    <img src="{{item.photoURL || \'assets/imgs/user.png\'}}">\n                  </ion-avatar>\n                  <div class="item-info">\n                    <h2 *ngIf="item.displayName">{{item.displayName}}</h2>\n                    <p *ngIf="item.disc"  [innerHTML]="item.disc" ></p>\n                    <!-- <ng-container *ngIf="item.isUser == \'1\'"> -->\n                  </div>\n                </div>\n\n                <button item-end outline class="btn-plain" (click)="sendSMS(item)" *ngIf="item.isUser == \'0\' && !item.isdisable">\n                  <ion-icon name="add"></ion-icon>Invite\n                </button>\n\n                <button item-end outline class="btn-plain" (click)="inviteReq(item)" *ngIf="item.isdisable">\n                  <ion-icon name="add"></ion-icon>Invited\n                </button>\n\n            </ion-item>\n        </div>\n        </div>\n   </ion-item-group>\n </ion-list>\n <div *ngIf="isData" padding-horizontal>\n        No contacts found!\n </div>\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\invite-friend\invite-friend.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_contact_contact__["a" /* ContactProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__["a" /* ChatProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_7__providers_sms_sms__["a" /* SmsProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_fcm_fcm__["a" /* FcmProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_9__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], InviteFriendPage);
    return InviteFriendPage;
}());

//# sourceMappingURL=invite-friend.js.map

/***/ })

});
//# sourceMappingURL=36.js.map