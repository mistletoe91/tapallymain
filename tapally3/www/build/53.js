webpackJsonp([53],{

/***/ 785:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EarningsPageModule", function() { return EarningsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__earnings__ = __webpack_require__(859);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EarningsPageModule = (function () {
    function EarningsPageModule() {
    }
    EarningsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__earnings__["a" /* EarningsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__earnings__["a" /* EarningsPage */]),
            ],
        })
    ], EarningsPageModule);
    return EarningsPageModule;
}());

//# sourceMappingURL=earnings.module.js.map

/***/ }),

/***/ 859:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EarningsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_sqlite__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_const__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_referralincentive_type_referralincentive_type__ = __webpack_require__(411);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var EarningsPage = (function () {
    function EarningsPage(sqlite, navCtrl, navParams, app, userservice, rtService) {
        this.sqlite = sqlite;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.app = app;
        this.userservice = userservice;
        this.rtService = rtService;
        this.pageDoneLoading = false;
        this.recordExist = false;
        this.allChatListing_HTML = [];
        this.allReferrals_HTML = [];
        this.isNoRecord = false;
    }
    EarningsPage.prototype.addbuddy = function () {
        this.app.getRootNav().push('ContactPage', { source: "ImportpPage" });
    };
    EarningsPage.prototype.ionViewDidLoad = function () {
        this.rtlistComplete = this.rtService.getList();
        this.fnGetLocalCache();
    };
    EarningsPage.prototype.backButtonClick = function () {
        this.navCtrl.pop({ animate: true, direction: "right" });
    };
    EarningsPage.prototype.goToIncentiveDeailsPage = function (item) {
        if (null != item) {
            this.navCtrl.push("IncentivedetailPage", { item: JSON.stringify(item) });
        }
    };
    //this is in multple places
    EarningsPage.prototype.getDetail = function (referral_type_id, referral_detail) {
        switch (referral_type_id) {
            case '1':
            case '2':
                //cash
                //Gift Card
                var onlyNumber = referral_detail.replace(/\D/g, '');
                return "Amount " + onlyNumber;
            case '3':
            case '4':
            default:
                return referral_detail;
        } //end switch
    };
    EarningsPage.prototype.getReferralRecieved = function () {
        var _this = this;
        this.allReferrals_HTML = [];
        this.userservice.getReferralRecieved().then(function (referralRecieved) {
            for (var i = 0; i < _this.allChatListing_HTML.length; i++) {
                for (var friendUid in referralRecieved) {
                    _this.recordExist = true;
                    if (friendUid == _this.allChatListing_HTML[i].uid) {
                        for (var myKey in referralRecieved[friendUid]) {
                            var referalType = '';
                            for (var myReferType in _this.rtlistComplete) {
                                if (referralRecieved[friendUid][myKey].business_referral_type_id == _this.rtlistComplete[myReferType].id) {
                                    referalType = _this.rtlistComplete[myReferType].name;
                                    break;
                                }
                            } //end for
                            var record = {
                                displayName: _this.allChatListing_HTML[i].displayName,
                                photoURL: _this.allChatListing_HTML[i].photoURL,
                                referral_displayName: referralRecieved[friendUid][myKey].referral.displayName,
                                redeemStatus: referralRecieved[friendUid][myKey].redeemStatus,
                                myKey: myKey,
                                friendUid: friendUid,
                                received_by: referralRecieved[friendUid][myKey].received_by,
                                sent_by: referralRecieved[friendUid][myKey].sent_by,
                                isBlock: _this.allChatListing_HTML[i].isBlock,
                                business_referral_detail: _this.getDetail(referralRecieved[friendUid][myKey].business_referral_type_id, referralRecieved[friendUid][myKey].business_referral_detail),
                                business_referral_type_id: referralRecieved[friendUid][myKey].business_referral_type_id,
                                business_referral_type_name: referalType,
                                business_referral_type_detail: referralRecieved[friendUid][myKey].business_referral_detail,
                                dateofmsg: referralRecieved[friendUid][myKey].dateofmsg,
                                timeofmsg: referralRecieved[friendUid][myKey].timeofmsg
                            };
                            //try to find unique record
                            var recJson = JSON.stringify(record);
                            var thisRecordExist = false;
                            for (var l in _this.allReferrals_HTML) {
                                var tmpJson = JSON.stringify(_this.allReferrals_HTML[l]);
                                if (recJson == tmpJson) {
                                    thisRecordExist = true;
                                    break;
                                } //endif
                            } //endif
                            if (!thisRecordExist) {
                                _this.allReferrals_HTML.push(record);
                            } //endif
                        }
                    } //endif
                } //end for
            } //end for
        }).catch(function (err) {
        });
    }; //end for
    EarningsPage.prototype.fnGetLocalCache = function () {
        var _this = this;
        this.sqlite.create({
            name: 'gr.db',
            location: 'default'
        }).then(function (db) {
            //Get Friends
            db.executeSql(__WEBPACK_IMPORTED_MODULE_4__app_const__["a" /* CONST */].create_table_statement_tapally_friends, {})
                .then(function (res) {
                db.executeSql("SELECT * FROM cache_tapally_friends", {})
                    .then(function (res) {
                    _this.pageDoneLoading = true;
                    if (res.rows.length > 0) {
                        //if row exists
                        _this.allChatListing_HTML = [];
                        _this.isNoRecord = false;
                        for (var i = 0; i < res.rows.length; i++) {
                            //////console.log (res.rows.item(i));
                            var isBlock_ = false;
                            if (res.rows.item(i).isBlock) {
                                isBlock_ = true;
                            }
                            var isActive_ = true;
                            if (!res.rows.item(i).isactive) {
                                isActive_ = false;
                            }
                            var record = {};
                            if (res.rows.item(i).gpflag > 0) {
                            }
                            else {
                                ////////console.log ("gpflaggpflaggpflaggpflag= 0");
                                //Individual
                                var lastMssg = '';
                                if (res.rows.item(i).lastMessage_message && res.rows.item(i).lastMessage_message != "undefined") {
                                    lastMssg = res.rows.item(i).lastMessage_message;
                                }
                                var tmofmsg = '';
                                if (res.rows.item(i).timeofmsg) {
                                    tmofmsg = res.rows.item(i).timeofmsg;
                                }
                                record = {
                                    uid: res.rows.item(i).uid,
                                    displayName: res.rows.item(i).displayName,
                                    gpflag: res.rows.item(i).gpflag,
                                    isActive: isActive_,
                                    isBlock: isBlock_,
                                    mobile: res.rows.item(i).mobile,
                                    selection: res.rows.item(i).selection,
                                    unreadmessage: 0,
                                    photoURL: res.rows.item(i).photoURL,
                                    groupimage: res.rows.item(i).groupimage,
                                    groupName: res.rows.item(i).groupName
                                };
                                //mytodo : there is some bug causing multiple duplciate entries being inserted in this.allChatListing_HTML. Please fix . I have been mitigating them but good if we can fix them
                                _this.allChatListing_HTML.push(record);
                            } //endif
                        } //end for
                        if (_this.allChatListing_HTML.length > 0) {
                            _this.isNoRecord = true;
                        }
                        _this.getReferralRecieved();
                    }
                })
                    .catch(function (e) {
                    //Error in operation
                });
            })
                .catch(function (e) {
            }); //create table
        }).catch(function (e) {
        }); //create database
    }; //end function
    EarningsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-earnings',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\earnings\earnings.html"*/'<ion-header >\n  <ion-navbar hideBackButton>\n\n    <ion-buttons left>\n        <button ion-button (click)="backButtonClick()">\n             <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n\n    <ion-title>Your Earnings</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <ion-card  *ngIf="pageDoneLoading && !recordExist" class="maincard_bubble" text-center padding>\n    <ion-card-content >\n      <ion-card-title>\n        You Do not have earnings yet\n        </ion-card-title>\n      <p>\n        When you send referrals to business owners, you earn incentives from them. So let\'s first start inviting your contacts and let TapAlly make whole process easy, transparent and fair to everyone.\n      </p>\n      <button   style="margin-top:15px;" ion-button (click)="addbuddy()" color="secondary"  >Let`s Start Inviting</button>\n    </ion-card-content>\n </ion-card>\n\n <ion-card  *ngIf="pageDoneLoading && recordExist" class="maincard_bubble" text-center padding>\n   <ion-card-content >\n     <ion-card-title>\n       You Earned it hard\n       </ion-card-title>\n     <p>\n        Tap to redeem your referral incentives\n     </p>\n   </ion-card-content>\n</ion-card>\n\n\n<p *ngIf="pageDoneLoading && recordExist">Referrals You Sent</p>\n <ion-list class="ionlistcss" *ngIf="allReferrals_HTML.length > 0">\n   <ion-item *ngFor="let item of allReferrals_HTML"  tappable (click)="goToIncentiveDeailsPage(item)">\n     <ion-avatar item-start>\n       <img *ngIf="item.isBlock == false" class="avtar-img-list"\n         src="{{item.photoURL || \'assets/imgs/user.png\'}}">\n       <img *ngIf="item.isBlock == true" class="avtar-img-list"\n         src="{{\'assets/imgs/user.png\'}}">\n     </ion-avatar>\n     <h2>{{item.displayName}}</h2>\n     <p>Referral Sent <ion-badge item-end>{{item.referral_displayName}}</ion-badge></p>\n     <p>Incentive  <ion-badge color="primary" item-end>{{item.business_referral_type_name}} {{item.business_referral_type_detail}}</ion-badge></p>\n     <ion-icon *ngIf="item.redeemStatus<=0"   item-end name="help-circle"></ion-icon>\n     <ion-icon *ngIf="item.redeemStatus>=1 && item.redeemStatus<2"   item-end name="checkmark-circle"></ion-icon>\n     <ion-icon *ngIf="item.redeemStatus>=2 " color="secondary"   item-end name="checkmark-circle"></ion-icon>\n\n\n   </ion-item>\n</ion-list>\n\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\earnings\earnings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ionic_native_sqlite__["a" /* SQLite */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */], __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */], __WEBPACK_IMPORTED_MODULE_5__providers_referralincentive_type_referralincentive_type__["a" /* ReferralincentiveTypeProvider */]])
    ], EarningsPage);
    return EarningsPage;
}());

//# sourceMappingURL=earnings.js.map

/***/ })

});
//# sourceMappingURL=53.js.map