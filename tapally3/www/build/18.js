webpackJsonp([18],{

/***/ 820:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterbusinessPageModule", function() { return RegisterbusinessPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__registerbusiness__ = __webpack_require__(894);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RegisterbusinessPageModule = (function () {
    function RegisterbusinessPageModule() {
    }
    RegisterbusinessPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__registerbusiness__["a" /* RegisterbusinessPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__registerbusiness__["a" /* RegisterbusinessPage */]),
            ],
        })
    ], RegisterbusinessPageModule);
    return RegisterbusinessPageModule;
}());

//# sourceMappingURL=registerbusiness.module.js.map

/***/ }),

/***/ 894:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterbusinessPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_cats_cats__ = __webpack_require__(410);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_referralincentive_type_referralincentive_type__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var RegisterbusinessPage = (function () {
    function RegisterbusinessPage(navCtrl, navParams, fb, http, userservice, tpStorageService, catservice, rtService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.http = http;
        this.userservice = userservice;
        this.tpStorageService = tpStorageService;
        this.catservice = catservice;
        this.rtService = rtService;
        this.showheader = true;
        this.submittedAndEmailisWrong = false;
        this.showincentivedetail = false;
        this.doIHaveRegisteredBusinessAlready = false;
        this.submitted = false;
        this.msgstatus = false;
        this.msgstatus_referraldetail = false;
        this.treatNewInstall = false;
        this.referral_incentive_detail = '';
        this.referral_incentive_detail_placeholder = '';
        this.nextPage = "ImportpPage";
        if (this.navParams.get('treatNewInstall') > 0) {
            this.treatNewInstall = true;
            this.doIHaveRegisteredBusinessAlready = false;
        }
        this.cats = this.catservice.getCats();
        this.rtlistComplete = this.rtService.getList();
        //this.assignCatName ();
        this.selectOptions = {
            title: 'Business Category',
            subTitle: 'Please select an appropriate category'
        };
        this.selectOptionsTP = {
            title: 'Referral Incentive Type',
            subTitle: 'If your customer send you referral, what type of incentive would you offer to them ?'
        };
        //businessadd
        /*
       this.authForm=this.fb.group({
         businessname:['',Validators.compose([Validators.required])],
         useremail: ['', Validators.compose([Validators.required, Validators.email])],
         bcats:['',Validators.compose([Validators.required])],
         rtlist:[''],
         referral_incentive_detail_txt:['']
       })
       useremail: ['', Validators.compose([Validators.required, Validators.email])],
       */
        this.authForm = this.fb.group({
            businessname: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            useremail: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            bcats: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            rtlist: [0],
            referral_incentive_detail_txt: ['']
        });
        this.rtlist = 0;
    }
    RegisterbusinessPage.prototype.tpInitilizeFromStorage = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (res) {
            if (res) {
                _this.userId = res;
            }
        }).catch(function (e) { });
        this.tpStorageService.getItem('bcats').then(function (res) {
            if (res) {
                _this.bcatsObj = res;
            }
        }).catch(function (e) { });
        this.tpStorageService.getItem('useremail').then(function (res) {
            if (res) {
                _this.useremail = res;
            }
        }).catch(function (e) { });
        this.tpStorageService.getItem('businessName').then(function (res) {
            if (res) {
                _this.doIHaveRegisteredBusinessAlready = true;
            }
            else {
                _this.doIHaveRegisteredBusinessAlready = false;
            }
        }).catch(function (e) { });
    };
    RegisterbusinessPage.prototype.ionViewWillEnter = function () {
        this.tpInitilizeFromStorage();
    };
    RegisterbusinessPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    RegisterbusinessPage.prototype.assignCatName = function () {
        if (!this.bcatsObj) {
            this.bcatName = "";
            return;
        }
        for (var x = 0; x <= this.cats.length; x++) {
            if (this.cats[x].id == this.bcatsObj) {
                this.bcatName = this.cats[x].name;
            }
        } //end for
    };
    RegisterbusinessPage.prototype.backButtonClick = function () {
        this.navCtrl.pop({ animate: true, direction: "forward" });
    };
    RegisterbusinessPage.prototype.btnSkipStep = function () {
        //mytodo mytodonow : check if contacts are already imported then just go to tabspage
        this.navCtrl.setRoot(this.nextPage);
    };
    RegisterbusinessPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (res) {
            if (res) {
                _this.userId = res;
                _this.loadbusinessname();
            }
        }).catch(function (e) {
            if (__WEBPACK_IMPORTED_MODULE_6_firebase___default.a && __WEBPACK_IMPORTED_MODULE_6_firebase___default.a.auth() && __WEBPACK_IMPORTED_MODULE_6_firebase___default.a.auth().currentUser) {
                _this.userId = __WEBPACK_IMPORTED_MODULE_6_firebase___default.a.auth().currentUser.uid;
                _this.loadbusinessname();
            }
        });
    };
    RegisterbusinessPage.prototype.validateEmail = function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    RegisterbusinessPage.prototype.loadbusinessname = function () {
        var _this = this;
        //Validation pass
        this.userservice.getBusinessDetails().then(function (res) {
            //jaswinder
            if (res) {
                _this.msgstatus = true;
                _this.businessname = res['displayName'];
                _this.tpStorageService.setItem('businessName', _this.businessname);
                _this.bcats = res['bcats'];
                _this.rtlist = res['referral_type_id'];
                _this.referral_incentive_detail_txt = res['referral_detail'];
                _this.onChange_incentivetype(_this.rtlist);
                _this.doIHaveRegisteredBusinessAlready = true;
                //this.assignCatName ();
            }
            else {
                _this.tpStorageService.setItem('businessName', '');
                _this.businessname = '';
                _this.useremail = '';
                _this.bcats = 0;
                _this.doIHaveRegisteredBusinessAlready = false;
                _this.rtlist = 0;
                _this.referral_incentive_detail_txt = "";
            }
        }).catch(function (err) {
        });
        //Get user email : mytodo put this in sqlite as well and get other user info as well
        if (this.useremail == "null") {
            this.useremail = "";
        }
        if (!this.useremail) {
            this.userservice.getuserdetails().then(function (res) {
                if (res && res['useremail'] != "null") {
                    _this.useremail = res['useremail'];
                }
                else {
                    _this.useremail = "";
                }
            }).catch(function (err) {
            });
        } //endif
    }; //end function
    RegisterbusinessPage.prototype.onChange = function (e) {
        if (this.businessname.trim()) {
            this.msgstatus = true;
        }
        else {
            this.msgstatus = false;
        }
    };
    RegisterbusinessPage.prototype.onChange_incentivetype = function (e) {
        this.referral_incentive_detail = 'Incentive Detail';
        this.referral_incentive_detail_placeholder = '';
        if (e) {
            switch (e) {
                case '1':
                case '2':
                    //cash
                    //Gift Card
                    this.referral_incentive_detail = 'Enter Referral Amount (Numbers Only)';
                    this.referral_incentive_detail_placeholder = 'E.g. 10 For $10';
                    this.showincentivedetail = true;
                    break;
                case '3':
                    //Extra service
                    this.referral_incentive_detail = 'Enter Service Detail';
                    this.referral_incentive_detail_placeholder = 'E.g. 2 Hours Extra Serivce';
                    this.showincentivedetail = true;
                    break;
                case '4':
                    //Other
                    this.referral_incentive_detail = 'Enter Incentive Detail';
                    this.referral_incentive_detail_placeholder = 'E.g. Any Other Incentive Detail';
                    this.showincentivedetail = true;
                    break;
                default:
                    this.showincentivedetail = false;
            } //end switch
        }
        else {
            this.showincentivedetail = false;
        }
    };
    RegisterbusinessPage.prototype.save = function () {
        console.log("this.validateEmail(this.useremail)");
        console.log(this.validateEmail(this.useremail));
        console.log(this.useremail);
        if (!this.validateEmail(this.useremail)) {
            //error on email
            this.submittedAndEmailisWrong = true;
        }
        else {
            //email is good
            this.save_();
        }
    };
    RegisterbusinessPage.prototype.save_ = function () {
        var _this = this;
        //console.log ("tp3123");
        this.submitted = true;
        if (this.authForm.valid) {
            //console.log ("tp3124");
            if (this.businessname.trim() != null || this.businessname.trim() != "") {
                //console.log ("tp3125");
                this.businessname = this.businessname.trim();
                this.useremail = this.useremail.trim();
                //console.log ("tp3126");
                if (this.doIHaveRegisteredBusinessAlready) {
                    //I am updating business
                }
                else {
                    //I am registering business
                    this.tpStorageService.getItem('userUID').then(function (res_uid) {
                        _this.tpStorageService.getItem('code').then(function (res_phone) {
                            _this.tpStorageService.getItem('verificationCredential').then(function (res_countryCode) {
                                _this.tpStorageService.getItem('myDisplayName').then(function (res_myDisplayName) {
                                    _this.http.get('https://tapally.com/wp-json/log/v1/createbiz?phone=' + res_phone + '&countrycode=' + res_countryCode + '&uid=' + res_uid + '&businessname=' + _this.businessname + '&email=' + _this.useremail + '&bcat=' + _this.bcats + '&displayname=' + res_myDisplayName + '').subscribe(function (dddd) {
                                    }, function (err) {
                                    });
                                }).catch(function (e) { });
                            }).catch(function (e) { });
                        }).catch(function (e) { });
                    }).catch(function (e) { });
                } //endif
                this.userservice.registerBusiness(this.businessname, this.useremail, this.bcats, this.rtlist, this.referral_incentive_detail_txt).then(function (res) {
                    //console.log ("tp3127");
                    if (true) {
                        if (_this.doIHaveRegisteredBusinessAlready && !_this.treatNewInstall) {
                            //I am updating business
                            //this.navCtrl.popToRoot ();
                            _this.navCtrl.setRoot('TabsPage');
                        }
                        else {
                            //I am registering business
                            _this.navCtrl.setRoot(_this.nextPage);
                        }
                    }
                }).catch(function (err) {
                    //console.log ("Error tp3130");
                });
            }
            else {
            }
        }
    };
    RegisterbusinessPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-registerbusiness',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\registerbusiness\registerbusiness.html"*/'<ion-header >\n  <ion-navbar hideBackButton>\n\n    <ion-buttons left>\n        <button ion-button (click)="backButtonClick()">\n             <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n\n    <ion-title *ngIf="doIHaveRegisteredBusinessAlready">Update Business</ion-title>\n    <ion-title *ngIf="!doIHaveRegisteredBusinessAlready">Register Business</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n\n\n\n  <ion-card class="maincard_bubble"  text-center *ngIf="!doIHaveRegisteredBusinessAlready">\n    <ion-card-content>\n      <ion-card-title>\n        Register Your Business\n        </ion-card-title>\n      <p>\n        TapAlly let business owner build strong referral base, automate referrals, generate sales and boost revenue growth through co-branded app.\n      </p>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card class="maincard_bubble"  text-center *ngIf="doIHaveRegisteredBusinessAlready">\n    <ion-card-content>\n      <ion-card-title>\n        Update Business\n        </ion-card-title>\n      <p>\n        TapAlly let business owner build strong referral base, automate referrals, generate sales and boost revenue growth through co-branded app.\n      </p>\n    </ion-card-content>\n  </ion-card>\n\n  <form [formGroup]="authForm" (submit)="save()">\n\n    <ion-label class="content-title"> Business Name</ion-label>\n    <ion-input type="text" (input)="onChange($event.keyCode)" formControlName="businessname" id="businessname" [value]="businessname" [(ngModel)]="businessname"\n    maxlength="100" placeholder="Business Name"></ion-input>\n    <ion-label *ngIf="(!authForm.controls.businessname.valid||!msgstatus)  && (authForm.controls.businessname.dirty || submitted)">\n        <p class="errormsg">Please enter a valid name.</p>\n    </ion-label>\n\n    <ion-label class="content-title"> Email</ion-label>\n    <ion-input type="email" email (input)="onChange($event.keyCode)" formControlName="useremail" id="useremail" [value]="useremail" [(ngModel)]="useremail"\n    maxlength="300" placeholder="Email"></ion-input>\n    <ion-label *ngIf="(submittedAndEmailisWrong) || ((!authForm.controls.useremail.valid||!msgstatus)  && (authForm.controls.useremail.dirty || submitted))">\n        <p class="errormsg" >Please enter a valid email.</p>\n    </ion-label>\n\n\n    <ion-label class="content-title"> Business Category</ion-label>\n    <ion-item id="selectionitem">\n    <ion-select placeholder="Please Select" [selectOptions]="selectOptions" [(ngModel)]="bcats" formControlName="bcats" id="bcats" >\n    <ion-option [selected]="selectOptions == 0" *ngFor="let item of cats" value="{{item.id}}">{{item.name}}</ion-option>\n    </ion-select>\n  </ion-item>\n\n\n  <ion-label class="content-title"> Referral Incentive Type</ion-label>\n  <p class="infotext">Motivate your consumers by offering incentives for referrals they send back to you</p>\n  <ion-item id="referralincentive">\n  <ion-select (ionChange)="onChange_incentivetype($event)" placeholder="Please Select" [selectOptions]="selectOptionsTP" [(ngModel)]="rtlist" formControlName="rtlist" id="rtlist" >\n  <ion-option *ngFor="let item of rtlistComplete" [selected]="selectOptionsTP == 0" value="{{item.id}}">{{item.name}}</ion-option>\n  </ion-select>\n  </ion-item>\n\n  <ion-label *ngIf="showincentivedetail" class="content-title"> {{referral_incentive_detail}}</ion-label>\n  <ion-input *ngIf="showincentivedetail" type="text" (input)="onChange($event.keyCode)" formControlName="referral_incentive_detail_txt" id="referral_incentive_detail_txt" [value]="referral_incentive_detail_txt" [(ngModel)]="referral_incentive_detail_txt"\n  maxlength="300" placeholder="{{referral_incentive_detail_placeholder}}"></ion-input>\n  <ion-label *ngIf="(!authForm.controls.referral_incentive_detail_txt.valid||!msgstatus)  && (authForm.controls.referral_incentive_detail_txt.dirty || submitted)">\n      <p class="errormsg">Please enter a valid entry.</p>\n  </ion-label>\n\n\n<!--\n  <ion-label class="content-title hide"> Business Address</ion-label>\n  <ion-input type="text" (input)="onChange($event.keyCode)" formControlName="businessadd" id="businessadd" [value]="businessadd" [(ngModel)]="businessadd"\n  maxlength="30" placeholder="Business Name"></ion-input>\n  <ion-label *ngIf="(!authForm.controls.businessadd.valid||!msgstatus)  && (authForm.controls.businessadd.dirty || submitted)">\n      <p class="errormsg">Please enter a valid address.</p>\n  </ion-label>\n-->\n\n  <ion-card text-center class="btngrouptop">\n    <button ion-button class="button-secondary" type="submit" [disabled]="!authForm.valid || !this.msgstatus" >Submit</button>\n    <ion-item text-center class="btnskipstep" *ngIf="!doIHaveRegisteredBusinessAlready && treatNewInstall" (click)="btnSkipStep()" clear >SKIP STEP</ion-item>\n  </ion-card>\n\n  </form>\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\registerbusiness\registerbusiness.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_7__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_cats_cats__["a" /* CatProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_referralincentive_type_referralincentive_type__["a" /* ReferralincentiveTypeProvider */]])
    ], RegisterbusinessPage);
    return RegisterbusinessPage;
}());

//# sourceMappingURL=registerbusiness.js.map

/***/ })

});
//# sourceMappingURL=18.js.map