webpackJsonp([54],{

/***/ 784:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DisplaynamePageModule", function() { return DisplaynamePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__displayname__ = __webpack_require__(858);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DisplaynamePageModule = (function () {
    function DisplaynamePageModule() {
    }
    DisplaynamePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__displayname__["a" /* DisplaynamePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__displayname__["a" /* DisplaynamePage */]),
            ],
        })
    ], DisplaynamePageModule);
    return DisplaynamePageModule;
}());

//# sourceMappingURL=displayname.module.js.map

/***/ }),

/***/ 858:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DisplaynamePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DisplaynamePage = (function () {
    function DisplaynamePage(navCtrl, navParams, fb, app, userservice, tpStorageService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.app = app;
        this.userservice = userservice;
        this.tpStorageService = tpStorageService;
        this.submitted = false;
        this.msgstatus = false;
        this.showheader = true;
        this.authForm = this.fb.group({
            username: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])]
        });
    }
    DisplaynamePage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    DisplaynamePage.prototype.ionViewDidLoad = function () {
        this.loadusername();
    };
    DisplaynamePage.prototype.loadusername = function () {
        var _this = this;
        var userId;
        //console.log ("firebase.auth().currentUser");
        //console.log (firebase.auth().currentUser);
        if (__WEBPACK_IMPORTED_MODULE_4_firebase___default.a.auth().currentUser) {
            userId = __WEBPACK_IMPORTED_MODULE_4_firebase___default.a.auth().currentUser.uid;
        }
        else {
            //console.log ("..Trying to get userUID from storage service");
            this.tpStorageService.getItem('userUID').then(function (res) {
                //console.log ("res +++ res");
                //console.log (res);
                if (res) {
                    _this.userId = res;
                }
            }).catch(function (e) {
                _this.tpStorageService.setItem('userUID', userId);
                _this.userservice.saveUserIdInSqlLite(userId);
            });
        }
        if (userId != undefined) {
            this.userservice.getuserdetails().then(function (res) {
                if (res) {
                    _this.msgstatus = true;
                    _this.username = res['displayName'];
                }
                else {
                    _this.username = '';
                }
            }).catch(function (err) {
            });
        }
        else {
            this.username = '';
        }
    };
    DisplaynamePage.prototype.onChange = function (e) {
        var usnr = this.username;
        if (usnr.trim()) {
            this.msgstatus = true;
        }
        else {
            this.msgstatus = false;
        }
    };
    DisplaynamePage.prototype.save = function () {
        var _this = this;
        this.submitted = true;
        if (this.authForm.valid) {
            if (this.username.trim() != null || this.username.trim() != "") {
                this.username = this.username.trim();
                this.userservice.updatedisplayname(this.username).then(function (res) {
                    if (res.success) {
                        _this.app.getRootNav().push("RegisterbusinessPage", { treatNewInstall: 1 });
                    }
                });
            }
            else {
            }
        }
    };
    DisplaynamePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-displayname',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\displayname\displayname.html"*/'\n<ion-content padding>\n\n  <ion-card  class="maincard_bubble" text-center padding  >\n    <ion-card-content >\n      <ion-card-title>\n        Tell us about you\n        </ion-card-title>\n      <p>\n        Your name will help other network identify you and connect with ease\n      </p>\n    </ion-card-content>\n </ion-card>\n\n\n\n  <form [formGroup]="authForm" (submit)="save()">\n\n\n    <ion-label class="content-title"> Your Name</ion-label>\n    <ion-input type="text" (input)="onChange($event.keyCode)" formControlName="username" id="username" [value]="username" \n    [(ngModel)]="username" maxlength="30" placeholder="Your Name"></ion-input>\n    <ion-label *ngIf="(!authForm.controls.username.valid || !msgstatus)  && (authForm.controls.username.dirty || submitted)">\n        <p class="errormsg">Please enter a valid Name.</p>\n    </ion-label>\n    <button ion-button class="button-secondary" type="submit" [disabled]="!authForm.valid || !this.msgstatus" >Submit</button>\n  </form>\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\displayname\displayname.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */],
            __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], DisplaynamePage);
    return DisplaynamePage;
}());

//# sourceMappingURL=displayname.js.map

/***/ })

});
//# sourceMappingURL=54.js.map