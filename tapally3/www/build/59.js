webpackJsonp([59],{

/***/ 779:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConsumerinfoPageModule", function() { return ConsumerinfoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__consumerinfo__ = __webpack_require__(853);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ConsumerinfoPageModule = (function () {
    function ConsumerinfoPageModule() {
    }
    ConsumerinfoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__consumerinfo__["a" /* ConsumerinfoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__consumerinfo__["a" /* ConsumerinfoPage */]),
            ],
        })
    ], ConsumerinfoPageModule);
    return ConsumerinfoPageModule;
}());

//# sourceMappingURL=consumerinfo.module.js.map

/***/ }),

/***/ 853:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConsumerinfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ConsumerinfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ConsumerinfoPage = (function () {
    function ConsumerinfoPage(navCtrl, app, navParams) {
        this.navCtrl = navCtrl;
        this.app = app;
        this.navParams = navParams;
    }
    ConsumerinfoPage.prototype.addbuddy = function () {
        this.app.getRootNav().push('ContactPage', { source: "ImportpPage" });
    };
    ConsumerinfoPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad ConsumerinfoPage');
    };
    ConsumerinfoPage.prototype.backButtonClick = function () {
        this.navCtrl.pop({ animate: true, direction: "forward" });
    };
    ConsumerinfoPage.prototype.sendToBusinessReg = function () {
        this.app.getRootNav().push("RegisterbusinessPage", { treatNewInstall: 0 });
    };
    ConsumerinfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-consumerinfo',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\consumerinfo\consumerinfo.html"*/'<ion-header >\n  <ion-navbar hideBackButton>\n\n    <ion-buttons left>\n        <button ion-button (click)="backButtonClick()">\n             <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n\n    <ion-title>Earn Incentives</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <ion-card  class="maincard_bubble" text-center padding>\n    <ion-card-content >\n      <ion-card-title>\n        You Deserve Incentives For Helping Businesses\n        </ion-card-title>\n      <p>\n        Follow two easy steps to maximize earning incentives everyday!\n      </p>\n\n\n    </ion-card-content>\n </ion-card>\n\n\n <ion-card class="mycard_1">\n   <ion-card-content>\n     <ion-card-title>\n        <ion-icon name="checkmark-circle"></ion-icon> Add Business Owner\n       </ion-card-title>\n     <p>\n       First invite your known business owner from contact list.\n     </p>\n   </ion-card-content>\n </ion-card>\n <ion-card class="mycard_2"  >\n   <ion-card-content>\n     <ion-card-title>\n        <ion-icon name="checkmark-circle"></ion-icon> Send Referrals\n       </ion-card-title>\n     <p>\n       Everytime you send referral to business, the business-owner may reward you incentives (e.g. cash, gift card)\n     </p>\n\n     <button text-center  style="margin-top:15px;" ion-button (click)="addbuddy()" color="secondary"  >Let\'s Start Inviting</button>\n   </ion-card-content>\n </ion-card>\n\n<button  ion-button (click)="sendToBusinessReg()" color="primary"  >Business Owner ? Register Your Business</button>\n<p text-center>TapAlly let business owner build strong referral base, automate referrals, generate sales and boost revenue growth<p>\n\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\consumerinfo\consumerinfo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */]])
    ], ConsumerinfoPage);
    return ConsumerinfoPage;
}());

//# sourceMappingURL=consumerinfo.js.map

/***/ })

});
//# sourceMappingURL=59.js.map