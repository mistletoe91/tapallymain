webpackJsonp([10],{

/***/ 828:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchNextPageModule", function() { return SearchNextPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search_next__ = __webpack_require__(902);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SearchNextPageModule = (function () {
    function SearchNextPageModule() {
    }
    SearchNextPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__search_next__["a" /* SearchNextPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__search_next__["a" /* SearchNextPage */]),
            ],
        })
    ], SearchNextPageModule);
    return SearchNextPageModule;
}());

//# sourceMappingURL=search-next.module.js.map

/***/ }),

/***/ 902:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchNextPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SearchNextPage = (function () {
    function SearchNextPage(navCtrl, toastCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.myParam = 0;
        this.showheader = true;
        this.myParam = navParams.get('src');
        this.initializeItems();
    }
    SearchNextPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    SearchNextPage.prototype.fnRefer = function () {
        if (this.myParam == 2) {
            this.navCtrl.setRoot('SearchPage', {}, { animate: true });
            this.presentToast2();
        }
        else {
            if (this.myParam == 3) {
                this.navCtrl.setRoot('SettingsPage', {}, { animate: true });
                this.presentToast2();
            }
            else {
                this.navCtrl.setRoot('ChooseClubPage', {}, { animate: true });
                this.presentToast1();
            }
        }
    };
    SearchNextPage.prototype.presentToast1 = function () {
        var toast = this.toastCtrl.create({
            position: "top",
            message: 'Your successfully referred Gary to your contact',
            duration: 3000
        });
        toast.present();
    };
    SearchNextPage.prototype.presentToast2 = function () {
        var toast = this.toastCtrl.create({
            position: "top",
            message: 'Your send referral successfully',
            duration: 3000
        });
        toast.present();
    };
    SearchNextPage.prototype.send1 = function () {
        this.navCtrl.setRoot('SearchNextSendPage', {}, { animate: true });
    };
    SearchNextPage.prototype.initializeItems = function () {
        this.items = [
            'Kelly	Roberson',
            'Colleen	Bryant',
            'Buenos Aires',
            'Eric	Pearson',
            'Christina	Bowen',
            'Pat	Moore',
            'Myra	Parks',
            'Dianna	Mack',
            'Clayton	Barrett',
            'Cynthia	Ross',
            'Noah	Bryan',
            'Pat	Moore',
            'Myra	Parks',
            'Dianna	Mack',
        ];
    };
    SearchNextPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SearchNextPage');
    };
    SearchNextPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-search-next',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\search-next\search-next.html"*/'<ion-header  >\n  <ion-navbar color="primary" >\n    <ion-title>\n      Send Referral\n    </ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name=\'menu\'></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <div class="pull-center">\n      <p class="text_color_primary" >Tap on the contact to send referral</p>\n\n\n      <ion-searchbar (ionInput)="getItems($event)" placeholder="Search Contacts"></ion-searchbar>\n      <ion-list>\n        <ion-item (click)="fnRefer()"  *ngFor="let item of items">\n          <p>{{item}}</p>\n          <ion-icon end item-end   name=\'arrow-dropright-circle\'> </ion-icon>\n        </ion-item>\n      </ion-list>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\search-next\search-next.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */]])
    ], SearchNextPage);
    return SearchNextPage;
}());

//# sourceMappingURL=search-next.js.map

/***/ })

});
//# sourceMappingURL=10.js.map