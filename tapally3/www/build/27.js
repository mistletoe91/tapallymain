webpackJsonp([27],{

/***/ 811:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhoneverfyPageModule", function() { return PhoneverfyPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__phoneverfy__ = __webpack_require__(885);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PhoneverfyPageModule = (function () {
    function PhoneverfyPageModule() {
    }
    PhoneverfyPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__phoneverfy__["a" /* PhoneverfyPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__phoneverfy__["a" /* PhoneverfyPage */]),
            ],
        })
    ], PhoneverfyPageModule);
    return PhoneverfyPageModule;
}());

//# sourceMappingURL=phoneverfy.module.js.map

/***/ }),

/***/ 885:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PhoneverfyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(413);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_angularfireconfig__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Angular core


// Ionic-angular

// Providers



// Firebse javascript

var timeDurection = 120;
var PhoneverfyPage = (function () {
    function PhoneverfyPage(navCtrl, navParams, fb, userService, loading, authService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.userService = userService;
        this.loading = loading;
        this.authService = authService;
        this.maxTime = timeDurection;
        this.hidevalue = true;
        this.InvalidNumErr = '';
        this.authForm = this.fb.group({
            'otpTemp': [null, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required])]
        });
        this.otpTemp = this.authForm.controls['otpTemp'];
        this.verificationCredential = this.navParams.get('code');
        this.mobileNumber = this.navParams.get('number');
    }
    PhoneverfyPage.prototype.ionViewDidLoad = function () {
        this.message = "";
        this.StartTimer();
    };
    PhoneverfyPage.prototype.StartTimer = function () {
        var _this = this;
        this.timer = setTimeout(function (x) {
            if (_this.maxTime <= 0) { }
            _this.maxTime -= 1;
            if (_this.maxTime > 0) {
                _this.hidevalue = false;
                _this.StartTimer();
            }
            else {
                _this.hidevalue = true;
                _this.maxTime = timeDurection;
            }
        }, 1000);
    };
    PhoneverfyPage.prototype.verify = function (code) {
        var _this = this;
        this.loading.presentLoading();
        console.log(this.mobileNumber);
        console.log(this.verificationCredential);
        this.authService.otpVerify('phones/verification/check?api_key=' + __WEBPACK_IMPORTED_MODULE_6__app_app_angularfireconfig__["a" /* apiKey */] + '&country_code=' + this.verificationCredential + '&phone_number=' + this.mobileNumber + '&verification_code=' + code)
            .then(function (res) {
            if (res.status == 200) {
                _this.userService.addmobileUserIOS(_this.verificationCredential, _this.mobileNumber)
                    .then(function (data) {
                    _this.loading.dismissMyLoading();
                    //if i am invieted. lets make friends
                    //this.tpStorageService.getItem('invited_by').then((userId__: any) => {
                    //}).catch(e => { });
                    if (data.success) {
                        _this.navCtrl.setRoot("DisplaynamePage");
                    }
                    else {
                        _this.message = data.msg;
                    }
                });
            }
        }).catch(function (err) {
            _this.loading.dismissMyLoading();
            if (err.status == 401) {
                _this.message = "Enter Valid Verification Code";
            }
            if (err.status == 429) {
                _this.message = "You has tried to check too many codes. Try to enter valid verification code.";
            }
        });
    };
    PhoneverfyPage.prototype.resend = function () {
        var _this = this;
        this.loading.presentLoading();
        var data = {
            "via": "sms",
            "phone_number": this.mobileNumber,
            "country_code": this.verificationCredential,
            "api_key": __WEBPACK_IMPORTED_MODULE_6__app_app_angularfireconfig__["a" /* apiKey */]
        };
        this.authService.sendVerificationCode(data, 'phones/verification/start')
            .then(function (res) {
            _this.loading.dismissMyLoading();
            var data = JSON.parse(res['data']);
            if (res.status == 200) {
                _this.loading.presentToast(data['message']);
            }
            else {
                _this.loading.presentToast(data['message']);
            }
            _this.StartTimer();
        }).catch(function (err) {
            _this.loading.dismissMyLoading();
        });
    };
    PhoneverfyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-phoneverfy',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\phoneverfy\phoneverfy.html"*/'<ion-content   class=" bg-grey ">\n\n  <ion-card text-center class="topmargin_phone" >\n    <ion-card-content>\n      <ion-card-title>\n        Confirm The Code\n        </ion-card-title>\n      <p>\n        Enter the code from the message we sent to you\n      </p>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card>\n    <ion-card-content> \n      <div *ngIf="InvalidNumErr" class="validation-msg">\n            <p> {{InvalidNumErr}}</p>\n      </div>\n      <div *ngIf="message != \'\' ">{{message}}</div>\n      <ion-list>\n          <ion-item>\n            <ion-input type="number" minlength="4" maxlength="6" [formControl]="otpTemp" id="otpTemp" [(ngModel)]="otpNumber" name="otpTemp" placeholder="Enter OTP"></ion-input>\n          </ion-item>\n            <ion-item>\n              <button ion-button class="button-secondary" color="dark" item-right [disabled]="!authForm.valid" (click)="verify(otpNumber)">Submit</button>\n            </ion-item>\n            <ion-item>\n              <button ion-button class="button-primary" color="dark" item-right  [disabled]="hidevalue==false" (click)="resend()">Resend</button>\n            </ion-item>\n            <p *ngIf="hidevalue==false"> Didn\'t received yet? Click on resend button after {{maxTime}} seconds.</p>\n            <ion-item>\n            <div id="re-container"></div>\n          </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\phoneverfy\phoneverfy.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */]])
    ], PhoneverfyPage);
    return PhoneverfyPage;
}());

//# sourceMappingURL=phoneverfy.js.map

/***/ })

});
//# sourceMappingURL=27.js.map