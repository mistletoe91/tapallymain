webpackJsonp([13],{

/***/ 825:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchChatsPageModule", function() { return SearchChatsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search_chats__ = __webpack_require__(899);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SearchChatsPageModule = (function () {
    function SearchChatsPageModule() {
    }
    SearchChatsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__search_chats__["a" /* SearchChatsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__search_chats__["a" /* SearchChatsPage */]),
            ],
        })
    ], SearchChatsPageModule);
    return SearchChatsPageModule;
}());

//# sourceMappingURL=search-chats.module.js.map

/***/ }),

/***/ 899:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchChatsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_chat_chat__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_user_user__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SearchChatsPage = (function () {
    function SearchChatsPage(zone, app, navCtrl, chatservice, viewCtrl, storage, userservice, platform) {
        this.zone = zone;
        this.app = app;
        this.navCtrl = navCtrl;
        this.chatservice = chatservice;
        this.viewCtrl = viewCtrl;
        this.storage = storage;
        this.userservice = userservice;
        this.isNoRecord = false;
        this.showheader = true;
    }
    SearchChatsPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    SearchChatsPage.prototype.ionViewDidLoad = function () {
    };
    SearchChatsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.storage.get('allBuddyChats').then(function (res) {
            _this.buddy = _this.chatservice.buddy;
            _this.userservice.getuserdetails().then(function (res) {
                _this.photoURL = res['photoURL'];
            });
            _this.todaydate = _this.formatDate(new Date());
            _this.allChats = res;
            _this.tempallChats = _this.allChats;
        });
    };
    SearchChatsPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    SearchChatsPage.prototype.initializeContacts = function () {
        var _this = this;
        this.zone.run(function () {
            _this.allChats = _this.tempallChats;
        });
    };
    SearchChatsPage.prototype.searchuser = function (ev) {
        this.initializeContacts();
        var myArray = [];
        var isNoRecord = false;
        this.isNoRecord = false;
        var val = ev.target.value;
        if (val && val.trim() != '') {
            for (var i = 0; i < this.allChats.length; ++i) {
                myArray.push({ date: this.allChats[i].date, messages: [] });
                for (var j = 0; j < this.allChats[i].messages.length; ++j) {
                    if (this.allChats[i].messages[j].multiMessage) {
                    }
                    else if (this.allChats[i].messages[j].type == 'image') {
                    }
                    else if (this.allChats[i].messages[j].type == 'document') {
                    }
                    else {
                        if (this.allChats[i].messages[j].message.toLowerCase().indexOf(val.toLowerCase()) > -1) {
                            isNoRecord = true;
                            myArray[i].messages.push(this.allChats[i].messages[j]);
                        }
                    }
                }
            }
            if (isNoRecord != true) {
                this.isNoRecord = true;
            }
            this.allChats = myArray;
        }
    };
    SearchChatsPage.prototype.formatDate = function (date) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        if (dd < 10)
            dd = '0' + dd;
        if (mm < 10)
            mm = '0' + mm;
        return dd + '/' + mm + '/' + yyyy;
    };
    SearchChatsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-search-chats',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\search-chats\search-chats.html"*/'\n<ion-header  >\n  <ion-navbar >\n		<div class="search-wrap">\n			<ion-buttons class="close-left" left>\n				<button ion-button icon-only (click)="dismiss()"><ion-icon ios="md-close" md="md-close" ></ion-icon></button>\n			</ion-buttons>\n			<ion-searchbar class="searchbar" placeholder="Search" (ionInput)="searchuser($event)"></ion-searchbar>\n		</div>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content #content>\n	<div class = "chatwindow">\n	    <ion-list no-lines>\n	      <div *ngIf="isNoRecord" padding text-center>No record found!</div>\n	      <ng-container *ngFor = "let items of allChats; let j = index">\n	        <ion-list-header *ngIf="items.date == todaydate && items.messages.length > 0" align="center">Today</ion-list-header>\n	        <ion-list-header *ngIf="items.date != todaydate && items.messages.length > 0" align="center">{{items.date}}</ion-list-header>\n\n	        <div *ngFor = "let item of items.messages; let i = index">\n						<ion-item text-wrap >\n							<div [ngClass]="{ \'select-item\': item.selection }" (press)="popoverdialog(item,$event)" (click)="popoveranothermsg(item,$event)">\n								<div class="msg-wrap you"  *ngIf="item.sentby === buddy.uid" >\n									<ion-avatar>\n										<img src="{{buddy.photoURL}}">\n									</ion-avatar>\n									<div [ngClass]="item.type == \'image\'?\'bubble-img\':\'bubble\'">\n											<div class="msg" *ngIf="item.type == \'message\'">{{item.message}}</div>\n											<img class="msg" *ngIf="item.type == \'location\'" src="assets/imgs/location.png"\n											(click)="showLocation(item.message)" />\n											<img class="msg" *ngIf="item.type == \'document\'" src="assets/imgs/docs.png"  />\n\n											<ng-container class="msg" *ngIf="item.multiMessage && item.type==\'image\'">\n													<div (click)="openAllImg(item.message)">\n															<img class="msg-partision" *ngFor="let pic of item.message | slice:0:4; let ii = index "\n																	src="{{pic}}" />\n															<span class="img-number overlay">+ {{item.message.length - 4}}</span>\n													</div>\n											</ng-container>\n\n											<ng-container *ngIf="!item.multiMessage ">\n													<img class="msg" *ngIf="item.type == \'image\'" #mychatImage src="{{item.message}}"\n															(click)="presentImage(mychatImage)" />\n											</ng-container>\n											<ng-container class="msg" *ngIf="item.multiMessage && item.type == \'contact\'" >\n													<img class="msg contact" src="assets/imgs/user.png" />\n													<div (click)="openContacts(item.message)" class="view-msg">View {{ item.message.length }} Contacts</div>\n											</ng-container>\n										<div class="read-recipients">\n											<p class="icon">{{item.timeofmsg}}</p>\n										</div>\n									</div>\n								</div>\n								<div class="msg-wrap me" *ngIf="item.sentby != buddy.uid">\n									<ion-avatar>\n											<img src="{{photoURL}}">\n									</ion-avatar>\n									<div [ngClass]="item.type == \'image\'?\'bubble-img\':\'bubble\'">\n											<div class="msg" *ngIf="item.type == \'message\'">{{item.message}}</div>\n											<img class="msg" *ngIf="item.type == \'document\'" src="assets/imgs/docs.png" (click)="openDoc(item.message)" />\n											<img class="msg" *ngIf="item.type == \'location\'" src="assets/imgs/location.png"\n													(click)="showLocation(item.message)" />\n\n											<audio controls class="msg" *ngIf="item.type == \'audio\'">\n													<source src="{{item.message}}" type="audio/mpeg">\n\n											</audio>\n											<ng-container class="msg" *ngIf="item.multiMessage && item.type==\'image\'" >\n													<div (click)="openAllImg(item.message)">\n															<img class="msg-partision" *ngFor="let pic of item.message | slice:0:4; let ii = index "\n																	src="{{pic}}" />\n															<span class="img-number overlay">+ {{item.message.length - 4}}</span>\n													</div>\n											</ng-container>\n											<ng-container *ngIf="!item.multiMessage ">\n													<img class="msg" *ngIf="item.type == \'image\'" #mychatImage src="{{item.message}}"\n															(click)="presentImage(mychatImage)" />\n											</ng-container>\n											<ng-container class="msg" *ngIf="item.multiMessage && item.type == \'contact\'" >\n													<img class="msg contact" src="assets/imgs/user.png" />\n													<div (click)="openContacts(item.message)" class="view-msg">View {{ item.message.length }} Contact</div>\n											</ng-container>\n\n\n												<div class="read-recipients">\n													<p class="icon">{{item.timeofmsg}}</p>\n													<ion-icon *ngIf="!item.isRead" name="checkmark"></ion-icon>\n													<ion-icon *ngIf="item.isRead" color="secondary" name="checkmark" class="checkmark-icon"></ion-icon>\n													<ion-icon class="checkmark-icon icon-align icon-align-ios" *ngIf="item.isRead" color="secondary" name="checkmark"></ion-icon>\n												</div>\n\n									</div>\n							</div>\n						</div>\n						</ion-item>\n					</div>\n	      </ng-container>\n	    </ion-list>\n	  </div>\n\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\search-chats\search-chats.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_chat_chat__["a" /* ChatProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_4__providers_user_user__["a" /* UserProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */]])
    ], SearchChatsPage);
    return SearchChatsPage;
}());

//# sourceMappingURL=search-chats.js.map

/***/ })

});
//# sourceMappingURL=13.js.map