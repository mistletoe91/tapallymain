webpackJsonp([37],{

/***/ 801:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IncentivedetailPageModule", function() { return IncentivedetailPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__incentivedetail__ = __webpack_require__(875);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var IncentivedetailPageModule = (function () {
    function IncentivedetailPageModule() {
    }
    IncentivedetailPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__incentivedetail__["a" /* IncentivedetailPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__incentivedetail__["a" /* IncentivedetailPage */]),
            ],
        })
    ], IncentivedetailPageModule);
    return IncentivedetailPageModule;
}());

//# sourceMappingURL=incentivedetail.module.js.map

/***/ }),

/***/ 875:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IncentivedetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_sqlite__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_chat_chat__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_fcm_fcm__ = __webpack_require__(98);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var IncentivedetailPage = (function () {
    function IncentivedetailPage(fcm, platform, toastCtrl, chatservice, userservice, sqlite, navCtrl, navParams, app) {
        this.fcm = fcm;
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.chatservice = chatservice;
        this.userservice = userservice;
        this.sqlite = sqlite;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.app = app;
        this.pageDoneLoading = false;
        this.recordExist = true;
        this.item = {
            "received_by": "",
            "myKey": "",
            "sent_by": ""
        };
    }
    IncentivedetailPage.prototype.buddychat = function (buddy) {
        //mytodo : it should go to buddychatpage but it is not working on IOS
        //this.navCtrl.setRoot('BuddychatPage', {back_button_pop_on : false});
        if (this.platform.is('ios')) {
            //ios
            this.navCtrl.setRoot('TabsPage');
            var toast = this.toastCtrl.create({
                message: 'Operation performed successfully',
                duration: 3000,
                position: 'top'
            });
            toast.present();
        }
        else {
            //android
            this.navCtrl.setRoot('BuddychatPage', { back_button_pop_on: false });
        }
    };
    IncentivedetailPage.prototype.sendBuddyMessage_RedeemRequest = function (buddy) {
        var _this = this;
        var msg = "Request For Incentives";
        this.chatservice.addnewmessageRedeemRequest(msg, 'message', '', this.item.myKey, this.item.sent_by, this.item.received_by).then(function () {
            if (buddy) {
                var newMessage = 'You received a request to redeem incentives';
                _this.fcm.sendNotification(buddy, newMessage, 'chatpage');
                _this.buddychat(buddy);
            }
        });
    };
    IncentivedetailPage.prototype.fnOpenChatPage = function () {
        var _this = this;
        this.userservice.getbuddydetails(this.item.received_by).then(function (buddy) {
            _this.chatservice.initializebuddy(buddy);
            _this.buddychat(buddy);
        });
    };
    IncentivedetailPage.prototype.fnAskBuddyToAddSomeIncentives = function () {
        var _this = this;
        //Send request to add some incentive and register business
        this.userservice.getbuddydetails(this.item.received_by).then(function (buddy) {
            _this.chatservice.initializebuddy(buddy);
            _this.sendBuddyMessage_RedeemRequest(buddy);
        });
    };
    IncentivedetailPage.prototype.fnSendRedeemIncentiveRequest = function () {
        var _this = this;
        //Send requet to redeem Incentive
        this.userservice.getbuddydetails(this.item.received_by).then(function (buddy) {
            //this.chatservice.initializebuddy(buddy);
            _this.sendBuddyMessage_RedeemRequest(buddy);
            _this.navCtrl.pop({ animate: true, direction: "right" });
            var toast = _this.toastCtrl.create({
                message: 'Request has been posted',
                duration: 3000,
                position: 'top'
            });
            toast.present();
        });
    };
    IncentivedetailPage.prototype.ionViewDidLoad = function () {
        //this.rtlistComplete = this.rtService.getList();
        if (this.navParams.get('item')) {
            this.item = JSON.parse(this.navParams.get('item'));
            this.pageDoneLoading = true;
        }
    };
    IncentivedetailPage.prototype.backButtonClick = function () {
        this.navCtrl.pop({ animate: true, direction: "right" });
    };
    IncentivedetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-incentivedetail',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\incentivedetail\incentivedetail.html"*/'<ion-header >\n  <ion-navbar hideBackButton>\n\n    <ion-buttons left>\n        <button ion-button (click)="backButtonClick()">\n             <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n\n    <ion-title>Referal Detail</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <ion-card  *ngIf="pageDoneLoading" class="maincard_bubble" text-center padding>\n    <ion-card-content >\n      <ion-card-title>\n        You Sent Referral To {{item.displayName}}\n        </ion-card-title>\n      <p>\n         You sent referral of <ion-badge color="secondary" item-end>{{item.referral_displayName}}</ion-badge> to <ion-badge item-end>{{item.displayName}}</ion-badge> on {{item.dateofmsg}} at {{item.timeofmsg}} ET. <span *ngIf="item.business_referral_type_id<=0">However <ion-badge item-end>{{item.displayName}}</ion-badge> has not specified any incentives for referrals</span> <span *ngIf="item.business_referral_type_id>0">{{item.displayName}} has specified an incentive of <ion-badge item-end>{{item.business_referral_type_name}} : {{item.business_referral_detail}}</ion-badge></span> <span *ngIf="item.business_referral_type_id>0 && item.redeemStatus<=0">However you have not redeemed incentive yet. </span> <span *ngIf="item.business_referral_type_id>0 && item.redeemStatus==1">You have send request to {{item.displayName}} to redeem your referral incentive</span> <span *ngIf="item.redeemStatus>1">You have sucessfully redeemed your incentive from {{item.displayName}}. In case of any question reach out to {{item.displayName}}</span>\n      </p>\n\n    </ion-card-content>\n </ion-card>\n\n\n       <button block  *ngIf="item.business_referral_type_id<=0 && item.redeemStatus<2" style="margin-top:15px;" ion-button (click)="fnAskBuddyToAddSomeIncentives()" color="primary"  >Ask For Incentives</button>\n\n       <button block  *ngIf="item.business_referral_type_id>0 && item.redeemStatus<=0" style="margin-top:15px;" ion-button (click)="fnSendRedeemIncentiveRequest()" color="primary"  >Send Redeem Request</button>\n\n       <button block  *ngIf="item.business_referral_type_id>0 && item.redeemStatus==1" style="margin-top:15px;" ion-button (click)="fnSendRedeemIncentiveRequest()" color="primary"  >Send Redeem Request Again</button>\n\n       <button block  *ngIf="item.business_referral_type_id>0 && item.redeemStatus>1" style="margin-top:15px;" ion-button (click)="fnOpenChatPage()" color="secondary"  >Chat With {{item.displayName}}</button>\n\n\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\incentivedetail\incentivedetail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__providers_fcm_fcm__["a" /* FcmProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */], __WEBPACK_IMPORTED_MODULE_4__providers_chat_chat__["a" /* ChatProvider */], __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_sqlite__["a" /* SQLite */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */]])
    ], IncentivedetailPage);
    return IncentivedetailPage;
}());

//# sourceMappingURL=incentivedetail.js.map

/***/ })

});
//# sourceMappingURL=37.js.map