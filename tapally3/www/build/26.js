webpackJsonp([26],{

/***/ 812:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PickContactFromPageModule", function() { return PickContactFromPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pick_contact_from__ = __webpack_require__(886);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PickContactFromPageModule = (function () {
    function PickContactFromPageModule() {
    }
    PickContactFromPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__pick_contact_from__["a" /* PickContactFromPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__pick_contact_from__["a" /* PickContactFromPage */]),
            ],
        })
    ], PickContactFromPageModule);
    return PickContactFromPageModule;
}());

//# sourceMappingURL=pick-contact-from.module.js.map

/***/ }),

/***/ 886:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PickContactFromPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_contact_contact__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_requests_requests__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_sms_sms__ = __webpack_require__(408);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_fcm_fcm__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_app_angularfireconfig__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









//import { GroupsProvider } from '../../providers/groups/groups';


var PickContactFromPage = (function () {
    function PickContactFromPage(events, platform, loadingProvider, contactProvider, userservice, navCtrl, navParams, alertCtrl, requestservice, chatservice, zone, smsservice, fcm, modalCtrl, toastCtrl, tpStorageService) {
        this.events = events;
        this.platform = platform;
        this.loadingProvider = loadingProvider;
        this.contactProvider = contactProvider;
        this.userservice = userservice;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.requestservice = requestservice;
        this.chatservice = chatservice;
        this.zone = zone;
        this.smsservice = smsservice;
        this.fcm = fcm;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.tpStorageService = tpStorageService;
        this.showheader = true;
        this.contactList = [];
        this.allregisteUserData = [];
        this.metchContact = [];
        this.mainArray = [];
        this.tempArray = [];
        this.userpic = __WEBPACK_IMPORTED_MODULE_9__app_app_angularfireconfig__["c" /* env */].userPic;
        this.isData = false;
        this.reqStatus = false;
        this.isInviteArray = [];
        this.newrequest = {};
        this.temparr = [];
        this.filteredusers = [];
        this.friend_asking_referral_uid = navParams.get('friend_asking_referral_uid');
        this.friend_asking_referral_displayName = navParams.get('friend_asking_referral_displayName');
        this.friend_asking_referral_catId = navParams.get('friend_asking_referral_catId');
        this.friend_asking_referral_catName = navParams.get('friend_asking_referral_catName');
        this.source = navParams.get('source');
        this.cntCounter = 0;
    }
    PickContactFromPage.prototype.tpInitilizeFromStorage = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (res) {
            if (res) {
                _this.userId = res;
            }
        }).catch(function (e) { });
        this.tpStorageService.getItem('updated').then(function (res) {
            if (res) {
                _this.updatedRef = res;
            }
        }).catch(function (e) { });
    };
    PickContactFromPage.prototype.backButtonClick = function () {
        this.navCtrl.pop({
            animate: true,
            direction: "forward"
        });
    };
    PickContactFromPage.prototype.pickContactExecute = function (buddy, isTapAllyMember) {
        //console.log ("pickContactExecutepickContactExecute");
        //console.log (buddy);
        //console.log (this.myfriends);
        //console.log (this.friend_asking_referral_uid);
        //Tapally Member so send push notification
        /*
           - First Get all buddies
           - Then select send to. It should be this.friend_asking_referral_uid
           - Then select message with buddy information
           - Then send push notification
           - Also send chat messages
           - Redirect back to buddypage
        */
        for (var key = 0; key < this.myfriends.length; key++) {
            ////console.log (this.myfriends[key].displayName)
            //Find out who is receipient
            if (this.friend_asking_referral_uid == this.myfriends[key].uid) {
                //send to the one who asked for referral
                var catStr = '';
                if (this.friend_asking_referral_catName) {
                    catStr = '(' + this.friend_asking_referral_catName + ')';
                }
                this.newmessage = 'Hi I am sending ' + catStr + ' referral to you : ' + buddy.displayName + " (" + buddy.mobile + ")";
                this.addmessage(this.myfriends[key], buddy, this.myfriends[key]);
                //also tell the one who got referred
                if (isTapAllyMember) {
                    //referral is member of Tapally
                    this.newmessage = 'Hi I send your referral to ' + this.myfriends[key].displayName + " (" + this.myfriends[key].mobile + ")";
                    if (catStr) {
                        this.newmessage += '. ' + this.myfriends[key].displayName + ' is asking a referral for ' + catStr;
                    } //endif
                    this.addmessage(buddy, this.myfriends[key], this.myfriends[key]);
                }
                else {
                    //referral is not member of Tapally
                    console.log("this.myfriends[key]");
                    console.log(this.myfriends[key]);
                    var msg = 'Hi, I send your referral to ' + this.myfriends[key].displayName + " (" + this.myfriends[key].mobile + "). Sign up & download app at https://tapally.com ";
                    this.sendSMSCustomMsg(buddy, msg, this.myfriends[key]);
                } //endif
                break;
            }
        } //end for
    };
    PickContactFromPage.prototype.sendToNextPage = function (originalChat) {
        this.cntCounter++;
        //console.log ("cntCounter:" + this.cntCounter);
        if (this.cntCounter >= 2) {
            this.cntCounter = 0;
            this.chatservice.initializebuddy(originalChat);
            if (this.source == "buddypage") {
                //mytodo : this should go to buddychatpage as well but it was not working properly so i temporarliy sending to tabspage with a toast
                this.navCtrl.setRoot('TabsPage');
                var toast = this.toastCtrl.create({
                    message: 'Your referral sent successfully. You can see status by clicking on your contact`s icon',
                    duration: 5000
                });
                toast.present();
            }
            else {
                this.navCtrl.setRoot('BuddychatPage', { back_button_pop_on: false });
            }
        }
    };
    //here buddy is receipient and referral is contact being sent
    PickContactFromPage.prototype.addmessage = function (buddy, referral, originalChat) {
        var _this = this;
        //Send Push notification : mytodo check if the buddy is online or not. if online then no need to send push notification
        var newMessage = 'You have received a message';
        this.fcm.sendNotification(buddy, newMessage, 'chatpage');
        //Send chat message
        this.chatservice.addnewmessageDirectReferral(this.newmessage, 'message', buddy, referral, 'backward').then(function () {
            _this.newmessage = '';
            _this.sendToNextPage(originalChat);
        });
    };
    PickContactFromPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.cntCounter = 0;
        this.tpInitilizeFromStorage();
        this.requestservice.getmyfriends();
        this.myfriends = [];
        this.events.subscribe('friends', function () {
            _this.events.unsubscribe('friends');
            _this.myfriends = [];
            _this.myfriends = _this.requestservice.myfriends;
        });
    };
    PickContactFromPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.loadingProvider.presentLoading();
        this.contactProvider.getSimJsonContacts()
            .then(function (res) {
            console.log("asdjasdas");
            console.log(res);
            Object.keys(res).forEach(function (ele) {
                if (ele == "contacts") {
                    _this.mainArray = [];
                    Object.keys(res["contacts"]).forEach(function (key) {
                        _this.mainArray.push({
                            "displayName": res["contacts"][key].displayName,
                            "mobile": res["contacts"][key].mobile,
                            "mobile_formatted": res["contacts"][key].mobile_formatted,
                            "status": "",
                            "isBlock": false,
                            "isUser": 0,
                            "dim": false,
                            "isdisable": false,
                            "photoURL": res["contacts"][key].photoURL,
                            "uid": res["contacts"][key].uid
                        });
                    });
                } //endif
            });
            _this.loadingProvider.dismissMyLoading();
        }).catch(function (e) { });
    }; //end function
    PickContactFromPage.prototype.removeDuplicates = function (originalArray, prop) {
        var newArray = [];
        var lookupObject = {};
        originalArray.forEach(function (item, index) {
            lookupObject[originalArray[index][prop]] = originalArray[index];
        });
        Object.keys(lookupObject).forEach(function (element) {
            newArray.push(lookupObject[element]);
        });
        return newArray;
    };
    PickContactFromPage.prototype.ionViewWillLeave = function () {
        //this.groupservice.getmygroups();
        //this.requestservice.getmyfriends();
        this.loadingProvider.dismissMyLoading();
    };
    PickContactFromPage.prototype.initializeContacts = function () {
        this.mainArray = this.tempArray;
    };
    PickContactFromPage.prototype.refreshPage = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Sync All Contacts',
            message: 'It may take few minutes to re-sync all contacts from your device. Do you agree ? ',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                        //console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Agree',
                    handler: function () {
                        //console.log('Agree clicked');
                        _this.contactProvider.deleteFromTable();
                        _this.navCtrl.setRoot('ContactimportprogressPage', { source: 'PickContactFromPage' });
                    }
                }
            ]
        });
        confirm.present();
    };
    PickContactFromPage.prototype.searchuser = function (ev) {
        if (this.platform.is('ios')) {
            return this.searchuser_ios(ev);
        }
        else {
            return this.searchuser_android(ev);
        }
    };
    PickContactFromPage.prototype.searchuser_ios = function (ev) {
        var _this = this;
        //initize the array
        if (this.tempArray.length <= 0) {
            this.tempArray = this.mainArray;
        }
        else {
            this.mainArray = this.tempArray;
        } //endif
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.mainArray.forEach(function (item, index) {
                if (_this.mainArray[index].displayName.toLowerCase().indexOf(val.toLowerCase()) > -1) {
                    _this.mainArray[index].dim = false;
                }
                else {
                    _this.mainArray[index].dim = true;
                } //endif
            });
        }
        else {
            this.mainArray.forEach(function (item, index) {
                _this.mainArray[index].dim = false;
            });
        } //endif 
    }; //end function
    PickContactFromPage.prototype.searchuser_android = function (ev) {
        //initize the array
        if (this.tempArray.length <= 0) {
            this.tempArray = this.mainArray;
        }
        else {
            this.mainArray = this.tempArray;
        } //endif
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.mainArray = this.mainArray.filter(function (item) {
                if (item.displayName != undefined) {
                    return (item.displayName.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
            });
        }
    };
    PickContactFromPage.prototype.inviteReq = function (recipient) {
        /*let alert = this.alertCtrl.create({
            title: 'Invitation',
            subTitle: 'Invitation already sent to ' + recipient.displayName + '.',
            buttons: ['Ok']
        });
        alert.present();
        */
    };
    PickContactFromPage.prototype.sendreq = function (recipient) {
        var _this = this;
        this.reqStatus = true;
        this.newrequest.sender = this.userId;
        this.newrequest.recipient = recipient.uid;
        if (this.newrequest.sender === this.newrequest.recipient)
            alert('You are your friend always');
        else {
            var successalert_1 = this.alertCtrl.create({
                title: 'Request sent',
                subTitle: 'Your request has been sent to ' + recipient.displayName + '.',
                buttons: ['ok']
            });
            var userName_1;
            this.userservice.getuserdetails().then(function (res) {
                userName_1 = res.displayName;
                var newMessage = userName_1 + " has sent you friend request.";
                _this.fcm.sendNotification(recipient, newMessage, 'sendreq');
            });
            this.requestservice.sendrequest(this.newrequest).then(function (res) {
                if (res.success) {
                    _this.reqStatus = false;
                    successalert_1.present();
                    var sentuser = _this.mainArray.indexOf(recipient);
                    _this.mainArray[sentuser].status = "pending";
                }
            }).catch(function (err) { });
        }
    };
    PickContactFromPage.prototype.buddychat = function (buddy) {
        this.chatservice.initializebuddy(buddy);
        this.navCtrl.setRoot('BuddychatPage', { back_button_pop_on: false });
    };
    PickContactFromPage.prototype.sendSMSCustomMsg = function (item, msg, originalChat) {
        var _this = this;
        this.loadingProvider.presentLoading();
        this.smsservice.sendSmsCustomMsg(item.mobile, msg).then(function (res) {
            _this.loadingProvider.dismissMyLoading();
            _this.sendToNextPage(originalChat);
        }).catch(function (err) {
            //console.log (err);
            _this.loadingProvider.dismissMyLoading();
            _this.sendToNextPage(originalChat);
            /*let alert = this.alertCtrl.create({
                title: 'Message Send',
                subTitle: 'Error for sending message to ' + item.displayName + '.',
                buttons: ['Ok']
            });
            alert.present();
            */
        });
    }; //end function
    PickContactFromPage.prototype.sendSMS = function (item) {
        var _this = this;
        this.loadingProvider.presentLoading();
        this.smsservice.sendSms(item.mobile).then(function (res) {
            _this.loadingProvider.dismissMyLoading();
            if (res) {
            }
            else {
            }
        }).catch(function (err) {
            _this.loadingProvider.dismissMyLoading();
        });
    };
    PickContactFromPage.prototype.addnewContact = function (myEvent) {
        var profileModal = this.modalCtrl.create("AddContactPage");
        profileModal.present({
            ev: myEvent
        });
        profileModal.onDidDismiss(function (pages) { });
    };
    PickContactFromPage.prototype.goBack = function () {
        this.navCtrl.setRoot('TabsPage');
    };
    PickContactFromPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-pick-contact-from',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\pick-contact-from\pick-contact-from.html"*/'<ion-header  >\n  <ion-navbar hideBackButton >\n    <div class="header-wrap">\n        <div class="flex-start">\n          <ion-buttons left>\n              <button ion-button (click)="goBack()">\n                   <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n              </button>\n          </ion-buttons>\n          <div class="header-title">Pick Your Contact</div>\n        </div>\n        <div class="flex-end">\n          <ion-buttons end>\n              <button ion-button icon-only (click)="refreshPage()">\n                <ion-icon class="rightsidebtncss"  name="refresh-circle"></ion-icon>\n              </button>\n          </ion-buttons>\n        </div>\n    </div>\n  </ion-navbar>\n  <ion-searchbar placeholder="Search" (ionInput)="searchuser($event)"></ion-searchbar>\n  <p class="referral_for_friend" >Pick a referral to send back to  {{friend_asking_referral_displayName}}</p>\n</ion-header>\n<ion-content>\n\n  <ion-list [virtualScroll]="mainArray"  class="contactslistion" > \n    <ion-item *virtualItem="let item"  [ngClass]="item.dim?\'itemside_dim\':\'itemside\'"  >\n      <ion-avatar item-start >\n        <img *ngIf="item.isBlock == false" src="{{item.photoURL || userpic}}">\n        <img *ngIf="item.isBlock == true" src="{{\'assets/imgs/user.png\' || userpic }}">\n      </ion-avatar>\n      <ion-note class="itemnote"  item-start>\n        <h2 class="displaynamecss" *ngIf="item.displayName">{{item.displayName}} </h2>\n        <p *ngIf="item.mobile_formatted"  [innerHTML]="item.mobile_formatted" ></p>\n      </ion-note>\n\n      <button item-end  (click)="pickContactExecute(item,true)" *ngIf="item.uid">\n        <ion-icon class="mainiconcontact" name="checkmark-circle"></ion-icon>\n      </button>\n      <button item-end   (click)="pickContactExecute(item,false)" *ngIf="!item.uid && !item.isdisable">\n        <ion-icon class="mainiconcontact"  name="add-circle"></ion-icon>\n      </button>\n      <button item-end   (click)="pickContactExecute(item,false)" *ngIf="!item.uid && item.isdisable">\n        <ion-icon class="mainiconcontact"   name="time"></ion-icon>\n      </button>\n    </ion-item>\n  </ion-list>\n\n <div *ngIf="isData" padding-horizontal>\n        No contacts found!\n </div>\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\pick-contact-from\pick-contact-from.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_contact_contact__["a" /* ContactProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__["a" /* ChatProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_7__providers_sms_sms__["a" /* SmsProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_fcm_fcm__["a" /* FcmProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_10__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], PickContactFromPage);
    return PickContactFromPage;
}());

//# sourceMappingURL=pick-contact-from.js.map

/***/ })

});
//# sourceMappingURL=26.js.map