webpackJsonp([68],{

/***/ 770:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlockUserPageModule", function() { return BlockUserPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__block_user__ = __webpack_require__(844);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BlockUserPageModule = (function () {
    function BlockUserPageModule() {
    }
    BlockUserPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__block_user__["a" /* BlockUserPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__block_user__["a" /* BlockUserPage */]),
            ],
        })
    ], BlockUserPageModule);
    return BlockUserPageModule;
}());

//# sourceMappingURL=block-user.module.js.map

/***/ }),

/***/ 844:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BlockUserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_requests_requests__ = __webpack_require__(403);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BlockUserPage = (function () {
    function BlockUserPage(navCtrl, navParams, userservice, zone, events, alertCtrl, requestservice, platform) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.userservice = userservice;
        this.zone = zone;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.requestservice = requestservice;
        this.platform = platform;
        this.blockUsers = [];
        this.blockRequest = [];
        this.showheader = true;
        events.subscribe('block-users', function () {
            _this.blockUsers = _this.userservice.blockUsers;
        });
        events.subscribe('block-request', function () {
            _this.blockRequest = _this.requestservice.blockRequest;
        });
    }
    BlockUserPage.prototype.ionViewDidLoad = function () {
        this.userservice.getAllBlockUsers();
        this.requestservice.getAllBlockRequest();
    };
    BlockUserPage.prototype.openFriend = function () {
        this.navCtrl.push('FriendListPage');
    };
    BlockUserPage.prototype.backButtonClick = function () {
        this.navCtrl.setRoot("TabsPage");
    };
    BlockUserPage.prototype.ubblockuser = function (item) {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: 'Unblock ' + item.displayName + '.',
            buttons: [
                {
                    text: 'CANCEL',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'UNBLOCK',
                    handler: function () {
                        _this.userservice.unblockUser(item).then(function (res) {
                            if (res) {
                                _this.userservice.getAllBlockUsers();
                            }
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    BlockUserPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    BlockUserPage.prototype.ubblockrequest = function (item) {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: 'Unblock ' + item.displayName + '.',
            buttons: [
                {
                    text: 'CANCEL',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'UNBLOCK',
                    handler: function () {
                        _this.requestservice.unblockRequest(item).then(function (res) {
                            if (res) {
                                _this.requestservice.getAllBlockRequest();
                            }
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    BlockUserPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-block-user',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\block-user\block-user.html"*/'<ion-header>\n  <ion-navbar hideBackButton>\n    <ion-buttons left>\n        <button ion-button (click)="backButtonClick()">\n             <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n    <ion-title>Blocked contacts </ion-title>\n    <ion-buttons right class=\'rightsidebtncss\'>\n      <button ion-button icon-only (click)="openFriend()">\n        <ion-icon class="rightsidebtncss" ios="md-person-add" md="md-person-add"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content class="bg-grey" padding>\n  <ion-list *ngIf="blockUsers">\n      <ng-container *ngFor="let item of blockUsers">\n        <ion-item (press)="ubblockuser(item)">\n          <ion-avatar item-start class="avtar-class"><img src="{{item.photoURL || \'assets/imgs/user.png\'}}"></ion-avatar>\n          <h2>{{item.displayName}}</h2>\n        </ion-item>\n      </ng-container>\n  </ion-list>\n\n  <ion-list *ngIf="blockRequest">\n    <ion-item-group>\n        <div class="border-bottom" *ngFor="let item of blockRequest" >\n          <ion-item no-lines (press)="ubblockrequest(item)">\n              <ion-avatar item-start class="avtar-class">\n                <img src="{{item.photoURL || \'assets/imgs/user.png\'}}">\n              </ion-avatar>\n              <h2 class="name-class">{{item.displayName}}</h2>\n          </ion-item>\n        </div>\n    </ion-item-group>\n  </ion-list>\n  <div padding text-center *ngIf="blockUsers.length == 0 && blockRequest.length == 0">\n    No record found!\n  </div>\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\block-user\block-user.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */]])
    ], BlockUserPage);
    return BlockUserPage;
}());

//# sourceMappingURL=block-user.js.map

/***/ })

});
//# sourceMappingURL=68.js.map