webpackJsonp([66],{

/***/ 772:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BroadcastPageModule", function() { return BroadcastPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__broadcast__ = __webpack_require__(846);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BroadcastPageModule = (function () {
    function BroadcastPageModule() {
    }
    BroadcastPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__broadcast__["a" /* BroadcastPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__broadcast__["a" /* BroadcastPage */]),
            ],
        })
    ], BroadcastPageModule);
    return BroadcastPageModule;
}());

//# sourceMappingURL=broadcast.module.js.map

/***/ }),

/***/ 846:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BroadcastPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_chat_chat__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_fcm_fcm__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_requests_requests__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var BroadcastPage = (function () {
    function BroadcastPage(fcm, loading, requestservice, fb, events, chatservice, userservice, navCtrl, toastCtrl, navParams, tpStorageService) {
        this.fcm = fcm;
        this.loading = loading;
        this.requestservice = requestservice;
        this.fb = fb;
        this.events = events;
        this.chatservice = chatservice;
        this.userservice = userservice;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.tpStorageService = tpStorageService;
        this.myrequests = [];
        this.requestcounter = null;
        this.isenabled = true;
        this.newmessage = '';
        this.debugMe = true;
        this.showPaidSign = false;
        this.doIhavebusinessAccount = false;
        this.showheader = true;
        this.isData = false;
        this.authForm = this.fb.group({
            'message': [null, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].required])]
        });
        this.message = this.authForm.controls['message'];
    }
    /*
    Test message
     value="Father`s Day. Big Red Weekend. Our lowest prices of the season. Stop everything. Up to 20% Off Now"
  
     */
    BroadcastPage.prototype.backButtonClick = function () {
        ////console.log ("dasdasdasdasdas");
        ////console.log (this.navParams.get('broadcast_retry'));
        if (this.navParams.get('broadcast_retry')) {
            this.navCtrl.setRoot("TabsPage");
        }
        else {
            this.navCtrl.pop({ animate: true, direction: "forward" });
        }
    };
    BroadcastPage.prototype.tpInitilizeFromStorage = function () {
        var _this = this;
        this.doIhavebusinessAccount = false;
        this.tpStorageService.getItem('businessName').then(function (res) {
            if (res) {
                _this.doIhavebusinessAccount = true;
            }
        }).catch(function (e) { });
    };
    BroadcastPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.tpInitilizeFromStorage();
        this.paidOrNot();
        this.requestservice.getmyfriends();
        this.myfriends = [];
        this.events.subscribe('friends', function () {
            _this.events.unsubscribe('friends');
            _this.myfriends = [];
            _this.myfriends = _this.requestservice.myfriends;
        });
        /*this.requestservice.getmyrequests();
        this.events.subscribe('gotrequests', () => {
            this.myrequests = [];
            this.myrequests = this.requestservice.userdetails;
            if (this.myrequests) {
                this.requestcounter = this.myrequests.length;
            }
        })
        */
    };
    //KEep it same as chats.ts :
    //If any thing change in this function make sure it is same as chats.ts
    BroadcastPage.prototype.paidOrNot = function () {
        var _this = this;
        this.showPaidSign = false;
        this.tpStorageService.getItem('business_created').then(function (business_created_raw) {
            if (business_created_raw) {
                /* start */
                var AcceptableDelayInMicroseconds = 1000; //Before we show warning to business that they have not paid yet
                var timeStamp = Date.now();
                var business_created = parseInt(business_created_raw);
                //Do not check for first few minutes/hours/days ( as specified in AcceptableDelayInMicroseconds )
                //But check every single time afterwards
                if ((business_created + AcceptableDelayInMicroseconds) < timeStamp) {
                    _this.tpStorageService.getItem('paid').then(function (paid_raw) {
                        var paid = parseInt(paid_raw);
                        //check API everytime for now
                        if (true) {
                            _this.userservice.getBusinessDetails().then(function (res) {
                                //this.tpStorageService.setItem('businessName', res['displayName']);
                                //this.tpStorageService.setItem('bcats', res['bcats']);
                                if (!paid || paid <= 0) {
                                    //console.log ("Indeed Business has not paid yet");
                                    _this.showPaidSign = true;
                                }
                            }).catch(function (err) {
                            });
                        } //endif
                    }).catch(function (e) { });
                }
                else {
                } //endif
                /* end */
            }
        }).catch(function (e) { });
    }; //end function
    BroadcastPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    BroadcastPage.prototype.broadcast = function () {
        //console.log ("Broadcasting..1.");
        this.newmessage = this.newmessage.trim();
        if (this.newmessage != '') {
        }
        else {
            this.loading.presentToast('Please enter a valid message...');
            return;
        }
        this.isenabled = false;
        this.cntCounter = 0;
        if (this.myfriends.length > 0) {
            for (var key = 0; key < this.myfriends.length; key++) {
                this.addmessage(this.myfriends[key]);
            } //end for
        }
        else {
            //You have no friends
            this.loading.presentToast('It appears like your contact list is empty. Invite your friends and let`s get started');
            this.navCtrl.setRoot('TabsPage');
        }
    };
    BroadcastPage.prototype.presentPopover = function (myEvent) {
        this.navCtrl.push("NotificationPage");
        //this.requestcounter = this.myrequests.length;
    };
    BroadcastPage.prototype.sendToNextPage = function () {
        this.cntCounter++;
        if (this.cntCounter >= this.myfriends.length) {
            this.cntCounter = 0;
            this.navCtrl.setRoot("BroadcastSubmittedPage");
        }
    };
    BroadcastPage.prototype.sendToBusinessReg = function () {
        this.navCtrl.push("RegisterbusinessPage", { treatNewInstall: 0 });
    };
    BroadcastPage.prototype.addmessage = function (buddy) {
        var _this = this;
        this.newmessage = this.newmessage.trim();
        //Send Push notification : mytodo check if the buddy is online or not. if online then no need to send push notification
        var newMessage = 'You have received a message';
        this.fcm.sendNotification(buddy, newMessage, 'chatpage');
        //Send chat message
        this.chatservice.addnewmessageBroadcast(this.newmessage, 'message', buddy, 0, true).then(function () {
            _this.newmessage = '';
            _this.sendToNextPage();
        });
    };
    BroadcastPage.prototype.ionViewDidLoad = function () {
    };
    BroadcastPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-broadcast',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\broadcast\broadcast.html"*/'<ion-header  >\n  <ion-navbar hideBackButton>\n\n    <ion-buttons left>\n        <button ion-button (click)="backButtonClick()">\n             <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n    <ion-title>Send Broadcast</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <ion-card  class="maincard_bubble" text-center padding *ngIf="!ever_referral_sent">\n    <ion-card-content >\n      <ion-card-title>\n        Market to your clients\n        </ion-card-title>\n      <p>\n        Send branded push notifications with high level of delivery & better engagement than emails and texts\n      </p>\n      <button *ngIf="!doIhavebusinessAccount" style="margin-top:15px;" ion-button (click)="sendToBusinessReg()" color="primary" block>Register Your Business</button>\n    </ion-card-content>\n </ion-card>\n\n<div *ngIf="doIhavebusinessAccount">\n  <h5 class="headingmine">Text To Broadcast (e.g. Promo, Ad)</h5>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col col-10>\n        <ion-textarea  [(ngModel)]="newmessage" [formControl]="message"  type="text"  rows="7" name="message" class="newmsg" placeholder="Write your message ..."></ion-textarea>\n      </ion-col>\n      <ion-col col-2>\n        <button  [disabled]="!isenabled" (click)="broadcast()" ion-button class="btnbroadcast"  color="primary"  >\n        <ion-icon  ios="md-send"  md="md-send">  </ion-icon>\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n\n  <div padding>\n  </div>\n</div>\n\n\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\broadcast\broadcast.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_fcm_fcm__["a" /* FcmProvider */], __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_5__providers_requests_requests__["a" /* RequestsProvider */], __WEBPACK_IMPORTED_MODULE_7__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */], __WEBPACK_IMPORTED_MODULE_2__providers_chat_chat__["a" /* ChatProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */], __WEBPACK_IMPORTED_MODULE_8__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], BroadcastPage);
    return BroadcastPage;
}());

//# sourceMappingURL=broadcast.js.map

/***/ })

});
//# sourceMappingURL=66.js.map