webpackJsonp([3],{

/***/ 836:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsernamePageModule", function() { return UsernamePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__username__ = __webpack_require__(911);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var UsernamePageModule = (function () {
    function UsernamePageModule() {
    }
    UsernamePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__username__["a" /* UsernamePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__username__["a" /* UsernamePage */]),
            ],
        })
    ], UsernamePageModule);
    return UsernamePageModule;
}());

//# sourceMappingURL=username.module.js.map

/***/ }),

/***/ 911:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsernamePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UsernamePage = (function () {
    function UsernamePage(navCtrl, navParams, viewCtrl, fb) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.fb = fb;
        this.msgstatus = true;
        this.showheader = true;
        this.username = this.navParams.get('userName');
    }
    UsernamePage.prototype.ionViewDidLoad = function () {
    };
    UsernamePage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    UsernamePage.prototype.save = function () {
        var data = { username: this.username };
        this.viewCtrl.dismiss(data);
    };
    UsernamePage.prototype.onChange = function (e) {
        if (this.username.trim()) {
            this.msgstatus = true;
        }
        else {
            this.msgstatus = false;
        }
    };
    UsernamePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UsernamePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-username',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\username\username.html"*/'\n<ion-header >\n\n  <ion-navbar color="dark">\n    <div class="header-wrap">\n      <div class="flex-start">\n          <ion-buttons left>\n              <button ion-button icon-only (click)="dismiss()"><ion-icon ios="md-close" md="md-close" ></ion-icon></button>\n          </ion-buttons>\n      </div>\n      <div class="flex-end">\n          <ion-buttons right>\n              <button  ion-button icon-only [disabled]=" !this.msgstatus" (click)="save()" ><ion-icon ios="md-checkmark" md="md-checkmark" ></ion-icon></button>\n          </ion-buttons>\n      </div>\n    </div>\n\n\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n<ion-card>\n  <ion-card-content>\n    <ion-list>\n      <ion-item>\n        <ion-input type="text" (input)="onChange($event.keyCode)"id="username"  [(ngModel)]="username" maxlength="30" placeholder="Display Name"></ion-input>\n      </ion-item>\n    </ion-list>\n  </ion-card-content>\n</ion-card>\n\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\username\username.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */]])
    ], UsernamePage);
    return UsernamePage;
}());

//# sourceMappingURL=username.js.map

/***/ })

});
//# sourceMappingURL=3.js.map