webpackJsonp([20],{

/***/ 818:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferralPageModule", function() { return ReferralPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__referral__ = __webpack_require__(892);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ReferralPageModule = (function () {
    function ReferralPageModule() {
    }
    ReferralPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__referral__["a" /* ReferralPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__referral__["a" /* ReferralPage */]),
            ],
        })
    ], ReferralPageModule);
    return ReferralPageModule;
}());

//# sourceMappingURL=referral.module.js.map

/***/ }),

/***/ 892:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReferralPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_cats_cats__ = __webpack_require__(410);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_requests_requests__ = __webpack_require__(403);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ReferralPage = (function () {
    function ReferralPage(platform, navCtrl, navParams, catservice, events, app, requestservice, viewCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.catservice = catservice;
        this.events = events;
        this.app = app;
        this.requestservice = requestservice;
        this.viewCtrl = viewCtrl;
        this.myrequests = [];
        this.requestcounter = null;
        this.showheader = true;
        this.isVirgin = false;
        platform.ready().then(function () {
            _this.initializeItems();
        });
    }
    ReferralPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ReferralPage.prototype.sendToBroadcastPage = function () {
        this.app.getRootNav().push("BroadcastPage", { broadcast_retry: false });
    };
    ReferralPage.prototype.presentPopover = function (myEvent) {
        this.navCtrl.push("NotificationPage");
        //this.requestcounter = this.myrequests.length;
    };
    ReferralPage.prototype.ionViewWillLeave = function () {
    };
    ReferralPage.prototype.ionViewWillEnter = function () {
        /*this.requestservice.getmyrequests();
        this.events.subscribe('gotrequests', () => {
            this.myrequests = [];
            this.myrequests = this.requestservice.userdetails;
            if (this.myrequests) {
                this.requestcounter = this.myrequests.length;
            }
        })
        */
    };
    ReferralPage.prototype.sendreferraldirectly = function () {
        this.navCtrl.push("SearchNextPage", { src: 1 });
    };
    ReferralPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the ev target
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
                return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    ReferralPage.prototype.sendToEarningsPage = function () {
        this.app.getRootNav().push("EarningsPage");
    };
    ReferralPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReferralPage');
    };
    //not used
    ReferralPage.prototype.fnGoToSearchNextPage = function () {
        this.navCtrl.push("SearchNextPage", { src: 1 });
    };
    ReferralPage.prototype.fnGoToHomeAfterINeedTodayPage = function (item_id) {
        this.navCtrl.push("HomeAfterINeedTodayPage", { cat_id: item_id });
    };
    ReferralPage.prototype.initializeItems = function () {
        this.items = this.catservice.getCats();
    };
    ReferralPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-referral',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\referral\referral.html"*/'<ion-header >\n  <ion-navbar >\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title >Exchange Referral</ion-title>\n    <ion-buttons   end class="btnInHeaderVeryLast">\n      <button ion-button icon-only (click)="sendToBroadcastPage()">\n        <ion-icon name="megaphone"   class="megaphone"></ion-icon>\n      </button>\n      <button ion-button icon-only (click)="sendToEarningsPage()">\n        <ion-icon  name="checkmark-circle" class="notification"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n\n\n<ion-content padding>\n\n<div>\n      <div class="pull-center" style="margin:20px 0px;">\n        <h4 class="text_color_primary" >Get Trusted Business Referrals</h4>\n\n        <div tappable style="height:408px;overflow:hidden;border:1px solid #e2dbf9;">\n          <ion-searchbar (ionInput)="getItems($event)" placeholder="Search Services"></ion-searchbar>\n          <ion-list no-lines>\n            <ion-item tappable nolines class="itemlistref mainitemlist"  *ngFor="let item of items" (click)="fnGoToHomeAfterINeedTodayPage(item.id)">\n                 <p  >{{item.name}}</p>\n                 <ion-icon end item-end   name=\'arrow-dropright-circle\'> </ion-icon>\n            </ion-item>\n          </ion-list>\n        </div>\n        </div>\n</div>\n\n        <!--\n        <button clear (click)="sendreferraldirectly()" ion-button color="primary" block>or send Referral Directly To Contact</button>\n      -->\n\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\referral\referral.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_cats_cats__["a" /* CatProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */],
            __WEBPACK_IMPORTED_MODULE_3__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */]])
    ], ReferralPage);
    return ReferralPage;
}());

//# sourceMappingURL=referral.js.map

/***/ })

});
//# sourceMappingURL=20.js.map