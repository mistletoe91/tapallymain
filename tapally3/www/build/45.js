webpackJsonp([45],{

/***/ 793:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupchatPageModule", function() { return GroupchatPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__groupchat__ = __webpack_require__(867);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GroupchatPageModule = (function () {
    function GroupchatPageModule() {
    }
    GroupchatPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__groupchat__["a" /* GroupchatPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__groupchat__["a" /* GroupchatPage */]),
            ],
        })
    ], GroupchatPageModule);
    return GroupchatPageModule;
}());

//# sourceMappingURL=groupchat.module.js.map

/***/ }),

/***/ 867:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupchatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_groups_groups__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_imagehandler_imagehandler__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_img_viewer__ = __webpack_require__(409);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_requests_requests__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_app_angularfireconfig__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var GroupchatPage = (function () {
    function GroupchatPage(navCtrl, navParams, imghandler, groupservice, events, userservice, actionSheet, zone, popoverCtrl, loading, modalCtrl, imageViewerCtrl, requestservice, platform, alertCtrl, tpStorageService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.imghandler = imghandler;
        this.groupservice = groupservice;
        this.events = events;
        this.userservice = userservice;
        this.actionSheet = actionSheet;
        this.zone = zone;
        this.popoverCtrl = popoverCtrl;
        this.loading = loading;
        this.modalCtrl = modalCtrl;
        this.imageViewerCtrl = imageViewerCtrl;
        this.requestservice = requestservice;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.tpStorageService = tpStorageService;
        this.owner = false;
        this.userpic = __WEBPACK_IMPORTED_MODULE_8__app_app_angularfireconfig__["c" /* env */].userPic;
        this.newmessage = '';
        this.allgroupMessage = [];
        this.isRefresher = false;
        this.msgstatus = false;
        this.showheader = true;
        this.selectedMessage = [];
        this.headerSelectionIcon = true;
        this.selectCounter = 0;
        this.groupMember = [];
        this.groupMemberStatus = [];
        this.backbuttonstatus = false;
        this._imageViewerCtrl = imageViewerCtrl;
        if (this.groupservice.currentgroupname != undefined) {
            this.groupName = this.groupservice.currentgroupname;
        }
        else {
            this.groupName = this.navParams.get('groupName');
            this.groupservice.currentgroupname = this.groupName;
        }
        this.groupImage = this.navParams.get('groupImage');
        this.alignuid = this.groupservice.userId;
        this.groupservice.getownership(this.groupName).then(function (res) {
            _this.groupservice.getgroupimage();
            _this.groupservice.getgroupmembers();
            if (res) {
                _this.owner = true;
            }
        }).catch(function (err) {
        });
    }
    GroupchatPage.prototype.backButtonClick = function () {
        this.navCtrl.pop({ animate: true, direction: "forward" });
    };
    GroupchatPage.prototype.ionViewCanEnter = function () {
        var _this = this;
        if (this.groupservice.currentgroupname != undefined) {
            this.groupName = this.groupservice.currentgroupname;
        }
        else {
            this.groupName = this.navParams.get('groupName');
            this.groupservice.currentgroupname = this.groupName;
        }
        this.groupservice.getgroupInfo(this.groupName).then(function (res) {
            _this.groupImage = res.groupimage;
        });
        this.groupservice.getgroupmsgs(this.groupName);
        this.getGroupMembers();
        this.events.subscribe('newgroupmsg', function () {
            //Do not unsubscribe this. it is necessary to read real-time messages
            //this.events.unsubscribe('newgroupmsg');
            _this.tpStorageService.getItem('userUID').then(function (res) {
                if (res) {
                    var currentuser = res;
                    _this.allgroupmsgs = [];
                    _this.imgornot = [];
                    _this.zone.run(function () {
                        _this.allgroupmsgs = _this.groupservice.groupmsgs;
                    });
                    var _loop_1 = function (key) {
                        if (_this.allgroupmsgs[key].sentby != currentuser) {
                            _this.userservice.getmsgblock(_this.allgroupmsgs[key].sentby).then(function (res) {
                                _this.allgroupmsgs[key].blockby = res;
                            });
                        }
                        else {
                            _this.allgroupmsgs[key].blockby = false;
                        }
                        d = new Date(_this.allgroupmsgs[key].timestamp);
                        hours = d.getHours();
                        minutes = "0" + d.getMinutes();
                        month = d.getMonth();
                        1;
                        da = d.getDate();
                        monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                        formattedTime = monthNames[month] + "-" + da + "-" + hours + ":" + minutes.substr(-2);
                        _this.allgroupmsgs[key].timestamp = formattedTime;
                        if (_this.allgroupmsgs[key].multiMessage == true) {
                            _this.imgornot.push(true);
                        }
                        else {
                            if (_this.allgroupmsgs[key].message.substring(0, 4) === 'http') {
                                _this.imgornot.push(true);
                            }
                            else {
                                _this.imgornot.push(false);
                            }
                        }
                    };
                    var d, hours, minutes, month, da, monthNames, formattedTime;
                    for (var key = 0; key < _this.allgroupmsgs.length; key++) {
                        _loop_1(key);
                    }
                    _this.allgroupMessage = [];
                    _this.allgroupMessage = _this.allgroupmsgs;
                    if (_this.allgroupMessage.length > 5) {
                        _this.scrollto();
                    }
                } //end res
            }).catch(function (e) { });
        });
    };
    GroupchatPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.groupservice.getgroupmsgs(this.groupName);
        this.userservice.getuserdetails().then(function (res) {
            _this.loginuserInfo = res;
        }).catch(function (err) {
        });
    };
    GroupchatPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
        this.groupservice.getintogroup(null);
        this.groupservice.getmygroups();
        this.requestservice.getmyfriends();
    };
    GroupchatPage.prototype.addmessage_async = function () {
        var _this = this;
        setTimeout(function () {
            _this.addgroupmsg();
        }, 1);
    }; //end async function
    GroupchatPage.prototype.addgroupmsg = function () {
        var _this = this;
        this.closeButtonClick();
        this.getGroupMembers();
        if (this.newmessage.trim()) {
            this.groupservice.addgroupmsg(this.newmessage, this.loginuserInfo, 'message', this.groupName).then(function (res) {
                _this.newmessage = '';
                _this.scrollto();
                _this.msgstatus = false;
            });
        }
        else {
            this.loading.presentToast('Please enter valid message...');
        }
    };
    GroupchatPage.prototype.scrollto = function () {
        /*setTimeout(() => {
          if(this.content._scroll)  {
            this.content.scrollToBottom();
          }
        }, 500);
        */
    };
    GroupchatPage.prototype.callFunctionScroll = function () {
        if (this.content._scroll && !this.isRefresher) {
            this.content.scrollToBottom(0);
        }
    };
    GroupchatPage.prototype.presentOwnerSheet = function () {
        var _this = this;
        var sheet = this.actionSheet.create({
            title: 'Group Actions',
            buttons: [
                {
                    text: 'Remove member',
                    icon: 'remove-circle',
                    handler: function () {
                        _this.navCtrl.push('GroupmembersPage');
                    }
                }, {
                    text: 'Delete Group',
                    icon: 'trash',
                    handler: function () {
                        _this.groupservice.deletegroup().then(function () {
                            _this.navCtrl.pop();
                        }).catch(function (err) {
                        });
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    icon: 'cancel',
                    handler: function () {
                    }
                }
            ]
        });
        sheet.present();
    };
    GroupchatPage.prototype.presentMemberSheet = function () {
        var _this = this;
        var sheet = this.actionSheet.create({
            title: 'Group Actions',
            buttons: [
                {
                    text: 'Leave Group',
                    icon: 'log-out',
                    handler: function () {
                        _this.groupservice.leavegroup().then(function () {
                            _this.navCtrl.pop();
                        }).catch(function (err) {
                        });
                    }
                }, {
                    text: 'Cancel',
                    role: 'cancel',
                    icon: 'cancel',
                    handler: function () {
                    }
                }
            ]
        });
        sheet.present();
    };
    GroupchatPage.prototype.openGroupDetail = function () {
        this.navCtrl.push('GroupinfoPage', { groupName: this.groupName });
    };
    GroupchatPage.prototype.openAttachment = function (myEvent) {
        var _this = this;
        var popover = this.popoverCtrl.create('GpattachmentsPage', { gname: this.groupName });
        popover.present({
            ev: myEvent
        });
        popover.onDidDismiss(function (pages) {
            if (pages != 0 && pages != undefined && pages != "undefined" && pages != null) {
                if (pages.page) {
                    _this.navCtrl.push(pages.page, { gpname: pages.gpname, flag: pages.flag });
                }
            }
        });
    };
    GroupchatPage.prototype.onChange = function (e) {
        if (this.newmessage.trim()) {
            this.msgstatus = true;
        }
        else {
            this.msgstatus = false;
        }
    };
    GroupchatPage.prototype.openAllImg = function (items) {
        if (this.selectedMessage.length == 0) {
            this.navCtrl.push("AllimgPage", { allimg: items });
        }
    };
    GroupchatPage.prototype.openDoc = function (path) {
        this.imghandler.openDocument(path).then(function (res) {
        }).catch(function (err) {
        });
    };
    GroupchatPage.prototype.openContacts = function (contacts) {
        this.navCtrl.push("ShowContactPage", { contacts: contacts });
    };
    GroupchatPage.prototype.showLocation = function (location) {
        this.navCtrl.push("MapPage", { location: location });
    };
    GroupchatPage.prototype.presentImage = function (myImage) {
        var imageViewer = this._imageViewerCtrl.create(myImage);
        imageViewer.present();
    };
    GroupchatPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    GroupchatPage.prototype.viewPopover = function (myevent) {
        var popove = this.popoverCtrl.create('GroupChatPopoverPage', { groupname: this.groupName });
        popove.present({ ev: myevent });
        popove.onDidDismiss(function () {
        });
    };
    GroupchatPage.prototype.pushMsg = function (item, event) {
        var flag = 0;
        if (this.selectedMessage.length != 0) {
            for (var i = 0; i < this.selectedMessage.length; i++) {
                if (item.messageId == this.selectedMessage[i].messageId) {
                    flag = 1;
                    item.selection = false;
                    this.selectedMessage.splice(i, 1);
                    break;
                }
                else {
                }
            }
            if (flag == 1) {
            }
            else {
                this.pushanotherMsg(item, event);
            }
        }
        else {
            item.selection = true;
            this.selectedMessage.push(item);
            if (this.headerSelectionIcon == true) {
                this.headerSelectionIcon = false;
                this.backbuttonstatus = true;
            }
            else {
                this.headerSelectionIcon = true;
                this.backbuttonstatus = false;
            }
        }
        this.selectCounter = this.selectedMessage.length;
        if (this.selectedMessage.length == 0) {
            this.headerSelectionIcon = true;
            this.backbuttonstatus = false;
        }
    };
    GroupchatPage.prototype.pushanotherMsg = function (item, event) {
        var flag = 0;
        if (this.selectedMessage.length >= 1) {
            for (var i = 0; i < this.selectedMessage.length; i++) {
                if (item.messageId == this.selectedMessage[i].messageId) {
                    item.selection = false;
                    flag = 1;
                    this.selectedMessage.splice(i, 1);
                    break;
                }
                else {
                    flag = 0;
                }
            }
            if (flag == 1) {
            }
            else {
                item.selection = true;
                this.selectedMessage.push(item);
            }
        }
        this.selectCounter = this.selectedMessage.length;
        if (this.selectedMessage.length == 0) {
            this.headerSelectionIcon = true;
            this.backbuttonstatus = false;
        }
    };
    GroupchatPage.prototype.deleteMessage = function () {
        var _this = this;
        if (this.selectedMessage.length > 1) {
            this.DeleteMsg = "Do you want to delete these messages?";
        }
        else {
            this.DeleteMsg = "Do you want to delete this message?";
        }
        this.loading.presentLoading();
        var alert = this.alertCtrl.create({
            title: 'Delete Message',
            message: this.DeleteMsg,
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () {
                        _this.closeButtonClick();
                        _this.loading.dismissMyLoading();
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        if (_this.selectedMessage.length != 0) {
                            _this.groupservice.deleteMessages(_this.selectedMessage).then(function (res) {
                                if (res) {
                                    _this.selectedMessage = [];
                                    if (_this.selectedMessage.length == 0) {
                                        _this.headerSelectionIcon = true;
                                        _this.backbuttonstatus = false;
                                        _this.loading.dismissMyLoading();
                                    }
                                }
                            }).catch(function (err) {
                            });
                        }
                    }
                }
            ]
        });
        alert.present().then(function () {
        });
    };
    GroupchatPage.prototype.closeButtonClick = function () {
        this.selectedMessage = [];
        this.selectCounter = 0;
        this.headerSelectionIcon = true;
        this.backbuttonstatus = false;
        for (var i = 0; i < this.allgroupMessage.length; i++) {
            this.allgroupMessage[i].selection = false;
        }
    };
    GroupchatPage.prototype.getGroupMembers = function () {
        var _this = this;
        this.groupservice.getgroupmembers();
        this.events.subscribe('gotmembers', function () {
            _this.events.unsubscribe('gotmembers');
            _this.groupMember = _this.groupservice.currentgroup;
            _this.groupservice.getgroupmemebrstatus(_this.groupMember);
            _this.events.subscribe('groupmemberstatus', function () {
                _this.events.unsubscribe('groupmemberstatus');
                _this.groupMemberStatus = _this.groupservice.groupmemeberStatus;
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('content'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Content */])
    ], GroupchatPage.prototype, "content", void 0);
    GroupchatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-groupchat',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\groupchat\groupchat.html"*/'<ion-header  >\n  <!-- hideBackButton="{{backbuttonstatus}}" -->\n\n\n    <ion-navbar color="hcolor" hideBackButton >\n      <ion-buttons left>\n          <button ion-button (click)="backButtonClick()">\n               <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n          </button>\n      </ion-buttons>\n\n      <div>\n        <div class="header-wrap" *ngIf="headerSelectionIcon">\n            <div class="flex-start">\n                <div class="title-wrap" (click)="openGroupDetail()">\n                    <ion-avatar class="user-img" left>\n                        <img src="{{groupImage}}">\n                    </ion-avatar>\n                    <div class="title-uname">\n                        <h3>{{groupName}}</h3>\n                    </div>\n                </div>\n            </div>\n            <div class="flex-end">\n                <ion-buttons end>\n                    <button ion-button icon-only (click)="viewPopover($event)">\n                        <ion-icon ios="md-more" md="md-more"></ion-icon>\n                    </button>\n                </ion-buttons>\n            </div>\n        </div>\n\n        <div class="select-wrap" *ngIf="!headerSelectionIcon">\n            <div class="flex-start">\n                <ion-buttons start class="left-arrow">\n                    <button start ion-button icon-only (click)="closeButtonClick()">\n                    <ion-icon ios="md-close" md="md-close"></ion-icon>\n                    </button>\n                </ion-buttons>\n                <div class="select-title" text-left left *ngIf="selectCounter > 0">\n                {{selectCounter}}\n                </div>\n            </div>\n            <div class="flex-end">\n                <ion-buttons end>\n                    <button ion-button icon-only (click)="deleteMessage()">\n                        <ion-icon ios="ios-trash" md="md-trash"></ion-icon>\n                    </button>\n                    <!-- <button ion-button icon-only (click)="starredMessage()">\n                        <ion-icon ios="ios-star" md="md-star"></ion-icon>\n                    </button> -->\n                </ion-buttons>\n            </div>\n        </div>\n      </div>\n    </ion-navbar>\n</ion-header>\n<ion-content #content>\n    <div class="chatwindow">\n        <ion-list no-lines *ngIf="allgroupMessage.length > 0">\n                <ion-item text-wrap *ngFor="let item of allgroupMessage; let i = index;let last = last;">\n                    {{last ? callFunctionScroll() : \'\'}}\n                    <div class="msg-wrap me" [ngClass]="{ \'select-item\': item.selection }" *ngIf="item.sentby == alignuid" (press)="pushMsg(item,$event)" (click)="pushanotherMsg(item,$event)">\n                        <ion-avatar>\n                            <img src="{{item.photoURL}}">\n                        </ion-avatar>\n                        <div [ngClass]="item.type == \'image\' || item.type == \'location\' || item.type == \'document\'?\'bubble-img\':\'bubble\'">\n                            <div class="displayname">{{item.displayName}}</div>\n                            <div class="msg" *ngIf="!imgornot[i] && item.type == \'message\'">{{item.message}}</div>\n                            <img class="msg" *ngIf="item.type == \'document\'" src="assets/imgs/docs.png" (click)="openDoc(item.message)" />\n\n                            <audio controls class="msg" *ngIf="item.type == \'audio\'">\n                                <source src="{{item.message}}" type="audio/mpeg">\n                            </audio>\n\n                            <ng-container class="msg" *ngIf="item.multiMessage && item.type == \'image\'">\n                                <div (click)="openAllImg(item.message)">\n                                    <img class="msg-partision" *ngFor="let pic of item.message | slice:0:4; let ii = index "\n                                        src="{{pic}}" />\n                                    <span class="img-number overlay">+\n                                        {{item.message.length - 4}}</span>\n                                </div>\n                            </ng-container>\n                            <ng-container class="msg" *ngIf="item.multiMessage && item.type == \'contact\'">\n                                <div>\n                                <img class="msg contact" src="assets/imgs/user.png" />\n                                <div *ngIf="item.message.length == 1"(click)="openContacts(item.message)" class="view-msg">View {{ item.message.length }} Contact</div>\n                                <div *ngIf="item.message.length > 1" (click)="openContacts(item.message)" class="view-msg">View {{ item.message.length }} Contacts</div>\n                                </div>\n                            </ng-container>\n\n                            <ng-container *ngIf="!item.multiMessage && item.type == \'image\'">\n                                <img class="msg" *ngIf="item.type == \'image\'" #mychatImage src="{{item.message}}"\n                                    (click)="presentImage(mychatImage)" />\n                            </ng-container>\n\n                            <ng-container class="msg" *ngIf="item.multiMessage && item.type == \'location\'">\n                                <img class="msg" (click)="showLocation(item.message)" src="assets/imgs/location.png" />\n                            </ng-container>\n                            <div class="read-recipients">\n                                <ion-icon *ngIf="item.isStarred" name="star" class="icon"></ion-icon>\n                                <p class="icon">{{item.timeofmsg}}</p>\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class="msg-wrap you" [ngClass]="{ \'select-item\': item.selection }" *ngIf="item.sentby != alignuid" (press)="pushMsg(item,$event)" (click)="pushanotherMsg(item,$event)">\n                        <ion-avatar>\n                            <img *ngIf="item.blockby == false" src="{{item.photoURL || userpic}}">\n                            <img *ngIf="item.blockby == true" src="{{\'assets/imgs/user.png\' || userpic}}">\n                        </ion-avatar>\n                        <div [ngClass]="item.type == \'image\' || item.type == \'location\' || item.type == \'document\' ?\'bubble-img\':\'bubble\'">\n                            <div class="displayname">{{item.displayName}}</div>\n\n                            <div class="msg" *ngIf="!imgornot[i] &&  item.type == \'message\'">{{item.message}}</div>\n\n                            <img class="msg" *ngIf="item.type == \'document\'" src="assets/imgs/docs.png" (click)="openDoc(item.message)" />\n                            <audio controls class="msg" *ngIf="item.type == \'audio\'">\n                                <source src="{{item.message}}" type="audio/mpeg">\n                            </audio>\n                            <ng-container class="msg" *ngIf="item.multiMessage && item.type == \'image\'">\n                                <div (click)="openAllImg(item.message)">\n                                    <img class="msg-partision" *ngFor="let pic of item.message | slice:0:4; let ii = index "\n                                        src="{{pic}}" />\n                                    <span class="img-number overlay">+ {{item.message.length - 4}}</span>\n                                </div>\n                            </ng-container>\n                            <ng-container class="msg" *ngIf="item.multiMessage && item.type == \'contact\'" click)="openContacts(item.message)">\n                                <div>\n                                <img class="msg contact" src="assets/imgs/user.png" />\n                                <div *ngIf="item.message.length == 1"(click)="openContacts(item.message)" class="view-msg">View {{ item.message.length }} Contact</div>\n                                <div *ngIf="item.message.length > 1" (click)="openContacts(item.message)" class="view-msg">View {{ item.message.length }} Contacts</div>\n                                </div>\n                                </ng-container>\n                            <ng-container *ngIf="!item.multiMessage && item.type == \'image\'">\n                                <img class="msg" *ngIf="item.type == \'image\'" #mychatImage src="{{item.message}}"\n                                    (click)="presentImage(mychatImage)" />\n                            </ng-container>\n                            <ng-container class="msg" *ngIf="item.multiMessage && item.type == \'location\'">\n                                <img class="msg" (click)="showLocation(item.message)" src="assets/imgs/location.png" />\n                            </ng-container>\n                            <div class="read-recipients">\n                                <ion-icon *ngIf="item.isStarred" name="star" class="icon"></ion-icon>\n                                <p class="icon">{{item.timeofmsg}}</p>\n                            </div>\n                        </div>\n                    </div>\n\n                </ion-item>\n        </ion-list>\n    </div>\n\n</ion-content>\n\n<ion-footer ion-fixed>\n    <ion-toolbar class="no-border" color="white">\n        <ion-input tappable [(ngModel)]="newmessage" placeholder="Write your message ..." class="newmsg" (click)="scrollto()" (input)="onChange($event.keyCode)"></ion-input>\n        <ion-buttons end>\n            <a ion-button (click)="openAttachment($event)">\n                <ion-icon class="ion-image" ios="md-attach" md="md-attach"></ion-icon>\n            </a>\n        </ion-buttons>\n        <ion-buttons end>\n            <button ion-button round type="submit" class="sentbtn" [disabled]="!this.msgstatus" (click)="addmessage_async()">\n                <ion-icon ios="md-send" md="md-send" color="wcolor" style="font-size: 2.2em;"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\groupchat\groupchat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_imagehandler_imagehandler__["a" /* ImagehandlerProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_groups_groups__["a" /* GroupsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["y" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_img_viewer__["a" /* ImageViewerController */],
            __WEBPACK_IMPORTED_MODULE_7__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_9__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], GroupchatPage);
    return GroupchatPage;
}());

//# sourceMappingURL=groupchat.js.map

/***/ })

});
//# sourceMappingURL=45.js.map