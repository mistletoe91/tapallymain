webpackJsonp([28],{

/***/ 810:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhonePageModule", function() { return PhonePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__phone__ = __webpack_require__(884);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PhonePageModule = (function () {
    function PhonePageModule() {
    }
    PhonePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__phone__["a" /* PhonePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__phone__["a" /* PhonePage */]),
            ],
        })
    ], PhonePageModule);
    return PhonePageModule;
}());

//# sourceMappingURL=phone.module.js.map

/***/ }),

/***/ 884:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PhonePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_tpstorage_tpstorage__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_auth__ = __webpack_require__(413);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_app_angularfireconfig__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_map__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/*
<div class="welcome-text" text-center>
  <h1 class="text-white">Welcome</h1>
  <p class="text-white">Enter your mobile number to get started !!</p>
</div>
*/
var PhonePage = (function () {
    function PhonePage(navCtrl, navParams, authService, loadingCtrl, fb, platform, alertCtrl, loading, http, modalCtrl, tpStorageService) {
        //this.platform.ready().then((readySource) => {
        //  Keyboard.disableScroll(true)
        //});
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.authService = authService;
        this.loadingCtrl = loadingCtrl;
        this.fb = fb;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.loading = loading;
        this.http = http;
        this.modalCtrl = modalCtrl;
        this.tpStorageService = tpStorageService;
        this.phoneNumber = { number: '', type: '' };
        this.isCountrySelect = true;
        this.submitAttempt = false;
        this.countryName = "Select Country";
        this.countryTemp = false;
        this.countryCode = "";
        this.InvalidNumErr = '';
        this.authForm = this.fb.group({
            'number': [null, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].pattern('^[0-9]+$'), __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required])],
            "mySelectCountry": ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required])]
        });
        this.number = this.authForm.controls['number'];
        //Default Country :
        //mytodo : Right now its Canada
        this.countryName = 'Canada';
        this.countryCode = '1'; //test
        this.phoneNumber.type = this.countryCode;
        this.countryTemp = true;
    }
    PhonePage.prototype.tpInitilizeFromStorage = function () {
        var _this = this;
        this.tpStorageService.getItem('country').then(function (res) {
            if (res) {
                _this.myCountry = res;
            }
        }).catch(function (e) { });
        this.tpStorageService.getItem('code').then(function (res) {
            if (res) {
                _this.myCode = res;
            }
        }).catch(function (e) { });
    };
    PhonePage.prototype.ionViewDidLoad = function () {
        this.mySelectCountry = "1";
        this.loading.dismissMyLoading();
        this.tpInitilizeFromStorage();
        this.tpStorageService.removeItem("country");
        this.tpStorageService.removeItem("code");
    };
    PhonePage.prototype.listCountry = function () {
        var _this = this;
        var countryListModal = this.modalCtrl.create('CountryListPage', { type: this.phoneNumber.type });
        countryListModal.present();
        countryListModal.onDidDismiss(function (data) {
            if (data == undefined) {
                if (JSON.parse(_this.myCountry) == null) {
                    _this.countryName = "Select Country";
                    _this.countryCode = "";
                }
                else {
                    _this.countryName = JSON.parse(_this.myCountry);
                    _this.countryCode = JSON.parse(_this.myCode);
                    _this.phoneNumber.type = _this.countryCode;
                    _this.countryTemp = true;
                }
            }
            else {
                _this.countryName = data.name;
                _this.phoneNumber.type = data.code;
                _this.countryCode = data.code;
                _this.countryTemp = false;
            }
        });
    };
    PhonePage.prototype.doLoginwithMobile = function (phoneNumber) {
        var _this = this;
        this.submitAttempt = true;
        if (this.phoneNumber.type == '') {
            this.countryTemp = true;
        }
        if (this.authForm.valid && this.phoneNumber.type != '') {
            this.loading.presentLoading();
            var data = {
                "via": "sms",
                "phone_number": phoneNumber.number,
                "country_code": this.mySelectCountry,
                "api_key": __WEBPACK_IMPORTED_MODULE_7__app_app_angularfireconfig__["a" /* apiKey */]
            };
            this.authService.sendVerificationCode(data, 'phones/verification/start')
                .then(function (res) {
                _this.loading.dismissMyLoading();
                var data = JSON.parse(res['data']);
                if (res.status == 200) {
                    _this.loading.presentToast(data['message']);
                    _this.navCtrl.setRoot("PhoneverfyPage", { code: _this.mySelectCountry, number: phoneNumber.number });
                }
                else {
                    _this.loading.presentToast(data['message']);
                }
            }).catch(function (err) {
                _this.loading.dismissMyLoading();
                if (err.status == 400) {
                    _this.InvalidNumErr = "Invalid Mobile Number.";
                }
                else {
                    console.log(err);
                    _this.loading.presentToast('Something went wrong. Please try again.');
                }
            });
        }
    };
    PhonePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-phone',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\phone\phone.html"*/'<ion-content class="bg-img">\n\n\n\n\n  <ion-card  class="maincard_bubble" text-center padding  >\n    <ion-card-content >\n      <ion-card-title>\n        Welcome To TapAlly\n        </ion-card-title>\n      <p>\n        Enter your mobile number to get started\n      </p>\n    </ion-card-content>\n </ion-card>\n\n\n  <ion-card  >\n    <ion-card-content>\n      <form  [formGroup]="authForm">\n        <ion-list>\n           <!--\n            <ion-item (click)="listCountry()" class="textarea-lbl" >\n              <div>\n                <p>{{countryName}}<span *ngIf="countryCode">{{\' (+\'+countryCode+\')\'}}</span></p>\n              </div>\n            </ion-item>\n          -->\n\n          <ion-select   [(ngModel)]="mySelectCountry" value="1"   formControlName="mySelectCountry" >\n            <ion-option value="93">Afghanistan</ion-option>\n            <ion-option value="355">Albania</ion-option>\n            <ion-option value="213">Algeria</ion-option>\n            <ion-option value="684">American Samoa</ion-option>\n            <ion-option value="376">Andorra</ion-option>\n            <ion-option value="244">Angola</ion-option>\n            <ion-option value="809">Anguilla</ion-option>\n            <ion-option value="268">Antigua</ion-option>\n            <ion-option value="54">Argentina</ion-option>\n            <ion-option value="374">Armenia</ion-option>\n            <ion-option value="297">Aruba</ion-option>\n            <ion-option value="247">Ascension Island</ion-option>\n            <ion-option value="61">Australia</ion-option>\n            <ion-option value="672">Australian External Territories</ion-option>\n            <ion-option value="43">Austria</ion-option>\n            <ion-option value="994">Azerbaijan</ion-option>\n            <ion-option value="242">Bahamas</ion-option>\n            <ion-option value="246">Barbados</ion-option>\n            <ion-option value="973">Bahrain</ion-option>\n            <ion-option value="880">Bangladesh</ion-option>\n            <ion-option value="375">Belarus</ion-option>\n            <ion-option value="32">Belgium</ion-option>\n            <ion-option value="501">Belize</ion-option>\n            <ion-option value="229">Benin</ion-option>\n            <ion-option value="809">Bermuda</ion-option>\n            <ion-option value="975">Bhutan</ion-option>\n            <ion-option value="284">British Virgin Islands</ion-option>\n            <ion-option value="591">Bolivia</ion-option>\n            <ion-option value="387">Bosnia and Hercegovina</ion-option>\n            <ion-option value="267">Botswana</ion-option>\n            <ion-option value="55">Brazil</ion-option>\n            <ion-option value="284">British V.I.</ion-option>\n            <ion-option value="673">Brunei Darussalm</ion-option>\n            <ion-option value="359">Bulgaria</ion-option>\n            <ion-option value="226">Burkina Faso</ion-option>\n            <ion-option value="257">Burundi</ion-option>\n            <ion-option value="855">Cambodia</ion-option>\n            <ion-option value="237">Cameroon</ion-option>\n            <ion-option value="1" Selected="true">Canada</ion-option>\n            <ion-option value="238">CapeVerde Islands</ion-option> \n            <ion-option value="345">Cayman Islands</ion-option>\n            <ion-option value="238">Cape Verdi</ion-option>\n            <ion-option value="236">Central African Republic</ion-option>\n            <ion-option value="235">Chad</ion-option>\n            <ion-option value="56">Chile</ion-option>\n            <ion-option value="86">China (People\'s Republic)</ion-option>\n            <ion-option value="886">China-Taiwan</ion-option>\n            <ion-option value="57">Colombia</ion-option>\n            <ion-option value="269">Comoros and Mayotte</ion-option>\n            <ion-option value="242">Congo</ion-option>\n            <ion-option value="682">Cook Islands</ion-option>\n            <ion-option value="506">Costa Rica</ion-option>\n            <ion-option value="385">Croatia</ion-option>\n            <ion-option value="53">Cuba</ion-option>\n            <ion-option value="357">Cyprus</ion-option>\n            <ion-option value="420">Czech Republic</ion-option>\n            <ion-option value="45">Denmark</ion-option>\n            <ion-option value="246">Diego Garcia</ion-option>\n            <ion-option value="767">Dominca</ion-option>\n            <ion-option value="809">Dominican Republic</ion-option>\n            <ion-option value="253">Djibouti</ion-option>\n            <ion-option value="593">Ecuador</ion-option>\n            <ion-option value="20">Egypt</ion-option>\n            <ion-option value="503">El Salvador</ion-option>\n            <ion-option value="240">Equatorial Guinea</ion-option>\n            <ion-option value="291">Eritrea</ion-option>\n            <ion-option value="372">Estonia</ion-option>\n            <ion-option value="251">Ethiopia</ion-option>\n            <ion-option value="500">Falkland Islands</ion-option>\n            <ion-option value="298">Faroe (Faeroe) Islands (Denmark)</ion-option>\n            <ion-option value="679">Fiji</ion-option>\n            <ion-option value="358">Finland</ion-option>\n            <ion-option value="33">France</ion-option>\n            <ion-option value="596">French Antilles</ion-option>\n            <ion-option value="594">French Guiana</ion-option>\n            <ion-option value="241">Gabon (Gabonese Republic)</ion-option>\n            <ion-option value="220">Gambia</ion-option>\n            <ion-option value="995">Georgia</ion-option>\n            <ion-option value="49">Germany</ion-option>\n            <ion-option value="233">Ghana</ion-option>\n            <ion-option value="350">Gibraltar</ion-option>\n            <ion-option value="30">Greece</ion-option>\n            <ion-option value="299">Greenland</ion-option>\n            <ion-option value="473">Grenada/Carricou</ion-option>\n            <ion-option value="671">Guam</ion-option>\n            <ion-option value="502">Guatemala</ion-option>\n            <ion-option value="224">Guinea</ion-option>\n            <ion-option value="245">Guinea-Bissau</ion-option>\n            <ion-option value="592">Guyana</ion-option>\n            <ion-option value="509">Haiti</ion-option>\n            <ion-option value="504">Honduras</ion-option>\n            <ion-option value="852">Hong Kong</ion-option>\n            <ion-option value="36">Hungary</ion-option>\n            <ion-option value="354">Iceland</ion-option>\n            <ion-option value="91">India</ion-option>\n            <ion-option value="62">Indonesia</ion-option>\n            <ion-option value="98">Iran</ion-option>\n            <ion-option value="964">Iraq</ion-option>\n            <ion-option value="353">Ireland (Irish Republic; Eire)</ion-option>\n            <ion-option value="972">Israel</ion-option>\n            <ion-option value="39">Italy</ion-option>\n            <ion-option value="225">Ivory Coast (La Cote d\'Ivoire)</ion-option>\n            <ion-option value="876">Jamaica</ion-option>\n            <ion-option value="81">Japan</ion-option>\n            <ion-option value="962">Jordan</ion-option>\n            <ion-option value="7">Kazakhstan</ion-option>\n            <ion-option value="254">Kenya</ion-option>\n            <ion-option value="855">Khmer Republic (Cambodia/Kampuchea)</ion-option>\n            <ion-option value="686">Kiribati Republic (Gilbert Islands)</ion-option>\n            <ion-option value="82">Korea, Republic of (South Korea)</ion-option>\n            <ion-option value="850">Korea, People\'s Republic of (North Korea)</ion-option>\n            <ion-option value="965">Kuwait</ion-option>\n            <ion-option value="996">Kyrgyz Republic</ion-option>\n            <ion-option value="371">Latvia</ion-option>\n            <ion-option value="856">Laos</ion-option>\n            <ion-option value="961">Lebanon</ion-option>\n            <ion-option value="266">Lesotho</ion-option>\n            <ion-option value="231">Liberia</ion-option>\n            <ion-option value="370">Lithuania</ion-option>\n            <ion-option value="218">Libya</ion-option>\n            <ion-option value="423">Liechtenstein</ion-option>\n            <ion-option value="352">Luxembourg</ion-option>\n            <ion-option value="853">Macao</ion-option>\n            <ion-option value="389">Macedonia</ion-option>\n            <ion-option value="261">Madagascar</ion-option>\n            <ion-option value="265">Malawi</ion-option>\n            <ion-option value="60">Malaysia</ion-option>\n            <ion-option value="960">Maldives</ion-option>\n            <ion-option value="223">Mali</ion-option>\n            <ion-option value="356">Malta</ion-option>\n            <ion-option value="692">Marshall Islands</ion-option>\n            <ion-option value="596">Martinique (French Antilles)</ion-option>\n            <ion-option value="222">Mauritania</ion-option>\n            <ion-option value="230">Mauritius</ion-option>\n            <ion-option value="269">Mayolte</ion-option>\n            <ion-option value="52">Mexico</ion-option>\n            <ion-option value="691">Micronesia (F.S. of Polynesia)</ion-option>\n            <ion-option value="373">Moldova</ion-option>\n            <ion-option value="33">Monaco</ion-option>\n            <ion-option value="976">Mongolia</ion-option>\n            <ion-option value="473">Montserrat</ion-option>\n            <ion-option value="212">Morocco</ion-option>\n            <ion-option value="258">Mozambique</ion-option>\n            <ion-option value="95">Myanmar (former Burma)</ion-option>\n            <ion-option value="264">Namibia (former South-West Africa)</ion-option>\n            <ion-option value="674">Nauru</ion-option>\n            <ion-option value="977">Nepal</ion-option>\n            <ion-option value="31">Netherlands</ion-option>\n            <ion-option value="599">Netherlands Antilles</ion-option>\n            <ion-option value="869">Nevis</ion-option>\n            <ion-option value="687">New Caledonia</ion-option>\n            <ion-option value="64">New Zealand</ion-option>\n            <ion-option value="505">Nicaragua</ion-option>\n            <ion-option value="227">Niger</ion-option>\n            <ion-option value="234">Nigeria</ion-option>\n            <ion-option value="683">Niue</ion-option>\n            <ion-option value="850">North Korea</ion-option>\n            <ion-option value="1 670">North Mariana Islands (Saipan)</ion-option>\n            <ion-option value="47">Norway</ion-option>\n            <ion-option value="968">Oman</ion-option>\n            <ion-option value="92">Pakistan</ion-option>\n            <ion-option value="680">Palau</ion-option>\n            <ion-option value="507">Panama</ion-option>\n            <ion-option value="675">Papua New Guinea</ion-option>\n            <ion-option value="595">Paraguay</ion-option>\n            <ion-option value="51">Peru</ion-option>\n            <ion-option value="63">Philippines</ion-option>\n            <ion-option value="48">Poland</ion-option>\n            <ion-option value="351">Portugal (includes Azores)</ion-option>\n            <ion-option value="1 787">Puerto Rico</ion-option>\n            <ion-option value="974">Qatar</ion-option>\n            <ion-option value="262">Reunion (France)</ion-option>\n            <ion-option value="40">Romania</ion-option>\n            <ion-option value="7">Russia</ion-option>\n            <ion-option value="250">Rwanda (Rwandese Republic)</ion-option>\n            <ion-option value="670">Saipan</ion-option>\n            <ion-option value="378">San Marino</ion-option>\n            <ion-option value="239">Sao Tome and Principe</ion-option>\n            <ion-option value="966">Saudi Arabia</ion-option>\n            <ion-option value="221">Senegal</ion-option>\n            <ion-option value="381">Serbia and Montenegro</ion-option>\n            <ion-option value="248">Seychelles</ion-option>\n            <ion-option value="232">Sierra Leone</ion-option>\n            <ion-option value="65">Singapore</ion-option>\n            <ion-option value="421">Slovakia</ion-option>\n            <ion-option value="386">Slovenia</ion-option>\n            <ion-option value="677">Solomon Islands</ion-option>\n            <ion-option value="252">Somalia</ion-option>\n            <ion-option value="27">South Africa</ion-option>\n            <ion-option value="34">Spain</ion-option>\n            <ion-option value="94">Sri Lanka</ion-option>\n            <ion-option value="290">St. Helena</ion-option>\n            <ion-option value="869">St. Kitts/Nevis</ion-option>\n            <ion-option value="508">St. Pierre &(et) Miquelon (France)</ion-option>\n            <ion-option value="249">Sudan</ion-option>\n            <ion-option value="597">Suriname</ion-option>\n            <ion-option value="268">Swaziland</ion-option>\n            <ion-option value="46">Sweden</ion-option>\n            <ion-option value="41">Switzerland</ion-option>\n            <ion-option value="963">Syrian Arab Republic (Syria)</ion-option>\n            <ion-option value="689">Tahiti (French Polynesia)</ion-option>\n            <ion-option value="886">Taiwan</ion-option>\n            <ion-option value="7">Tajikistan</ion-option>\n            <ion-option value="255">Tanzania (includes Zanzibar)</ion-option>\n            <ion-option value="66">Thailand</ion-option>\n            <ion-option value="228">Togo (Togolese Republic)</ion-option>\n            <ion-option value="690">Tokelau</ion-option>\n            <ion-option value="676">Tonga</ion-option>\n            <ion-option value="1 868">Trinidad and Tobago</ion-option>\n            <ion-option value="216">Tunisia</ion-option>\n            <ion-option value="90">Turkey</ion-option>\n            <ion-option value="993">Turkmenistan</ion-option>\n            <ion-option value="688">Tuvalu (Ellice Islands)</ion-option>\n            <ion-option value="256">Uganda</ion-option>\n            <ion-option value="380">Ukraine</ion-option>\n            <ion-option value="971">United Arab Emirates</ion-option>\n            <ion-option value="44">United Kingdom</ion-option>\n            <ion-option value="598">Uruguay</ion-option>\n            <ion-option value="1">USA</ion-option>\n            <ion-option value="7">Uzbekistan</ion-option>\n            <ion-option value="678">Vanuatu (New Hebrides)</ion-option>\n            <ion-option value="39">Vatican City</ion-option>\n            <ion-option value="58">Venezuela</ion-option>\n            <ion-option value="84">Viet Nam</ion-option>\n            <ion-option value="1 340">Virgin Islands</ion-option>\n            <ion-option value="681">Wallis and Futuna</ion-option>\n            <ion-option value="685">Western Samoa</ion-option>\n            <ion-option value="381">Yemen (People\'s Democratic Republic of)</ion-option>\n            <ion-option value="967">Yemen Arab Republic (North Yemen)</ion-option>\n            <ion-option value="381">Yugoslavia (discontinued)</ion-option>\n            <ion-option value="243">Zaire</ion-option>\n            <ion-option value="260">Zambia</ion-option>\n            <ion-option value="263">Zimbabwe</ion-option>\n          </ion-select>\n\n            <div *ngIf="countryName == \'Select country\' && countryTemp" class="validation-msg">\n              <p>Please select country.</p>\n            </div>\n            <ion-item>\n                <ion-input class="txtinput" type="number" [formControl]="number" id="number" name="number" [(ngModel)]="phoneNumber.number" placeholder="Enter your mobile number"></ion-input>\n            </ion-item>\n            <div *ngIf="number.errors && (number.touched || submitAttempt)" class="validation-msg">\n              <p> Please enter valid mobile number.</p>\n            </div>\n            <div *ngIf="InvalidNumErr" class="validation-msg">\n              <p> {{InvalidNumErr}}</p>\n            </div>\n            <ion-item>\n              <button ion-button class="button-secondary" type="submit" (click)="doLoginwithMobile(phoneNumber)">Submit</button>\n            </ion-item>\n            <ion-item>\n              <div id="re-container"></div>\n            </ion-item>\n        </ion-list>\n      </form>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\phone\phone.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["q" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["s" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], PhonePage);
    return PhonePage;
}());

//# sourceMappingURL=phone.js.map

/***/ })

});
//# sourceMappingURL=28.js.map