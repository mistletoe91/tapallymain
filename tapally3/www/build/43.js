webpackJsonp([43],{

/***/ 795:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupmembersPageModule", function() { return GroupmembersPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__groupmembers__ = __webpack_require__(869);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GroupmembersPageModule = (function () {
    function GroupmembersPageModule() {
    }
    GroupmembersPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__groupmembers__["a" /* GroupmembersPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__groupmembers__["a" /* GroupmembersPage */]),
            ],
        })
    ], GroupmembersPageModule);
    return GroupmembersPageModule;
}());

//# sourceMappingURL=groupmembers.module.js.map

/***/ }),

/***/ 869:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupmembersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_groups_groups__ = __webpack_require__(404);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GroupmembersPage = (function () {
    function GroupmembersPage(navCtrl, navParams, groupservice, events, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.groupservice = groupservice;
        this.events = events;
        this.platform = platform;
        this.showheader = true;
    }
    GroupmembersPage.prototype.backButtonClick = function () {
        this.navCtrl.pop({ animate: true, direction: "forward" });
    };
    GroupmembersPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.groupmembers = this.groupservice.currentgroup;
        this.tempgrpmembers = this.groupmembers;
        this.events.subscribe('gotintogroup', function () {
            _this.events.unsubscribe('gotintogroup');
            _this.groupmembers = _this.groupservice.currentgroup;
            _this.tempgrpmembers = _this.groupmembers;
        });
    };
    GroupmembersPage.prototype.ionViewWillLeave = function () {
    };
    GroupmembersPage.prototype.ionViewDidLoad = function () {
    };
    GroupmembersPage.prototype.initializeItems = function () {
        this.groupmembers = this.tempgrpmembers;
    };
    GroupmembersPage.prototype.searchuser = function (ev) {
        this.initializeItems();
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.groupmembers = this.groupmembers.filter(function (item) {
                return (item.displayName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
            });
        }
    };
    GroupmembersPage.prototype.removemember = function (member) {
        var _this = this;
        var index = this.groupmembers.findIndex(function (o) {
            return o.uid === member.uid;
        });
        if (index !== -1)
            this.groupmembers.splice(index, 1);
        if (this.groupmembers.length == 0) {
            this.groupservice.deletegroup().then(function () {
                _this.navCtrl.setRoot('TabsPage');
            }).catch(function (err) {
            });
        }
        else {
            this.groupservice.deletemember(member);
        }
    };
    GroupmembersPage.prototype.goBack = function () {
        this.navCtrl.setRoot("TabsPage");
    };
    GroupmembersPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-groupmembers',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\groupmembers\groupmembers.html"*/'<ion-header  >\n  <ion-navbar hideBackButton>\n\n    <ion-buttons left>\n        <button ion-button (click)="backButtonClick()">\n             <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n\n    <ion-title>Group Members</ion-title>\n    <ion-searchbar placeholder="Search" class="searchbar"\n    (ionInput)="searchuser($event)"></ion-searchbar>\n  </ion-navbar>\n</ion-header>\n\n\n\n<ion-content>\n	<ion-list no-lines>\n	  <!-- <ion-item-sliding *ngFor="let key of groupmembers"> -->\n	    <ion-item *ngFor="let key of groupmembers">\n	      <ion-avatar item-left>\n	        <img src="{{key.photoURL}}">\n	      </ion-avatar>\n	      <h2>{{key.displayName}}</h2>\n	      <button ion-button icon-only item-end (click)="removemember(key)">\n	        <ion-icon name="trash"></ion-icon>\n	      </button>\n	    </ion-item>\n	    <!-- <ion-item-options slide="left">\n	      <button ion-button color="danger" (click)="removemember(key)">\n	        <ion-icon name="trash"></ion-icon>\n	        Remove\n	      </button>\n	    </ion-item-options>\n	  </ion-item-sliding> -->\n	</ion-list>\n\n	<div *ngIf="groupmembers?.length == 0" align="center">\n		 No result found!\n	</div>\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\groupmembers\groupmembers.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_groups_groups__["a" /* GroupsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */]])
    ], GroupmembersPage);
    return GroupmembersPage;
}());

//# sourceMappingURL=groupmembers.js.map

/***/ })

});
//# sourceMappingURL=43.js.map