webpackJsonp([22],{

/***/ 816:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilepicPageModule", function() { return ProfilepicPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profilepic__ = __webpack_require__(890);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProfilepicPageModule = (function () {
    function ProfilepicPageModule() {
    }
    ProfilepicPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profilepic__["a" /* ProfilepicPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profilepic__["a" /* ProfilepicPage */]),
            ],
        })
    ], ProfilepicPageModule);
    return ProfilepicPageModule;
}());

//# sourceMappingURL=profilepic.module.js.map

/***/ }),

/***/ 890:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilepicPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_imagehandler_imagehandler__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_requests_requests__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_groups_groups__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ionic_img_viewer__ = __webpack_require__(409);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_app_angularfireconfig__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Angular core

// Ionic-angular

// Providers





// Ionic image viewer



var ProfilepicPage = (function () {
    function ProfilepicPage(navCtrl, navParams, imgservice, zone, events, loadingProvider, userservice, loadingCtrl, modalCtrl, alertCtrl, imageViewerCtrl, viewCtrl, platform, requestservice, groupservice, tpStorageService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.imgservice = imgservice;
        this.zone = zone;
        this.events = events;
        this.loadingProvider = loadingProvider;
        this.userservice = userservice;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.imageViewerCtrl = imageViewerCtrl;
        this.viewCtrl = viewCtrl;
        this.platform = platform;
        this.requestservice = requestservice;
        this.groupservice = groupservice;
        this.tpStorageService = tpStorageService;
        this.imgurl = __WEBPACK_IMPORTED_MODULE_8__app_app_angularfireconfig__["c" /* env */].userPic;
        this.isDisabled = false;
        this.profileData = {
            username: '',
            disc: '',
            imgurl: __WEBPACK_IMPORTED_MODULE_8__app_app_angularfireconfig__["c" /* env */].userPic
        };
        this.usernamePlaceholder = "Enter your name";
        this.discPlaceholder = 'About yourself';
        this.showheader = true;
        this.name = navParams.get('name');
        this.img = navParams.get('img');
        this._imageViewerCtrl = imageViewerCtrl;
    }
    ProfilepicPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    ProfilepicPage.prototype.presentImage = function (myImage) {
        var imageViewer = this._imageViewerCtrl.create(myImage);
        imageViewer.present();
    };
    ProfilepicPage.prototype.ionViewWillEnter = function () {
        this.loadingProvider.presentLoading();
        this.tpInitilizeFromStorage();
    };
    ProfilepicPage.prototype.tpInitilizeFromStorage = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (res) {
            if (res) {
                _this.userId = res;
                _this.loaduserdetails();
            }
        }).catch(function (e) { });
    };
    ProfilepicPage.prototype.loaduserdetails = function () {
        var _this = this;
        var userId = this.userId;
        if (userId != undefined) {
            this.isDisabled = true;
            this.userservice.getuserdetails().then(function (res) {
                _this.loadingProvider.dismissMyLoading();
                if (res) {
                    var desc = res['disc'].replace(new RegExp('\n', 'g'), "<br />");
                    if (res['displayName'] != undefined) {
                        _this.profileData.username = res['displayName'];
                    }
                    if (res['photoURL'] != undefined) {
                        _this.profileData.imgurl = res['photoURL'];
                    }
                    if (desc != undefined) {
                        _this.profileData.disc = desc;
                    }
                }
            }).catch(function (err) {
            });
        }
        else {
            this.loadingProvider.dismissMyLoading();
            this.profileData = {
                username: '',
                disc: '',
                imgurl: __WEBPACK_IMPORTED_MODULE_8__app_app_angularfireconfig__["c" /* env */].userPic
            };
        }
    };
    ProfilepicPage.prototype.backButtonClick = function () {
        this.navCtrl.pop({ animate: true, direction: "forward" });
    };
    ProfilepicPage.prototype.choseImage = function () {
        var _this = this;
        console.log("Choose Image");
        this.imgservice.uploadimage().then(function (uploadedurl) {
            _this.loadingProvider.dismissMyLoading();
            _this.zone.run(function () {
                _this.profileData.imgurl = uploadedurl;
                _this.updateproceed(_this.profileData.imgurl);
            });
        }).catch(function (err) {
            if (err == 20) {
                _this.navCtrl.pop();
            }
            _this.loadingProvider.dismissMyLoading();
        });
    };
    ProfilepicPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    ProfilepicPage.prototype.updateproceed = function (imgurl) {
        this.userservice.updateimage(imgurl).then(function (res) {
            if (res.success) {
                console.log("Userservice is updated with imgurl");
            }
            else {
                //alert(res);
            }
        });
    };
    ProfilepicPage.prototype.proceed = function () {
        var _this = this;
        this.userservice.directLogin().then(function (data) {
            if (data.success) {
                _this.navCtrl.setRoot('TabsPage');
            }
        });
    };
    ProfilepicPage.prototype.username = function (username) {
        var _this = this;
        var profileModal = this.modalCtrl.create("UsernamePage", { userName: username });
        profileModal.onDidDismiss(function (data) {
            if (data != undefined) {
                _this.profileData.username = data.username;
                _this.userservice.updatedisplayname(data.username).then(function (res) {
                    if (res.success) {
                    }
                });
            }
            else {
            }
        });
        profileModal.present();
    };
    ProfilepicPage.prototype.discription = function (disc) {
        var _this = this;
        var profileModal = this.modalCtrl.create("DiscPage", { disc: disc });
        profileModal.onDidDismiss(function (data) {
            if (data != undefined) {
                data.disc = data.disc.replace(new RegExp('\n', 'g'), "<br />");
                _this.profileData.disc = data.disc;
                _this.userservice.updatedisc(data.disc).then(function (res) {
                    if (res.success) {
                    }
                });
            }
            else {
            }
        });
        profileModal.present();
    };
    ProfilepicPage.prototype.deleteProfile = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Delete User',
            message: 'Do you want to delete this Account?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                        _this.userservice.deleteUser().then(function (data) {
                            if (data.success) {
                                _this.navCtrl.setRoot('PhonePage');
                            }
                            else {
                            }
                        }).catch(function (err) {
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    ProfilepicPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profilepic',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\profilepic\profilepic.html"*/'<ion-header >\n  <ion-navbar hideBackButton>\n    <ion-buttons left>\n        <button ion-button (click)="backButtonClick()">\n             <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n    <ion-title>Profile</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="bg-grey_old">\n  <div class="bg-profile-wrap">\n    <div [style.background]="\'url(\'+ profileData.imgurl + \')\'" class="bg-profile"></div>\n    <div class="profile-image">\n      <ion-img [src]="profileData.imgurl" class="imgcss"  #myImage (click)="presentImage(myImage)"></ion-img> \n      <button ion-button round outline color="btn-white" (click)="choseImage()" style="padding: 0 1.2rem;">\n        <ion-icon name="camera"></ion-icon>\n      </button>\n\n    </div>\n  </div>\n\n\n\n  <div class="bg-grey_old" padding-vertical>\n    <ion-card>\n      <ion-card-content>\n        <ion-list>\n          <ion-item (click)="username(profileData.username)">\n            <ion-label>\n              <p *ngIf="profileData.username != \'\'">{{profileData.username}}</p>\n              <p *ngIf="profileData.username == \'\'">{{usernamePlaceholder}}</p>\n            </ion-label>\n          </ion-item>\n\n          <ion-item class="textarea-lbl" (click)="discription(profileData.disc)">\n            <div>\n              <p *ngIf="profileData.disc != \'\'" [innerHTML]="profileData.disc"></p>\n              <p *ngIf="profileData.disc == \'\'" [innerHTML]="discPlaceholder"></p>\n            </div>\n          </ion-item>\n          <button ion-button class="button-secondary" (click)="proceed()" [disabled]="profileData.username == \'\'">Update</button>\n          <button ion-button style="display:none;" class="button-secondary hide" (click)="deleteProfile()" [disabled]="profileData.username == \'\'">Delete</button>\n          </ion-list>\n      </ion-card-content>\n    </ion-card>\n  </div>\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\profilepic\profilepic.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_imagehandler_imagehandler__["a" /* ImagehandlerProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_7_ionic_img_viewer__["a" /* ImageViewerController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_5__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_groups_groups__["a" /* GroupsProvider */],
            __WEBPACK_IMPORTED_MODULE_9__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], ProfilepicPage);
    return ProfilepicPage;
}());

//# sourceMappingURL=profilepic.js.map

/***/ })

});
//# sourceMappingURL=22.js.map