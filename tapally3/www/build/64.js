webpackJsonp([64],{

/***/ 774:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuddychatPageModule", function() { return BuddychatPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__buddychat__ = __webpack_require__(848);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_image_loader__ = __webpack_require__(412);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var BuddychatPageModule = (function () {
    function BuddychatPageModule() {
    }
    BuddychatPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__buddychat__["a" /* BuddychatPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__buddychat__["a" /* BuddychatPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_image_loader__["a" /* IonicImageLoader */]
            ],
        })
    ], BuddychatPageModule);
    return BuddychatPageModule;
}());

//# sourceMappingURL=buddychat.module.js.map

/***/ }),

/***/ 848:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BuddychatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_chat_chat__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_imagehandler_imagehandler__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_fcm_fcm__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_requests_requests__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_contact_contact__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ionic_img_viewer__ = __webpack_require__(409);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_tpstorage_tpstorage__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_cats_cats__ = __webpack_require__(410);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_sqlite__ = __webpack_require__(71);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








//import { Storage } from '@ionic/storage';






var BuddychatPage = (function () {
    //private onResumeSubscription: Subscription;
    function BuddychatPage(navCtrl, navParams, 
        //public storage: Storage,
        fcm, chatservice, events, catservice, zone, sqlite, loadingCtrl, imgstore, fb, loading, app, userservice, actionSheetCtrl, eleRef, popoverCtrl, alertCtrl, requestservice, contactProvider, modalCtrl, platform, imageViewerCtrl, tpStorageService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fcm = fcm;
        this.chatservice = chatservice;
        this.events = events;
        this.catservice = catservice;
        this.zone = zone;
        this.sqlite = sqlite;
        this.loadingCtrl = loadingCtrl;
        this.imgstore = imgstore;
        this.fb = fb;
        this.loading = loading;
        this.app = app;
        this.userservice = userservice;
        this.actionSheetCtrl = actionSheetCtrl;
        this.eleRef = eleRef;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.requestservice = requestservice;
        this.contactProvider = contactProvider;
        this.modalCtrl = modalCtrl;
        this.platform = platform;
        this.imageViewerCtrl = imageViewerCtrl;
        this.tpStorageService = tpStorageService;
        this.newmessage = '';
        this.newmessageTmp = '';
        this.isRefresher = false;
        this.allmessages = [];
        this.filterallmessages = [];
        this.tempFilterallmessages = [];
        this.allmessagesGroups = [];
        this.isData = false;
        this.datacounter = 10;
        this.headercopyicon = true;
        this.showheader = true;
        this.selectAllMessage = [];
        this.selectCounter = 0;
        this.isuserBlock = false;
        this.msgstatus = false;
        this.backbuttonstatus = false;
        ////console.log ("Buddypage Constructor");
        this.platform.ready().then(function (readySource) {
            _this.catItems = _this.catservice.getCats();
            _this.referralRequestPrefix = 'Referral Request';
            _this.referralRequestPostfix = '';
            _this._imageViewerCtrl = imageViewerCtrl;
            _this.authForm = _this.fb.group({
                'message': [null, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required])]
            });
            //mytodo you can read the photoURL from cache as well , but use with caution as it may not be uptodate
            _this.userservice.getuserdetails().then(function (res) {
                _this.photoURL = res['photoURL'];
            });
            _this.message = _this.authForm.controls['message'];
            _this.buddy = _this.chatservice.buddy; // buddy is receipient of chat
            //The resume event emits when the native platform pulls the application out from the background
            /*
            this.onResumeSubscription = platform.resume.subscribe(() => {
                this.chatservice.buddymessageRead();
            });
            */
            _this.events.subscribe('newmessage', function () {
                ////console.log ("Buddypage Newmessage subscription ")
                _this.zone.run(function () {
                    _this.MessageSubscription();
                });
            });
        });
    }
    BuddychatPage.prototype.fnGetReferralDetail = function (item) {
        if (null != item) {
            this.navCtrl.push("IncentivedetailPage", { item: JSON.stringify(item) });
        }
    };
    BuddychatPage.prototype.fnSendToEarningsPage = function () {
        this.navCtrl.push("EarningsPage");
    };
    BuddychatPage.prototype.fnviewincentiverequst = function (item) {
        var _this = this;
        this.tpStorageService.get('userUID').then(function (storage_res) {
            _this.app.getRootNav().push('ViewandredeemPage', { requestId: item.request_to_release_incentive, referral_send_by: item.sentby, referral_received_by: storage_res });
        }).catch(function (e) {
        });
    };
    BuddychatPage.prototype.MessageSubscription = function () {
        this.allmessages = [];
        this.allmessagesGroups = [];
        for (var msgKey in this.chatservice.buddymessages) {
            if (null == this.chatservice.buddymessages[msgKey].dateofmsg) {
                continue;
            }
            this.chatservice.buddymessages[msgKey].selection = false;
            this.chatservice.buddymessages[msgKey].selectCatName = '';
            if (this.chatservice.buddymessages[msgKey].referral_type == null) {
                this.chatservice.buddymessages[msgKey].referral_type = "";
            }
            if (this.chatservice.buddymessages[msgKey].selectCatId != null) {
                for (var g = 0; g < this.catItems.length; g++) {
                    if (this.catItems[g]['id'] == this.chatservice.buddymessages[msgKey].selectCatId) {
                        this.chatservice.buddymessages[msgKey].selectCatName = this.catItems[g]['name'];
                        break;
                    }
                }
            }
            var tmpArr = {
                'dateofmsg': this.chatservice.buddymessages[msgKey].dateofmsg,
                'message_item': [this.chatservice.buddymessages[msgKey]]
            };
            //Check if data exists
            var foundKey = false;
            for (var myKey in this.allmessagesGroups) {
                if (this.allmessagesGroups[myKey].dateofmsg == this.chatservice.buddymessages[msgKey].dateofmsg) {
                    this.allmessagesGroups[myKey].message_item.push(this.chatservice.buddymessages[msgKey]);
                    foundKey = true;
                    break;
                }
            } //end for
            if (!foundKey) {
                this.allmessagesGroups.push(tmpArr);
            }
            this.allmessages.push(this.chatservice.buddymessages[msgKey]); //this is used for search the chats
        } //end for
        ////////console.log ("Done ji");
        ////////console.log (this.allmessagesGroups);
        //this.tpStorageService.set('allBuddyChats', this.allmessages);//this is used for search the chats
        //});
    };
    BuddychatPage.prototype.ionViewWillLeave = function () {
        this.events.unsubscribe('newmessage');
        this.chatservice.buddymessages = [];
        //this.navCtrl.pop();
    };
    BuddychatPage.prototype.ionViewDidEnter = function () {
        ////console.log ("Buddypage ionViewDidEnter");
        this.todaydate = this.formatDate(new Date());
        this.chatservice.getbuddymessages(this.datacounter);
        this.scrollToBottom_();
    };
    BuddychatPage.prototype.saveContactInPhone = function (item) {
        var _this = this;
        item.referral.phoneNumber = item.referral.mobile;
        this.contactProvider.addContact(item.referral).then(function (res) {
            if (res) {
                _this.chatservice.updateContactSaved(item.messageId);
                for (var x = 0; x < _this.allmessagesGroups.length; x++) {
                    var msgMy = _this.allmessagesGroups[x].message_item;
                    for (var y = 0; y < msgMy.length; y++) {
                        if (msgMy[y].messageId == item.messageId) {
                            _this.allmessagesGroups[x].message_item[y].referralSavedOnPhone = true;
                            break;
                        }
                    }
                }
            }
        });
    };
    BuddychatPage.prototype.doRefresh = function (refresher) {
        this.isRefresher = true;
        this.UnreadMSG = 0;
        this.datacounter += 10;
        this.chatservice.getbuddymessages(this.datacounter);
        this.closeButtonClick();
        setTimeout(function () {
            refresher.complete();
        }, 500);
    };
    BuddychatPage.prototype.viewBuddy = function (name, mobile, disc, img, status) {
        this.navCtrl.push("ViewBuddyPage", { img: img, name: name, mobile: mobile, disc: disc, userstatus: status });
    };
    BuddychatPage.prototype.referralSendForward = function (myEvent) {
        // I want to send buddy's referral to someone else
        this.navCtrl.push("SendReferralForwardPage", { source: "buddypage", buddy_uid: this.buddy.uid, buddy_displayName: this.buddy.displayName });
    };
    BuddychatPage.prototype.referralSendBackward = function (myEvent) {
        this.navCtrl.push("SendReferralBackwardPage", { source: "buddypage", buddy_uid: this.buddy.uid, buddy_displayName: this.buddy.displayName });
    };
    BuddychatPage.prototype.sendReferral = function (friend_asking_referral, catid) {
        this.navCtrl.push("FriendAskingReferralPage", { friend_asking_referral_uid: friend_asking_referral.uid, friend_asking_referral_displayName: friend_asking_referral.displayName, catid: catid });
    };
    BuddychatPage.prototype.formatDate = function (date) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        if (dd < 10)
            dd = '0' + dd;
        if (mm < 10)
            mm = '0' + mm;
        return dd + '/' + mm + '/' + yyyy;
    };
    BuddychatPage.prototype.addmessage_async = function () {
        var _this = this;
        this.newmessageTmp = this.newmessage;
        this.newmessage = '';
        ////////console.log ("addmessage_async");
        setTimeout(function () {
            _this.addmessage();
        }, 1);
    }; //end async function
    BuddychatPage.prototype.addmessage = function () {
        var _this = this;
        //this.closeButtonClick()
        this.UnreadMSG = 0;
        this.selectAllMessage = [];
        this.selectCounter = 0;
        this.headercopyicon = true;
        this.userservice.getstatusblock(this.buddy).then(function (res) {
            ////////console.log ("-getstatus");
            if (!res) {
                _this.newmessageTmp = _this.newmessageTmp.trim();
                if (_this.newmessageTmp != '') {
                    ////////console.log ("-if message is not empty");
                    _this.fcm.sendNotification(_this.buddy, 'You have received a message', 'chatpage');
                    //this.events.unsubscribe('newmessage');//to avoid triggring multiple calls
                    //mytodo : above fix doesn`t work. Please try something else
                    _this.chatservice.addnewmessage(_this.newmessageTmp, 'message', _this.buddyStatus).then(function () {
                        /*this.events.subscribe('newmessage', () => {
                            this.MessageSubscription ();
                        });
                        */
                        _this.newmessage = '';
                        _this.scrollto();
                        _this.msgstatus = false;
                    });
                }
                else {
                    _this.newmessage = _this.newmessageTmp;
                    _this.loading.presentToast('Please enter valid message...');
                }
            }
            else {
                //user might be blocked
                _this.newmessage = _this.newmessageTmp;
                var alert_1 = _this.alertCtrl.create({
                    message: 'Unblock ' + _this.buddy.displayName + ' to send a message.',
                    buttons: [
                        {
                            text: 'CANCEL',
                            role: 'cancel',
                            handler: function () {
                            }
                        },
                        {
                            text: 'UNBLOCK',
                            handler: function () {
                                _this.userservice.unblockUser(_this.buddy).then(function (res) {
                                    if (res) {
                                        _this.addmessage();
                                    }
                                });
                            }
                        }
                    ]
                });
                alert_1.present();
            }
        });
        ////////console.log ("-End Function");
    }; //end function
    BuddychatPage.prototype.scrollto = function () {
        /*
          setTimeout(() => {
              if(this.content._scroll)  {
                this.content.scrollToBottom();
              }
          }, 1000);
          */
    };
    BuddychatPage.prototype.scrollToBottom_ = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.content._scroll) {
                _this.content.scrollToBottom();
            }
        }, 1);
    };
    BuddychatPage.prototype.sendPicMsg = function () {
        var _this = this;
        this.UnreadMSG = 0;
        this.userservice.getstatus(this.buddy).then(function (res) {
            if (!res) {
                _this.loading.presentLoading();
                _this.imgstore.picmsgstore().then(function (imgurl) {
                    _this.chatservice.addnewmessage(imgurl, 'image', _this.buddyStatus).then(function () {
                        _this.loading.dismissMyLoading();
                        if (_this.buddyStatus != 'online') {
                            var newMessage = 'You have received a message';
                            _this.fcm.sendNotification(_this.buddy, newMessage, 'chatpage');
                        }
                        _this.scrollto();
                        _this.newmessage = '';
                    });
                }).catch(function (err) {
                    _this.loading.dismissMyLoading();
                    alert(err);
                });
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    message: 'Unblock ' + _this.buddy.displayName + ' to send a message.',
                    buttons: [
                        {
                            text: 'CANCEL',
                            role: 'cancel',
                            handler: function () {
                            }
                        },
                        {
                            text: 'UNBLOCK',
                            handler: function () {
                                _this.userservice.unblockUser(_this.buddy).then(function (res) {
                                    if (res) {
                                        _this.sendPicMsg();
                                    }
                                });
                            }
                        }
                    ]
                });
                alert_2.present();
            }
        });
    };
    BuddychatPage.prototype.getAllMessage = function (key) {
        ////////console.log ("getAllMessage CALLED");
        ////////console.log (key);
        ////////console.log (this.allmessages);
        var messagesAll = [];
        var counter = 0;
        // for(let i = 0; i<key;i++){
        for (var j = this.allmessages.length; j > 0; j--) {
            if (counter < key) {
                var msg = [];
                for (var k = this.allmessages[j - 1].messages.length; k > 0; k--) {
                    if (counter < key) {
                        msg.push(this.allmessages[j - 1].messages[k - 1]);
                    }
                    else {
                        break;
                    }
                    counter++;
                }
                messagesAll.push({ date: this.allmessages[j - 1].date, messages: msg });
            }
            else {
                break;
            }
        }
        var filterallmessages = [];
        var sortDate = this.sortdata(messagesAll);
        var _loop_1 = function (i) {
            var tempmsg = [];
            for (var j = sortDate[i].messages.length; j > 0; j--) {
                ////////console.log ("inside loop of sortDate");
                tempmsg.push(sortDate[i].messages[j - 1]);
            }
            this_1.zone.run(function () {
                filterallmessages.push({ date: sortDate[i].date, messages: tempmsg });
            });
        };
        var this_1 = this;
        ////////console.log ("Check h1");
        for (var i = 0; i < sortDate.length; i++) {
            _loop_1(i);
        }
        filterallmessages.sort(function (a, b) {
            var nameA = a.date; // ignore upper and lowercase
            var nameB = b.date; // ignore upper and lowercase
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        });
        this.filterallmessages = filterallmessages;
        // this.filterallmessages = [];
        // for (let i = filterallmessages.length - 1; i >= 0; i--) {
        //   this.filterallmessages.push(filterallmessages[i]);
        // }
        // this.filterallmessages = this.sortdata(this.filterallmessages);
        this.scrollto();
        // this.filterallmessages = this.sortdata(messagesAll);
        //   let tmpmessage = [];
        //   this.filterallmessages.forEach(element => {
        //     if (element.messages.length > 3) {
        //       let imgcter=0
        //       let imgcontainer=[];
        //       element.messages.forEach(ele => {
        //         if(ele.type == 'image'){
        //           imgcter++;
        //           imgcontainer.push(ele)
        //             if(imgcter>4){
        //             }
        //         }else{
        //           tmpmessage.push(ele);
        //         }
        //       });
        //     }
        //     tmpmessage.push(element);
        //   });
    };
    BuddychatPage.prototype.sortdata = function (data) {
        return data.sort(function (a, b) {
            var keyA = new Date(a.date), keyB = new Date(b.date);
            // Compare the 2 dates
            if (keyA < keyB)
                return -1;
            if (keyA > keyB)
                return 1;
            return 0;
        });
    };
    BuddychatPage.prototype.backButtonClick = function () {
        if (this.navParams.get('back_button_pop_on') || this.navParams.get('back_button_pop_on') == "true") {
            this.navCtrl.pop({ animate: true, direction: "right" });
        }
        else {
            this.navCtrl.setRoot('TabsPage');
        }
        //
    };
    BuddychatPage.prototype.closeButtonClick = function () {
        this.selectAllMessage = [];
        this.selectCounter = 0;
        this.headercopyicon = true;
        this.backbuttonstatus = false;
        for (var i = 0; i < this.filterallmessages.length; i++) {
            for (var j = 0; j < this.filterallmessages[i].messages.length; j++) {
                this.filterallmessages[i].messages[j].selection = false;
            }
        }
    };
    BuddychatPage.prototype.popoverdialog = function (item, event) {
        var flag = 0;
        if (this.selectAllMessage.length != 0) {
            for (var i = 0; i < this.selectAllMessage.length; i++) {
                if (item.messageId == this.selectAllMessage[i].messageId) {
                    flag = 1;
                    //event.target.classList.remove('select-item');
                    item.selection = false;
                    this.selectAllMessage.splice(i, 1);
                    break;
                }
                else {
                }
            }
            if (flag == 1) {
            }
            else {
                this.popoveranothermsg(item, event);
            }
        }
        else {
            item.selection = true;
            this.selectAllMessage.push(item);
            if (this.headercopyicon == true) {
                this.headercopyicon = false;
                this.backbuttonstatus = true;
            }
            else {
                this.headercopyicon = true;
                this.backbuttonstatus = false;
            }
        }
        this.selectCounter = this.selectAllMessage.length;
        if (this.selectAllMessage.length == 0) {
            this.headercopyicon = true;
            this.backbuttonstatus = false;
        }
    };
    BuddychatPage.prototype.popoveranothermsg = function (item, event) {
        var flag = 0;
        if (this.selectAllMessage.length >= 1) {
            for (var i = 0; i < this.selectAllMessage.length; i++) {
                if (item.messageId == this.selectAllMessage[i].messageId) {
                    item.selection = false;
                    flag = 1;
                    this.selectAllMessage.splice(i, 1);
                    break;
                }
                else {
                    flag = 0;
                }
            }
            if (flag == 1) {
            }
            else {
                item.selection = true;
                this.selectAllMessage.push(item);
            }
        }
        this.selectCounter = this.selectAllMessage.length;
        if (this.selectAllMessage.length == 0) {
            this.headercopyicon = true;
            this.backbuttonstatus = false;
        }
    };
    BuddychatPage.prototype.deleteMessage = function () {
        var _this = this;
        if (this.selectAllMessage.length > 1) {
            this.DeleteMsg = "Do you want to delete these messages?";
        }
        else {
            this.DeleteMsg = "Do you want to delete this message?";
        }
        this.loading.presentLoading();
        var alert = this.alertCtrl.create({
            title: 'Delete Message',
            message: this.DeleteMsg,
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () {
                        _this.closeButtonClick();
                        _this.loading.dismissMyLoading();
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        if (_this.selectAllMessage.length != 0) {
                            _this.chatservice.deleteMessages(_this.selectAllMessage).then(function (res) {
                                if (res) {
                                    _this.selectAllMessage = [];
                                    if (_this.selectAllMessage.length == 0) {
                                        _this.headercopyicon = true;
                                        _this.backbuttonstatus = false;
                                        _this.loading.dismissMyLoading();
                                    }
                                }
                            }).catch(function (err) {
                            });
                        }
                    }
                }
            ]
        });
        alert.present().then(function () {
        });
    };
    BuddychatPage.prototype.viewPopover = function (myEvent) {
        var _this = this;
        var popover = this.popoverCtrl.create('MenuBlockPage', { buddy: this.buddy });
        popover.present({ ev: myEvent });
        popover.onDidDismiss(function (data) {
            _this.userservice.getstatusblockuser(_this.buddy);
        });
    };
    //newkk
    BuddychatPage.prototype.callFunctionScroll = function () {
        if (this.content._scroll && !this.isRefresher) {
            this.content.scrollToBottom(0);
        }
    };
    BuddychatPage.prototype.ionViewWillEnter = function () {
        ////console.log ("BUDDYPAGE ionViewWillEnter");
    };
    BuddychatPage.prototype.openAttachment = function (myEvent) {
        this.openAttachment_(myEvent);
        /*
        //mytodo : disabling "Media Libary " on ios because i could not make it work. But we need to make it work
        if (this.platform.is('ios')) {
          this.sendPicMsg_ ();
        } else {
          this.openAttachment_(myEvent);
        }
        */
    };
    //duplicating from attachment.ts
    BuddychatPage.prototype.sendPicMsg_ = function () {
        var _this = this;
        this.imgstore.cameraPicmsgStore().then(function (imgurl) {
            _this.chatservice.addnewmessage(imgurl, 'image', _this.buddyStatus).then(function () {
                var newMessage = 'You have received a message';
                _this.fcm.sendNotification(_this.buddy, newMessage, 'chatpage');
            });
        }).catch(function (err) {
        });
    }; //end function
    BuddychatPage.prototype.openAttachment_ = function (myEvent) {
        var _this = this;
        var popover = this.popoverCtrl.create('AttachmentsPage', { buddy: this.buddy });
        popover.present({
            ev: myEvent
        });
        popover.onDidDismiss(function (pages) {
            if (pages != 0 && pages != undefined && pages != "undefined" && pages != null) {
                if (pages.page) {
                    _this.navCtrl.push(pages.page, { buddy: pages.buddy, flag: pages.flag });
                }
            }
            else {
            }
        });
    };
    BuddychatPage.prototype.presentImage = function (myImage) {
        if (this.selectAllMessage.length == 0) {
            var imageViewer = this._imageViewerCtrl.create(myImage);
            imageViewer.present();
            //imageViewer.onDidDismiss(() => ));
        }
    };
    BuddychatPage.prototype.openAllImg = function (items, $event) {
        if (this.selectAllMessage.length == 0) {
            this.navCtrl.push("AllimgPage", { allimg: items });
        }
    };
    BuddychatPage.prototype.openDoc = function (path) {
        if (this.selectAllMessage.length == 0) {
            this.imgstore.openDocument(path).then(function (res) {
            }).catch(function (err) {
            });
        }
    };
    BuddychatPage.prototype.openContacts = function (contacts) {
        if (this.selectAllMessage.length == 0) {
            this.navCtrl.push("ShowContactPage", { contacts: contacts });
        }
    };
    BuddychatPage.prototype.showLocation = function (location) {
        if (this.selectAllMessage.length == 0) {
            this.navCtrl.push("MapPage", { location: location });
        }
    };
    BuddychatPage.prototype.onChange = function (e) {
        if (this.newmessage.trim()) {
            this.msgstatus = true;
        }
        else {
            this.msgstatus = false;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('content'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Content */])
    ], BuddychatPage.prototype, "content", void 0);
    BuddychatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-buddychat',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\buddychat\buddychat.html"*/'<ion-header  >\n  <!-- hideBackButton="{{backbuttonstatus}}" -->\n    <ion-navbar  hideBackButton  >\n      <ion-buttons left>\n          <button ion-button (click)="backButtonClick()">\n               <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n          </button>\n      </ion-buttons>\n\n        <div class="header-wrap" *ngIf="headercopyicon">\n            <div class="flex-start">\n                <!-- <ion-buttons class="left-arrow">\n                    <button ion-button icon-only (click)="backButtonClick()">\n                        <ion-icon ios="md-arrow-round-back" md="md-arrow-round-back"></ion-icon>\n                    </button>\n                </ion-buttons> -->\n                <div *ngIf="buddy && buddy.photoURL" class="title-wrap" (click)="viewBuddy(buddy.displayName,buddy.mobile,buddy.disc,buddy.photoURL,isuserBlock)" text-left left>\n                   <ion-avatar class="user-img" left>\n                      <!--  <img src="{{buddy.photoURL}}" *ngIf="isuserBlock == false"> -->\n							 <img-loader src="{{buddy.photoURL}}" useImg></img-loader>\n                        <img src="assets/imgs/user.png" *ngIf="isuserBlock == true">\n                    </ion-avatar>\n\n                    <div class="title-uname">\n                        <h5>{{buddy.displayName}}</h5>\n                        <div class="font" *ngIf="isuserBlock == false">{{buddyStatus}}</div>\n                    </div>\n                </div>\n            </div>\n            <!-- mytodo following functionality is not tested . so test and fix before uncommenting -->\n            <div class="flex-end hide">\n                <ion-buttons end>\n                    <button ion-button icon-only (click)="viewPopover($event)">\n                        <ion-icon ios="md-more" md="md-more"></ion-icon>\n                    </button>\n                </ion-buttons>\n            </div>\n        </div>\n        <div class="select-wrap" *ngIf="!headercopyicon">\n            <div class="flex-start">\n                <ion-buttons  start class="left-arrow">\n                    <button start ion-button icon-only (click)="closeButtonClick()">\n                    <ion-icon ios="md-close" md="md-close"></ion-icon>\n                    </button>\n                </ion-buttons>\n                <div class="select-title" text-left left *ngIf="selectCounter > 0">\n                {{selectCounter}}\n                </div>\n            </div>\n            <div class="flex-end">\n                <ion-buttons end>\n                    <button ion-button icon-only (click)="deleteMessage()">\n                        <ion-icon ios="ios-trash" md="md-trash"></ion-icon>\n                    </button>\n                </ion-buttons>\n            </div>\n        </div>\n    </ion-navbar>\n</ion-header>\n<ion-content #content>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content class="dayheader"  pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles"\n            refreshingText="Refreshing...">\n        </ion-refresher-content>\n    </ion-refresher>\n    <div class="chatwindow">\n        <ion-list no-lines>\n            <ng-container *ngFor="let items  of allmessagesGroups ; ">\n                <ion-list-header class="dayheader" *ngIf="items.dateofmsg == todaydate" align="center">Today</ion-list-header>\n                <ion-list-header class="dayheader" *ngIf="items.dateofmsg != todaydate" align="center">{{items.dateofmsg}}</ion-list-header>\n                <div *ngFor="let item of items.message_item ; let i = index;let last = last; ">\n                     {{last ? callFunctionScroll() : \'\'}}\n                    <ion-item text-wrap>\n                        <div [ngClass]="{ \'select-item\': item.selection }" (press)="popoverdialog(item,$event)" (click)="popoveranothermsg(item,$event)">\n                            <div class="msg-wrap you" *ngIf="item.sentby === buddy.uid">\n                                <ion-avatar *ngIf="isuserBlock == false">\n                             <!--    <img src="{{buddy.photoURL}}"> --> \n								 <img-loader src="{{buddy.photoURL}}" useImg></img-loader>\n                                </ion-avatar>\n                                <ion-avatar *ngIf="isuserBlock == true">\n                                    <img src="assets/imgs/user.png">\n                                </ion-avatar>\n\n                                <div [ngClass]="item.type == \'image\' || item.type == \'location\' || item.type == \'document\' ?\'bubble-img\':\'bubble\'">\n\n                                   <div class="msg" *ngIf="item.type == \'message\' && (!item.selectCatId || item.selectCatId<=0) && !item.referral_type && !item.request_to_release_incentive && !item.referral_redeemed">{{item.message}}</div>\n\n                                   <div class="msg" *ngIf="item.type == \'message\' && item.referral_redeemed">\n                                       <h2 class="headinginchat" (click)="fnSendToEarningsPage ()" class="headinginchat"><span>Incentives Redeemed</span></h2>\n                                       <p class="typeofreferral">Your request to redeem incentives has been approved</p>\n                                       <div><ion-badge (click)="fnSendToEarningsPage()"  color="dark" class="savecnt" text-wrap >View Details</ion-badge></div>\n                                  </div>\n\n\n                                       <div class="msg" *ngIf="item.type == \'message\' && item.request_to_release_incentive">\n                                           <h2 (click)="sendReferral (buddy, item.selectCatId)" class="headinginchat"><span>Redeem Incentives</span></h2>\n                                           <p class="typeofreferral">Redeem Incentives For Referral Request</p>\n                                           <div><ion-badge (click)="fnviewincentiverequst(item)"  color="dark" class="savecnt" text-wrap >View Redeem Request</ion-badge></div>\n                                      </div>\n\n                                    <div class="msg" *ngIf="item.type == \'message\' && item.referral_type && item.referral_type == \'backward\'">\n                                        <h2   class="headinginchat"><span>Referral Sent</span></h2>\n                                        <p class="typeofreferral">Hi, I am sending a referral to you</p>\n                                        <div *ngIf="item.referral.mobile">{{item.referral.displayName}} ({{item.referral.mobile}})</div>\n                                        <div *ngIf="!item.referralSavedOnPhone"><ion-badge (click)="saveContactInPhone(item)"  color="primary" class="savecnt" text-wrap >Save Contact</ion-badge></div>\n                                        <div *ngIf="item.referralSavedOnPhone"><ion-badge   color="secondary" class="savecnt" text-wrap >Contact Saved</ion-badge></div>\n                                    </div>\n\n                                    <div class="msg" *ngIf="item.type == \'message\' && item.referral_type && item.referral_type == \'forward\'">\n                                        <h2   class="headinginchat"><span> Referral Forwarded</span></h2>\n                                        <p class="typeofreferral">Hi, I forwarded your referral to</p>\n                                        <div *ngIf="item.referral.mobile">{{item.referral.displayName}} ({{item.referral.mobile}})</div>\n                                        <div *ngIf="!item.referralSavedOnPhone"><ion-badge (click)="saveContactInPhone(item)"  color="primary" class="savecnt" text-wrap >Save Contact</ion-badge></div>\n                                        <div *ngIf="item.referralSavedOnPhone"><ion-badge   color="secondary" class="savecnt" text-wrap >Contact Saved</ion-badge></div>\n                                    </div>\n\n                                    <div class="msg" *ngIf="item.type == \'message\' && item.selectCatId && item.selectCatId>0">\n                                        <h2 (click)="sendReferral (buddy, item.selectCatId)" class="headinginchat"><span>Referral Request</span> <ion-icon class="referralicon"    item-end name=\'arrow-dropright-circle\'></ion-icon></h2>\n                                        <p class="typeofreferral">Hi, Can you send me a reliable referral for <ion-badge  class="catname" text-wrap >{{item.selectCatName}}</ion-badge></p>\n                                        <div *ngIf="item.message && !item.request_to_release_incentive && !item.referral_redeemed">"{{item.message}}"</div>\n                                    </div>\n\n\n                                    <img class="msg" *ngIf="item.type == \'document\'" src="assets/imgs/docs.png" (click)="openDoc(item.message)" />\n                                    <audio controls class="msg" *ngIf="item.type == \'audio\'">\n                                        <source src="{{item.message}}" type="audio/mpeg">\n                                    </audio>\n                                    <img class="msg" *ngIf="item.type == \'location\'" src="assets/imgs/location.png"\n                                        (click)="showLocation(item.message)" />\n                                    <!-- <div class="msg" *ngIf="item.type == \'audio\'" (click)="playAudio(item.message)">Audio</div> -->\n                                    <ng-container class="msg" *ngIf="item.multiMessage && item.type==\'image\'">\n                                        <div (click)="openAllImg(item.message,$event)" class="selection-data">\n                                          <!--  <img class="msg-partision" *ngFor="let pic of item.message | slice:0:4; let ii = index "\n                                                src="{{pic}}" /> -->\n												\n												<img-loader class="msg-partision" src="{{pic}}" *ngFor="let pic of item.message | slice:0:4; let ii = index "  useImg></img-loader>\n												\n												\n                                            <span class="img-number overlay">+ {{item.message.length - 4}}</span>\n                                        </div>\n                                    </ng-container>\n\n                                    <ng-container class="msg" *ngIf="!item.multiMessage && item.type == \'image\'">\n                                      <!--  <img src="{{item.message}}" *ngIf="item.type == \'image\'" class="msg" #mychatImage\n                                            (click)="presentImage(mychatImage)" /> -->\n									<img-loader *ngIf="item.type == \'image\'" class="msg" src="{{item.message}}" #mychatImage\n                                            (click)="presentImage(mychatImage)"></img-loader>		\n											\n											\n                                    </ng-container>\n\n                                    <ng-container class="msg" *ngIf="item.multiMessage && item.type == \'contact\'" >\n                                        <img class="msg contact" src="assets/imgs/user.png" />\n                                        <div *ngIf="item.message.length == 1"(click)="openContacts(item.message)" class="view-msg">View {{ item.message.length }} Contact</div>\n                                        <div *ngIf="item.message.length > 1" (click)="openContacts(item.message)" class="view-msg">View {{ item.message.length }} Contacts</div>\n                                    </ng-container>\n\n                                    <div class="read-recipients">\n                                            <ion-icon *ngIf="item.isStarred" name="star" class="icon"></ion-icon>\n                                            <p class="icon">{{item.timeofmsg}}</p>\n                                    </div>\n\n                                </div>\n                            </div>\n                            <div class="msg-wrap me" *ngIf="item.sentby != buddy.uid ">\n                                <ion-avatar>\n                                 <!--   <img src="{{photoURL}}"> -->\n									 <img-loader src="{{photoURL}}" useImg></img-loader>\n                                </ion-avatar>\n                                <div [ngClass]="item.type == \'image\' || item.type == \'location\' || item.type == \'document\' ?\'bubble-img\':\'bubble\'">\n\n                                  <div class="msg" *ngIf="item.type == \'message\' && item.referral_redeemed">\n                                        <h2  class="headinginchat"><span>Incentives Redeemed</span></h2>\n                                       <p class="typeofreferral">You have approved request to redeem incentives</p>\n                                   </div>\n\n\n                                  <div class="msg" *ngIf="item.type == \'message\' && item.request_to_release_incentive">\n                                       <h2  class="headinginchat"><span>Redeem Incentives</span></h2>\n                                       <p class="typeofreferral">Your request to redeem incentives for referral has been sent</p>\n                                 </div>\n\n                                    <div class="msg" *ngIf="item.type == \'message\' && (!item.selectCatId || item.selectCatId<=0) && !item.referral_type && !item.referral_redeemed && !item.request_to_release_incentive">{{item.message}}</div>\n\n\n\n                                    <div class="msg" *ngIf="item.type == \'message\' && item.referral_type && item.referral_type == \'backward\'">\n                                        <h2   class="headinginchat"><span>Referral Sent</span> </h2>\n                                        <p class="typeofreferral">Hi, I am sending a referral to you</p>\n                                        <div *ngIf="item.referral.mobile">{{item.referral.displayName}} ({{item.referral.mobile}})</div>\n                                        <div *ngIf="!item.referralSavedOnPhone && false"><ion-badge (click)="fnGetReferralDetail(item)"  color="primary" class="savecnt" text-wrap >Referral Detail</ion-badge></div>\n                                        <div *ngIf="item.referralSavedOnPhone"><ion-badge   color="secondary" class="savecnt" text-wrap >Contact Saved</ion-badge></div>\n                                    </div>\n\n                                    <div class="msg" *ngIf="item.type == \'message\' && item.referral_type && item.referral_type == \'forward\'">\n                                        <h2  class="headinginchat"><span>Referral Forwarded</span> </h2>\n                                        <p class="typeofreferral">Hi, I forwarded your referral to</p>\n                                        <div *ngIf="item.referral.mobile">{{item.referral.displayName}} ({{item.referral.mobile}})</div>\n                                        <div *ngIf="!item.referralSavedOnPhone && false"><ion-badge (click)="fnGetReferralDetail(item)"  color="primary" class="savecnt" text-wrap >Referral Detail</ion-badge></div>\n                                        <div *ngIf="item.referralSavedOnPhone"><ion-badge   color="secondary" class="savecnt" text-wrap >Contact Saved</ion-badge></div>\n                                    </div>\n\n                                    <div class="msg" *ngIf="item.type == \'message\' && item.selectCatId && item.selectCatId>0">\n                                        <h2 (click)="sendReferral (buddy, item.selectCatId)" class="headinginchat"><span>Referral Request</span> <ion-icon class="referralicon"    item-end name=\'arrow-dropright-circle\'></ion-icon></h2>\n                                        <p class="typeofreferral">Hi, Can you send me a reliable referral for <ion-badge  class="catname" text-wrap >{{item.selectCatName}}</ion-badge></p>\n                                        <div *ngIf="item.message && !item.referral_redeemed">"{{item.message}}"</div>\n                                    </div>\n\n\n                                    <img class="msg" *ngIf="item.type == \'document\'" src="assets/imgs/docs.png" (click)="openDoc(item.message)" />\n                                    <img class="msg" *ngIf="item.type == \'location\'" src="assets/imgs/location.png"\n                                        (click)="showLocation(item.message)" />\n\n                                    <audio controls class="msg" *ngIf="item.type == \'audio\'">\n                                        <source src="{{item.message}}" type="audio/mpeg">\n                                    </audio>\n                                    <!-- <div class="msg" *ngIf="item.type == \'audio\'" (click)="playAudio(item.message)">Audio</div> -->\n                                    <ng-container class="msg" *ngIf="item.multiMessage && item.type==\'image\'" >\n                                        <div (click)="openAllImg(item.message)" class="selection-data">\n                                         <!--   <img class="msg-partision" *ngFor="let pic of item.message | slice:0:4; let ii = index "\n                                                src="{{pic}}" /> -->\n							 <img-loader class="msg-partision" src="{{pic}}" *ngFor="let pic of item.message | slice:0:4; let ii = index "  useImg></img-loader>\n												\n                                            <span class="img-number overlay">+ {{item.message.length - 4}}</span>\n                                        </div>\n                                    </ng-container>\n                                    <ng-container class="msg" *ngIf="!item.multiMessage ">\n                                     <!--     <img src="{{item.message}}" class="msg" *ngIf="item.type == \'image\'" #mychatImage\n                                          (click)="presentImage(mychatImage)" /> --> \n										  \n										  	<img-loader *ngIf="item.type == \'image\'" class="msg" src="{{item.message}}" #mychatImage\n                                            (click)="presentImage(mychatImage)" useImg></img-loader>\n										  \n                                    </ng-container>\n                                    <ng-container class="msg" *ngIf="item.multiMessage && item.type == \'contact\'" >\n                                        <img class="msg contact" src="assets/imgs/user.png" />\n                                        <div *ngIf="item.message.length == 1"(click)="openContacts(item.message)" class="view-msg">View {{ item.message.length }} Contact</div>\n                                        <div *ngIf="item.message.length > 1" (click)="openContacts(item.message)" class="view-msg">View {{ item.message.length }} Contacts</div>\n                                    </ng-container>\n\n\n\n                                        <div class="read-recipients">\n                                            <ion-icon *ngIf="item.isStarred" name="star" class="icon"></ion-icon>\n                                            <p class="icon">{{item.timeofmsg}}</p>\n                                            <ion-icon *ngIf="!item.isRead" name="checkmark" class="icon"></ion-icon>\n                                            <!-- <ion-icon *ngIf="!item.isRead" name="checkmark"></ion-icon> -->\n                                            <ion-icon *ngIf="item.isRead" color="secondary" class="checkmark-icon" name="checkmark"></ion-icon>\n                                            <ion-icon *ngIf="item.isRead" color="secondary" class="checkmark-icon icon-align icon-align-ios" name="checkmark"></ion-icon>\n                                        </div>\n\n\n                                    </div>\n\n                            </div>\n                        </div>\n                    </ion-item>\n                  </div>\n            </ng-container>\n        </ion-list>\n    </div>\n</ion-content>\n\n<ion-footer ion-fixed>\n  <ion-list style="display:none;">\n    <ion-item>\n      <button ion-button clear item-start> <ion-icon class="referralicon"  name="arrow-dropleft-circle"></ion-icon> </button>\n      <button  ion-button clear  item-end><ion-icon class="referralicon"  name="arrow-dropright-circle"></ion-icon></button>\n    </ion-item>\n  </ion-list>\n    <form [formGroup]="authForm">\n        <ion-toolbar class="no-border" color="white">\n            <ion-input tappable [(ngModel)]="newmessage" [formControl]="message" (click)="scrollto()" (input)="onChange($event.keyCode)" name="message" class="newmsg" placeholder="Write your message ..."></ion-input>\n            <ion-buttons end *ngIf="selectAllMessage.length == 0">\n                <a ion-button (click)="openAttachment($event)">\n                    <ion-icon class="ion-image" ios="md-attach" md="md-attach"></ion-icon>\n                </a>\n                <button ion-button (click)="referralSendBackward($event)" clear item-start> <ion-icon class="referralicon"  name="arrow-dropleft-circle"></ion-icon> </button>\n                <button  ion-button (click)="referralSendForward($event)" clear  item-end><ion-icon class="referralicon"  name="arrow-dropright-circle"></ion-icon></button>\n            </ion-buttons>\n            <ion-buttons end>\n                <button ion-button round type="submit" class="sentbtn" [disabled]="!this.msgstatus || !selectAllMessage.length == 0"\n                    (click)="addmessage_async()">\n                    <ion-icon ios="md-send" md="md-send" color="wcolor" style="font-size: 2.2em;"></ion-icon>\n                </button>\n            </ion-buttons>\n        </ion-toolbar>\n    </form>\n</ion-footer>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\buddychat\buddychat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_6__providers_fcm_fcm__["a" /* FcmProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_chat_chat__["a" /* ChatProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_12__providers_cats_cats__["a" /* CatProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_13__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_imagehandler_imagehandler__["a" /* ImagehandlerProvider */],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */],
            __WEBPACK_IMPORTED_MODULE_7__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["y" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_8__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_9__providers_contact_contact__["a" /* ContactProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_10_ionic_img_viewer__["a" /* ImageViewerController */],
            __WEBPACK_IMPORTED_MODULE_11__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], BuddychatPage);
    return BuddychatPage;
}());

//# sourceMappingURL=buddychat.js.map

/***/ })

});
//# sourceMappingURL=64.js.map