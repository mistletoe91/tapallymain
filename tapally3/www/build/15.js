webpackJsonp([15],{

/***/ 823:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestSubmittedPageModule", function() { return RequestSubmittedPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__request_submitted__ = __webpack_require__(897);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RequestSubmittedPageModule = (function () {
    function RequestSubmittedPageModule() {
    }
    RequestSubmittedPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__request_submitted__["a" /* RequestSubmittedPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__request_submitted__["a" /* RequestSubmittedPage */]),
            ],
        })
    ], RequestSubmittedPageModule);
    return RequestSubmittedPageModule;
}());

//# sourceMappingURL=request-submitted.module.js.map

/***/ }),

/***/ 897:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestSubmittedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RequestSubmittedPage = (function () {
    function RequestSubmittedPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.showheader = true;
    }
    RequestSubmittedPage.prototype.ionViewDidLoad = function () {
    };
    RequestSubmittedPage.prototype.gotoTabsPage = function () {
        this.navCtrl.setRoot("TabsPage");
    };
    RequestSubmittedPage.prototype.gotoChatsPage = function () {
        this.navCtrl.setRoot("ReferralPage");
    };
    RequestSubmittedPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    RequestSubmittedPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-request-submitted',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\request-submitted\request-submitted.html"*/'<ion-header  >\n  <ion-navbar color="primary" hideBackButton >\n    <ion-title>\n      Request Submitted\n    </ion-title>\n    <ion-buttons left>\n        <button ion-button (click)="gotoTabsPage()">\n             <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n\n  <div class="pull-center givepadding">\n      <h3>Your Request Has Been Submitted</h3>\n      <p> It may take few minutes to hours before your contacts in network refer you.\n      </p>\n      <button ion-button  (click)="gotoTabsPage()" color="primary" block>Done</button>\n      <button ion-button  (click)="gotoChatsPage()" color="primary" clear>Submit Another Request</button>\n<br />\n<!--\n        <p>Need someone right now ? </p>\n        <button ion-button color="primary" block>Browse nearby busineeses</button>\n      -->\n  </div>\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\request-submitted\request-submitted.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */]])
    ], RequestSubmittedPage);
    return RequestSubmittedPage;
}());

//# sourceMappingURL=request-submitted.js.map

/***/ })

});
//# sourceMappingURL=15.js.map