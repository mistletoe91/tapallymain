webpackJsonp([21],{

/***/ 817:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferralInfopagePageModule", function() { return ReferralInfopagePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__referral_infopage__ = __webpack_require__(891);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ReferralInfopagePageModule = (function () {
    function ReferralInfopagePageModule() {
    }
    ReferralInfopagePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__referral_infopage__["a" /* ReferralInfopagePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__referral_infopage__["a" /* ReferralInfopagePage */]),
            ],
        })
    ], ReferralInfopagePageModule);
    return ReferralInfopagePageModule;
}());

//# sourceMappingURL=referral-infopage.module.js.map

/***/ }),

/***/ 891:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReferralInfopagePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ReferralInfopagePage = (function () {
    function ReferralInfopagePage(app, navCtrl, navParams, tpStorageService) {
        this.app = app;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tpStorageService = tpStorageService;
        this.iamABusinessOwner = false;
        this.showheader = true;
    }
    ReferralInfopagePage.prototype.ionViewWillLeave = function () {
    };
    ReferralInfopagePage.prototype.tpInitilizeFromStorage = function () {
        var _this = this;
        this.tpStorageService.getItem('business_created').then(function (res) {
            if (res) {
                _this.iamABusinessOwner = true;
            }
        }).catch(function (e) { });
    };
    ReferralInfopagePage.prototype.sendToEarningsPage = function () {
        this.app.getRootNav().push("EarningsPage");
    };
    ReferralInfopagePage.prototype.presentPopover = function (myEvent) {
        this.navCtrl.push("NotificationPage");
        //this.requestcounter = this.myrequests.length;
    };
    ReferralInfopagePage.prototype.sendToBroadcastPage = function () {
        this.app.getRootNav().push("BroadcastPage", { broadcast_retry: false });
    };
    ReferralInfopagePage.prototype.ionViewWillEnter = function () {
        this.showheader = true;
        this.tpInitilizeFromStorage();
    };
    ReferralInfopagePage.prototype.sendreferraldirectly = function () {
        this.navCtrl.push("ReferralPage");
    };
    ReferralInfopagePage.prototype.sendToRegisterBuesiness = function () {
        this.navCtrl.push("RegisterbusinessPage", { treatNewInstall: 0 });
    };
    ReferralInfopagePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReferralInfopagePage');
    };
    ReferralInfopagePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-referral-infopage',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\referral-infopage\referral-infopage.html"*/'<ion-header >\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Exchange Referral</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="sendToBroadcastPage()">\n        <ion-icon name="megaphone"   class="megaphone"></ion-icon>\n      </button>\n      <button ion-button icon-only (click)="sendToEarningsPage()">\n        <ion-icon  name="checkmark-circle" class="notification"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-card  class="maincard_bubble" text-center padding *ngIf="!ever_referral_sent">\n    <ion-card-content >\n      <ion-card-title>\n        Exchange Referrals\n        </ion-card-title>\n      <p>\n        Send or receive referrals with your friends and earn loyalty from your favorite businesses\n      </p>\n      <button style="margin-top:15px;"  (click)="sendreferraldirectly()" ion-button color="primary" block>Get Started Now</button>\n      <p *ngIf="!iamABusinessOwner" style="margin-top:15px;border-bottom:1px solid #bbb;"   (click)="sendToRegisterBuesiness()" color="primary"  >Business Owner? Grow Sales & Revenue</p>\n    </ion-card-content>\n </ion-card>\n\n\n\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\referral-infopage\referral-infopage.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], ReferralInfopagePage);
    return ReferralInfopagePage;
}());

//# sourceMappingURL=referral-infopage.js.map

/***/ })

});
//# sourceMappingURL=21.js.map