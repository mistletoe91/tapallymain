webpackJsonp([16],{

/***/ 822:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestStatusPageModule", function() { return RequestStatusPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__request_status__ = __webpack_require__(896);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RequestStatusPageModule = (function () {
    function RequestStatusPageModule() {
    }
    RequestStatusPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__request_status__["a" /* RequestStatusPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__request_status__["a" /* RequestStatusPage */]),
            ],
        })
    ], RequestStatusPageModule);
    return RequestStatusPageModule;
}());

//# sourceMappingURL=request-status.module.js.map

/***/ }),

/***/ 896:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestStatusPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RequestStatusPage = (function () {
    function RequestStatusPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.showheader = true;
    }
    RequestStatusPage.prototype.gotoPage = function () {
        //testing
        this.navCtrl.setRoot("ChooseClubPage");
    };
    RequestStatusPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    RequestStatusPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad RequestStatusPage');
    };
    RequestStatusPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-request-status',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\request-status\request-status.html"*/'<ion-header  >\n  <ion-navbar color="primary" >\n    <ion-title>\n      Your Request Status\n    </ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name=\'menu\'></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content >\n  <ion-item padding class="highcard profilecard pull-center" no-lines>\n       <h1>You Requested <br /><span class="text_color_primary strongwords">Website Designer</span></h1>\n       <h3>on May 2, 2018 at 02:30 pm</h3>\n   </ion-item>\n\n   <ion-list>\n     <ion-list-header color="light" no-lines>\n       In Your Contact List\n     </ion-list-header>\n\n        <ion-item>\n          <h2><span class="text_color_primary strongwords">Mary Standingonwall</span> <span style="color:#32db64;font-size:1.4rem;">  <ion-icon name="pin"></ion-icon> 2.5 km  </span></h2>\n          <h3>(231) 231-3231</h3>\n          <p></p>\n          <ion-icon class="hideme" item-end name="megaphone"></ion-icon>\n          <ion-icon class="hideme" item-end name="arrow-dropup-circle"></ion-icon>\n        </ion-item>\n\n     <ion-list-header color="light" no-lines>\n       Referrals Recieved\n     </ion-list-header>\n     <ion-item>\n       <h2><span class="text_color_primary strongwords">Garry Singh</span> <span    style="text-align:right;width:100%;color:#32db64;font-size:1.4rem;">  <ion-icon name="pin"></ion-icon> 5 km</span></h2>\n       <h3>(647) 123-1423 </h3>\n       <p>Referred By Arando Qualiejo Djsuodis  <ion-icon color="strong" name="contact"></ion-icon>  <span class="hideme" style="color:#32db64"><ion-icon name="star-half"></ion-icon> 4.1 (24)</span></p>\n        <ion-icon class="hideme" item-end name="megaphone"></ion-icon>\n        <ion-icon class="hideme" item-end name="arrow-dropup-circle"></ion-icon>\n     </ion-item>\n     <ion-item>\n       <h2><span class="text_color_primary strongwords">Manny Kumar</span> <span style="color:#32db64;font-size:1.4rem;">  <ion-icon name="pin"></ion-icon> 5 km</span></h2>\n        <h3>(132) 532-4233 </h3>\n       <p>Referred By Buenos Airessd  <ion-icon color="strong" name="contact"></ion-icon>  <span class="hideme" style="color:#32db64"><ion-icon name="star-half"></ion-icon> 4.1 (24)</span></p>\n       <ion-icon class="hideme" item-end name="megaphone"></ion-icon>\n       <ion-icon class="hideme" item-end name="arrow-dropup-circle"></ion-icon>\n     </ion-item>\n     <ion-list-header color="light" no-lines>\n       Nearby Plumbers\n     </ion-list-header>\n     <ion-item style="border:1px solid #aaa">\n       <h2><span class="text_color_primary strongwords">Baljinder Singh</span> <span style="color:#32db64;font-size:1.4rem;">  <ion-icon name="pin"></ion-icon> 34 km </span></h2>\n       <h3>(321) 321-4232 </h3>\n       <ion-icon class="hideme" item-end name="megaphone"></ion-icon>\n       <ion-icon class="hideme" item-end name="arrow-dropup-circle"></ion-icon>\n     </ion-item>\n     <ion-item>\n       <h2><span class="text_color_primary strongwords">Patander Sandhu</span> <span style="color:#32db64;font-size:1.4rem;">  <ion-icon name="pin"></ion-icon> 12 km </span></h2>\n       <h3>(312) 632-3123 </h3>\n       <ion-icon class="hideme" item-end name="megaphone"></ion-icon>\n       <ion-icon class="hideme" item-end name="arrow-dropup-circle"></ion-icon>\n     </ion-item>\n\n     <ion-item>\n       <h2><span class="text_color_primary strongwords">John Andreson</span> <span style="color:#32db64;font-size:1.4rem;">  <ion-icon name="pin"></ion-icon> 3 km </span></h2>\n       <h3>(416) 534-1423 </h3>\n       <ion-icon class="hideme" item-end name="megaphone"></ion-icon>\n       <ion-icon class="hideme" item-end name="arrow-dropup-circle"></ion-icon>\n     </ion-item>\n\n\n\n </ion-list>\n\n</ion-content>\n\n<ion-footer >\n  <ion-toolbar>\n\n    <div  >\n      <button ion-button (click)="gotoPage()" color="primary" block>Back</button>\n    </div>\n\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\request-status\request-status.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */]])
    ], RequestStatusPage);
    return RequestStatusPage;
}());

//# sourceMappingURL=request-status.js.map

/***/ })

});
//# sourceMappingURL=16.js.map