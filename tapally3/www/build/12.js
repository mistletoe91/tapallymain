webpackJsonp([12],{

/***/ 826:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchGroupChatPageModule", function() { return SearchGroupChatPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search_group_chat__ = __webpack_require__(900);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SearchGroupChatPageModule = (function () {
    function SearchGroupChatPageModule() {
    }
    SearchGroupChatPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__search_group_chat__["a" /* SearchGroupChatPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__search_group_chat__["a" /* SearchGroupChatPage */]),
            ],
        })
    ], SearchGroupChatPageModule);
    return SearchGroupChatPageModule;
}());

//# sourceMappingURL=search-group-chat.module.js.map

/***/ }),

/***/ 900:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchGroupChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_groups_groups__ = __webpack_require__(404);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SearchGroupChatPage = (function () {
    function SearchGroupChatPage(navCtrl, navParams, viewCtrl, groupService, events, zone, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.groupService = groupService;
        this.events = events;
        this.zone = zone;
        this.platform = platform;
        this.allgroupMessage = [];
        this.tempChat = [];
        this.showheader = true;
        this.alignuid = this.groupService.userId;
    }
    SearchGroupChatPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    SearchGroupChatPage.prototype.ionViewCanEnter = function () {
        var _this = this;
        if (this.groupService.currentgroupname != undefined) {
            this.groupName = this.groupService.currentgroupname;
        }
        else {
            this.groupName = this.navParams.get('groupName');
            this.groupService.currentgroupname != this.groupName;
        }
        this.events.subscribe('newgroupmsg', function () {
            _this.events.unsubscribe('newgroupmsg');
            _this.allgroupmsgs = [];
            _this.imgornot = [];
            _this.zone.run(function () {
                _this.allgroupmsgs = _this.groupService.groupmsgs;
            });
            for (var key in _this.allgroupmsgs) {
                var d = new Date(_this.allgroupmsgs[key].timestamp);
                var hours = d.getHours();
                var minutes = "0" + d.getMinutes();
                var month = d.getMonth();
                1;
                var da = d.getDate();
                var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                var formattedTime = monthNames[month] + "-" + da + "-" + hours + ":" + minutes.substr(-2);
                _this.allgroupmsgs[key].timestamp = formattedTime;
                if (_this.allgroupmsgs[key].multiMessage == true) {
                    _this.imgornot.push(true);
                }
                else {
                    if (_this.allgroupmsgs[key].message.substring(0, 4) === 'http') {
                        _this.imgornot.push(true);
                    }
                    else {
                        _this.imgornot.push(false);
                    }
                }
            }
            _this.allgroupMessage = [];
            _this.allgroupMessage = _this.allgroupmsgs;
            _this.tempChat = _this.allgroupMessage;
            if (_this.allgroupMessage.length > 5) {
                _this.scrollto();
            }
        });
        this.groupService.getgroupmsgs(this.groupName);
    };
    SearchGroupChatPage.prototype.initializeContacts = function () {
        var _this = this;
        this.zone.run(function () {
            _this.allgroupMessage = _this.tempChat;
        });
    };
    SearchGroupChatPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    SearchGroupChatPage.prototype.search = function (ev) {
        this.initializeContacts();
        var myArray = [];
        var isNoRecord = false;
        this.isNoRecord = false;
        var val = ev.target.value;
        if (val && val.trim() != '') {
            for (var i = 0; i < this.allgroupMessage.length; ++i) {
                if (this.allgroupMessage[i].multiMessage) {
                }
                else if (this.allgroupMessage[i].type == 'image') {
                }
                else if (this.allgroupMessage[i].type == 'document') {
                }
                else {
                    if (this.allgroupMessage[i].message.toLowerCase().indexOf(val.toLowerCase()) > -1) {
                        isNoRecord = true;
                        myArray.push(this.allgroupMessage[i]);
                    }
                }
            }
            if (isNoRecord != true) {
                this.isNoRecord = true;
            }
            this.allgroupMessage = myArray;
        }
    };
    SearchGroupChatPage.prototype.scrollto = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.content._scroll) {
                _this.content.scrollToBottom();
            }
        }, 3000);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('content'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Content */])
    ], SearchGroupChatPage.prototype, "content", void 0);
    SearchGroupChatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-search-group-chat',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\search-group-chat\search-group-chat.html"*/'<ion-header  >\n\n  <ion-navbar >\n    	<div class="search-wrap">\n			<ion-buttons class="close-left" left>\n				<button ion-button icon-only (click)="dismiss()"><ion-icon ios="md-close" md="md-close" ></ion-icon></button>\n			</ion-buttons>\n			<ion-searchbar class="searchbar" placeholder="Search" (ionInput)="search($event)"></ion-searchbar>\n		</div>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content #content>\n	<div class="chatwindow">\n		<div *ngIf="isNoRecord" padding text-center>No record found!</div>\n		<ion-list no-lines *ngIf="allgroupMessage.length > 0">\n				<ion-item text-wrap *ngFor="let item of allgroupMessage; let i = index">\n					<div class="msg-wrap me" *ngIf="item.sentby == alignuid">\n						<ion-avatar>\n							<img src="{{item.photoURL}}">\n						</ion-avatar>\n						<div [ngClass]="item.type == \'image\' || item.type == \'location\' || item.type == \'document\'?\'bubble-img\':\'bubble\'">\n							<div class="displayname">{{item.displayName}}</div>\n							<div class="msg" *ngIf="!imgornot[i] && item.type == \'message\'">{{item.message}}</div>\n							<img class="msg" *ngIf="item.type == \'document\'" src="assets/imgs/docs.png" />\n\n							<audio controls class="msg" *ngIf="item.type == \'audio\'">\n								<source src="{{item.message}}" type="audio/mpeg">\n							</audio>\n\n							<ng-container class="msg" *ngIf="item.multiMessage && item.type == \'image\'">\n								<img class="msg-partision" *ngFor="let pic of item.message | slice:0:4; let ii = index " src="{{pic}}" />\n                    		    <span class="img-number overlay">+{{item.message.length - 4}}</span>\n							</ng-container>\n\n							<ng-container class="msg" *ngIf="item.multiMessage && item.type == \'contact\'">\n                                <div>\n                                	<img class="msg contact" src="assets/imgs/user.png" />\n                                	<div class="view-msg">View {{ item.message.length }} Contact</div>\n                                </div>\n							</ng-container>\n\n							<ng-container *ngIf="!item.multiMessage && item.type == \'image\'">\n                                <img class="msg" *ngIf="item.type == \'image\'" #mychatImage src="{{item.message}}"/>\n                            </ng-container>\n\n                            <ng-container class="msg" *ngIf="item.multiMessage && item.type == \'location\'">\n                                <img class="msg" src="assets/imgs/location.png" />\n							</ng-container>\n\n							<div class="msg-time">\n                                <p>{{item.timeofmsg}}</p>\n                            </div>\n						</div>\n					</div>\n\n					<div class="msg-wrap you" *ngIf="item.sentby != alignuid">\n						<ion-avatar>\n							<img src="{{item.photoURL}}">\n						</ion-avatar>\n\n						<div [ngClass]="item.type == \'image\' || item.type == \'location\' || item.type == \'document\' ?\'bubble-img\':\'bubble\'">\n							<div class="displayname">{{item.displayName}}</div>\n\n                            <div class="msg" *ngIf="!imgornot[i] &&  item.type == \'message\'">{{item.message}}</div>\n\n                            <img class="msg" *ngIf="item.type == \'document\'" src="assets/imgs/docs.png" />\n\n							<audio controls class="msg" *ngIf="item.type == \'audio\'">\n                                <source src="{{item.message}}" type="audio/mpeg">\n							</audio>\n\n							<ng-container class="msg" *ngIf="item.multiMessage && item.type == \'image\'">\n                                <div>\n                                    <img class="msg-partision" *ngFor="let pic of item.message | slice:0:4; let ii = index " src="{{pic}}" />\n                                    <span class="img-number overlay">+ {{item.message.length - 4}}</span>\n                                </div>\n							</ng-container>\n\n							<ng-container class="msg" *ngIf="item.multiMessage && item.type == \'contact\'" click)="openContacts(item.message)">\n                                <div>\n                                	<img class="msg contact" src="assets/imgs/user.png" />\n                               		<div class="view-msg">View {{ item.message.length }} Contact</div>\n                                </div>\n							</ng-container>\n\n							<ng-container *ngIf="!item.multiMessage && item.type == \'image\'">\n                                <img class="msg" *ngIf="item.type == \'image\'" #mychatImage src="{{item.message}}" />\n							</ng-container>\n\n                            <ng-container class="msg" *ngIf="item.multiMessage && item.type == \'location\'">\n                                <img class="msg" src="assets/imgs/location.png" />\n							</ng-container>\n\n							<div class="msg-time">\n                                <p>{{item.timeofmsg}}</p>\n                            </div>\n						</div>\n					</div>\n				</ion-item>\n		</ion-list>\n	</div>\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\search-group-chat\search-group-chat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_groups_groups__["a" /* GroupsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */]])
    ], SearchGroupChatPage);
    return SearchGroupChatPage;
}());

//# sourceMappingURL=search-group-chat.js.map

/***/ })

});
//# sourceMappingURL=12.js.map