webpackJsonp([7],{

/***/ 831:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendReferralForwardPageModule", function() { return SendReferralForwardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__send_referral_forward__ = __webpack_require__(905);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SendReferralForwardPageModule = (function () {
    function SendReferralForwardPageModule() {
    }
    SendReferralForwardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__send_referral_forward__["a" /* SendReferralForwardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__send_referral_forward__["a" /* SendReferralForwardPage */]),
            ],
        })
    ], SendReferralForwardPageModule);
    return SendReferralForwardPageModule;
}());

//# sourceMappingURL=send-referral-forward.module.js.map

/***/ }),

/***/ 905:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SendReferralForwardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SendReferralForwardPage = (function () {
    function SendReferralForwardPage(navCtrl, navParams, tpStorageService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tpStorageService = tpStorageService;
        this.showheader = true;
        this.thisIsMe = false;
        this.tpStorageService.get('userUID').then(function (myUID) {
            _this.buddy_displayName = navParams.get('buddy_displayName');
            _this.buddy_uid = navParams.get('buddy_uid');
            _this.sourcePage = navParams.get('source');
            if (myUID == _this.buddy_uid) {
                _this.thisIsMe = true;
            } //endif
        }).catch(function (e) {
        });
    }
    SendReferralForwardPage.prototype.ionViewWillLeave = function () {
    };
    SendReferralForwardPage.prototype.forwardContact = function () {
        this.navCtrl.push("PickContactToPage", { source: this.sourcePage, friend_to_forward_uid: this.buddy_uid, friend_to_forward_displayName: this.buddy_displayName, friend_to_forward_catId: "", friend_to_forward_catName: "" });
    };
    SendReferralForwardPage.prototype.backButtonClick = function () {
        console.log("BACK BUTTON CLICK");
        //this.navCtrl.setRoot('TabsPage');
        //This does not work : dont know why
        this.navCtrl.pop({ animate: true, direction: "forward" });
    };
    SendReferralForwardPage.prototype.ionViewDidLoad = function () {
    };
    SendReferralForwardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-send-referral-forward',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\send-referral-forward\send-referral-forward.html"*/'<ion-header >\n  <ion-navbar hideBackButton>\n\n    <ion-buttons left>\n        <button ion-button (click)="backButtonClick()">\n             <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n\n    <ion-title>Forward Contact</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n\n    <ion-card-content *ngIf="thisIsMe"  class="herocard" text-center>\n      <ion-card-title>\n        Forward {{buddy_displayName}}\'s contact\n        </ion-card-title>\n      <p>\n        People you invite will use this button to forward your referral directly to their friends and family\n      </p>\n      <button  disabled  ion-button color="primary" block>Forward Referral</button>\n    </ion-card-content>\n\n    <ion-card-content *ngIf="!thisIsMe"  class="herocard" text-center>\n      <ion-card-title>\n        Forward {{buddy_displayName}}\'s contact to someone you know\n        </ion-card-title>\n      <p>This will help {{buddy_displayName}} connect with your known contact\n      </p>\n      <button (click)="forwardContact()" ion-button color="primary" block>Forward Contact</button>\n    </ion-card-content>\n\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\send-referral-forward\send-referral-forward.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], SendReferralForwardPage);
    return SendReferralForwardPage;
}());

//# sourceMappingURL=send-referral-forward.js.map

/***/ })

});
//# sourceMappingURL=7.js.map