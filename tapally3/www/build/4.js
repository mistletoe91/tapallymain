webpackJsonp([4],{

/***/ 834:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageModule", function() { return TabsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs__ = __webpack_require__(908);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TabsPageModule = (function () {
    function TabsPageModule() {
    }
    TabsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__tabs__["a" /* TabsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__tabs__["a" /* TabsPage */]),
            ],
        })
    ], TabsPageModule);
    return TabsPageModule;
}());

//# sourceMappingURL=tabs.module.js.map

/***/ }),

/***/ 908:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_const__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsPage = (function () {
    //tab4Root: string = 'BroadcastPage';
    function TabsPage(navCtrl, navParams, sqlite, tpStorageService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sqlite = sqlite;
        this.tpStorageService = tpStorageService;
        //tab1Root: string = 'HomePage';
        this.tab2Root = 'ChatsPage';
        this.tab3Root = 'ReferralPage';
    }
    TabsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.tpStorageService.getItem('ever_referral_sent').then(function (res) {
            if (res) {
                _this.tab3Root = "ReferralPage";
            }
            else {
                _this.CheckFlagForRedirection();
                _this.tab3Root = "ReferralInfopagePage";
            }
        }).catch(function (e) {
            _this.CheckFlagForRedirection();
            _this.tab3Root = "ReferralInfopagePage";
        });
    };
    TabsPage.prototype.CheckFlagForRedirection = function () {
        var _this = this;
        this.sqlite.create({
            name: 'gr.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql(__WEBPACK_IMPORTED_MODULE_3__app_const__["a" /* CONST */].create_table_statement_key_val, {})
                .then(function (res) {
                db.executeSql("SELECT val FROM key_val where key like 'ever_referral_sent'", {})
                    .then(function (res) {
                    if (res.rows.length > 0) {
                        //Record exists
                        //console.log ("Referral Record exists : so setting tab to ReferralPage ");
                        _this.tab3Root = "ReferralPage";
                        _this.tpStorageService.setItem('ever_referral_sent', "1");
                    }
                    else {
                        //Record does not exist
                        //console.log ("Referral Record DOES NOT exists : so setting tab to ReferralInfopagePage ");
                        _this.tab3Root = "ReferralInfopagePage";
                    } //endif
                })
                    .catch(function (e) {
                });
            })
                .catch(function (e) {
            }); //create table
        }); //create database
    }; //end function
    TabsPage.prototype.ionViewDidLoad = function () {
    };
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tabs',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\tabs\tabs.html"*/'<ion-tabs  tabsPlacement="bottom" id="mysupertabs">\n  <!--<ion-tab [root]="tab1Root" tabTitle="Home" tabIcon="home"></ion-tab>-->\n  <ion-tab [root]="tab2Root" tabTitle="My Contacts" tabIcon="contacts"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="Get Referral" tabIcon="information-circle"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\tabs\tabs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */], __WEBPACK_IMPORTED_MODULE_4__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ })

});
//# sourceMappingURL=4.js.map