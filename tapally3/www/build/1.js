webpackJsonp([1],{

/***/ 838:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewandredeemPageModule", function() { return ViewandredeemPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__viewandredeem__ = __webpack_require__(913);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ViewandredeemPageModule = (function () {
    function ViewandredeemPageModule() {
    }
    ViewandredeemPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__viewandredeem__["a" /* ViewandredeemPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__viewandredeem__["a" /* ViewandredeemPage */]),
            ],
        })
    ], ViewandredeemPageModule);
    return ViewandredeemPageModule;
}());

//# sourceMappingURL=viewandredeem.module.js.map

/***/ }),

/***/ 913:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewandredeemPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_sqlite__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_chat_chat__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_referralincentive_type_referralincentive_type__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ViewandredeemPage = (function () {
    function ViewandredeemPage(toastCtrl, chatservice, sqlite, navCtrl, navParams, app, userservice, rtService, tpStorageService) {
        this.toastCtrl = toastCtrl;
        this.chatservice = chatservice;
        this.sqlite = sqlite;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.app = app;
        this.userservice = userservice;
        this.rtService = rtService;
        this.tpStorageService = tpStorageService;
        this.referralDetail = [];
        this.businessRegistered = false;
    }
    ViewandredeemPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.tpStorageService.getItem('business_created').then(function (business_created_raw) {
            //Business Registered
            _this.businessRegistered = true;
            _this.ionViewDidLoad_async();
        }).catch(function (e) {
            //Business not registered
            _this.pageDoneLoading = true;
        });
    };
    ViewandredeemPage.prototype.ionViewDidLoad_async = function () {
        var _this = this;
        this.referralDetail.push({ 'referral': { 'displayName': '' } });
        this.rtlistComplete = this.rtService.getList();
        if (this.navParams.get('requestId')) {
            this.requestId = this.navParams.get('requestId');
            this.referral_send_by = this.navParams.get('referral_send_by');
            this.referral_received_by = this.navParams.get('referral_received_by');
            this.userservice.getbuddydetails(this.referral_send_by).then(function (ref) {
                _this.oneWhoSendReferral = ref;
                _this.pageDoneLoading = true;
            });
            this.userservice.getReferralRecievedByID(this.referral_send_by, this.referral_received_by, this.requestId).then(function (referralRecieved) {
                var referalType = '';
                //console.log ("referralRecieved");
                //console.log (referralRecieved);
                for (var myReferType in _this.rtlistComplete) {
                    if (referralRecieved["business_referral_type_id"] == _this.rtlistComplete[myReferType].id) {
                        referralRecieved["business_referral_type_name"] = _this.rtlistComplete[myReferType].name;
                        break;
                    }
                } //end for
                _this.redeemStatus = referralRecieved["redeemStatus"];
                _this.referralDetail[0] = referralRecieved;
                _this.pageDoneLoading = true;
            });
        }
    };
    ViewandredeemPage.prototype.fnMarkIncentiveRedeemed = function () {
        var _this = this;
        this.userservice.fnMarkIncentiveRedeemed_(this.referral_send_by, this.referral_received_by, this.requestId);
        this.chatservice.addnewmessageRedeemDone('Incentive for referral are marked redeemed', 'message', this.referral_send_by, this.requestId, this.referral_send_by, this.referral_received_by).then(function () {
            //mytodo : send user to buddypage
            _this.navCtrl.setRoot('TabsPage');
            var toast = _this.toastCtrl.create({
                message: 'You have successfully redeemed request for incentives. You can see the status by clicking on your contact`s icon',
                duration: 5000
            });
            toast.present();
        });
    };
    ViewandredeemPage.prototype.fnSendToBusinessPage = function () {
        this.navCtrl.push("RegisterbusinessPage", { treatNewInstall: 0 });
    };
    ViewandredeemPage.prototype.backButtonClick = function () {
        //mytodo : bug : when user go back to buddypage chat does not work properly. Cannot recieve or send messages. Fix that
        //this.navCtrl.pop({animate: true, direction: "right"});
        //temporarliy send to tabs page
        this.navCtrl.setRoot('TabsPage');
    };
    ViewandredeemPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-viewandredeem',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\viewandredeem\viewandredeem.html"*/'<ion-header >\n  <ion-navbar hideBackButton>\n\n    <ion-buttons left>\n        <button ion-button (click)="backButtonClick()">\n             <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n    <ion-title *ngIf="redeemStatus ==2">Incentive Redeemed</ion-title>\n    <ion-title *ngIf="redeemStatus !=2">Redeem Incentive</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <ion-card  *ngIf="pageDoneLoading && !businessRegistered" class="maincard_bubble" text-center padding>\n    <ion-card-content >\n      <ion-card-title *ngIf="redeemStatus !=2">\n        Register Your Business\n        </ion-card-title>\n      <p>\n         In order to use this feature, you need to register your business. Businesses enjoy co-branding and other benefits. \n      </p>\n    </ion-card-content>\n </ion-card>\n\n  <ion-card  *ngIf="pageDoneLoading && businessRegistered" class="maincard_bubble" text-center padding>\n    <ion-card-content >\n      <ion-card-title *ngIf="redeemStatus !=2">\n        Redeem Incentives To {{oneWhoSendReferral.displayName}}\n        </ion-card-title>\n      <ion-card-title *ngIf="redeemStatus ==2">\n        You Redeemed Incentives To {{oneWhoSendReferral.displayName}}\n        </ion-card-title>\n      <p>\n         Your contact {{oneWhoSendReferral.displayName}} send you a referral for {{referralDetail[0].referral.displayName}}. At that point in time, you have updated in your business settings that incentives for referral would be <ion-badge>{{referralDetail[0].business_referral_type_name}}</ion-badge> : <ion-badge>{{referralDetail[0].business_referral_detail}}</ion-badge>\n      </p>\n    </ion-card-content>\n </ion-card>\n\n\n  <button  *ngIf="businessRegistered && pageDoneLoading && redeemStatus!=2" block  style="margin-top:15px;" ion-button (click)="fnMarkIncentiveRedeemed()" color="secondary"  >Mark Incentive Redeeemed</button>\n\n  <button  *ngIf="businessRegistered && pageDoneLoading && redeemStatus==2" block  style="margin-top:15px;" ion-button (click)="backButtonClick()" color="primary"  >Go Back</button>\n\n  <button  *ngIf="!businessRegistered && pageDoneLoading" block  style="margin-top:15px;" ion-button (click)="fnSendToBusinessPage()" color="dark"  >Let`s Get Started</button>\n\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\viewandredeem\viewandredeem.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */], __WEBPACK_IMPORTED_MODULE_4__providers_chat_chat__["a" /* ChatProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_sqlite__["a" /* SQLite */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */], __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */], __WEBPACK_IMPORTED_MODULE_5__providers_referralincentive_type_referralincentive_type__["a" /* ReferralincentiveTypeProvider */], __WEBPACK_IMPORTED_MODULE_6__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], ViewandredeemPage);
    return ViewandredeemPage;
}());

//# sourceMappingURL=viewandredeem.js.map

/***/ })

});
//# sourceMappingURL=1.js.map