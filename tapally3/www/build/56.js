webpackJsonp([56],{

/***/ 782:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CountryListPageModule", function() { return CountryListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__country_list__ = __webpack_require__(856);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CountryListPageModule = (function () {
    function CountryListPageModule() {
    }
    CountryListPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__country_list__["a" /* CountryListPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__country_list__["a" /* CountryListPage */]),
            ],
        })
    ], CountryListPageModule);
    return CountryListPageModule;
}());

//# sourceMappingURL=country-list.module.js.map

/***/ }),

/***/ 856:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CountryListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CountryListPage = (function () {
    function CountryListPage(loading, navCtrl, navParams, http, tpStorageService, viewCtrl) {
        this.loading = loading;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.tpStorageService = tpStorageService;
        this.viewCtrl = viewCtrl;
        this.showheader = true;
    }
    CountryListPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.loading.presentLoading();
        this.http.get('assets/data/countryList.json')
            .subscribe(function (data) {
            var arr = Object.keys(data).map(function (k) { return data[k]; });
            _this.loading.dismissMyLoading();
            _this.countryList = arr;
            _this.tempcountryList = arr;
        });
        var code = this.navParams.get('type');
        if (!code) {
            this.selection = '';
        }
        else {
            this.selection = code;
        }
    };
    CountryListPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    CountryListPage.prototype.SelectedCountry = function (name) {
        this.tpStorageService.setItem("country", JSON.stringify(name));
        this.tpStorageService.setItem("code", JSON.stringify(this.selection.toString()));
        var data = { 'name': name, 'code': this.selection };
        this.viewCtrl.dismiss(data);
    };
    CountryListPage.prototype.initializeItems = function () {
        this.countryList = this.tempcountryList;
    };
    CountryListPage.prototype.closePopover = function () {
        this.viewCtrl.dismiss();
    };
    CountryListPage.prototype.searchItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.countryList = this.countryList.filter(function (item) {
                var plus = "+" + item.code;
                return (item.name.toLowerCase().indexOf(val.toLowerCase().trim()) > -1) || (item.code.toLowerCase().indexOf(val.toLowerCase().trim()) > -1) || (plus.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
            });
        }
    };
    CountryListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-country-list',template:/*ion-inline-start:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\country-list\country-list.html"*/'<ion-header  >\n	<ion-navbar color="darkblue">\n		<div class="header-wrap">\n			<div class="header-title" text-left>Select Country</div>\n			<div class="flex-end">\n					<ion-buttons end>\n							<button class="popover-btn" ion-button icon-only (click)="closePopover()">\n								<ion-icon ios="md-close" md="md-close" ></ion-icon>\n							</button>\n					</ion-buttons>\n			</div>\n		</div>\n	</ion-navbar>\n	<ion-searchbar class="searchbar"\n	(ionInput)="searchItems($event)">\n	</ion-searchbar>\n</ion-header>\n\n<ion-content class="" padding>\n	<ion-list class="countryList" radio-group [(ngModel)]="selection" >\n		<ion-item  *ngFor="let country of countryList" >\n	   		<ion-label >{{country.name}} <span *ngIf="country.code !=\'\' ">( +{{country.code}} )</span></ion-label>\n	    	<ion-radio (ionSelect)="SelectedCountry(country.name)"  name="countryName" [value]="country.code"></ion-radio>\n		</ion-item>\n	</ion-list>\n</ion-content>\n'/*ion-inline-end:"F:\tapally\mistletoe91-tapallymain-d73b9c405ec9\tapally3\src\pages\country-list\country-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_5__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["B" /* ViewController */]])
    ], CountryListPage);
    return CountryListPage;
}());

//# sourceMappingURL=country-list.js.map

/***/ })

});
//# sourceMappingURL=56.js.map